﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Toyota.Common.Generalist;

namespace TAM.Passport.TdkIntegrations
{
    public static class PassportSettingsAccessor
    {
        public static string PassportUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["Passport:Url"].TrimEnd('/');
            }
        }

        public static Guid PassportAppId
        {
            get
            {
                return Guid.Parse(ConfigurationManager.AppSettings["Passport:AppId"]);
            }
        }

        public static string SCAppInfoId
        {
            get
            {
                return ConfigurationManager.AppSettings["SCAppInfo:Id"];
            }
        }

        public static string SCAppInfoName
        {
            get
            {
                return ConfigurationManager.AppSettings["SCAppInfo:Name"];
            }
        }

        public static string SCAppInfoDescription
        {
            get
            {
                return ConfigurationManager.AppSettings["SCAppInfo:Description"];
            }
        }

        public static ApplicationType SCAppInfoType
        {
            get
            {
                /*var enumString = ConfigurationManager.AppSettings["SCAppInfo:Type"];
                var success = Enum.TryParse<ApplicationType>(enumString, out var type);

                if (success)
                {
                    return type;
                }*/

                return ApplicationType.Web;
            }
        }

        public static string SCAppInfoRuntime
        {
            get
            {
                return ConfigurationManager.AppSettings["SCAppInfo:Runtime"];
            }
        }
    }
}
