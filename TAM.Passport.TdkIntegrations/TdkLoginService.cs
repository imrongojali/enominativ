﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using TAM.Passport.LegacySdk;
using Toyota.Common.Credential;
using Toyota.Common.Generalist;
using Toyota.Common.Lookup;

namespace TAM.Passport.TdkIntegrations
{
    public class TdkLoginService
    {
        public const string TdkLookupSessionKey = "__tdk_Lookup__";
        public const string PassportLoginIdSessionKey = "PassportLoginId";
        public const string RoleModelFileName = "role-function-feature-map.json";

        private readonly HttpContext Context;

        public TdkLoginService()
        {
            this.Context = HttpContext.Current;
        }

        public void FixLoginPageQuirks()
        {
            Context.Session["firstTimeLogin"] = null;
            Context.Session["CurrentUserStructure"] = null;
            Context.Session["IsSystemAdministrator"] = null;
        }

        public void Logout()
        {
            Context.Session.Clear();
        }

        public User User
        {
            get
            {
                var lookup = (ILookup)Context.Session[TdkLookupSessionKey];

                if (lookup == null)
                {
                    return null;
                }

                return lookup.Get<User>();
            }
        }

        public bool IsLoggedIn
        {
            get
            {
                return User != null;
            }
        }

        public Dictionary<string, RoleMetadata> ParseRoleModels()
        {
            var path = Context.Server.MapPath("~/" + RoleModelFileName);
            var json = System.IO.File.ReadAllText(path);
            var roles = JsonConvert.DeserializeObject<List<RoleMetadata>>(json);

            return roles.ToDictionary(Q => Q.Id, Q => Q);
        }

        public ApplicationInfo GetAppInfoFromSettings()
        {
            return new ApplicationInfo
            {
                Description = PassportSettingsAccessor.SCAppInfoDescription,
                Id = PassportSettingsAccessor.SCAppInfoId,
                Name = PassportSettingsAccessor.SCAppInfoName,
                Runtime = PassportSettingsAccessor.SCAppInfoRuntime,
                Type = PassportSettingsAccessor.SCAppInfoType,
                RuntimeParameters = null
            };
        }

        public void Login(LoginClaims claim)
        {
            var lookup = new SimpleLookup();
            Context.Session[TdkLookupSessionKey] = lookup;
            Context.Session[PassportLoginIdSessionKey] = claim.SessionId;

            var appInfo = GetAppInfoFromSettings();

            var user = new User
            {
                AccountValidityDate = DateTime.Today.AddYears(1),
                Address = "",
                Applications = new List<ApplicationInfo> { appInfo },
                BirthDate = new DateTime(1990, 1, 1),
                Company = new CompanyInfo
                {
                    Description = "test toyota astra motor",
                    Id = "TAM",
                    Name = "Toyota Astra Motor"
                },
                DefaultApplication = appInfo,
                Emails = new List<string> { claim.Email },
                FirstName = claim.Name,
                Gender = GenderType.Unknown,
                Id = claim.EmployeeId,
                InActiveDirectory = false,
                IsPasswordNeedToBeReset = false,
                LastName = "",
                LockTimeout = 24 * 60,
                MaximumConcurrentLogin = 1,
                Password = null,
                PasswordExpirationDate = DateTime.Today.AddYears(5),
                PhoneNumbers = new List<PhoneNumberInfo>(),
                RegistrationNumber = "", //Unused
                Roles = CompileRoles(claim.Roles),
                SessionTimeout = 60 * 60,
                Username = claim.Username
            };

            lookup.Add(user);
            Context.Session.Timeout = 60 * 60;
        }

        private IList<Role> CompileRoles(IEnumerable<string> roles)
        {
            var map = ParseRoleModels();

            return roles.Select(role =>
            {
                var data = map[role];

                var result = new Role
                {
                    // Application = LoginMaker.PdtsApplicationInfo, // Field missing in this dll but exists in PDTS?!
                    Description = data.Description,
                    Functions = CompileFunctions(data.Functions),
                    Id = role,
                    LockTimeout = 60 * 60,
                    Name = data.Name,
                    SessionTimeout = 60 * 60
                };

                return result;
            }).ToList();
        }

        public IList<AuthorizationFunction> CompileFunctions(IEnumerable<FunctionMetadata> functions)
        {
            return functions.Select(function => new AuthorizationFunction
            {
                // Application = LoginMaker.PdtsApplicationInfo, // Field missing in this dll but exists in PDTS?!
                Description = function.Description,
                Features = CompileFeatures(function.Features),
                Id = function.Id,
                Name = function.Name
            }).ToList();
        }

        public IList<AuthorizationFeature> CompileFeatures(IEnumerable<FeatureMetadata> features)
        {
            return features.Select(feature => new AuthorizationFeature
            {
                // Application = LoginMaker.PdtsApplicationInfo, // Field missing in this dll but exists in PDTS?!
                Description = feature.Description,
                Id = feature.Id,
                Name = feature.Name,
                Qualifiers = new List<AuthorizationFeatureQualifier>()
            }).ToList();
        }
    }
}
