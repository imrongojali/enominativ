﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TAM.Passport.LegacySdk;

namespace TAM.Passport.TdkIntegrations
{
    public abstract class PassportShimLoginController : Controller
    {
        private readonly PassportApi PassportMan;
        private readonly TdkLoginService LoginMan;

        public PassportShimLoginController()
        {
            this.PassportMan = new PassportApi(PassportSettingsAccessor.PassportUrl, PassportSettingsAccessor.PassportAppId);
            this.LoginMan = new TdkLoginService();

            //add by hendry to bypass passport
            //MUST remove if prod
#if DEBUG

            var roles = new List<string>();
            roles.Add("eFaktur-ADMIN");
            roles.Add("eFaktur-PIC_LEXUS_AR");
            roles.Add("eFaktur-USER_ADMIN_VAT_IN");
            roles.Add("eFaktur-TAX_VAT_IN");
            roles.Add("eFaktur-USER_ADMIN_WHT");
            roles.Add("eFaktur-DIVISION_HEAD");
            roles.Add("eFaktur-PIC_TAX_VAT_IN");
            roles.Add("eFaktur-PIC_TAX_VAT_OUT");
            roles.Add("eFaktur-PIC_TAX_WHT");
            roles.Add("eFaktur-PIC_SUMMARY_PAYMENT_AND_REPORTING");
            roles.Add("eFaktur-ACCOUNTING_HEAD");
            roles.Add("eFaktur-TAX_HEAD");
            roles.Add("eFaktur-FINANCING_HEAD");
            roles.Add("eFaktur-PIC_AR");
            roles.Add("eFaktur-PIC_AP");
            roles.Add("eFaktur-EFAKTUR_SUPER_ADMIN");
            roles.Add("eFaktur-USER_ADMIN_DIVISION");
            roles.Add("eFaktur-PIC_TAX_VISUALIZATION_MS_STRATEGY");
            roles.Add("eFaktur-PIC_REGISTER_TAX_FACILITY");

            var claim = new LoginClaims
            {
                Issuer = "TAM.Passport",
                Username = "efaktur.user",
                Audience = new Guid(),
                IssuedAt = DateTimeOffset.Now,
                IssuedAtUnix = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds,
                Expiration = DateTimeOffset.Now.AddYears(100),
                ExpirationUnix = (Int32)(DateTime.UtcNow.AddYears(100).Subtract(new DateTime(1970, 1, 1))).TotalSeconds,
                Name = "efaktur user",
                Email = "efaktur.user@toyota.astra.co.id",
                EmployeeId = "1992EFAKTUR",
                Roles = roles
            };

            LoginMan.Login(claim);
#endif

        }

        [NonAction]
        private ActionResult RedirectLocalTo(string uri)
        {
            if (string.IsNullOrEmpty(uri) == false)
            {
                if (uri.StartsWith("/", StringComparison.Ordinal) == false)
                {
                    uri = '/' + uri;
                }
                if (Url.IsLocalUrl(uri))
                {
                    return Redirect(uri);
                }
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Index(string page)
        {
            LoginMan.FixLoginPageQuirks();
            
            if (LoginMan.IsLoggedIn)
            {
                return RedirectLocalTo(page);
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string TAMSignOnToken, string page)
        {
            if (string.IsNullOrEmpty(TAMSignOnToken))
            {
                ModelState.AddModelError(nameof(TAMSignOnToken), "Login Token is Missing!");
                return View();
            }

            var claim = PassportMan.VerifyToken(TAMSignOnToken);
            if (claim == null)
            {
                ModelState.AddModelError(nameof(TAMSignOnToken), "Login Token is invalid!");
                return View();
            }
            if (PassportMan.VerifyClaims(claim) == false)
            {
                ModelState.AddModelError(nameof(TAMSignOnToken), "Login claim is invalid!");
                return View();
            }
            if (PassportMan.CheckSession(claim.SessionId) == false)
            {
                ModelState.AddModelError(nameof(TAMSignOnToken), "Login Session is invalid!");
                return View();
            }

            LoginMan.Login(claim);
            return RedirectLocalTo(page);
        }

        public ActionResult Logout()
        {
            LoginMan.Logout();
            return RedirectToAction("Index");
        }
    }
}
