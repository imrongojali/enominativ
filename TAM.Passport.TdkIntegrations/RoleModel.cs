﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TAM.Passport.TdkIntegrations
{
    public abstract class MetadataBase
    {
        public string Id { set; get; }

        public string Name { set; get; }

        public string Description { set; get; }
    }

    public class RoleMetadata : MetadataBase
    {
        public List<FunctionMetadata> Functions { get; set; }
    }

    public class FunctionMetadata : MetadataBase
    {
        public List<FeatureMetadata> Features { get; set; }
    }

    public class FeatureMetadata : MetadataBase
    {
    }
}
