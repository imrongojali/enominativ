﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace TAM.Passport.LegacySdk
{
    public class JsonContent : StringContent
    {
        public JsonContent(object o) : base(JsonConvert.SerializeObject(o, Formatting.None), System.Text.Encoding.UTF8, "application/json")
        {
        }
    }
}
