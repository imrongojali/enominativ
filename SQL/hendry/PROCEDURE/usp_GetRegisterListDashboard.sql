USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetRegisterListDashboard]    Script Date: 26/07/2019 20:40:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[usp_GetRegisterListDashboard]
	@SortBy int = 0,
	@SortDirection varchar(4) = 'ASC',
	@TaxFacCategory varchar(100) = '',
	@TaxFacNo varchar(100) = '',
	@SupplierName varchar(100) = '',
	@SupplierCountry varchar(100) = '',
	@SupplierCountryName varchar(100) = '',
	@SupplierEstDateFrom varchar(100) = '01-01-1900',
	@SupplierEstDateTo varchar(100) = '31-12-9999',
	@FacDocSignerName varchar(100) = '',
	@RecordedDateFrom varchar(100) = '01-01-1900',
	@RecordedDateTo varchar(100) = '31-12-9999',
	@ApprovalStatus varchar(100) = '',
	@PDFFacStatus varchar(100) = '',
	@SKBType varchar(100) = '',
	@TaxFacDateFrom varchar(100) = '01-01-1900',
	@TaxFacDateTo varchar(100) = '31-12-9999',
	@SupplierNPWP varchar(100) = '',
	@SupplierPhoneNo varchar(100) = '',
	@FacValidPeriodFrom varchar(100) = '01-01-1900',
	@FacValidPeriodTo varchar(100) = '31-12-9999',
	@FacDocSignerTitle varchar(100) = '',
	@RecordedBy varchar(100) = '',
	@ApprovalDateFrom varchar(100) = '01-01-1900',
	@ApprovalDateTo varchar(100) = '31-12-9999',
    @FromNumber int,
	@ToNumber int

AS
BEGIN

	DECLARE @SortField varchar(100)
	SET @SortField = CASE @SortBy
	WHEN 1 THEN 'RG.TaxFacCategory'
	WHEN 2 THEN 'RG.SKBType'
	WHEN 3 THEN 'RG.TaxFacNo'
	WHEN 4 THEN 'RG.TaxFacDate'
	WHEN 5 THEN 'RG.SupplierName'
	WHEN 6 THEN 'RG.SupplierNPWP'
	WHEN 7 THEN 'RG.SupplierPhoneNo'
	WHEN 8 THEN 'RG.SupplierCountry'
	WHEN 9 THEN 'RG.SupplierCountryName'
	WHEN 10 THEN 'RG.SupplierEstDate'
	WHEN 11 THEN 'RG.FacValidPeriodFrom'
	WHEN 12 THEN 'RG.FacValidPeriodTo'
	WHEN 13 THEN 'RG.FacDocSignerName'
	WHEN 14 THEN 'RG.FacDocSignerTitle'
	WHEN 15 THEN 'RG.RecordedDate'
	WHEN 16 THEN 'RG.RecordedBy'
	WHEN 17 THEN 'RG.ApprovalStatus'
	WHEN 18 THEN 'RG.ApprovalDate'
	WHEN 19 THEN 'RG.RegistrationNumber'
	WHEN 20 THEN 'RG.PDFFacStatus'
	WHEN 21 THEN 'RG.UploadFile'

	ELSE 'RG.SKBTypeOthers' END

	DECLARE 
	@Query2 varchar(max) = ''

	DECLARE 
	@Query varchar(max) = 'SELECT
	ROW_NUMBER() OVER (ORDER BY '+ @SortField + ' ' + @SortDirection + ') AS RowNum,
	RG.TaxFacCategory,
	--RG.SKBType,
	'+ '(select ParamValue from TB_M_GeneralParam where paramtype = ''SKBType'' and id = RG.SKBType) as SKBType,
	RG.TaxFacNo,
	RG.TaxFacDate,
	RG.SupplierName,
	RG.SupplierNPWP,
	RG.SupplierPhoneNo,
	RG.SupplierCountry,
	'+ '(select ParamValue from TB_M_GeneralParam where paramtype = ''Country'' and ParamCode = RG.SupplierCountry) as SupplierCountryName,
	RG.SupplierEstDate,
	RG.FacValidPeriodFrom,
	RG.FacValidPeriodTo,
	RG.FacDocSignerName,
	RG.FacDocSignerTitle,
	RG.RecordedDate,
	RG.RecordedBy,
	RG.ApprovalStatus,
	RG.ApprovalDate,
	RG.RegistrationNumber,
	RG.PDFFacStatus,
	RG.UploadFile
	Into #MyTemp1
	FROM dbo.TB_M_FACILITY RG' ,
	@QueryTaxFacCategory varchar(max) = '',
	@QueryTaxFacNo varchar(max) = '',
	@QuerySupplierName varchar(max) = '',
	@QuerySupplierCountry varchar(max) = '',
	@QuerySupplierCountryName varchar(max) = '',
	@QuerySupplierEstDate varchar(max) = '',
	@QueryFacDocSignerName varchar(max) = '',
	@QueryRecordedDate varchar(max) = '',
	@QueryApprovalStatus varchar(max) = '',
	@QueryPDFFacStatus varchar(max) = '',

	@QuerySKBType varchar(max) = '',
	@QueryTaxFacDate varchar(max) = '',
	@QuerySupplierNPWP varchar(max) = '',
	@QuerySupplierPhoneNo varchar(max) = '',
	--@QuerySupplierCountryCode varchar(max) = '',
	@QueryFacValidPeriod varchar(max) = '',
	@QueryFacDocSignerTitle varchar(max) = '',
	@QueryRecordedBy varchar(max) = '',
	@QueryApprovalDate varchar(max) = ''
	
	SELECT @QueryTaxFacCategory = 
		dbo.uf_LookupDynamicQueryGenerator(@TaxFacCategory, 'RG.TaxFacCategory')

	SELECT @QueryTaxFacNo = 
		dbo.uf_LookupDynamicQueryGenerator(@TaxFacNo, 'RG.TaxFacNo')

	SELECT @QuerySupplierName = 
		dbo.uf_LookupDynamicQueryGenerator(@SupplierName, 'RG.SupplierName')

	SELECT @QuerySupplierCountry = 
		dbo.uf_LookupDynamicQueryGenerator(@SupplierCountry, 'RG.SupplierCountry')

		SELECT @QuerySupplierCountryName = 
		dbo.uf_LookupDynamicQueryGenerator(@SupplierCountryName, 'RG.SupplierCountryName')
		  
	SELECT @QuerySupplierEstDate = 
		dbo.uf_DateRangeDynamicQueryGenerator(
			@SupplierEstDateFrom + ' ' + '0:00 AM', 
			@SupplierEstDateTo + ' ' + '11:59 PM', 
			'RG.SupplierEstDate'
	)

	SELECT @QueryFacDocSignerName = 
		dbo.uf_LookupDynamicQueryGenerator(@FacDocSignerName, 'RG.FacDocSignerName')

	SELECT @QueryRecordedDate = 
		dbo.uf_DateRangeDynamicQueryGenerator(
			@RecordedDateFrom + ' ' + '0:00 AM', 
			@RecordedDateTo + ' ' + '11:59 PM', 
			'RG.RecordedDate'
	)

	SELECT @QueryApprovalStatus = 
		dbo.uf_LookupDynamicQueryGenerator(@ApprovalStatus, 'RG.ApprovalStatus')

	SELECT @QueryPDFFacStatus = 
		dbo.uf_LookupDynamicQueryGenerator(@PDFFacStatus, 'RG.PDFFacStatus')

	SELECT @QuerySKBType = 
		dbo.uf_LookupDynamicQueryGenerator(@SKBType, 'RG.SKBType')
			
	SELECT @QueryTaxFacDate = 
		dbo.uf_DateRangeDynamicQueryGenerator(
			@TaxFacDateFrom + ' ' + '0:00 AM', 
			@TaxFacDateTo + ' ' + '11:59 PM', 
			'RG.TaxFacDate'
	)

	SELECT @QuerySupplierNPWP = 
		dbo.uf_LookupDynamicQueryGenerator(@SupplierNPWP, 'RG.SupplierNPWP')

	SELECT @QuerySupplierPhoneNo = 
		dbo.uf_LookupDynamicQueryGenerator(@SupplierPhoneNo, 'RG.SupplierPhoneNo')

	--SELECT @QueryFacValidPeriod = 
	--	dbo.uf_DateRangeDynamicQueryGenerator(
	--		@FacValidPeriodFrom + ' ' + '0:00 AM', 
	--		@FacValidPeriodTo + ' ' + '11:59 PM', 
	--		'RG.FacValidPeriod'
	--)

	SELECT @QueryFacDocSignerTitle = 
		dbo.uf_LookupDynamicQueryGenerator(@FacDocSignerTitle, 'RG.FacDocSignerTitle')

	SELECT @QueryRecordedBy = 
		dbo.uf_LookupDynamicQueryGenerator(@RecordedBy, 'RG.RecordedBy')

	SELECT @QueryApprovalDate = 
		dbo.uf_DateRangeDynamicQueryGenerator(
			@ApprovalDateFrom + ' ' + '0:00 AM', 
			@ApprovalDateTo + ' ' + '11:59 PM', 
			'RG.ApprovalDate'
	)
	SELECT
	ROW_NUMBER() OVER (ORDER BY RG.SKBTypeOthers ASC) AS RowNum,
	RG.TaxFacCategory,
	--RG.SKBType,
	(select ParamValue from TB_M_GeneralParam where paramtype = 'SKBType' and id = RG.SKBType) as SKBType,
	RG.TaxFacNo,
	RG.TaxFacDate,
	RG.SupplierName,
	RG.SupplierNPWP,
	RG.SupplierPhoneNo,
	RG.SupplierCountry,
	(select ParamValue from TB_M_GeneralParam where paramtype = 'Country' and ParamCode = RG.SupplierCountry) as SupplierCountryName,
	RG.SupplierEstDate,
	RG.FacValidPeriodFrom,
	RG.FacValidPeriodTo,
	RG.FacDocSignerName,
	RG.FacDocSignerTitle,
	RG.RecordedDate,
	RG.RecordedBy,
	RG.ApprovalStatus,
	RG.ApprovalDate,
	RG.RegistrationNumber,
	RG.PDFFacStatus,
	RG.UploadFile
	Into #MyTemp1
	FROM dbo.TB_M_FACILITY RG

	--print(@Query)
	EXEC(@Query)
	 
	set @Query2 = 'Select * from #MyTemp1 RG WHERE RG.TaxFacCategory is not null ' +
		@QueryTaxFacCategory +
		@QueryTaxFacNo +
		@QuerySupplierName +
		@QuerySupplierCountry +
		@QuerySupplierCountryName + 
		@QuerySupplierEstDate +
		@QueryFacDocSignerName +
		@QueryRecordedDate + 
		@QueryApprovalStatus +
		@QueryPDFFacStatus +
        @QuerySKBType +
		@QueryTaxFacDate +
		@QuerySupplierNPWP + 
		@QuerySupplierPhoneNo +
		--@QueryFacValidPeriod +
		@QueryFacDocSignerTitle +
		@QueryRecordedBy +
		@QueryApprovalDate +
		 '  and  RowNum BETWEEN ' + 
		CONVERT(VARCHAR, @FromNumber) + ' AND ' + 
		CONVERT(VARCHAR, @ToNumber)

	exec(@Query2)
	
	Drop table #MyTemp1 ;
END