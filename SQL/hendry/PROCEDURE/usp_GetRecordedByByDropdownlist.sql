USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetKDTransaksiDropDown]    Script Date: 26/07/2019 18:43:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_GetRecordedByByDropdown]
@ParamType as int 
as
begin
	select [RecordedBy] as id,[RecordedBy] as Name 
	from [TB_M_Facility] where 
	[RecordedBy]= @ParamType
	order by [RecordedBy]
end
