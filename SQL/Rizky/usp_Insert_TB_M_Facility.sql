ALTER PROCEDURE [dbo].[usp_Insert_TB_M_Facility]
	-- Add the parameters for the stored procedure here
	@TaxFacCategory char(3),
	@TaxFacNo varchar(100),
	@SKBType varchar(100),
	@SKBTypeOthers varchar(100),
	@TaxFacDate datetime,
	@SupplierName varchar(100),
	@SupplierNPWP varchar(50),
	@SupplierAddress varchar(200),
	@SupplierPhoneNo varchar(100),
	@SupplierCountry varchar(50),
	@SupplierEstDate datetime,
	@FacValidPeriodFrom datetime,
	@FacValidPeriodTo datetime,
	@FacDocSignerName varchar(100),
	@FacDocSignerTitle varchar(100),
	@RecordedDate datetime,
	@RecordedBy varchar(100),
	@UploadFile varchar(100),
	@Seq int,
	@PDFFacStatus varchar(100),
	@ApprovalStatus varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @CekValidPeriodTO as date
	DECLARE @CekSKBNumber as varchar(max)
	DECLARE @CekSequenceNumber as int = 0

	DECLARE @idParam VARCHAR(50),@SupplierCountryCode VARCHAR(50)

	SELECT @idParam=Id FROM TB_M_GeneralParam WHERE ParamType='Country' AND ParamCode='IDN'

	SET @SupplierCountryCode=CASE WHEN @SupplierCountry='IDN' AND @TaxFacCategory='SKB' THEN @idParam ELSE @SupplierCountry end	

	SET @CekValidPeriodTo = (SELECT TOP 1 Convert(date, FacValidPeriodTo) FROM TB_M_Facility WHERE SupplierNPWP = @SupplierNPWP ORDER BY FacValidPeriodTo DESC)

	SET @CekSKBNumber = (select count(TaxFacNo) from TB_M_Facility where TaxFacNo = @TaxFacNo and ApprovalStatus <> 'Reject' )

	SET @CekSequenceNumber = (SELECT top 1 Seq +1 as SequenceNo FROM TB_M_Facility WHERE TaxFacNo = @TaxFacNo ORDER BY Seq DESC)

	IF(@TaxFacCategory = 'SKB')
	BEGIN
		set @SupplierEstDate = null
	END

	IF(@CekSequenceNumber is null)
	BEGIN
		SET @CekSequenceNumber =  1
	END


	IF(@CekSKBNumber = 0)
		BEGIN
			IF(@FacValidPeriodFrom >= @TaxFacDate)
				BEGIN
					IF(@FacValidPeriodFrom < @FacValidPeriodTo)--betul
						BEGIN
							IF(@CekValidPeriodTo IS NOT NULL)
								BEGIN 
									IF(Convert(date, @FacValidPeriodTo) <= @CekValidPeriodTO)
										BEGIN
											DECLARE @ErrorValidatePeriodTo varchar(max)
											SET @ErrorValidatePeriodTo = 'Valid Period To must be greater than Valid Period From'+space(1)+Format(@CekValidPeriodTO, 'dd-MM-yyyy')

											RAISERROR (@ErrorValidatePeriodTo, 16, 1)
										RETURN
										END	
									ELSE
									BEGIN
									INSERT INTO TB_M_Facility(
										[TaxFacCategory],[TaxFacNo],[SKBType],[SKBTypeOthers],[TaxFacDate]
										,[SupplierName],[SupplierNPWP],[SupplierAddress],[SupplierPhoneNo]
										,[SupplierCountry],[SupplierEstDate],[FacValidPeriodFrom],[FacValidPeriodTo]
										,[FacDocSignerName],[FacDocSignerTitle],[RecordedDate],[RecordedBy]
										,[UploadFile],[PDFFacStatus],[Seq], [ApprovalStatus]
									)
									VALUES (
										@TaxFacCategory,@TaxFacNo,@SKBType,@SKBTypeOthers,@TaxFacDate
										,@SupplierName,@SupplierNPWP,@SupplierAddress,@SupplierPhoneNo
										,@SupplierCountryCode,@SupplierEstDate,@FacValidPeriodFrom,@FacValidPeriodTo
										,@FacDocSignerName,@FacDocSignerTitle,@RecordedDate,@RecordedBy
										,@UploadFile, @PDFFacStatus, @CekSequenceNumber, @ApprovalStatus
									)
								END
							END
							ELSE
							BEGIN 
								INSERT INTO TB_M_Facility(
									[TaxFacCategory],[TaxFacNo],[SKBType],[SKBTypeOthers],[TaxFacDate]
									,[SupplierName],[SupplierNPWP],[SupplierAddress],[SupplierPhoneNo]
									,[SupplierCountry],[SupplierEstDate],[FacValidPeriodFrom],[FacValidPeriodTo]
									,[FacDocSignerName],[FacDocSignerTitle],[RecordedDate],[RecordedBy]
									,[UploadFile],[PDFFacStatus],[Seq], [ApprovalStatus]
								)
								VALUES (
									@TaxFacCategory,@TaxFacNo,@SKBType,@SKBTypeOthers,@TaxFacDate
									,@SupplierName,@SupplierNPWP,@SupplierAddress,@SupplierPhoneNo
									,@SupplierCountryCode,@SupplierEstDate,@FacValidPeriodFrom,@FacValidPeriodTo
									,@FacDocSignerName,@FacDocSignerTitle,@RecordedDate,@RecordedBy
									,@UploadFile, @PDFFacStatus, @CekSequenceNumber, @ApprovalStatus
								)
							END
						END
					ELSE
						BEGIN 
							RAISERROR ('Valid Period To must be greater than Valid Period From ', 16,1)
						RETURN
					END
				END
			ELSE
				BEGIN
					DECLARE @ErrorTransactionDate varchar(max)
					SET @ErrorTransactionDate = @TaxFacCategory + space(1) +'Validity Period (From) no less than' + space(1) +@TaxFacCategory+  SPACE(1)+ 'Date';
					
					RAISERROR (@ErrorTransactionDate, 16, 1)
					RETURN
				END 
			END
		ELSE
		BEGIN 
			DECLARE @ErrorSKBNumber varchar(max)
			SET @ErrorSKBNumber = @TaxFacCategory +space(1)+ 'already exists in validity Period';

				RAISERROR (@ErrorSKBNumber, 16,1)
			RETURN
		END
	----
	----
END