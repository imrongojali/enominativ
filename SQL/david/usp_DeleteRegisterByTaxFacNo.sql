/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.1742)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteWHTByID]    Script Date: 7/19/2019 5:28:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create PROCEDURE [dbo].[usp_DeleteRegisterByTaxFacNo] 
	@TaxFacNoList varchar (MAX)

AS
BEGIN
	SELECT ColumnData
	INTO #tempTaxFacNo
	FROM uf_SplitString(@TaxFacNoList, ';')

	DELETE FROM TB_M_Facility
	WHERE TaxFacNo IN (SELECT ColumnData From #tempTaxFacNo ) 

	DROP TABLE #tempTaxFacNo

END


