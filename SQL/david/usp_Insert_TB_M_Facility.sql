/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.1742)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_Insert_TB_M_Facility]    Script Date: 7/22/2019 5:26:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_Insert_TB_M_Facility]
	-- Add the parameters for the stored procedure here
	@TaxFacCategory char(3),
	@TaxFacNo varchar(100),
	@SKBType varchar(100),
	@SKBTypeOthers varchar(100),
	@TaxFacDate datetime,
	@SupplierName varchar(100),
	@SupplierNPWP varchar(15),
	@SupplierAddress varchar(200),
	@SupplierPhoneNo varchar(100),
	@SupplierCountry varchar(50),
	@SupplierEstDate datetime,
	@FacValidPeriodFrom datetime,
	@FacValidPeriodTo datetime,
	@FacDocSignerName varchar(100),
	@FacDocSignerTitle varchar(100),
	@RecordedDate datetime,
	@RecordedBy varchar(100),
	@UploadFile varchar(100),
	@Seq int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO TB_M_Facility(
		[TaxFacCategory],[TaxFacNo],[SKBType],[SKBTypeOthers],[TaxFacDate]
		,[SupplierName],[SupplierNPWP],[SupplierAddress],[SupplierPhoneNo]
		,[SupplierCountry],[SupplierEstDate],[FacValidPeriodFrom],[FacValidPeriodTo]
		,[FacDocSignerName],[FacDocSignerTitle],[RecordedDate],[RecordedBy]
		,[UploadFile],[Seq]
	)
	VALUES (
		@TaxFacCategory,@TaxFacNo,@SKBType,@SKBTypeOthers,@TaxFacDate
		,@SupplierName,@SupplierNPWP,@SupplierAddress,@SupplierPhoneNo
		,@SupplierCountry,@SupplierEstDate,@FacValidPeriodFrom,@FacValidPeriodTo
		,@FacDocSignerName,@FacDocSignerTitle,@RecordedDate,@RecordedBy
		,@UploadFile, @Seq
	)

END

