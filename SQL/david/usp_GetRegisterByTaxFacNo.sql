/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.1742)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetRegisterByTaxFacNo]    Script Date: 7/18/2019 6:20:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_GetRegisterByTaxFacNo]
	@TaxFacNoList varchar (MAX)
AS
BEGIN
	SELECT ColumnData
	INTO #tempTaxFacNo
	FROM uf_SplitString(@TaxFacNoList, ';')

	SELECT 
	   [TaxFacCategory] as Field1
      ,(select ParamValue from [dbo].[TB_M_GeneralParam] where paramtype = 'SKBType' and id = [SKBType]) as Field2
      ,[TaxFacNo] as Field3
      ,[TaxFacDate] as Field4
      ,[SupplierName] as Field5
      ,[SupplierNPWP] as Field6
      ,[SupplierAddress] as Field7
      ,[SupplierPhoneNo] as Field8
      ,(select ParamValue from [dbo].[TB_M_GeneralParam] where paramtype = 'country' and ParamCode = [SupplierCountry]) as Field9
	  ,[SupplierCountry] as Field10
      ,[SupplierEstDate] as Field11
      ,[FacValidPeriodFrom] as Field12
      ,[FacValidPeriodTo] as Field13
      ,[FacDocSignerName] as Field14
      ,[FacDocSignerTitle] as Field15
      ,[RecordedDate] as Field16
      ,[RecordedBy] as Field17
      ,[ApprovalStatus] as Field18
      ,[ApprovalDate] as Field19
      ,[PDFFacStatus]as Field20
    FROM [TAM_EFAKTUR].[dbo].[TB_M_Facility]  
	WHERE [TaxFacNo] IN (SELECT ColumnData From #tempTaxFacNo )

	drop table #tempTaxFacNo
END


	
