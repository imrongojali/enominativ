/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.1742)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TAM_EFAKTUR]
GO

/****** Object:  Table [dbo].[TB_M_Facility]    Script Date: 7/22/2019 8:54:48 PM ******/
DROP TABLE [dbo].[TB_M_Facility]
GO

/****** Object:  Table [dbo].[TB_M_Facility]    Script Date: 7/22/2019 8:54:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TB_M_Facility](
	[TaxFacCategory] [char](3) NULL,
	[TaxFacNo] [varchar](100) NOT NULL,
	[SKBType] [varchar](50) NULL,
	[SKBTypeOthers] [varchar](100) NULL,
	[TaxFacDate] [datetime] NULL,
	[SupplierName] [varchar](100) NULL,
	[SupplierNPWP] [varchar](15) NULL,
	[SupplierAddress] [varchar](200) NULL,
	[SupplierPhoneNo] [varchar](100) NULL,
	[SupplierCountry] [varchar](50) NULL,
	[SupplierEstDate] [datetime] NULL,
	[FacValidPeriodFrom] [datetime] NULL,
	[FacValidPeriodTo] [datetime] NULL,
	[FacDocSignerName] [varchar](100) NULL,
	[FacDocSignerTitle] [varchar](100) NULL,
	[RecordedDate] [datetime] NULL,
	[RecordedBy] [varchar](100) NULL,
	[ApprovalStatus] [varchar](100) NULL,
	[ApprovalDate] [datetime] NULL,
	[UploadFile] [varchar](100) NULL,
	[RegistrationNumber] [varchar](100) NULL,
	[PDFFacStatus] [varchar](100) NULL,
	[Seq] [int] NOT NULL,
 CONSTRAINT [PK_TB_M_Facility] PRIMARY KEY CLUSTERED 
(
	[TaxFacNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


