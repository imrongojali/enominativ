/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.1742)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetWHTPDFUrlById]    Script Date: 7/19/2019 10:13:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[usp_GetRegisterPDFUrlByTaxFacNo] 
	@TaxFacNoList VARCHAR(MAX)
AS
BEGIN
	SELECT ColumnData
	INTO #tempTaxFacNo
	FROM uf_SplitString(@TaxFacNoList, ';')

	SELECT UploadFile FROM TB_M_Facility
	WHERE 
	 [TaxFacNo] IN (SELECT ColumnData From #tempTaxFacNo )
		AND NULLIF(UploadFile, '') IS NOT NULL

	DROP TABLE #tempTaxFacNo
END

