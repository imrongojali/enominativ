USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetApprovalStatusByDropdownlist]    Script Date: 05/08/2019 10:49:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_GetApprovalStatusByDropdownlist]
	
AS
BEGIN
	--SELECT
	--	NEWID() AS Id, 
	--	ApprovalStatus AS Name
	--FROM (
	--	SELECT DISTINCT ApprovalStatus FROM TB_M_Facility
	--	--WHERE ISNULL(CreatedByPV,'') <> ''
	--	) A
	--ORDER BY ApprovalStatus asc

	SELECT id Id, ParamValue Name FROM dbo.TB_M_GeneralParam AS tmgp WHERE tmgp.ParamType='ApprovalStatusFacility'
END


SELECT*FROM dbo.TB_M_GeneralParam AS tmgp WHERE tmgp.ParamType='ApprovalStatusFacility'
SELECT*FROM dbo.TB_M_GeneralParam AS tmgp WHERE tmgp.ParamValue LIKE '%Wait For Approval%'

INSERT dbo.TB_M_GeneralParam
        ( Id ,
          ParamCode ,
          ParamName ,
          ParamType ,
          ParamValue ,
          CreatedOn ,
          CreatedBy ,
          ModifiedOn ,
          LastModifiedBy ,
          RowStatus
        )
VALUES  ( NEWID() , -- Id - uniqueidentifier
          'ASF01' , -- ParamCode - varchar(50)
          'Wait For Approval' , -- ParamName - varchar(50)
          'ApprovalStatusFacility' , -- ParamType - varchar(50)
          'Wait For Approval' , -- ParamValue - varchar(50)
          GETDATE() , -- CreatedOn - datetime
          'System' , -- CreatedBy - varchar(50)
          GETDATE() , -- ModifiedOn - datetime
          '' , -- LastModifiedBy - varchar(50)
          0  -- RowStatus - bit
        )

		INSERT dbo.TB_M_GeneralParam
        ( Id ,
          ParamCode ,
          ParamName ,
          ParamType ,
          ParamValue ,
          CreatedOn ,
          CreatedBy ,
          ModifiedOn ,
          LastModifiedBy ,
          RowStatus
        )
VALUES  ( NEWID() , -- Id - uniqueidentifier
          'ASF02' , -- ParamCode - varchar(50)
          'Approved' , -- ParamName - varchar(50)
          'ApprovalStatusFacility' , -- ParamType - varchar(50)
          'Approved' , -- ParamValue - varchar(50)
          GETDATE() , -- CreatedOn - datetime
          'System' , -- CreatedBy - varchar(50)
          GETDATE() , -- ModifiedOn - datetime
          '' , -- LastModifiedBy - varchar(50)
          0  -- RowStatus - bit
        )


				INSERT dbo.TB_M_GeneralParam
        ( Id ,
          ParamCode ,
          ParamName ,
          ParamType ,
          ParamValue ,
          CreatedOn ,
          CreatedBy ,
          ModifiedOn ,
          LastModifiedBy ,
          RowStatus
        )
VALUES  ( NEWID() , -- Id - uniqueidentifier
          'ASF03' , -- ParamCode - varchar(50)
          'Reject' , -- ParamName - varchar(50)
          'ApprovalStatusFacility' , -- ParamType - varchar(50)
          'Reject' , -- ParamValue - varchar(50)
          GETDATE() , -- CreatedOn - datetime
          'System' , -- CreatedBy - varchar(50)
          GETDATE() , -- ModifiedOn - datetime
          '' , -- LastModifiedBy - varchar(50)
          0  -- RowStatus - bit
        )
