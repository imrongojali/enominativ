SELECT*FROM dbo.TB_M_ConfigurationData AS tmcd WHERE tmcd.ConfigKey='UrlTemplateUploadEBupot'



			INSERT INTO dbo.TB_M_ConfigurationData
	        ( Id ,
	          ConfigKey ,
	          ConfigValue ,
	          CreatedOn ,
	          CreatedBy ,
	          ModifiedOn ,
	          ModifiedBy ,
	          RowStatus
	        )
	VALUES  ( NEWID() , -- Id - uniqueidentifier
	          'UrlTemplateUploadEBupot' , -- ConfigKey - varchar(100)
	          '/FileStorage/TemplateUpload/Upload eBupot File.xls' , -- ConfigValue - varchar(500)
	          GETDATE() , -- CreatedOn - datetime
	          'System' , -- CreatedBy - varchar(50)
	          GETDATE() , -- ModifiedOn - datetime
	          '' , -- ModifiedBy - varchar(50)
	          0  -- RowStatus - bit
	        )
