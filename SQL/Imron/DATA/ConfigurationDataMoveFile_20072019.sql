--SELECT*FROM TB_M_ConfigurationData where ConfigKey = 'SourceQRPDF'

INSERT INTO dbo.TB_M_ConfigurationData
        ( Id ,
          ConfigKey ,
          ConfigValue ,
          CreatedOn ,
          CreatedBy ,
          ModifiedOn ,
          ModifiedBy ,
          RowStatus
        )
VALUES  ( NEWID() , -- Id - uniqueidentifier
          'SourceEBupot' , -- ConfigKey - varchar(100)
          'D:\outlook-attachments\' , -- ConfigValue - varchar(500)
          GETDATE() , -- CreatedOn - datetime
          'efaktur.user' , -- CreatedBy - varchar(50)
          GETDATE() , -- ModifiedOn - datetime
          'efaktur.user' , -- ModifiedBy - varchar(50)
          0  -- RowStatus - bit
        ),
		( NEWID() , -- Id - uniqueidentifier
          'ProcessEBupot' , -- ConfigKey - varchar(100)
          'D:\EFB_PATH\eBupotPDF\eBupot_Process\' , -- ConfigValue - varchar(500)
          GETDATE() , -- CreatedOn - datetime
          'efaktur.user' , -- CreatedBy - varchar(50)
          GETDATE() , -- ModifiedOn - datetime
          'efaktur.user' , -- ModifiedBy - varchar(50)
          0  -- RowStatus - bit
        ),
		( NEWID() , -- Id - uniqueidentifier
          'SuccessEBupot' , -- ConfigKey - varchar(100)
          'D:\EFB_PATH\eBupotPDF\eBupot_Success\' , -- ConfigValue - varchar(500)
          GETDATE() , -- CreatedOn - datetime
          'efaktur.user' , -- CreatedBy - varchar(50)
          GETDATE() , -- ModifiedOn - datetime
          'efaktur.user' , -- ModifiedBy - varchar(50)
          0  -- RowStatus - bit
        ),
		( NEWID() , -- Id - uniqueidentifier
          'FailEBupot' , -- ConfigKey - varchar(100)
          'D:\EFB_PATH\eBupotPDF\eBupot_Fail\' , -- ConfigValue - varchar(500)
          GETDATE() , -- CreatedOn - datetime
          'efaktur.user' , -- CreatedBy - varchar(50)
          GETDATE() , -- ModifiedOn - datetime
          'efaktur.user' , -- ModifiedBy - varchar(50)
          0  -- RowStatus - bit
        )