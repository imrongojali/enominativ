USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetWHTEBupotOutherFileById]    Script Date: 21/07/2019 15:45:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_GetDownloadEBupotFileById] @WHTId VARCHAR(MAX)
AS
    BEGIN
        SELECT  ColumnData
        INTO    #tempSelectedisDownloadBupot
        FROM    uf_SplitString(@WHTId, ';')

        SELECT  ROW_NUMBER() OVER ( ORDER BY WT.Id ) AS RowNum ,
                WT.Id ,
                WT.TaxArticle ,
                WT.NPWPPenjual ,
                WT.NamaPenjual ,
                WT.eBupotReferenceNo
        FROM    TB_R_WHT WT
        WHERE   WT.Id IN ( SELECT   ColumnData
                           FROM     #tempSelectedisDownloadBupot )
                AND ISNULL(WT.isDownloadeBupot, 0) = 1

        DROP TABLE #tempSelectedisDownloadBupot
    END





	
