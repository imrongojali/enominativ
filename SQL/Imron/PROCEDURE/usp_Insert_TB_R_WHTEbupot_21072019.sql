USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_Insert_TB_R_WHTEbupot]    Script Date: 21/07/2019 05:31:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_Insert_TB_R_WHTEbupot]
    @TaxArticle VARCHAR(30) ,
    @eBupotNumber VARCHAR(50) ,
    @StatusInvoice VARCHAR(50) ,
    @SupplierName VARCHAR(200) ,
    @OriginalSupplierName VARCHAR(200) ,
    @SupplierNPWP VARCHAR(30) ,
    @SupplierAddress VARCHAR(400) ,
    @WHTAmount DECIMAL(18, 4) ,
    @WHTTarif DECIMAL(18, 4) ,
    @WHTTaxAmount DECIMAL(18, 4) ,
    @Description VARCHAR(400) ,
    @InvoiceDate DATETIME ,
    @NomorInvoice VARCHAR(50) ,
    @eBupotReferenceNo VARCHAR(60) ,
    @TaxType VARCHAR(20) ,
    @MapCode VARCHAR(15) ,
    @SPTType VARCHAR(20) ,
    @WithholdingTaxDate DATETIME ,
    @TaxPeriodMonth VARCHAR(10) ,
    @TaxPeriodYear VARCHAR(10) ,
    @AdditionalInfo VARCHAR(200) ,
    @PVNumber VARCHAR(100) ,
    @PVCreatedBy VARCHAR(100) ,
    @SAPDocNumber VARCHAR(100) ,
    @PostingDate DATETIME ,
    @ReceiveFileDate DATETIME ,
    @ApprovalStatus VARCHAR(100) ,
    @ApprovalDesc VARCHAR(400) ,
    @GLAccount VARCHAR(200) ,
    @CreatedBy VARCHAR(100) ,
    @CreatedOn DATETIME ,
    @RowStatus BIT
AS
    BEGIN

        SET NOCOUNT ON;
        DECLARE @ResultCode BIT = 1 ,
            @ResultDesc VARCHAR(MAX) = 'File has been uploaded successfully'
        DECLARE @Cek VARCHAR(500)
	
        SET @Cek = ( SELECT COUNT(*)
                     FROM   TB_R_WHT
                     WHERE  LTRIM(RTRIM(eBupotReferenceNo)) = LTRIM(RTRIM(@eBupotReferenceNo))
                   )

        IF ISNULL(@Cek, 0) = 1   --cek kalo udah ada yang sama sebelum nya
            BEGIN
                UPDATE  TB_R_WHT
                SET     TaxArticle = @TaxArticle ,
                        eBupotNumber = @eBupotNumber ,
						NomorBuktiPotong=@eBupotNumber,
                        StatusInvoice = @StatusInvoice ,
                        NamaPenjual = @SupplierName ,
                        NamaPenjualOriginal = @OriginalSupplierName ,
                        NPWPPenjual = @SupplierNPWP ,
                        alamatNPWP = @SupplierAddress ,
                        jumlahDPP = @WHTAmount ,
                        Tarif = @WHTTarif ,
                        jumlahPPH = @WHTTaxAmount ,
                        Description = @Description ,
                        InvoiceDate = @InvoiceDate ,
                        NomorInvoice = @NomorInvoice ,
--eBupotReferenceNumber=@eBupotReferenceNumber ,
                        JenisPajak = @TaxType ,
                        MapCode = @MapCode ,
                        SPTType = @SPTType ,
                        TanggalBuktiPotong = @WithholdingTaxDate ,
                        MasaPajak = @TaxPeriodMonth ,
                        TahunPajak = @TaxPeriodYear ,
                        TaxAdditionalInfo = @AdditionalInfo ,
                        NomorPV = @PVNumber ,
                        PVCreatedBy = @PVCreatedBy ,
                        NomorDocSAP = @SAPDocNumber ,
                        PostingDate = @PostingDate ,
                        TanggalPenerimaan = @ReceiveFileDate ,
                        ApprovalStatus = @ApprovalStatus ,
                        ApprovalDesc = @ApprovalDesc ,
                        RelatedGLAccount = @GLAccount ,
                        CreatedBy = @CreatedBy ,
                        CreatedOn = @CreatedOn ,
                        RowStatus = @RowStatus
                WHERE   LTRIM(RTRIM(eBupotReferenceNo)) = LTRIM(RTRIM(@eBupotReferenceNo))

            END

        ELSE
            BEGIN
                INSERT  INTO TB_R_WHT
                        ( Id ,
                          TaxArticle ,
                          eBupotNumber ,
						  NomorBuktiPotong,
                          StatusInvoice ,
                          NamaPenjual ,
                          NamaPenjualOriginal ,
                          NPWPPenjual ,
                          alamatNPWP ,
                          jumlahDPP ,
                          Tarif ,
                          jumlahPPH ,
                          Description ,
                          InvoiceDate ,
                          NomorInvoice ,
                          eBupotReferenceNo ,
                          JenisPajak ,
                          MapCode ,
                          SPTType ,
                          TanggalBuktiPotong ,
                          MasaPajak ,
                          TahunPajak ,
                          TaxAdditionalInfo ,
                          NomorPV ,
                          PVCreatedBy ,
                          NomorDocSAP ,
                          PostingDate ,
                          TanggalPenerimaan ,
                          ApprovalStatus ,
                          ApprovalDesc ,
                          RelatedGLAccount ,
                          CreatedBy ,
                          CreatedOn ,
                          RowStatus 
	                    )
                VALUES  ( NEWID() ,
                          @TaxArticle ,
                          @eBupotNumber ,
						  @eBupotNumber ,
                          @StatusInvoice ,
                          @SupplierName ,
                          @OriginalSupplierName ,
                          @SupplierNPWP ,
                          @SupplierAddress ,
                          @WHTAmount ,
                          @WHTTarif ,
                          @WHTTaxAmount ,
                          @Description ,
                          @InvoiceDate ,
                          @NomorInvoice ,
                          @eBupotReferenceNo ,
                          @TaxType ,
                          @MapCode ,
                          @SPTType ,
                          @WithholdingTaxDate ,
                          @TaxPeriodMonth ,
                          @TaxPeriodYear ,
                          @AdditionalInfo ,
                          @PVNumber ,
                          @PVCreatedBy ,
                          @SAPDocNumber ,
                          @PostingDate ,
                          @ReceiveFileDate ,
                          @ApprovalStatus ,
                          @ApprovalDesc ,
                          @GLAccount ,
                          @CreatedBy ,
                          @CreatedOn ,
                          @RowStatus
	                    )
            END

        SELECT  @ResultCode AS ResultCode ,
                @ResultDesc AS ResultDesc
    END


