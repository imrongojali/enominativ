USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_Insert_TB_M_Facility]    Script Date: 30/07/2019 23:33:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_Insert_TB_M_Facility]
	-- Add the parameters for the stored procedure here
	@TaxFacCategory char(3),
	@TaxFacNo varchar(100),
	@SKBType varchar(100),
	@SKBTypeOthers varchar(100),
	@TaxFacDate datetime,
	@SupplierName varchar(100),
	@SupplierNPWP varchar(30),
	@SupplierAddress varchar(200),
	@SupplierPhoneNo varchar(100),
	@SupplierCountry varchar(50),
	@SupplierEstDate datetime,
	@FacValidPeriodFrom datetime,
	@FacValidPeriodTo datetime,
	@FacDocSignerName varchar(100),
	@FacDocSignerTitle varchar(100),
	@RecordedDate datetime,
	@RecordedBy varchar(100),
	@UploadFile varchar(100),
	@Seq int,
	@PDFFacStatus varchar(100),
	@ApprovalStatus varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @CekValidPeriodTO as date
	DECLARE @CekSKBNumber as varchar(max)
	DECLARE @CekSequenceNumber as int = 0

	DECLARE @idParam VARCHAR(50),@SupplierCountryCode VARCHAR(50)

	SELECT @idParam=Id FROM TB_M_GeneralParam WHERE ParamType='Country' AND ParamCode='IDN'

	SET @SupplierCountryCode=CASE WHEN @SupplierCountry='IDN' AND @TaxFacCategory='SKB' THEN @idParam ELSE @SupplierCountry end

	

	SET @CekValidPeriodTO = (SELECT TOP 1 Convert(date, FacValidPeriodTo) FROM TB_M_Facility WHERE SupplierNPWP = @SupplierNPWP ORDER BY FacValidPeriodTo DESC)

	SET @CekSKBNumber = (select count(TaxFacNo) from TB_M_Facility where TaxFacNo = @TaxFacNo and ApprovalStatus <> 'Reject' )

	SET @CekSequenceNumber = (SELECT top 1 Seq +1 as SequenceNo FROM TB_M_Facility WHERE TaxFacNo = @TaxFacNo ORDER BY Seq DESC)

	IF(@CekSequenceNumber is null)
	BEGIN
		SET @CekSequenceNumber =  1
	END

	IF(@FacValidPeriodFrom > @FacValidPeriodTo)
	BEGIN
		RAISERROR ('Valid Period From must be greater than Valid Period To ', 16,1)
		return
	END

	ELSE IF(@CekSKBNumber > 0)
	BEGIN
		RAISERROR ('SKB Number already exists ', 16,1)
		return
	END

	ELSE IF(@CekValidPeriodTO IS NOT NULL)
	BEGIN 
		IF(Convert(date, @FacValidPeriodTo) <= @CekValidPeriodTO)
		BEGIN
			DECLARE @ErrorValidatePeriodTo varchar(max)
			SET @ErrorValidatePeriodTo = 'Valid Period From must be greater than Valid Period To'+space(1)+Format(@CekValidPeriodTO, 'dd-MM-yyyy')

			RAISERROR (@ErrorValidatePeriodTo, 16, 1)
			RETURN
		END	
		ELSE
		BEGIN
			INSERT INTO TB_M_Facility(
				[TaxFacCategory],[TaxFacNo],[SKBType],[SKBTypeOthers],[TaxFacDate]
				,[SupplierName],[SupplierNPWP],[SupplierAddress],[SupplierPhoneNo]
				,[SupplierCountry],[SupplierEstDate],[FacValidPeriodFrom],[FacValidPeriodTo]
				,[FacDocSignerName],[FacDocSignerTitle],[RecordedDate],[RecordedBy]
				,[UploadFile],[PDFFacStatus],[Seq], [ApprovalStatus]
			)
			VALUES (
				@TaxFacCategory,@TaxFacNo,@SKBType,@SKBTypeOthers,@TaxFacDate
				,@SupplierName,@SupplierNPWP,@SupplierAddress,@SupplierPhoneNo
				,@SupplierCountryCode,@SupplierEstDate,@FacValidPeriodFrom,@FacValidPeriodTo
				,@FacDocSignerName,@FacDocSignerTitle,@RecordedDate,@RecordedBy
				,@UploadFile, @PDFFacStatus, @CekSequenceNumber, @ApprovalStatus
			)
		END
	END
	
	ELSE
	BEGIN
		-- Insert statements for procedure here
	INSERT INTO TB_M_Facility(
		[TaxFacCategory],[TaxFacNo],[SKBType],[SKBTypeOthers],[TaxFacDate]
		,[SupplierName],[SupplierNPWP],[SupplierAddress],[SupplierPhoneNo]
		,[SupplierCountry],[SupplierEstDate],[FacValidPeriodFrom],[FacValidPeriodTo]
		,[FacDocSignerName],[FacDocSignerTitle],[RecordedDate],[RecordedBy]
		,[UploadFile],[PDFFacStatus],[Seq], [ApprovalStatus]
	)
	VALUES (
		@TaxFacCategory,@TaxFacNo,@SKBType,@SKBTypeOthers,@TaxFacDate
		,@SupplierName,@SupplierNPWP,@SupplierAddress,@SupplierPhoneNo
		,@SupplierCountryCode,@SupplierEstDate,@FacValidPeriodFrom,@FacValidPeriodTo
		,@FacDocSignerName,@FacDocSignerTitle,@RecordedDate,@RecordedBy
		,@UploadFile, @PDFFacStatus, @CekSequenceNumber, @ApprovalStatus
	)
	End
END

