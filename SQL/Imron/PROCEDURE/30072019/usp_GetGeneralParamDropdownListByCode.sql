USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetGeneralParamDropdownListByType]    Script Date: 30/07/2019 23:36:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_GetGeneralParamDropdownListByCode]
	@ParamType VARCHAR(50)
AS
BEGIN
	
	SELECT 
		ParamCode, 
		ParamValue AS Name
	FROM TB_M_GeneralParam
	WHERE ParamType = @ParamType
	ORDER BY ParamName

END
