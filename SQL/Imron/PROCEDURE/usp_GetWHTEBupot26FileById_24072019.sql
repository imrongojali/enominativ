USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetWHTEBupot26FileById]    Script Date: 23/07/2019 17:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_GetWHTEBupot26FileById] @WHTId VARCHAR(MAX)
AS
    BEGIN
        SELECT  ColumnData
        INTO    #tempSelectedWHTEBupot26Id
        FROM    uf_SplitString(@WHTId, ';')

	  


        SELECT  ROW_NUMBER() OVER ( ORDER BY WT.Id ) AS RowNum ,
                ISNULL(WT.MasaPajak, '') MasaPajak ,
                ISNULL(WT.TahunPajak, '') TahunPajak ,
                ISNULL(WT.TanggalBuktiPotong, GETDATE()) TglPemotongan ,
                ISNULL(dbo.RemoveSpecialChars(REPLACE(NPWPPenjual,
                                                      'abcdefghijklmnopqrstuvwzxy',
                                                      '')), '') TIN ,
                WT.NamaPenjual NamaWPTerpotong ,
                GETDATE() TglLahirWPTerpotong ,
                WT.AlamatNPWP AlamatWPTerpotong ,
                '' NoPasporWPTerpotong ,
                '' NoKitasWPTerpotong ,
                SupplierCountry KodeNegara ,
                jp.MapCode KodeObjekPajak ,
                CASE WHEN ISNULL(FacDocSignerName, '') = '' THEN 'N'
                     ELSE 'Y'
                END PenandatanganBPPengurus ,
                CONVERT(VARCHAR, CONVERT(DECIMAL(18, 0), JumlahPPH)) PenghasilanBruto ,
                '' PerkiraanPenghasilan ,
                CASE WHEN ISNULL(tmf.TaxFacCategory, '') = '' THEN 'N'
                     ELSE 'SKD'
                END MendapatkanFasilitas ,
                CONVERT(VARCHAR, CAST(WT.Tarif * 100 AS DECIMAL(18, 2))) TarifSKD ,
                tmf.TaxFacNo NomorAturanDTP ,
                '' NTPNDTP ,
                WT.TaxArticle JenisPajak ,
                '02' JenisDokumen ,
                eBupotReferenceNo eBupotReferenceNo ,
                WT.InvoiceDate InvoiceDate
        FROM    TB_R_WHT WT
                LEFT JOIN TB_M_JenisPajak jp ON WT.JenisPajak = jp.TransaksiTypeCode
                LEFT JOIN dbo.TB_M_Facility AS tmf ON WT.NPWPPenjual = tmf.SupplierNPWP
                                                      AND tmf.TaxFacCategory = 'COD'
        WHERE   WT.Id IN (
                SELECT  Id
                FROM    ( SELECT    WT.Id ,
                                    CASE WHEN WT.TaxArticle NOT IN ( '23',
                                                              '26' )
                                         THEN 'Please choose tax artikel data only 23 and 26.'
                                         ELSE ( CASE WHEN WT.StatusInvoice <> 'POSTED'
                                                     THEN 'Please choose tax artikel data only POSTED.'
                                                     WHEN ISNULL(WT.eBupotReferenceNo,
                                                              '') = ''
                                                     THEN 'Please choose eBupot Reference No null'
                                                     ELSE ( CASE
                                                              WHEN CONVERT(DECIMAL, ISNULL(dbo.RemoveSpecialChars(REPLACE(NPWPPenjual,
                                                              'abcdefghijklmnopqrstuvwzxy',
                                                              '')), '')) <= 0
                                                              AND ISNULL(WT.TaxAdditionalInfo,
                                                              '') = ''
                                                              THEN 'Please choose npwp and nik No null'
                                                              WHEN CONVERT(DECIMAL, ISNULL(dbo.RemoveSpecialChars(REPLACE(NPWPPenjual,
                                                              'abcdefghijklmnopqrstuvwzxy',
                                                              '')), '')) > 0
                                                              THEN ( CASE
                                                              WHEN jp.MapCode = '-'
                                                              THEN 'Please Map Code'
                                                              ELSE ( CASE
                                                              WHEN WT.JenisPajak IN (
                                                              '1599', '2199',
                                                              '2298', '2299',
                                                              '4299', '2399' )
                                                              AND WT.TaxArticle = '23'
                                                              AND tmf.ApprovalStatus <> 'Approved'
                                                              THEN 'Please choose Facility SKB'
                                                              WHEN WT.JenisPajak IN (
                                                              '2621', '2622',
                                                              '2623', '2624',
                                                              '2625', '2626',
                                                              '2627', '2628',
                                                              '2629', '2630',
                                                              '2631', '2632',
                                                              '2633', '2634',
                                                              '2635', '2636',
                                                              '2637', '2638',
                                                              '2639', '2640',
                                                              '2641', '2642',
                                                              '2643', '2644' )
                                                              AND WT.TaxArticle = '26'
                                                              AND tmf.ApprovalStatus <> 'Approved'
                                                              THEN 'Please choose Facility COD'
                                                              WHEN CONVERT(DATE, tmf.FacValidPeriodTo) < CONVERT(DATE, WT.PostingDate)
                                                              THEN 'Facility Expired'
                                                              ELSE 'Done'
                                                              END )
                                                              END )
                                                         WHEN    ISNULL(WT.TaxAdditionalInfo,
                                                              '') <> '' THEN 'Done'    END )
                                                END )
                                    END ErrorData
                          FROM      TB_R_WHT WT
                                    LEFT JOIN TB_M_JenisPajak jp ON WT.JenisPajak = jp.TransaksiTypeCode
                                    LEFT JOIN dbo.TB_M_Facility AS tmf ON WT.NPWPPenjual = tmf.SupplierNPWP
                          WHERE     WT.Id IN (
                                    SELECT  ColumnData
                                    FROM    #tempSelectedWHTEBupot26Id )
                                    AND WT.TaxArticle = '26'
                        ) a
                WHERE   a.ErrorData = 'Done' )
                AND WT.TaxArticle = '26'
                AND WT.StatusInvoice = 'POSTED'
                AND ISNULL(WT.eBupotReferenceNo, '') <> ''
               

					
        UPDATE  dbo.TB_R_WHT
        SET     DownloadStatus = 'Yes' ,
                DownloadeBupotDate = GETDATE() ,
                isDownloadeBupot = 1
        WHERE   Id IN (
                SELECT  Id
                FROM    ( SELECT    WT.Id ,
                                    CASE WHEN WT.TaxArticle NOT IN ( '23',
                                                              '26' )
                                         THEN 'Please choose tax artikel data only 23 and 26.'
                                         ELSE ( CASE WHEN WT.StatusInvoice <> 'POSTED'
                                                     THEN 'Please choose tax artikel data only POSTED.'
                                                     WHEN ISNULL(WT.eBupotReferenceNo,
                                                              '') = ''
                                                     THEN 'Please choose eBupot Reference No null'
                                                     ELSE ( CASE
                                                              WHEN CONVERT(DECIMAL, ISNULL(dbo.RemoveSpecialChars(REPLACE(NPWPPenjual,
                                                              'abcdefghijklmnopqrstuvwzxy',
                                                              '')), '')) <= 0
                                                              AND ISNULL(WT.TaxAdditionalInfo,
                                                              '') = ''
                                                              THEN 'Please choose npwp and nik No null'
                                                              WHEN CONVERT(DECIMAL, ISNULL(dbo.RemoveSpecialChars(REPLACE(NPWPPenjual,
                                                              'abcdefghijklmnopqrstuvwzxy',
                                                              '')), '')) > 0
                                                              THEN ( CASE
                                                              WHEN jp.MapCode = '-'
                                                              THEN 'Please Map Code'
                                                              ELSE ( CASE
                                                              WHEN WT.JenisPajak IN (
                                                              '1599', '2199',
                                                              '2298', '2299',
                                                              '4299', '2399' )
                                                              AND WT.TaxArticle = '23'
                                                              AND tmf.ApprovalStatus <> 'Approved'
                                                              THEN 'Please choose Facility SKB'
                                                              WHEN WT.JenisPajak IN (
                                                              '2621', '2622',
                                                              '2623', '2624',
                                                              '2625', '2626',
                                                              '2627', '2628',
                                                              '2629', '2630',
                                                              '2631', '2632',
                                                              '2633', '2634',
                                                              '2635', '2636',
                                                              '2637', '2638',
                                                              '2639', '2640',
                                                              '2641', '2642',
                                                              '2643', '2644' )
                                                              AND WT.TaxArticle = '26'
                                                              AND tmf.ApprovalStatus <> 'Approved'
                                                              THEN 'Please choose Facility COD'
                                                              WHEN CONVERT(DATE, tmf.FacValidPeriodTo) < CONVERT(DATE, WT.PostingDate)
                                                              THEN 'Facility Expired'
                                                              ELSE 'Done'
                                                              END )
                                                              END )
                                                         WHEN    ISNULL(WT.TaxAdditionalInfo,
                                                              '') <> '' THEN 'Done'    END )
                                                END )
                                    END ErrorData
                          FROM      TB_R_WHT WT
                                    LEFT JOIN TB_M_JenisPajak jp ON WT.JenisPajak = jp.TransaksiTypeCode
                                    LEFT JOIN dbo.TB_M_Facility AS tmf ON WT.NPWPPenjual = tmf.SupplierNPWP
                          WHERE     WT.Id IN (
                                    SELECT  ColumnData
                                    FROM    #tempSelectedWHTEBupot26Id )
                                    AND WT.TaxArticle = '26'
                        ) a
                WHERE   a.ErrorData = 'Done' )
                AND TaxArticle = '26'
                AND StatusInvoice = 'POSTED'
                AND ISNULL(eBupotReferenceNo, '') <> ''

        DROP TABLE #tempSelectedWHTEBupot26Id
    END

	--SELECT*FROM dbo.TB_M_Facility AS tmf









	
