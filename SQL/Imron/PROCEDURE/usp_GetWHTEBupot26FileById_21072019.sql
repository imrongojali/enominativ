USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetWHTEBupot26FileById]    Script Date: 21/07/2019 20:14:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_GetWHTEBupot26FileById] @WHTId VARCHAR(MAX)
AS
    BEGIN
        SELECT  ColumnData
        INTO    #tempSelectedWHTEBupot26Id
        FROM    uf_SplitString(@WHTId, ';')

	  
	   --DECLARE @Id VARCHAR(MAX) 
    --    DECLARE ref_wht CURSOR
    --    FOR
    --        SELECT  WT.Id
    --        FROM    TB_R_WHT WT
    --                LEFT JOIN TB_M_JenisPajak jp ON WT.JenisPajak = jp.TransaksiTypeCode
    --        WHERE   WT.Id IN ( SELECT   ColumnData
    --                           FROM     #tempSelectedWHTEBupot26Id )    AND WT.TaxArticle = '26'  
  
    --    OPEN ref_wht    
  
    --    FETCH NEXT FROM ref_wht INTO @Id 
  
    --    WHILE @@FETCH_STATUS = 0
    --        BEGIN   
			
    --            DECLARE @dt DATETIME = GETDATE() ,
    --                @dtMonth VARCHAR(2) = FORMAT(GETDATE(), 'MM') ,
    --                @noRef VARCHAR(50) ,
    --                @dataCode VARCHAR(5)

    --            SET @dataCode = 'NDF' + @dtMonth

    --            EXEC dbo.spGetNoTransaction '', @dt, @dataCode,
    --                @sequenceNo = @noRef OUTPUT
						 
    --            UPDATE  TB_R_WHT
    --            SET     eBupotReferenceNo = LTRIM(RTRIM(ISNULL(NomorInvoice, ''))) + @noRef
    --            WHERE   Id = @Id
    --                    AND ISNULL(eBupotReferenceNo, '') = ''
  
    --            FETCH NEXT FROM ref_wht INTO @Id
   
    --        END     
    --    CLOSE ref_wht;    
    --    DEALLOCATE ref_wht;

        SELECT  ROW_NUMBER() OVER ( ORDER BY WT.Id ) AS RowNum ,
                ISNULL(WT.MasaPajak, '') MasaPajak ,
                ISNULL(WT.TahunPajak, '') TahunPajak ,
                ISNULL(WT.TanggalBuktiPotong, GETDATE()) TglPemotongan ,
                ISNULL(dbo.RemoveSpecialChars(NPWPPenjual), '') TIN ,
                WT.NamaPenjual NamaWPTerpotong ,
                GETDATE() TglLahirWPTerpotong ,
                WT.AlamatNPWP AlamatWPTerpotong ,
                '' NoPasporWPTerpotong ,
                '' NoKitasWPTerpotong ,
                '' KodeNegara ,
                jp.MapCode KodeObjekPajak ,
                'N' PenandatanganBPPengurus ,
                CONVERT(VARCHAR, CONVERT(DECIMAL(18, 0), JumlahPPH)) PenghasilanBruto ,
                '' PerkiraanPenghasilan ,
                '' MendapatkanFasilitas ,
                CONVERT(VARCHAR, CAST(WT.Tarif * 100 AS DECIMAL(18, 2))) TarifSKD ,
                '' NomorAturanDTP ,
                '' NTPNDTP ,
                WT.TaxArticle JenisPajak ,
                '02' JenisDokumen ,
                eBupotReferenceNo eBupotReferenceNo ,
                WT.InvoiceDate InvoiceDate
        FROM    TB_R_WHT WT
                LEFT JOIN TB_M_JenisPajak jp ON WT.JenisPajak = jp.TransaksiTypeCode
        WHERE   WT.Id IN ( SELECT   ColumnData
                           FROM     #tempSelectedWHTEBupot26Id )
                AND WT.TaxArticle = '26'
                AND WT.StatusInvoice = 'POSTED'
                AND ISNULL(WT.eBupotReferenceNo, '') <> ''
                AND ( WT.NPWPPenjual <> '00.000.000.0-000.000'
                      OR ( WT.NPWPPenjual = '00.000.000.0-000.000'
                           AND ISNULL(WT.TaxAdditionalInfo, '') <> ''
                         )
                    )

					
        UPDATE  dbo.TB_R_WHT
        SET     DownloadStatus = 'Yes' ,
                DownloadeBupotDate = GETDATE() ,
                isDownloadeBupot = 1
        WHERE   Id IN (
                SELECT  WT.Id
                FROM    TTB_R_WHT WT
                        LEFT JOIN TB_M_JenisPajak jp ON WT.JenisPajak = jp.TransaksiTypeCode
                WHERE   WT.Id IN ( SELECT   ColumnData
                                   FROM     #tempSelectedWHTEBupot26Id )
                        AND WT.TaxArticle = '26'
                        AND WT.StatusInvoice = 'POSTED'
                        AND ISNULL(WT.eBupotReferenceNo, '') <> ''
                        AND ( WT.NPWPPenjual <> '00.000.000.0-000.000'
                              OR ( WT.NPWPPenjual = '00.000.000.0-000.000'
                                   AND ISNULL(WT.TaxAdditionalInfo, '') <> ''
                                 )
                            ) )

        DROP TABLE #tempSelectedWHTEBupot26Id
    END





	
