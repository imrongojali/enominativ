USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetRegisterByTaxFacNo]    Script Date: 04/08/2019 14:51:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_GetRegisterByTaxFacNo]
	@TaxFacNoList varchar (MAX)
AS
BEGIN
	SELECT ColumnData
	INTO #tempTaxFacNo
	FROM uf_SplitString(@TaxFacNoList, ';')

	SELECT 
	   [TaxFacCategory] as Field1
      ,(select ParamValue from [dbo].[TB_M_GeneralParam] where paramtype = 'SKBType' and id = [SKBType]) as Field2
      ,[TaxFacNo] as Field3
      ,FORMAT([TaxFacDate],'dd/MM/yyyy') as Field4
      ,[SupplierName] as Field5
      ,[SupplierNPWP] as Field6
      ,[SupplierAddress] as Field7
      ,[SupplierPhoneNo] as Field8
      ,ISNULL(b.ParamValue,'Indonesia') as Field9
	  ,ISNULL(b.ParamCode,'IDN') as Field10
      ,FORMAT([SupplierEstDate],'dd/MM/yyyy') as Field11
      ,FORMAT([FacValidPeriodFrom],'dd/MM/yyyy') as Field12
      ,FORMAT([FacValidPeriodTo],'dd/MM/yyyy') as Field13
      ,[FacDocSignerName] as Field14
      ,[FacDocSignerTitle] as Field15
      ,FORMAT([RecordedDate],'dd/MM/yyyy') as Field16
      ,[RecordedBy] as Field17
      ,[ApprovalStatus] as Field18
      ,FORMAT([ApprovalDate],'dd/MM/yyyy') as Field19
      ,CASE WHEN ISNULL([PDFFacStatus],0)=0 THEN 'NOT EXIST' ELSE 'EXIST' end as Field20
	  ,[RegistrationNumber] AS Field21
	   ,ISNULL(a.MapCode,'-' ) AS Field22
	   ,FORMAT([RecordedDate],'mm:ss') AS Field23
    FROM [TAM_EFAKTUR].[dbo].[TB_M_Facility]  a
	left JOIN [dbo].[TB_M_GeneralParam] b ON (CASE WHEN [TaxFacCategory]='COD' THEN a.SupplierCountry ELSE NEWID() END) = b.Id AND b.paramtype = 'country' 
	WHERE [TaxFacNo] IN (SELECT ColumnData From #tempTaxFacNo )

	drop table #tempTaxFacNo
END






	
