USE [TAM_EFAKTUR]
GO
/****** Object:  UserDefinedFunction [dbo].[uf_LookupDynamicQueryGenerator]    Script Date: 04/08/2019 06:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[uf_LookupDynamicQueryGenerator] 
(
	@LookupValue VARCHAR(MAX) = '', --if given NULL, Default as empty
	@ColumnName VARCHAR(max)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Query VARCHAR(MAX)

	IF @LookupValue != ''
		BEGIN
			SELECT
				@Query = 
				COALESCE(
					@Query + ' OR ',''
				) + ' ' + @ColumnName +' LIKE ''%' + ColumnData + '%''' 
			FROM uf_SplitString(@LookupValue, ';') --use function uf_SplitString with semi colon as separator
	
			SELECT @Query = ' AND (' + @Query + ')'
	
		END
	ELSE
		BEGIN
			SET @Query = ''
		END
	
	-- Return the result of the function
	RETURN @Query

END
