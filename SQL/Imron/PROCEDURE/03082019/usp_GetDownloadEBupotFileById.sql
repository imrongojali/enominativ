USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDownloadEBupotFileById]    Script Date: 04/08/2019 01:30:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_GetDownloadEBupotFileById] @WHTId VARCHAR(MAX)
AS
    BEGIN
        SELECT  ColumnData
        INTO    #tempSelectedisDownloadBupot
        FROM    uf_SplitString(@WHTId, ';')

		--SELECT*FROM dbo.TB_M_GeneralParam AS tmgp WHERE tmgp.ParamName LIKE '%approval%'

        
        DECLARE @CountDataIsDownlad INT ,
            @CountIsApprovalSukses INT ,
            @_ResultCode BIT ,
            @_ResultDesc VARCHAR(100)
        SELECT  @CountDataIsDownlad = COUNT(*)
        FROM    TB_R_WHT WT
        WHERE   WT.Id IN ( SELECT   ColumnData
                           FROM     #tempSelectedisDownloadBupot )
                AND ISNULL(WT.isDownloadeBupot, 0) = 1

        SELECT  @CountIsApprovalSukses = COUNT(*)
        FROM    TB_R_WHT WT
        WHERE   WT.Id IN ( SELECT   ColumnData
                           FROM     #tempSelectedisDownloadBupot )
                AND ISNULL(WT.ApprovalStatus, '') = 'Approval Sukses'

        IF ( @CountDataIsDownlad > 0
             AND @CountIsApprovalSukses > 0
           )
            BEGIN
                SET @_ResultCode = CONVERT(BIT, 1) 
                SET @_ResultDesc = 'File already download / approval status wht is approval success, are you sure want to download?'
            END
        ELSE
            IF ( @CountDataIsDownlad > 0
                 AND @CountIsApprovalSukses <= 0
               )
                BEGIN
                    SET @_ResultCode = CONVERT(BIT, 1) 
                    SET @_ResultDesc = 'File already download, are you sure want to download?'
                END
            ELSE
                IF ( @CountDataIsDownlad <= 0
                     AND @CountIsApprovalSukses > 0
                   )
                    BEGIN
                        SET @_ResultCode = CONVERT(BIT, 1) 
                        SET @_ResultDesc = 'Approval status wht is approval success, are you sure want to download?'
                    END
                ELSE
                    BEGIN
                        SET @_ResultCode = CONVERT(BIT, 0) 
                        SET @_ResultDesc = ''
                    END
          SELECT @_ResultCode ResultCode, @_ResultDesc ResultDesc

        DROP TABLE #tempSelectedisDownloadBupot
    END

	--UPDATE dbo.TB_R_WHT 
	--SET isDownloadeBupot=0
	--WHERE eBupotReferenceNo='276/BBN/X/2018-03102018-0002'





	
