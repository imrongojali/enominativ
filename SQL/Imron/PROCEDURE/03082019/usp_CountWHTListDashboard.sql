USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_CountWHTListDashboard]    Script Date: 04/08/2019 23:48:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_CountWHTListDashboard]
	@SortBy int = 0,
	@SortDirection varchar(4) = 'ASC',
	@TanggalFileDiterimaFrom varchar(50) = '01-01-1900',
	@TanggalFileDiterimaTo varchar(50) = '31-12-9999',
	@WaktuFileDiterimaFrom varchar(50) = '0:00 AM',
	@WaktuFileDiterimaTo varchar(50) = '11:59 PM',
	@TanggalDownloadFrom varchar(50) = '01-01-1900',
	@TanggalDownloadTo varchar(50) = '31-12-9999',
	@FromNumber int,
	@ToNumber int,
	@Perekam varchar(5000),
	@DivisionName varchar(5000),
	@Username varchar(5000),
	@FakturType varchar(5000),
    @NomorInvoice varchar(5000) = '',
	@NomorPV varchar(5000) = '',
	@PDFWithHolding varchar(100) = '',
	@NomorBuktiPotong varchar(5000) = '',
	@NPWPPenjual varchar(5000) = '',
	@NamaPenjual varchar(5000) = '',
	@NamaPenjualOriginal varchar(5000) = '',
	@WHTBaseAmount decimal(18,0),
	@WHTBaseAmountOperator varchar(3) = '',
	@WHTAmount decimal(18,0),
	@WHTAmountOperator varchar(3) = '',
	@Tarif decimal(18,4),
	@TarifOperator varchar(3) = '',
	@StatusInvoice varchar(5000) = '',
	@TaxArticle varchar(5000) = '',
	@JenisPajak varchar(5000) = '',
	@TaxAdditionalInfo varchar(5000) = '',
	@SPTType varchar(5000) = '',
	@NomorDocSAP varchar(5000) = '',
	@PostingDateFrom varchar(5000) = '01-01-1900',
	@PostingDateTo varchar(5000) = '31-12-9999' ,
	@TahunPajak varchar(5000) = '',
	@MasaPajak varchar(5000) = '',
	@TanggalBuktiPotongFrom varchar(50) = '01-01-1900',
	@TanggalBuktiPotongTo varchar(50) = '31-12-9999',
	@TanggalPenerimaanfrom varchar(50) = '01-01-1900',
	@TanggalPenerimaanto varchar(50) = '31-12-9999',
	@DownloadStatus varchar(5000) = '',
	@RelatedGLAccount varchar(5000) = '',
	@PVCreatedBy varchar(5000) = '',
	@ApprovalStatus varchar(5000) = '',
	@ApprovalDateFrom varchar(50) = '01-01-1900',
	@ApprovalDateTo varchar(50) = '31-12-9999',
	@ApprovalDesc varchar(5000) = '',
	@RecordTimeFrom varchar(50) = '01-01-1900',
	@RecordTimeTo varchar(50) = '31-12-9999',
	@InvoiceDateFrom varchar(50) = '01-01-1900',
	@InvoiceDateTo varchar(50) = '31-12-9999',
	@eBupotNumber varchar(5000) = '',
	@eBupotReferenceNo varchar(5000) = '',
	@MapCode varchar(5000) = '',
	@isFullAccess bit
AS
BEGIN
--declare @isFullAccess bit = 1;
	----WHTFullAccessDivision
	--DECLARE @WHTFullAccessRole varchar(MAX) =	''
	--SELECT @WHTFullAccessRole = ConfigValue FROM TB_M_ConfigurationData WHERE ConfigKey = 'WHTFullAccessRole'

	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT
	COUNT(WT.Id)
	FROM dbo.TB_R_WHT WT
	LEFT JOIN dbo.TB_R_CsvData CSV ON CSV.BatchFileName = WT.BatchFileName
	LEFT JOIN (SELECT ROW_NUMBER()OVER(Partition By tmf.SupplierNPWP Order By FacValidPeriodTo DESC)As RowNPWP, * FROM dbo.TB_M_Facility AS tmf)  AS tmf ON WT.NPWPPenjual = tmf.SupplierNPWP AND RowNPWP=1
                                                      AND tmf.TaxFacCategory = ''SKB''
	LEFT JOIN TB_M_JenisPajak jp ON WT.JenisPajak = jp.TransaksiTypeCode 
		 CROSS APPLY
					(
						SELECT SUM(JumlahPPH) AccumulatedWHTBase
						FROM dbo.TB_R_WHT trw
						WHERE trw.TaxArticle = ''21''
							  AND LTRIM(trw.TaxAdditionalInfo) = LTRIM(wt.TaxAdditionalInfo)
							  AND (LTRIM(trw.TaxAdditionalInfo) <> ''''
								   OR trw.TaxAdditionalInfo IS NULL)
							  AND trw.TahunPajak = wt.TahunPajak
							  AND trw.PostingDate BETWEEN DATETIMEFROMPARTS(YEAR(wt.PostingDate), 1, 1, 0, 0, 0, 0) AND DATEADD(SECOND, -1, DATEADD(DAY, 1, wt.PostingDate))
					) ad
		 
	WHERE WT.RowStatus = 0'+ @PDFWithholding + '',
	@QueryTanggalFileDiterima varchar(MAX) = '',
	@QueryTanggalDataTerbentuk varchar(MAX) = '',
	@QueryTanggalDownload varchar(MAX) = '',
	@QueryNomorInvoice varchar(MAX) = '',
	@QueryNomorPV varchar(MAX) = '',
	@QueryPDFWithHolding varchar(MAX) = '',
	@QueryNomorBuktiPotong varchar(MAX) = '',
	@QueryNPWPPenjual varchar(MAX) = '',
	@QueryNamaPenjual varchar(MAX) = '',
	@QueryNamaPenjualOriginal varchar(MAX) = '',
	@QueryJumlahDPP varchar(MAX) = '',
	@QUeryJumlahPPH varchar(MAX) = '',
	@QueryTarif varchar(MAX) = '',
	@QueryStatusInvoice varchar(MAX) = '',
	@QueryTaxArticle varchar(MAX) = '',
	@QueryJenisPajak varchar(MAX) = '',
	@QueryTaxAdditionalInfo varchar(MAX) = '',
	@QuerySPTType varchar(MAX) = '',
	@QueryNomorDocSAP varchar(MAX) = '',
	@QueryDownloadStatus varchar(MAX) = '',
	@QueryRelatedGLAccount varchar(MAX) = '',
	@QueryTanggalBuktiPotong varchar(MAX) = '',
	@QueryTanggalPenerimaan varchar(MAX) = '',
	@QueryPostingDate varchar(MAX) = '',
	@QueryPerekam varchar(MAX) = '',
	@QueryFakturType varchar(MAX) = '',
	@QueryBulanPajak varchar(MAX) = '',
	@QueryTahunPajak varchar(MAX) = '',
	@QueryPVCreatedBy varchar(MAX) = '',
	@QueryApprovalStatus varchar(MAX) = '',
	@QueryApprovalDate varchar(MAX) = '',
	@QueryApprovalDesc varchar(MAX) = '',
	@QueryApprovalRecordDate varchar(MAX) = '',
	@QueryInvoiceDate varchar(MAX) = '',
	@QueryeBupotNumber VARCHAR(MAX)='',
	@QueryeeBupotReference VARCHAR(MAX)='',
	@QueryeMapCode VARCHAR(MAX)=''

SELECT @QueryInvoiceDate = 
	dbo.uf_DateRangeDynamicQueryGenerator(
		@InvoiceDateFrom + ' ' + '0:00 AM', 
		@InvoiceDateTo + ' ' + '11:59 PM', 
		'WT.InvoiceDate'
	)

SELECT @QueryApprovalRecordDate = 
	dbo.uf_DateRangeDynamicQueryGenerator(
		@RecordTimeFrom + ' ' + '0:00 AM', 
		@RecordTimeTo + ' ' + '11:59 PM', 
		'WT.RecordTime'
)

SELECT @QueryApprovalDesc = 
dbo.uf_LookupDynamicQueryGenerator(@ApprovalDesc, 'WT.ApprovalDesc')

SELECT @QueryApprovalStatus = 
dbo.uf_LookupDynamicQueryGenerator(@ApprovalStatus, 'WT.ApprovalStatus')

SELECT @QueryApprovalDate = 
	dbo.uf_DateRangeDynamicQueryGenerator(
		@ApprovalDateFrom + ' ' + '0:00 AM', 
		@ApprovalDateTo + ' ' + '11:59 PM', 
		'WT.ApprovalDate'
	)

SELECT @QueryTanggalFileDiterima = 
dbo.uf_DateRangeDynamicQueryGenerator(
	@TanggalFileDiterimaFrom + ' ' + @WaktuFileDiterimaFrom, 
	@TanggalFileDiterimaTo + ' ' + @WaktuFileDiterimaTo, 
	'WT.CreatedOn'
)

SELECT @QueryTanggalDataTerbentuk = 
	dbo.uf_DateRangeDynamicQueryGenerator(
		@TanggalFileDiterimaFrom + ' ' + @WaktuFileDiterimaFrom, 
		@TanggalFileDiterimaTo + ' ' + @WaktuFileDiterimaTo, 
	'WT.CreatedOn'
)

SELECT @QueryTanggalDownload = 
	dbo.uf_DateRangeDynamicQueryGenerator(
		@TanggalDownloadFrom + ' ' + '0:00 AM', 
		@TanggalDownloadTo + ' ' + '11:59 PM', 
		'CSV.CsvTime'
	)

SELECT @QueryTanggalBuktiPotong = 
	dbo.uf_DateRangeDynamicQueryGenerator(
		@TanggalBuktiPotongFrom + ' ' + '0:00 AM', 
		@TanggalBuktiPotongTo + ' ' + '11:59 PM', 
		'WT.TanggalBuktiPotong'
	)

SELECT @QueryTanggalPenerimaan = 
	dbo.uf_DateRangeDynamicQueryGenerator(
		@TanggalPenerimaanfrom + ' ' + '0:00 AM', 
		@TanggalPenerimaanto + ' ' + '11:59 PM', 
		'WT.TanggalPenerimaan'
	)

SELECT @QueryPostingDate = 
	dbo.uf_DateRangeDynamicQueryGenerator(
		@PostingDateFrom + ' ' + '0:00 AM', 
		@PostingDateTo + ' ' + '11:59 PM', 
		'WT.PostingDate'
	)

SELECT @QueryNomorInvoice = 
dbo.uf_LookupDynamicQueryGenerator(@NomorInvoice, 'WT.NomorInvoice')

SELECT @QueryNomorPV = 
dbo.uf_LookupDynamicQueryGenerator(@NomorPV, 'WT.NomorPV')

SELECT @QueryPDFWithHolding = 
dbo.uf_LookupDynamicQueryGenerator(@PDFWithHolding, 'WT.PDFWithholding')

SELECT @QueryNPWPPenjual = 
dbo.uf_LookupDynamicQueryGenerator(@NPWPPenjual, 'WT.NPWPPenjual')

SELECT @QueryNamaPenjual = 
dbo.uf_LookupDynamicQueryGenerator(@NamaPenjual, 'WT.NamaPenjual')

SELECT @QueryNamaPenjualOriginal = 
dbo.uf_LookupDynamicQueryGenerator(@NamaPenjualOriginal, 'WT.NamaPenjualOriginal')

SELECT @QueryJumlahDPP =
dbo.uf_DecimalRangeDynamicQueryGenerator(@WHTBaseAmountOperator,@WHTBaseAmount , 'WT.JumlahDPP')

SELECT @QueryJumlahPPH =
dbo.uf_DecimalRangeDynamicQueryGenerator(@WHTAmountOperator, @WHTAmount, 'WT.JumlahPPH')

SELECT @QueryTarif =
dbo.uf_DecimalRangeDynamicQueryGenerator(@TarifOperator, @Tarif, 'cast(WT.Tarif*100 as decimal(18,2))')

SELECT @QueryStatusInvoice = 
dbo.uf_LookupDynamicQueryGeneratorWithBlank(@StatusInvoice, 'WT.StatusInvoice')

SELECT @QueryTaxArticle = 
dbo.uf_LookupDynamicQueryGenerator(@TaxArticle, 'WT.TaxArticle')

SELECT @QueryJenisPajak = 
dbo.uf_LookupDynamicQueryGenerator(@JenisPajak, 'WT.JenisPajak + '' - '' + jp.TransaksiTypeDescShort')

SELECT @QueryTaxAdditionalInfo = 
dbo.uf_LookupDynamicQueryGenerator(@TaxAdditionalInfo, 'WT.TaxAdditionalInfo')

SELECT @QuerySPTType = 
dbo.uf_LookupDynamicQueryGenerator(@SPTType, 'WT.SPTType')

SELECT @QueryNomorDocSAP = 
dbo.uf_LookupDynamicQueryGenerator(@NomorDocSAP, 'WT.NomorDocSAP')

SELECT @QueryDownloadStatus = 
dbo.uf_LookupDynamicQueryGenerator(@DownloadStatus, 'WT.DownloadStatus')

SELECT @QueryRelatedGLAccount = 
dbo.uf_LookupDynamicQueryGenerator(@RelatedGLAccount, 'WT.RelatedGLAccount')

SELECT @QueryNomorBuktiPotong = 
dbo.uf_LookupDynamicQueryGenerator(@NomorBuktiPotong, 'WT.NomorBuktiPotong')

SELECT @QueryPVCreatedBy = 
dbo.uf_LookupDynamicQueryGenerator(@PVCreatedBy, 'WT.PVCreatedBy')

SELECT @QueryBulanPajak = 
dbo.uf_LookupDynamicQueryGenerator(@MasaPajak, 'WT.MasaPajak')

SELECT @QueryTahunPajak = 
dbo.uf_LookupDynamicQueryGenerator(@TahunPajak, 'WT.TahunPajak')

SELECT @QueryeBupotNumber = 
dbo.uf_LookupDynamicQueryGenerator(@eBupotNumber, 'WT.eBupotNumber')

SELECT @QueryeeBupotReference = 
dbo.uf_LookupDynamicQueryGenerator(@eBupotReferenceNo, 'WT.eBupotReferenceNo')

SELECT @QueryeMapCode = 
dbo.uf_LookupDynamicQueryGenerator(@MapCode, 'case when WT.JenisPajak=''3299'' then tmf.MapCode else  WT.MapCode end')

DECLARE @DivId varchar(MAX), @DivCode varchar(MAX)
	set @DivId = (select DivisionId from tb_m_user where USERNAME = @Username) --ambil 1 id bedasarkan username
	set @DivCode = (select divisioncode from TB_M_Division where id = @DivId) -- ambil divcode bedasarkan id @DivId

--IF NOT EXISTS (SELECT 1 FROM dbo.uf_SplitString(@WHTFullAccessRole, ';' ) WHERE ColumnData = @DivisionName) 
if(@isFullAccess=0)
		--AND @Perekam <> @Username
BEGIN
	SELECT @QueryPerekam = 
	dbo.uf_LookupDynamicQueryGenerator(@Perekam, 'WT.CreatedBy') 
	set @QueryPerekam =replace(@QueryPerekam ,'%','') + ' AND WT.CreatedBy in 
	(select username from tb_m_user where DivisionId in 
	(select id from tb_m_division where divisioncode = ''' + @DivCode + ''' ))'
END
ELSE
BEGIN
	SELECT @QueryPerekam = 
	dbo.uf_LookupDynamicQueryGenerator(@Perekam, 'WT.CreatedBy')
	set @QueryPerekam =replace(@QueryPerekam ,'%','')
END

SELECT @QueryFakturType = 
dbo.uf_LookupDynamicQueryGenerator(@FakturType, 'WT.FakturType')
set @QueryFakturType =replace(@QueryFakturType ,'%','')

SET @Query = 
		@Query + 
		REPLACE(@QueryTanggalFileDiterima, 'AND (', 'AND ((') +
		' OR ' +
		RIGHT(@QueryTanggalDataTerbentuk, LEN(@QueryTanggalDataTerbentuk) - 4) +
		')' +
		@QueryTanggalFileDiterima +
		@QueryTanggalDataTerbentuk +
		@QueryTanggalDownload +
		@QueryNomorInvoice +
		@QueryNomorPV +
		@QueryNomorBuktiPotong +
		@QueryNPWPPenjual +
		@QueryNamaPenjual +
		@QueryNamaPenjualOriginal +
		@QueryJumlahDPP +
		@QUeryJumlahPPH +
		@QueryTarif +
		@QueryStatusInvoice +
		@QueryTaxArticle +
		@QueryJenisPajak +
		@QueryTaxAdditionalInfo +
		@QuerySPTType +
		@QueryNomorDocSAP +
		@QueryDownloadStatus +
		@QueryRelatedGLAccount +
		@QueryTanggalBuktiPotong +
		@QueryTanggalPenerimaan +
		@QueryPostingDate +
		@QueryPerekam +
		@QueryFakturType +
		@QueryBulanPajak + 
		@QueryTahunPajak +
		@QueryPVCreatedBy +
		@QueryApprovalStatus +
		@QueryApprovalDate + 
		@QueryApprovalDesc +
		@QueryApprovalRecordDate +
		@QueryInvoiceDate +
		@QueryeBupotNumber +
		@QueryeeBupotReference +
		@QueryeMapCode 
print(@Query)
EXEC(@Query)

END