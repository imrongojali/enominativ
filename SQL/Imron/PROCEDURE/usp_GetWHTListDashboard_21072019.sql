USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetWHTListDashboard]    Script Date: 21/07/2019 20:02:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_GetWHTListDashboard]
	@SortBy int = 0,
	@SortDirection varchar(4) = 'ASC',
		@TanggalFileDiterimaFrom varchar(50) = '01-01-1900',
	@TanggalFileDiterimaTo varchar(50) = '31-12-9999',
	@WaktuFileDiterimaFrom varchar(50) = '0:00 AM',
	@WaktuFileDiterimaTo varchar(50) = '11:59 PM',
	@TanggalDownloadFrom varchar(50) = '01-01-1900',
	@TanggalDownloadTo varchar(50) = '31-12-9999',
	@FromNumber int,
	@ToNumber int,
	@Perekam varchar(5000),
	@DivisionName varchar(5000),
	@Username varchar(5000),
	@FakturType varchar(5000),
    @NomorInvoice varchar(5000) = '',
	@NomorPV varchar(5000) = '',
	@PDFWithHolding varchar(100) = '',
	@NomorBuktiPotong varchar(5000) = '',
	@NPWPPenjual varchar(5000) = '',
	@NamaPenjual varchar(5000) = '',
	@NamaPenjualOriginal varchar(5000) = '',
	@WHTBaseAmount decimal(18,0),
	@WHTBaseAmountOperator varchar(3) = '',
	@WHTAmount decimal(18,0),
	@WHTAmountOperator varchar(3) = '',
	@Tarif decimal(18,4),
	@TarifOperator varchar(3) = '',
	@StatusInvoice varchar(5000) = '',
	@TaxArticle varchar(5000) = '',
	@JenisPajak varchar(5000) = '',
	@TaxAdditionalInfo varchar(5000) = '',
	@SPTType varchar(5000) = '',
	@NomorDocSAP varchar(5000) = '',
	@PostingDateFrom varchar(5000) = '01-01-1900',
	@PostingDateTo varchar(5000) = '31-12-9999' ,
	@TahunPajak varchar(5000) = '',
	@MasaPajak varchar(5000) = '',
	@TanggalBuktiPotongFrom varchar(50) = '01-01-1900',
	@TanggalBuktiPotongTo varchar(50) = '31-12-9999',
	@TanggalPenerimaanfrom varchar(50) = '01-01-1900',
	@TanggalPenerimaanto varchar(50) = '31-12-9999',
	@DownloadStatus varchar(5000) = '',
	@RelatedGLAccount varchar(5000) = '',
	@PVCreatedBy varchar(5000) = '',
	@ApprovalStatus varchar(5000) = '',
	@ApprovalDateFrom varchar(50) = '01-01-1900',
	@ApprovalDateTo varchar(50) = '31-12-9999',
	@ApprovalDesc varchar(5000) = '',
	@RecordTimeFrom varchar(50) = '01-01-1900',
	@RecordTimeTo varchar(50) = '31-12-9999',
	@InvoiceDateFrom varchar(50) = '01-01-1900',
	@InvoiceDateTo varchar(50) = '31-12-9999',
	@eBupotNumber varchar(5000) = '',
	@eBupotReferenceNo varchar(5000) = '',
	@MapCode varchar(5000) = '',
	@isFullAccess BIT

AS
BEGIN
	--declare @isFullAccess bit = 1;

	DECLARE @SortField VARCHAR(50) 
	SET @SortField =  CASE @SortBy
	WHEN 1 THEN	'WT.NomorInvoice'
	WHEN 2 THEN	'WT.NomorPV' 
	WHEN 3 THEN	'WT.PDFWithholding'
	WHEN 4 THEN	'WT.NomorBuktiPotong'
	WHEN 5 THEN	'WT.NPWPPenjual'
	WHEN 6 THEN	'WT.NamaPenjual'
	WHEN 7 THEN	'WT.JumlahDPP'
	WHEN 8 THEN	'WT.JumlahPPH'
	WHEN 9 THEN	'WT.Tarif'
	WHEN 10 THEN 'WT.StatusInvoice'
	WHEN 11 THEN 'WT.TAXArticle'
	WHEN 12 THEN 'WT.JenisPajak'
	WHEN 13 THEN 'WT.TaxAdditionalInfo'
	WHEN 14 THEN 'WT.SPTType'
	WHEN 15 THEN 'WT.NomorDocSAP'
	WHEN 16 THEN 'WT.PostingDate'
	WHEN 17 THEN 'WT.TahunPajak'
	WHEN 18 THEN 'WT.MasaPajak'
	WHEN 19 THEN 'WT.TanggalBuktiPotong'
	WHEN 20 THEN 'WT.TanggalPenerimaan'
	WHEN 21 THEN 'WT.DownloadStatus'
	WHEN 22 THEN 'CSV.CsvTime'
	WHEN 23 THEN 'WT.CreatedOn'
	WHEN 24 THEN 'WT.RelatedGLAccount'
	WHEN 25 THEN 'WT.PVCreatedBy'
	WHEN 26 THEN 'WT.ApprovalStatus'
	WHEN 27 THEN 'WT.ApprovalDate'
	WHEN 28 THEN 'WT.ApprovalDesc'
	WHEN 29 THEN 'WT.RecordTime'
	WHEN 30 THEN 'cast(WT.TanggalPenerimaan as time)'
	WHEN 31 THEN 'WT.BatchFileName'
	WHEN 32 THEN 'CASE WHEN WT.RecordTime IS NULL THEN 0 ELSE 1 END'
	WHEN 33 THEN 'WT.NamaPenjualOriginal'
	WHEN 34 THEN 'ad.AccumulatedWHTBase'
	WHEN 35 THEN 'WT.InvoiceDate'
	WHEN 36 THEN 'WT.eBupotNumber'
	WHEN 37 THEN 'WT.eBupotReferenceNo'
	WHEN 38 THEN 'WT.MapCode'

	ELSE 'WT.NPWPPenjual' END
	
	--WHTFullAccessDivision efb
	--DECLARE @WHTFullAccessRole varchar(MAX) =	''
	--SELECT @WHTFullAccessRole = ConfigValue FROM TB_M_ConfigurationData WHERE ConfigKey = 'WHTFullAccessRole'

	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT
		ROW_NUMBER() OVER (ORDER BY '+ @SortField + ' ' + @SortDirection + ') AS RowNum,
		WT.Id, 
		WT.NomorInvoice,
		WT.NomorPV, 
		case when WT.PDFWithholding is null then ''Not Exist'' else ''Exist'' end as PDFWithholding,
		WT.NomorBuktiPotong,
		WT.NPWPPenjual,
		WT.NamaPenjual,
		WT.JumlahDPP,
		WT.JumlahPPH,
		cast(WT.Tarif*100 as decimal(18,2)) AS Tarif,
		WT.StatusInvoice,
		WT.TAXArticle,
		WT.JenisPajak,
		WT.TaxAdditionalInfo,
		WT.SPTType,
		WT.NomorDocSAP,
		WT.PostingDate,
		WT.TahunPajak,
		WT.MasaPajak,
		WT.TanggalBuktiPotong,
		WT.TanggalPenerimaan,
		WT.DownloadStatus,
		isnull(WT.DownloadeBupotDate ,CSV.CsvTime) CsvTime,
		WT.CreatedOn,
		WT.RelatedGLAccount,
		WT.PVCreatedBy,
		WT.Description,
		WT.ApprovalStatus,
	    WT.ApprovalDate,
	    WT.ApprovalDesc,
	    WT.RecordTime,
		WT.BatchFileName,
		WT.InvoiceDate,
		CASE WHEN WT.RecordTime IS NULL THEN ''N/A'' ELSE ''Recorded'' END AS RecordStatus,
		WT.NamaPenjualOriginal,
		jp.TransaksiTypeDescShort AS WHTShortDescription,
        ad.AccumulatedWHTBase,
		WT.eBupotNumber,
		WT.eBupotReferenceNo,
		WT.MapCode
		FROM dbo.TB_R_WHT WT

		LEFT JOIN dbo.TB_R_CsvData CSV ON CSV.BatchFileName = WT.BatchFileName
		LEFT JOIN TB_M_JenisPajak jp ON WT.JenisPajak = jp.TransaksiTypeCode 
		     CROSS APPLY
					(
						SELECT SUM(JumlahPPH) AccumulatedWHTBase
						FROM dbo.TB_R_WHT trw
						WHERE trw.TaxArticle = ''21''
							  AND LTRIM(trw.TaxAdditionalInfo) = LTRIM(wt.TaxAdditionalInfo)
							  AND (LTRIM(trw.TaxAdditionalInfo) <> ''''
								   OR trw.TaxAdditionalInfo IS NULL)
							  AND trw.TahunPajak = wt.TahunPajak
							  AND trw.PostingDate BETWEEN DATETIMEFROMPARTS(YEAR(wt.PostingDate), 1, 1, 0, 0, 0, 0) AND DATEADD(SECOND, -1, DATEADD(DAY, 1, wt.PostingDate))
							  and wt.JenisPajak = trw.TaxArticle
					) ad

	WHERE WT.RowStatus = 0'  + @PDFWithholding + '',
	@QueryTanggalFileDiterima varchar(MAX) = '',
	@QueryTanggalDataTerbentuk varchar(MAX) = '',
	@QueryTanggalDownload varchar(MAX) = '',
	@QueryNomorInvoice varchar(MAX) = '',
	@QueryNomorPV varchar(MAX) = '',
	@QueryPDFWithHolding varchar(MAX) = '',
	@QueryNomorBuktiPotong varchar(MAX) = '',
	@QueryNPWPPenjual varchar(MAX) = '',
	@QueryNamaPenjual varchar(MAX) = '',
	@QueryNamaPenjualOriginal varchar(MAX) = '',
	@QueryJumlahDPP varchar(MAX) = '',
	@QUeryJumlahPPH varchar(MAX) = '',
	@QueryTarif varchar(MAX) = '',
	@QueryStatusInvoice varchar(MAX) = '',
	@QueryTaxArticle varchar(MAX) = '',
	@QueryJenisPajak varchar(MAX) = '',
	@QueryTaxAdditionalInfo varchar(MAX) = '',
	@QuerySPTType varchar(MAX) = '',
	@QueryNomorDocSAP varchar(MAX) = '',
	@QueryDownloadStatus varchar(MAX) = '',
	@QueryRelatedGLAccount varchar(MAX) = '',
	@QueryTanggalBuktiPotong varchar(MAX) = '',
	@QueryTanggalPenerimaan varchar(MAX) = '',
	@QueryPostingDate varchar(MAX) = '',
	@QueryPerekam varchar(MAX) = '',
	@QueryFakturType varchar(MAX) = '',
	@QueryBulanPajak varchar(MAX) = '',
	@QueryTahunPajak varchar(MAX) = '',
	@QueryPVCreatedBy varchar(MAX) = '',
	@QueryApprovalStatus varchar(MAX) = '',
	@QueryApprovalDate varchar(MAX) = '',
	@QueryApprovalDesc varchar(MAX) = '',
	@QueryApprovalRecordDate varchar(MAX) = '',
	@QueryInvoiceDate varchar(MAX) = '',
	@QueryeBupotNumber VARCHAR(MAX)='',
	@QueryeeBupotReference VARCHAR(MAX)='',
	@QueryeMapCode VARCHAR(MAX)=''


	

SELECT @QueryInvoiceDate = 
	dbo.uf_DateRangeDynamicQueryGenerator(
		@InvoiceDateFrom + ' ' + '0:00 AM', 
		@InvoiceDateTo + ' ' + '11:59 PM', 
		'WT.InvoiceDate'
	)

SELECT @QueryApprovalRecordDate = 
	dbo.uf_DateRangeDynamicQueryGenerator(
		@RecordTimeFrom + ' ' + '0:00 AM', 
		@RecordTimeTo + ' ' + '11:59 PM', 
		'WT.RecordTime'
	)

SELECT @QueryApprovalDesc = 
dbo.uf_LookupDynamicQueryGenerator(@ApprovalDesc, 'WT.ApprovalDesc')

SELECT @QueryApprovalStatus = 
dbo.uf_LookupDynamicQueryGenerator(@ApprovalStatus, 'WT.ApprovalStatus')

SELECT @QueryApprovalDate = 
	dbo.uf_DateRangeDynamicQueryGenerator(
		@ApprovalDateFrom + ' ' + '0:00 AM', 
		@ApprovalDateTo + ' ' + '11:59 PM', 
		'WT.ApprovalDate'
	)


SELECT @QueryTanggalFileDiterima = 
dbo.uf_DateRangeDynamicQueryGenerator(
	@TanggalFileDiterimaFrom + ' ' + @WaktuFileDiterimaFrom, 
	@TanggalFileDiterimaTo + ' ' + @WaktuFileDiterimaTo, 
	'WT.CreatedOn'
)

SELECT @QueryTanggalDataTerbentuk = 
	dbo.uf_DateRangeDynamicQueryGenerator(
		@TanggalFileDiterimaFrom + ' ' + @WaktuFileDiterimaFrom, 
		@TanggalFileDiterimaTo + ' ' + @WaktuFileDiterimaTo, 
	'WT.CreatedOn'
)

SELECT @QueryTanggalDownload = 
	dbo.uf_DateRangeDynamicQueryGenerator(
		@TanggalDownloadFrom + ' ' + '0:00 AM', 
		@TanggalDownloadTo + ' ' + '11:59 PM', 
		--	'CSV.CsvTime'
		'isnull(WT.DownloadeBupotDate ,CSV.CsvTime)'
	)

SELECT @QueryTanggalBuktiPotong = 
	dbo.uf_DateRangeDynamicQueryGenerator(
		@TanggalBuktiPotongFrom + ' ' + '0:00 AM', 
		@TanggalBuktiPotongTo + ' ' + '11:59 PM', 
		'WT.TanggalBuktiPotong'
	)

SELECT @QueryTanggalPenerimaan = 
	dbo.uf_DateRangeDynamicQueryGenerator(
		@TanggalPenerimaanfrom + ' ' + '0:00 AM', 
		@TanggalPenerimaanto + ' ' + '11:59 PM', 
		'WT.TanggalPenerimaan'
	)

SELECT @QueryPostingDate = 
	dbo.uf_DateRangeDynamicQueryGenerator(
		@PostingDateFrom + ' ' + '0:00 AM', 
		@PostingDateTo + ' ' + '11:59 PM', 
		'WT.PostingDate'
	)

SELECT @QueryPVCreatedBy = 
dbo.uf_LookupDynamicQueryGenerator(@PVCreatedBy, 'WT.PVCreatedBy')

SELECT @QueryBulanPajak = 
dbo.uf_LookupDynamicQueryGenerator(@MasaPajak, 'WT.MasaPajak')

SELECT @QueryTahunPajak = 
dbo.uf_LookupDynamicQueryGenerator(@TahunPajak, 'WT.TahunPajak')

SELECT @QueryNomorInvoice = 
dbo.uf_LookupDynamicQueryGenerator(@NomorInvoice, 'WT.NomorInvoice')

SELECT @QueryNomorPV = 
dbo.uf_LookupDynamicQueryGenerator(@NomorPV, 'WT.NomorPV')

SELECT @QueryPDFWithHolding = 
dbo.uf_LookupDynamicQueryGenerator(@PDFWithHolding, 'WT.PDFWithholding')

SELECT @QueryNPWPPenjual = 
dbo.uf_LookupDynamicQueryGenerator(@NPWPPenjual, 'WT.NPWPPenjual')

SELECT @QueryNamaPenjual = 
dbo.uf_LookupDynamicQueryGenerator(@NamaPenjual, 'WT.NamaPenjual')

SELECT @QueryNamaPenjualOriginal = 
dbo.uf_LookupDynamicQueryGenerator(@NamaPenjualOriginal, 'WT.NamaPenjualOriginal')

SELECT @QueryJumlahDPP =
dbo.uf_DecimalRangeDynamicQueryGenerator(@WHTBaseAmountOperator,@WHTBaseAmount , 'WT.JumlahDPP')

SELECT @QueryJumlahPPH =
dbo.uf_DecimalRangeDynamicQueryGenerator(@WHTAmountOperator, @WHTAmount, 'WT.JumlahPPH')

SELECT @QueryTarif =
dbo.uf_DecimalRangeDynamicQueryGenerator(@TarifOperator, @Tarif, 'cast(WT.Tarif*100 as decimal(18,2))')

SELECT @QueryStatusInvoice = 
dbo.uf_LookupDynamicQueryGeneratorWithBlank(@StatusInvoice, 'WT.StatusInvoice')

SELECT @QueryTaxArticle = 
dbo.uf_LookupDynamicQueryGenerator(@TaxArticle, 'WT.TaxArticle')

SELECT @QueryJenisPajak = 
dbo.uf_LookupDynamicQueryGenerator(@JenisPajak, 'WT.JenisPajak + '' - '' + jp.TransaksiTypeDescShort')

SELECT @QueryTaxAdditionalInfo = 
dbo.uf_LookupDynamicQueryGenerator(@TaxAdditionalInfo, 'WT.TaxAdditionalInfo')

SELECT @QuerySPTType = 
dbo.uf_LookupDynamicQueryGenerator(@SPTType, 'WT.SPTType')

SELECT @QueryNomorDocSAP = 
dbo.uf_LookupDynamicQueryGenerator(@NomorDocSAP, 'WT.NomorDocSAP')

SELECT @QueryDownloadStatus = 
dbo.uf_LookupDynamicQueryGenerator(@DownloadStatus, 'WT.DownloadStatus')

SELECT @QueryRelatedGLAccount = 
dbo.uf_LookupDynamicQueryGenerator(@RelatedGLAccount, 'WT.RelatedGLAccount')

SELECT @QueryNomorBuktiPotong = 
dbo.uf_LookupDynamicQueryGenerator(@NomorBuktiPotong, 'WT.NomorBuktiPotong')

SELECT @QueryeBupotNumber = 
dbo.uf_LookupDynamicQueryGenerator(@eBupotNumber, 'WT.eBupotNumber')

SELECT @QueryeeBupotReference = 
dbo.uf_LookupDynamicQueryGenerator(@eBupotReferenceNo, 'WT.eBupotReferenceNo')

SELECT @QueryeMapCode = 
dbo.uf_LookupDynamicQueryGenerator(@MapCode, 'WT.MapCode')



DECLARE @DivId varchar(MAX), @DivCode varchar(MAX)
	set @DivId = (select DivisionId from tb_m_user where USERNAME = @Username) --ambil 1 id bedasarkan username
	set @DivCode = (select divisioncode from TB_M_Division where id = @DivId) -- ambil divcode bedasarkan id @DivId

--IF NOT EXISTS (SELECT 1 FROM dbo.uf_SplitString(@WHTFullAccessRole, ';' ) WHERE ColumnData = @DivisionName) //efb
IF (@isFullAccess=0) 
		--AND @Perekam <> @Username
BEGIN
	SELECT @QueryPerekam = 
	dbo.uf_LookupDynamicQueryGenerator(@Perekam, 'WT.CreatedBy') 
	set @QueryPerekam =replace(@QueryPerekam ,'%','') + ' AND WT.CreatedBy in 
	(select username from tb_m_user where DivisionId in 
	(select id from tb_m_division where divisioncode = ''' + @DivCode + ''' ))'
END
ELSE
BEGIN
	SELECT @QueryPerekam = 
	dbo.uf_LookupDynamicQueryGenerator(@Perekam, 'WT.CreatedBy')
	set @QueryPerekam =replace(@QueryPerekam ,'%','')
END

SELECT @QueryFakturType = 
dbo.uf_LookupDynamicQueryGenerator(@FakturType, 'WT.FakturType')
set @QueryFakturType =replace(@QueryFakturType ,'%','')

SET @Query = 
		'SELECT * FROM (' +
		@Query + 
		REPLACE(@QueryTanggalFileDiterima, 'AND (', 'AND ((') +
		' OR ' +
		RIGHT(@QueryTanggalDataTerbentuk, LEN(@QueryTanggalDataTerbentuk) - 4) +
		')' +
		@QueryTanggalFileDiterima +
		@QueryTanggalDataTerbentuk +
		@QueryTanggalDownload +
		@QueryNomorInvoice +
		@QueryNomorPV +
		@QueryNomorBuktiPotong +
		@QueryNPWPPenjual +
		@QueryNamaPenjual +
		@QueryNamaPenjualOriginal +
		@QueryJumlahDPP +
		@QUeryJumlahPPH +
		@QueryTarif +
		@QueryStatusInvoice +
		@QueryTaxArticle +
		@QueryJenisPajak +
		@QueryTaxAdditionalInfo +
		@QuerySPTType +
		@QueryNomorDocSAP +
		@QueryDownloadStatus +
		@QueryRelatedGLAccount +
		@QueryTanggalBuktiPotong +
		@QueryTanggalPenerimaan +
		@QueryPostingDate +
		@QueryPerekam +
		@QueryFakturType +
		@QueryBulanPajak + 
		@QueryTahunPajak +
		@QueryPVCreatedBy +
		@QueryApprovalStatus +
		@QueryApprovalDate + 
		@QueryApprovalDesc +
		@QueryApprovalRecordDate +
		@QueryInvoiceDate +
		@QueryeBupotNumber +
		@QueryeeBupotReference +
		@QueryeMapCode +
		') TBL WHERE RowNum BETWEEN ' + 
		CONVERT(VARCHAR, @FromNumber) + ' AND ' + 
		CONVERT(VARCHAR, @ToNumber)
print(@Query)
EXEC(@Query)

END