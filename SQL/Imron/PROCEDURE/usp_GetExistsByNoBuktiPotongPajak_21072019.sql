USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetExistsByeBupotNumber]    Script Date: 21/07/2019 04:20:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[usp_GetExistsByNoBuktiPotongPajak]

	-- Add the parameters for the stored procedure here

    @refno VARCHAR(50)
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
      
                SELECT  COUNT(*) ExistsData
                FROM    dbo.TB_R_WHT AS trw
                WHERE   trw.NomorBuktiPotong = @refno
          
    END