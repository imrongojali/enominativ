USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[sp_PurchaseOrderNew_create]    Script Date: 20/07/2019 10:49:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dirga Mawardi
-- Create date: 2013
-- Description:	<Description,,>
-- =============================================
alter PROCEDURE [dbo].[sp_eBupot_compare]
    @eBupotNumber VARCHAR(50) ,
    @SourcePath VARCHAR(MAX) ,
    @FileName VARCHAR(MAX) ,
    @DestinationPath VARCHAR(MAX) ,
    @NewFileName VARCHAR(MAX)
AS
    BEGIN
   
        SET NOCOUNT ON;
        DECLARE @dateCreate DATETIME ,
            @errNum INT ,
            @isPDF int
		
		

        BEGIN TRANSACTION; 
        BEGIN TRY 
            SELECT  @dateCreate = GETDATE()  
	 
	

            SET NOCOUNT ON;

            IF EXISTS ( SELECT  *
                        FROM    dbo.TB_R_WHT AS trw
                        WHERE   trw.eBupotNumber = @eBupotNumber )
                BEGIN
                    UPDATE  dbo.TB_R_WHT
                    SET     PDFWithholding = @DestinationPath + @NewFileName
                    WHERE   eBupotNumber = @eBupotNumber

                    INSERT  INTO TB_M_PDFFiles
                            ( NomorBuktiPotong ,
                              SourcePath ,
                              FileName ,
                              DestinationPath ,
                              NewFileName ,
                              Date ,
                              Status
                            )
                    VALUES  ( @eBupotNumber ,
                              @SourcePath ,
                              @FileName ,
                              @DestinationPath ,
                              @NewFileName ,
                              @dateCreate ,
                              'Data Sesuai'
                            )

                    SET @isPDF = 1
                END
            ELSE
                BEGIN
                    SET @isPDF = 0
                END
	 
	
        END TRY									
        BEGIN CATCH
   
        
            IF @@TRANCOUNT > 0
                ROLLBACK TRANSACTION;
	 SELECT  0 isPDFData
        END CATCH;

        IF @@TRANCOUNT > 0
            COMMIT TRANSACTION;
        SELECT  @isPDF isPDFData
    END
