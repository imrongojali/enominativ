USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_Get_eBupotPath]    Script Date: 20/07/2019 03:32:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[usp_Get_eBupotPath]
as
begin

	Declare @eBupotSource varchar(300),@eBupotFail varchar(300), @eBupotProcess varchar(300),@eBupotSuccess varchar(300)
		

	set @eBupotSource = (select ConfigValue from TB_M_ConfigurationData where ConfigKey = 'SourceEBupot')
	set @eBupotProcess = (select ConfigValue from TB_M_ConfigurationData where ConfigKey = 'ProcessEBupot')
	set @eBupotSuccess = (select ConfigValue from TB_M_ConfigurationData where ConfigKey = 'SuccessEBupot')
	set @eBupotFail = (select ConfigValue from TB_M_ConfigurationData where ConfigKey = 'FailEBupot')
	

	select @eBupotSource as eBupotSource, @eBupotProcess as eBupotProcess, @eBupotSuccess as eBupotSuccess, @eBupotFail as eBupotFail
END

