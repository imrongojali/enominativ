USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateCustom_TB_R_WHT_NIK]    Script Date: 25/07/2019 01:01:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_UpdateCustom_TB_R_WHT_NIK]
    @status VARCHAR(2) = '' ,
    @eBupotReferenceNo VARCHAR(50) = '' ,
    @NomorPV varchar(200)='',
	@NamaPenjual VARCHAR(100)='',
    @TaxAdditionalInfo VARCHAR(500) = ''
AS
    BEGIN
	--DECLARE @ResultCode BIT = 1, @ResultDesc VARCHAR(MAX) = 'Success'
        BEGIN TRANSACTION;

        BEGIN TRY
            DECLARE @ResultCode BIT = 1 ,
                @ResultDesc VARCHAR(MAX) = 'Success'
            IF ( @status = 'EB' )
                BEGIN
                    UPDATE  dbo.TB_R_WHT
                    SET     TaxAdditionalInfo = @TaxAdditionalInfo
                    WHERE   eBupotReferenceNo = @eBupotReferenceNo AND NomorPV=@NomorPV
                END
				ELSE
                BEGIN
				 UPDATE  dbo.TB_R_WHT
                    SET     TaxAdditionalInfo = @TaxAdditionalInfo
                    WHERE  REPLACE(ltrim(RTRIM(NamaPenjual)),' ','') = REPLACE(ltrim(RTRIM(@NamaPenjual)),' ','')  AND NomorPV=@NomorPV --AND ISNULL(eBupotNumber,'')=''
                end

            SELECT  @ResultCode AS ResultCode ,
                    @ResultDesc AS ResultDesc
	
        END TRY
        BEGIN CATCH

            IF @@TRANCOUNT > 0
                ROLLBACK TRANSACTION;

		
            SELECT  CONVERT(BIT, 0) AS ResultCode ,
                    ERROR_MESSAGE() AS ResultDesc
    

        END CATCH;

        IF @@TRANCOUNT > 0
            COMMIT TRANSACTION;
    END