USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetExistsByeBupotNumber]    Script Date: 25/07/2019 00:41:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_GetExistsByeBupotNumber]

	-- Add the parameters for the stored procedure here
    @status VARCHAR(2) = '' ,
    @refno VARCHAR(60)='',
	@NomorPV varchar(200)='',
	@NamaPenjual VARCHAR(100)=''
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        IF ( @status = 'EB' )
            BEGIN
                SELECT  COUNT(*) ExistsData
                FROM    dbo.TB_R_WHT AS trw
                WHERE   trw.eBupotReferenceNo = @refno AND trw.NomorPV =@NomorPV
            END
        ELSE
            BEGIN
                SELECT  COUNT(*) ExistsData
                FROM    dbo.TB_R_WHT AS trw
                WHERE   trw.NomorPV = @NomorPV AND trw.NamaPenjual=@NamaPenjual
            END
    END