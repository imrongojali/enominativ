USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetWHTEBupotOutherFileById]    Script Date: 25/07/2019 02:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_GetWHTEBupotOutherFileById] @WHTId VARCHAR(MAX)
AS
    BEGIN
        SELECT  ColumnData
        INTO    #tempSelectedWHTEBupotOutherId
        FROM    uf_SplitString(@WHTId, ';')

       

        SELECT  *
        FROM    ( SELECT    ROW_NUMBER() OVER ( ORDER BY WT.Id ) AS RowNum ,
                            WT.Id ,
                            WT.TaxArticle ,
                            WT.NPWPPenjual ,
                            WT.NamaPenjual ,
                            WT.JenisPajak ,
                            CASE WHEN WT.TaxArticle NOT IN ( '23', '26' )
                                 THEN 'Please choose tax artikel data only 23 and 26.'
                                 ELSE ( CASE WHEN WT.StatusInvoice <> 'POSTED'
                                             THEN 'Please choose tax artikel data only POSTED.'
                                             WHEN ISNULL(WT.eBupotReferenceNo,
                                                         '') = ''
                                             THEN 'Please choose eBupot Reference No null'
                                             ELSE ( CASE WHEN WT.TaxArticle = '23'
                                                         THEN ( CASE
                                                              WHEN CONVERT(DECIMAL, ISNULL(dbo.RemoveSpecialChars(REPLACE(NPWPPenjual,
                                                              'abcdefghijklmnopqrstuvwzxy',
                                                              '')), '')) <= 0
                                                              AND ISNULL(WT.TaxAdditionalInfo,
                                                              '') = ''
                                                              THEN 'Please choose npwp and nik No null'
                                                              WHEN CONVERT(DECIMAL, ISNULL(dbo.RemoveSpecialChars(REPLACE(NPWPPenjual,
                                                              'abcdefghijklmnopqrstuvwzxy',
                                                              '')), '')) > 0
                                                              THEN ( 
															  --CASE
                 --                                             WHEN jp.MapCode = '-'
                 --                                             THEN 'Please Map Code'
                 --                                             ELSE ( 
															  CASE
                                                              WHEN WT.JenisPajak = '2399'
                                                              AND ISNULL(tmf.ApprovalStatus,'') <> 'Approved'
                                                              THEN 'Please choose Facility SKB'
                                                              WHEN CONVERT(DATE, ISNULL(tmf.FacValidPeriodTo,'1900-01-01')) < CONVERT(DATE, WT.PostingDate)
                                                              THEN 'Facility Expired SKB'
                                                              ELSE 'Done'
                                                              END
															   )
                                                              --END )
                                                              WHEN ISNULL(WT.TaxAdditionalInfo,
                                                              '') <> ''
                                                              THEN 'Done'
                                                              END )
                                                         WHEN WT.TaxArticle = '26'
                                                         THEN ( CASE
                                                              WHEN WT.JenisPajak IN (
                                                              '2621', '2622',
                                                              '2623', '2624',
                                                              '2625', '2626',
                                                              '2627', '2628',
                                                              '2629', '2630',
                                                              '2631', '2632',
                                                              '2633', '2634',
                                                              '2635', '2636',
                                                              '2637', '2638',
                                                              '2639', '2640',
                                                              '2641', '2642',
                                                              '2643', '2644' ) AND ISNULL(tmf.ApprovalStatus,'') <> 'Approved'
                                                              THEN 'Please choose Facility COD'
                                                              WHEN CONVERT(DATE, ISNULL(tmf.FacValidPeriodTo,'1900-01-01')) < CONVERT(DATE, WT.PostingDate)
                                                              THEN 'Facility Expired COD'
                                                              ELSE 'Done'
                                                              END )
                                                    END )
                                        END )
                            END ErrorData
                  FROM      TB_R_WHT WT
                            LEFT JOIN TB_M_JenisPajak jp ON WT.JenisPajak = jp.TransaksiTypeCode
                            LEFT JOIN dbo.TB_M_Facility AS tmf ON WT.NPWPPenjual = tmf.SupplierNPWP
                  WHERE     WT.Id IN ( SELECT   ColumnData
                                       FROM     #tempSelectedWHTEBupotOutherId )
                ) a
        WHERE   a.ErrorData <> 'Done'

		




     

        DROP TABLE #tempSelectedWHTEBupotOutherId
    END

--SELECT TOP 10 *FROM TB_R_SyncLog ORDER BY CreatedOn DESC





	
