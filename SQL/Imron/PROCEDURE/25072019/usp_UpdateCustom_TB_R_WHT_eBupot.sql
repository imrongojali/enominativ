USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateCustom_TB_R_WHT_eBupot]    Script Date: 25/07/2019 00:19:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_UpdateCustom_TB_R_WHT_eBupot]
    @eBupotReferenceNo VARCHAR(50) ,
    @ApprovalStatus VARCHAR(100) ,
    @ApprovalDesc VARCHAR(400) ,
    @MapCode VARCHAR(15) ,
    @eBupotNumber VARCHAR(50),
	@ApprovalTime DATETIME ,
    @RecordTime DATETIME 
AS
    BEGIN
	--DECLARE @ResultCode BIT = 1, @ResultDesc VARCHAR(MAX) = 'Success'
        BEGIN TRANSACTION;

        BEGIN TRY
            DECLARE @ResultCode BIT = 1 ,
                @ResultDesc VARCHAR(MAX) = 'Success'
            --IF EXISTS ( SELECT  *
            --            FROM    dbo.TB_R_WHT
            --            WHERE   eBupotReferenceNo = @eBupotReferenceNo
            --                    AND ISNULL(eBupotNumber, '') <> '' )
            --    BEGIN
            --        UPDATE  dbo.TB_R_WHT
            --        SET     ApprovalStatus = @ApprovalStatus ,
            --                ApprovalDesc = CASE WHEN ISNULL(@ApprovalDesc, '') = ''
            --                                    THEN NomorInvoice
            --                                    ELSE @ApprovalDesc
            --                               END ,
            --                MapCode = @MapCode
            --        WHERE   eBupotReferenceNo = @eBupotReferenceNo --AND ISNULL(eBupotNumber,'')=''
            --    END
            --ELSE
                --BEGIN
				  
                    UPDATE  dbo.TB_R_WHT
                    SET     ApprovalStatus = @ApprovalStatus ,
                            ApprovalDesc = CASE WHEN ISNULL(@ApprovalDesc, '') = ''
                                                THEN NomorInvoice
                                                ELSE @ApprovalDesc
                                           END ,
                            MapCode = @MapCode ,
                            eBupotNumber = CASE WHEN ISNULL(eBupotNumber,'')='' THEN @eBupotNumber ELSE eBupotNumber end  ,
                            NomorBuktiPotong = CASE WHEN ISNULL(NomorBuktiPotong,'')='' THEN @eBupotNumber ELSE NomorBuktiPotong END,
							 ApprovalDate = @ApprovalTime ,
							  RecordTime = CASE WHEN RecordTime IS NOT NULL
                                          THEN RecordTime
                                          ELSE @RecordTime
                                     END
                    WHERE   eBupotReferenceNo = @eBupotReferenceNo --AND ISNULL(eBupotNumber,'')='
                --END
	--SELECT*FROM @EbupotList


            SELECT  @ResultCode AS ResultCode ,
                    @ResultDesc AS ResultDesc
	
        END TRY
        BEGIN CATCH

            IF @@TRANCOUNT > 0
                ROLLBACK TRANSACTION;

		
            SELECT  CONVERT(BIT, 0) AS ResultCode ,
                    ERROR_MESSAGE() AS ResultDesc
    

        END CATCH;

        IF @@TRANCOUNT > 0
            COMMIT TRANSACTION;
    END