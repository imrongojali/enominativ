USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateCustom_TB_R_WHT_ApprovalStatusEbupot]    Script Date: 23/07/2019 11:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_UpdateCustom_TB_R_WHT_ApprovalStatusEbupot]
    @NomorBuktiPotong VARCHAR(100) ,
    @ApprovalStatus VARCHAR(50) ,
    @ApprovalTime DATETIME ,
    @RecordTime DATETIME ,
    @ApprovalDescription VARCHAR(50)
AS
    BEGIN
        DECLARE @ResultCode BIT = 1 ,
            @ResultDesc VARCHAR(MAX) = 'Success' ,
            @appDesc VARCHAR(400)



			

        --IF ( LTRIM(RTRIM(ISNULL(@NomorBuktiPotong, ''))) <> '' )
        --    BEGIN
        IF NOT EXISTS ( SELECT  1
                        FROM    TB_R_WHT
                        WHERE   ( NomorBuktiPotong = @NomorBuktiPotong
                                  AND ApprovalStatus NOT LIKE 'Reject'
                                ) --OR ( NomorBuktiPotong = @NomorBuktiPotong
                                        --     AND ApprovalStatus IS NULL
                                        --   )
										    )
            BEGIN
                SELECT  @ResultCode = 0
                SELECT  @ResultDesc = 'Withholding number does not exist or approval already finished '
		
            END

	IF NOT EXISTS ( SELECT  1
                        FROM    TB_R_WHT
                        WHERE   ( NomorBuktiPotong = @NomorBuktiPotong
                                  AND ApprovalStatus NOT LIKE 'Reject'
                                ) --OR ( NomorBuktiPotong = @NomorBuktiPotong
                                        --     AND ApprovalStatus IS NULL
                                        --   )
										    )
            BEGIN
                SELECT  @ResultCode = 0
                SELECT  @ResultDesc = 'Withholding number does not exist or approval already finished '
		
            END


	 


			
		

        IF NOT EXISTS ( SELECT  1
                        FROM    TB_M_GeneralParam
                        WHERE   ParamValue = @ApprovalStatus
                                AND ParamType = 'ApprovalStatus' )
            BEGIN
                SELECT  @ResultCode = 0
                SELECT  @ResultDesc = '''' + @ApprovalStatus
                        + ''' is not a valid Approval Status'
            END


			   --IF  EXISTS (SELECT 1 FROM  dbo.TB_R_WHT AS trw WHERE trw.NomorBuktiPotong=@NomorBuktiPotong AND ISNULL(trw.PDFWithholding,'')<>'')
      --      BEGIN
      --          SELECT  @ResultCode = 0
      --          SELECT  @ResultDesc = 'PDF Exists download from eBupot'
      --      END

			
	
        IF @ResultCode = 1
            BEGIN

                SELECT  @appDesc = CASE WHEN trw.TaxArticle IN ( '23', '26' )
                                        THEN ( CASE WHEN LTRIM(RTRIM(ISNULL(@ApprovalDescription,
                                                              ''))) = ''
                                                    THEN NomorInvoice
                                                    ELSE LTRIM(RTRIM(ISNULL(@ApprovalDescription,
                                                              '')))
                                               END )
                                        ELSE ( CASE WHEN LTRIM(RTRIM(ISNULL(@ApprovalDescription,
                                                              ''))) = ''
                                                    THEN NomorBuktiPotong
                                                    ELSE LTRIM(RTRIM(ISNULL(@ApprovalDescription,
                                                              '')))
                                               END )
                                   END
                FROM    dbo.TB_R_WHT AS trw
                WHERE   trw.NomorBuktiPotong = @NomorBuktiPotong

                UPDATE  TB_R_WHT
                SET     ApprovalStatus = @ApprovalStatus ,
                        ApprovalDate = @ApprovalTime ,
                        ApprovalDesc = @appDesc ,
                        RecordTime = CASE WHEN RecordTime IS NOT NULL
                                          THEN RecordTime
                                          ELSE @RecordTime
                                     END
		--WHERE
		--	NomorBuktiPotong = @NomorBuktiPotong
		--	AND ApprovalStatus NOT LIKE 'Reject'
                WHERE   ( NomorBuktiPotong = @NomorBuktiPotong
                          AND ApprovalStatus NOT LIKE 'Reject'
                        )
                                --OR ( NomorBuktiPotong = @NomorBuktiPotong
                                --     AND ApprovalStatus IS NULL
                                --   )
            END
            --END
        --ELSE
        --    IF ( LTRIM(RTRIM(ISNULL(@eBupotNumber, ''))) <> '' )
        --        BEGIN
        --            IF NOT EXISTS ( SELECT  1
        --                            FROM    TB_R_WHT
		
        --                            WHERE   ( eBupotNumber = @eBupotNumber
        --                                      AND ApprovalStatus NOT LIKE 'Reject'
        --                                    )
        --                                    OR ( eBupotNumber = @eBupotNumber
        --                                         AND ApprovalStatus IS NULL
        --                                       ) )
        --                BEGIN
        --                    SELECT  @ResultCode = 0
        --                    SELECT  @ResultDesc = 'Withholding number does not exist or approval already finished '
		
        --                END

        --            IF NOT EXISTS ( SELECT  1
        --                            FROM    TB_M_GeneralParam
        --                            WHERE   ParamValue = @ApprovalStatus
        --                                    AND ParamType = 'ApprovalStatus' )
        --                BEGIN
        --                    SELECT  @ResultCode = 0
        --                    SELECT  @ResultDesc = '''' + @ApprovalStatus
        --                            + ''' is not a valid Approval Status'
        --                END
	
        --            IF @ResultCode = 1
        --                BEGIN
        --                    UPDATE  TB_R_WHT
        --                    SET     ApprovalStatus = @ApprovalStatus ,
        --                            ApprovalDate = @ApprovalTime ,
        --                            ApprovalDesc = CASE WHEN LTRIM(RTRIM(ISNULL(@ApprovalDescription,
        --                                                      ''))) = ''
        --                                                THEN NomorInvoice
        --                                                ELSE LTRIM(RTRIM(ISNULL(@ApprovalDescription,
        --                                                      '')))
        --                                           END ,
        --                            RecordTime = CASE WHEN RecordTime IS NOT NULL
        --                                              THEN RecordTime
        --                                              ELSE @RecordTime
        --                                         END
		
        --                    WHERE   ( eBupotNumber = @eBupotNumber
        --                              AND ApprovalStatus NOT LIKE 'Reject'
        --                            )
        --                            OR ( eBupotNumber = @eBupotNumber
        --                                 AND ApprovalStatus IS NULL
        --                               )
        --                END
        --        END

        SELECT  @ResultCode AS ResultCode ,
                @ResultDesc AS ResultDesc
    END

