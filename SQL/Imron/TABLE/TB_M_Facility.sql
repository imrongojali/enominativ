USE [TAM_EFAKTUR]
GO

/****** Object:  Table [dbo].[TB_M_Facility]    Script Date: 18/07/2019 13:43:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TB_M_Facility](
	[TaxFacCategory] [CHAR](3) NULL,
	[TaxFacNo] [VARCHAR](100) NOT NULL,
	[SKBType] [VARCHAR](50) NULL,
	[SKBTypeOthers] [VARCHAR](100) NULL,
	[TaxFacDate] [DATETIME] NULL,
	[SupplierName] [VARCHAR](100) NULL,
	[SupplierNPWP] [VARCHAR](15) NULL,
	[SupplierAddress] [VARCHAR](200) NULL,
	[SupplierPhoneNo] [VARCHAR](100) NULL,
	[SupplierCountry] [VARCHAR](50) NULL,
	[SupplierEstDate] [DATETIME] NULL,
	[FacValidPeriodFrom] [DATETIME] NULL,
	[FacValidPeriodTo] [DATETIME] NULL,
	[FacDocSignerName] [VARCHAR](100) NULL,
	[FacDocSignerTitle] [VARCHAR](100) NULL,
	[RecordedDate] [DATETIME] NULL,
	[RecordedBy] [VARCHAR](100) NULL,
	[ApprovalStatus] [VARCHAR](100) NULL,
	[ApprovalDate] [DATETIME] NULL,
	[UploadFile] [VARCHAR](100) NULL,
	[RegistrationNumber] [VARCHAR](100) NULL,
	[PDFFacStatus] [VARCHAR](100) NULL,
 CONSTRAINT [PK_TB_M_Facility] PRIMARY KEY CLUSTERED 
(
	[TaxFacNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


