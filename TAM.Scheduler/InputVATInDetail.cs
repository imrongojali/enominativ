﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAM.EFaktur.Web.Models.VATIn;

namespace TAM.Scheduler
{
    class InputVATInDetail
    {
        public static void save(VATInDetailManualInputViewModel detail,Guid model)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_InsertCustom_TB_R_VATInDetail";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@Id", Guid.NewGuid());
            cn._sqlCommand.Parameters.AddWithValue("@VATInId", model);
            cn._sqlCommand.Parameters.AddWithValue("@NamaBarang", detail.UnitName);
            cn._sqlCommand.Parameters.AddWithValue("@HargaSatuan", detail.UnitPrice);
            cn._sqlCommand.Parameters.AddWithValue("@JumlahBarang", detail.Quantity);
            cn._sqlCommand.Parameters.AddWithValue("@HargaTotal", detail.TotalPrice);
            cn._sqlCommand.Parameters.AddWithValue("@Diskon", detail.Discount);
            cn._sqlCommand.Parameters.AddWithValue("@DPP", detail.DPP);
            cn._sqlCommand.Parameters.AddWithValue("@PPN", detail.PPN);
            cn._sqlCommand.Parameters.AddWithValue("@TarifPPNBM", detail.TarifPPNBM);
            cn._sqlCommand.Parameters.AddWithValue("@PPNBM", detail.PPNBM);
            cn._sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
            cn._sqlCommand.Parameters.AddWithValue("@CreatedBy", "System");
            cn._sqlCommand.ExecuteReader();
            cn.CloseConnection();
            cn.DisposeConnection();
        }
    }
}
