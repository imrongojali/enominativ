﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAM.Scheduler.OCR
{
    class SyncLog
    {
        public static void InsertLog(string nomor, string Filenames, string MessageType, string Message)
        {
            Connection cn = new Connection();

            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_Insert_TB_R_SyncLog";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@Id", Guid.NewGuid());
            cn._sqlCommand.Parameters.AddWithValue("@FileName", Filenames);
            cn._sqlCommand.Parameters.AddWithValue("@RowId", 0);
            cn._sqlCommand.Parameters.AddWithValue("@RowData", nomor);
            cn._sqlCommand.Parameters.AddWithValue("@MessageType", MessageType);
            cn._sqlCommand.Parameters.AddWithValue("@Message", Message);
            cn._sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
            cn._sqlCommand.Parameters.AddWithValue("@CreatedBy", "System");
            cn._sqlCommand.ExecuteNonQuery();

            cn.CloseConnection();
            cn.DisposeConnection();
        }

        public static void InsertLogEprocInvoice(string TransactionId, string _err)
        {
            Connection cn = new Connection();

            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_Insert_TB_R_EprocInvoice_Sync_Log";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@TransactionId", TransactionId);
            cn._sqlCommand.Parameters.AddWithValue("@error", _err);
            
            cn._sqlCommand.ExecuteNonQuery();

            cn.CloseConnection();
            cn.DisposeConnection();
        }
    } 
} 