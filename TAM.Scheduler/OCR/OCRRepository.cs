﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAM.Scheduler.OCR
{
    class OCRRepository
    {
        
        public static void inputOCR(string nomor, string SourcePath, string FileName)
        {
            
            Connection cn = new Connection();

            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_InputPDF";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            cn._sqlCommand.Parameters.AddWithValue("@Action", "Add");
            cn._sqlCommand.Parameters.AddWithValue("@Nomor", Convert.ToString(nomor));
            cn._sqlCommand.Parameters.AddWithValue("@SourcePath", Convert.ToString(SourcePath));
            cn._sqlCommand.Parameters.AddWithValue("@FileName", Convert.ToString(FileName));
            cn._sqlCommand.Parameters.AddWithValue("@DestinationPath", "");
            cn._sqlCommand.Parameters.AddWithValue("@NewFileName", Convert.ToString(nomor + ".pdf"));
            cn._sqlCommand.Parameters.AddWithValue("@Date", DateTime.Now);
            cn._sqlCommand.Parameters.AddWithValue("@Status", "Success Scan");

            cn._sqlCommand.ExecuteNonQuery();
            cn.CloseConnection();
            cn.DisposeConnection();
        }

        public static void CekBuktiPotong(string nomor)
        {
            Connection cn = new Connection();

            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_CekNoBuktiPotong";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@Nomor", Convert.ToString(nomor));
            cn._sqlCommand.ExecuteNonQuery();

            cn.CloseConnection();
            cn.DisposeConnection();
        }

        public static string CekNomorBP(string nomor)
        {
            Connection cn = new Connection();
            string status = "";

            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_CekNomorBP";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@Nomor", Convert.ToString(nomor));
            cn._sqlCommand.ExecuteNonQuery();

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            rd.Read();

            status = rd[0].ToString();

            cn.CloseConnection();
            cn.DisposeConnection();

            return status;
        }

        public static string ValidateNumber(string nomor)
        {
            string count = "";
            Connection cn = new Connection();

            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_ValidatePDFNumber";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@Nomor", Convert.ToString(nomor));
            cn._sqlCommand.ExecuteNonQuery();

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            rd.Read();

            count = rd[0].ToString();

            cn.CloseConnection();
            cn.DisposeConnection();

            return count;
        }
    }
}
