﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAM.Scheduler
{
    class Syncdata
    {
        public static void InsertSync(string data,Guid Id)
        {
            Connection cn = new Connection();

            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_Insert_TB_R_SyncData";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@Id", Id);
            cn._sqlCommand.Parameters.AddWithValue("@SyncType", "FM");
            cn._sqlCommand.Parameters.AddWithValue("@BussinessUnit", DBNull.Value);
            cn._sqlCommand.Parameters.AddWithValue("@SyncTime", DateTime.Now);
            cn._sqlCommand.Parameters.AddWithValue("@FileURL", data);
            cn._sqlCommand.Parameters.AddWithValue("@FileName", "");
            cn._sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
            cn._sqlCommand.Parameters.AddWithValue("@CreatedBy", "SyncData");
            cn._sqlCommand.ExecuteNonQuery();

            cn.CloseConnection();
            cn.DisposeConnection();
        }

        public static void InsertSyncDetail(string RowData, Guid SyncId)
        {
            Connection cn = new Connection();

            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_Insert_TB_R_SyncDataDetail";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@Id", Guid.NewGuid());
            cn._sqlCommand.Parameters.AddWithValue("@SyncId", SyncId);
            cn._sqlCommand.Parameters.AddWithValue("@RowId", 0);
            cn._sqlCommand.Parameters.AddWithValue("@RowData", RowData);
            cn._sqlCommand.Parameters.AddWithValue("@SyncStatus", 1);
            cn._sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
            cn._sqlCommand.Parameters.AddWithValue("@CreatedBy", "SyncData");
            cn._sqlCommand.ExecuteNonQuery();

            cn.CloseConnection();
            cn.DisposeConnection();

        }
    }
}
