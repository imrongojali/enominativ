﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAM.Scheduler.OCR;

namespace TAM.Scheduler.PrepaidWHT
{
    class PrepaidWHT
    {
        public static int SavePrepaidWHT(string NomorBuktiPotong, string SourcePath, string FileName, string DestinationPath, string NewFileName, string failPath)
        {

            //  


            try
            {
                int isPDFData = 0;
                Connection cn = new Connection();
                cn.CreateConnection();
                cn.OpenConnection();

                cn._sqlCommand.CommandText = "sp_PrepaidWHT_UploadFile";
                cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                cn._sqlCommand.Parameters.AddWithValue("@NomorBuktiPotong", NomorBuktiPotong);
                cn._sqlCommand.Parameters.AddWithValue("@SourcePath", SourcePath);
                cn._sqlCommand.Parameters.AddWithValue("@FileName", FileName);
                cn._sqlCommand.Parameters.AddWithValue("@DestinationPath", DestinationPath);
                cn._sqlCommand.Parameters.AddWithValue("@NewFileName", NewFileName);

                SqlDataReader rd = cn._sqlCommand.ExecuteReader();
                if (rd.Read())
                {
                    isPDFData = Convert.ToInt32(rd["isPDFData"]);
                }

                cn.CloseConnection();
                cn.DisposeConnection();
                return isPDFData;
            }
            catch (Exception ex)
            {

                SyncLog.InsertLog(failPath, NomorBuktiPotong, "Prepaid_WHT_ERR", ex.Message);
                return 0;

            }

        }
    }
}
