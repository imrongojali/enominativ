﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace TAM.Scheduler
{
    class Email
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsHtml { get; set; }

        public void Send()
        {
            var message = new MailMessage();
            message.Subject = Subject;
            message.Body = Body;
            message.IsBodyHtml = IsHtml;
            
            var emails = GetUserEmail().Distinct().ToList();
            var emails2 = GetFinanceTaxEmail();
            var allemails = emails.Union(emails2);
            foreach (var item in allemails)
            {
                if (item != null && item != "" && IsValidEmail(item))
                {
                    message.To.Add(new MailAddress(item));
                }
            }
            
            //try
            //{
            using (var smtp = new SmtpClient())
                {
                    smtp.Send(message);
                }
            //}
            //catch (Exception ex)
            //{
                //Console.WriteLine("Exception is: " + ex.Message);
                //if (ex.InnerException != null)
                //{
                //    Console.WriteLine("InnerException is: {0}", ex.InnerException);
                //}
                //Console.ReadLine();
            //}
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        //static void Main()
        //{
        //    if (CheckDataRemainder())
        //    {
        //        Email mail = new Email();
        //        mail.Subject = "[TAM ELVIS REMINDER] Reminder of to be expired PV tax invoice as " + DateTime.Now.ToString("dd/MM/yyyy hh:mm");
        //        mail.Body = CreateMailBody();
        //        mail.IsHtml = true;
        //        mail.Send();
        //    }
        //}

        public static string CreateMailBody()
        {
            string body = "";

            body = "<b>Dear Bapak dan Ibu,</b><br>";
            body += "<br>";
            body += "Anda memiliki {xx} Faktur Pajak yang sudah mendekati masa expired<br>";
            body += "Untuk detail sebagai berikut:<br>";
            body += "<br>";
            body += "<table width=\"100%\" border=\"1\" cellpadding=\"5\" style=\"border-collapse:collapse;\">";
            body += "<tr border=\"1\" bgcolor=\"#cccccc\">";
            body += "<th>No</th><th>PV Number</th><th>Invoice Status</th><th>PV Creator</th><th>Remaining Days</th><th>No. Faktur Pajak</th><th>Tanggal Faktur Pajak</th><th>DPP</th><th>PPN</th>";
            body += "</tr>";

            var datas = GetDataRemainder();

            foreach (DataRemainder data in datas)
            {
                body += "<tr border=\"1\">";
                body += "<td align=\"center\">" + data.No + "</td>";
                body += "<td align=\"center\"><a href=\"#\">" + data.PvNumber + "</a></td>";
                body += "<td align=\"center\">" + data.InvoiceStatus + "</td>";
                body += "<td align=\"center\">" + data.PvCreator + "</td>";
                body += "<td align=\"center\" style=\"color:red;\"><b>" + data.RemainingDays + " days</b></td>";
                body += "<td align=\"center\">" + data.NoFakturPajak + "</td>";
                body += "<td align=\"center\">" + data.TglFakturPajak.ToString("dd/MM/yyyy") + "</td>";
                body += "<td align=\"right\">" + int.Parse(data.Dpp.ToString()).ToString("0,0", CultureInfo.CreateSpecificCulture("el-GR")) + "</td>";
                body += "<td align=\"right\">" + int.Parse(data.Ppn.ToString()).ToString("0,0", CultureInfo.CreateSpecificCulture("el-GR")) + "</td>";
                body += "</tr>";
            }

            body += "</table>";

            body = body.Replace("{xx}", datas.Count.ToString());

            return body;
        }

        public static bool CheckDataRemainder()
        {
            bool Stt = false;

            Connection cn = new Connection();
            cn.CreateConnection2();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "sp_GetEmailRemainder";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            if (rd.Read())
            {
                Stt = true;
            }

            cn.CloseConnection();
            cn.DisposeConnection();

            return Stt;
        }

        public static List<DataRemainder> GetDataRemainder()
        {
            List<DataRemainder> dts = new List<DataRemainder>();
            int Nomor = 1;

            Connection cn = new Connection();
            cn.CreateConnection2();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "sp_GetEmailRemainder";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            while (rd.Read())
            {
                DataRemainder dt = new DataRemainder()
                {
                    No = Nomor.ToString(),
                    PvNumber = rd[0].ToString(),
                    InvoiceStatus = rd[1].ToString(),
                    PvCreator = rd[9].ToString(),
                    RemainingDays = rd[2].ToString(),
                    NoFakturPajak = rd[3].ToString(),
                    TglFakturPajak = DateTime.Parse(rd[4].ToString()),
                    Dpp = Double.Parse(rd[5].ToString()),
                    Ppn = Double.Parse(rd[6].ToString())
                };
                Nomor++;
                dts.Add(dt);
            }

            cn.CloseConnection();
            cn.DisposeConnection();

            return dts;
        }

        public static List<string> GetUserEmail()
        {
            List<string> emails = new List<string>();

            Connection cn = new Connection();
            cn.CreateConnection2();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "sp_GetEmailRemainder";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            while (rd.Read())
            {
                emails.Add(rd[10].ToString());
                emails.Add(rd[11].ToString());
            }

            cn.CloseConnection();
            cn.DisposeConnection();

            return emails;
        }

        public static List<string> GetFinanceTaxEmail()
        {
            List<string> emails = new List<string>();

            Connection cn = new Connection();
            cn.CreateConnection2();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "sp_GetEmailFinanceTax";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            while (rd.Read())
            {
                emails.Add(rd[0].ToString());
            }

            cn.CloseConnection();
            cn.DisposeConnection();

            return emails;
        }
    }

    public class DataRemainder
    {
        public string No { get; set; }
        public string PvNumber { get; set; }
        public string InvoiceStatus { get; set; }
        public string PvCreator { get; set; }
        public string RemainingDays { get; set; }
        public string NoFakturPajak { get; set; }
        public DateTime TglFakturPajak { get; set; }
        public Double Dpp { get; set; }
        public Double Ppn { get; set; }
    }
}
