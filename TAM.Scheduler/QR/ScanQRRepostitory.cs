﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.VATIn;
using TAM.Scheduler.OCR;
using TAM.Scheduler.QR;

namespace TAM.Scheduler.QR
{
    class ScanQRRepostitory
    {
        public static Guid SyncID { get; set; }

        //public static PathQR GetPath()
        //{
        //    PathQR path = new PathQR();

        //    Connection cn = new Connection();
        //    cn.CreateConnection();
        //    cn.OpenConnection();

        //    cn._sqlCommand.CommandText = "usp_BatchPDF";
        //    cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
        //    cn._sqlCommand.CommandTimeout = 0;

        //    SqlDataReader rd = cn._sqlCommand.ExecuteReader();
        //    rd.Read();

        //    path.SourcePath = rd[0].ToString();
        //    path.DestinationPath = rd[1].ToString();
        //    path.FailPath = rd[2].ToString();
        //    path.BatchPath = rd[3].ToString();

        //    cn.CloseConnection();
        //    cn.DisposeConnection();

        //    return path;
        //}

        public static PathQR GetQRPath()
        {
            PathQR path = new PathQR();

            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_Get_QRPDFPath";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            rd.Read();

            path.QRSourcePath = rd[0].ToString();
            path.QRDestinationPath = rd[1].ToString();
            path.QRFailPath = rd[2].ToString();
            path.QRProcessPath = rd[3].ToString();

            cn.CloseConnection();
            cn.DisposeConnection();

            return path;
        }

        public static void InsertQRUrl(string Filename, string URL)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_Insert_QRFileURL";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@Filename", Filename);
            cn._sqlCommand.Parameters.AddWithValue("@URL", URL);
            cn._sqlCommand.ExecuteReader();

            cn.CloseConnection();
            cn.DisposeConnection();
        }

        public static void UpdateQRStatus(string url, string flag)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_Update_QRURLStatus";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@url", url);
            cn._sqlCommand.Parameters.AddWithValue("@flag", flag);

            cn._sqlCommand.ExecuteReader();
            cn.CloseConnection();
            cn.DisposeConnection();
        }

        public static void InsertVATIn()
        {
            string URL = "";
            string fileName = "";
            try
            {
                Connection cn = new Connection();
                cn.CreateConnection();
                cn.OpenConnection();

                cn._sqlCommand.CommandText = "usp_GetQRUrl";
                cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader rd = cn._sqlCommand.ExecuteReader();
                while (rd.Read())
                {
                    URL = rd[0].ToString();
                    fileName = rd[1].ToString();
                    if (URL != "" || URL != null)
                    {
                        try
                        {
                            var response = new HttpResponseMessage();
                            resValidateFakturPm VATInXML = new resValidateFakturPm();

                            using (HttpClient client = new HttpClient())
                            {
                                #region Body
                                client.DefaultRequestHeaders.Accept.Clear();
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
                                Uri serviceUri = new Uri(URL, UriKind.Absolute);

                                XmlDocument doc = new XmlDocument();
                                response = client.GetAsync(serviceUri).Result;

                                var stream = new System.IO.MemoryStream(response.Content.ReadAsByteArrayAsync().Result);

                                var serializer = new XmlSerializer(typeof(resValidateFakturPm));
                                using (var reader = XmlReader.Create(stream))
                                {
                                    VATInXML = (resValidateFakturPm)serializer.Deserialize(reader);

                                    if (string.IsNullOrEmpty(VATInXML.nomorFaktur))
                                    {
                                        SyncLog.InsertLog(URL, fileName, "QR_ERR", "Server Error");
                                    }
                                    else
                                    {
                                        string InvoiceDate = VATInXML.tanggalFakturDateFormat.ToString("yyyy-MM-dd");
                                        DateTime expdate = Convert.ToDateTime(InvoiceDate).AddDays(1).AddMonths(3).AddDays(-1);
                                        DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);

                                        #region Map XML Model to ViewModel
                                        VATInManualInputViewModel model = new VATInManualInputViewModel();

                                        model.Id = Guid.NewGuid();
                                        model.InvoiceNumber = VATInXML.nomorFaktur;
                                        model.InvoiceNumberFull = VATInXML.nomorFaktur.FormatNomorFakturGabungan(VATInXML.kdJenisTransaksi, VATInXML.fgPengganti);
                                        model.KDJenisTransaksi = VATInXML.kdJenisTransaksi;
                                        model.FGPengganti = VATInXML.fgPengganti;
                                        model.InvoiceDate = VATInXML.tanggalFakturDateFormat.FormatSQLDate();
                                        model.ExpireDate = endOfMonth.ToString("yyyy-MM-dd");
                                        model.SupplierNPWP = VATInXML.npwpPenjual.FormatNPWP();
                                        model.SupplierName = VATInXML.namaPenjual;
                                        model.SupplierAddress = VATInXML.alamatPenjual;
                                        model.NPWPLawanTransaksi = VATInXML.npwpLawanTransaksi.FormatNPWP();
                                        model.NamaLawanTransaksi = VATInXML.namaLawanTransaksi;
                                        model.AlamatLawanTransaksi = VATInXML.alamatLawanTransaksi;
                                        model.StatusApprovalXML = VATInXML.statusApproval;
                                        model.StatusFakturXML = VATInXML.statusFaktur;
                                        model.VATBaseAmount = VATInXML.jumlahDpp;
                                        model.VATAmount = VATInXML.jumlahPpn;
                                        model.JumlahPPnBM = VATInXML.jumlahPpnBm;
                                        model.VATInDetails = new List<VATInDetailManualInputViewModel>();
                                        SyncID = Guid.NewGuid();
                                        try
                                        {
                                            #region Validasi
                                            string result = ValidateVATInQR.ValidateNPWP(model.NPWPLawanTransaksi);
                                            if (result == "")
                                            {
                                                if (model.StatusFakturXML != "Faktur Dibatalkan")
                                                {
                                                    result = ValidateVATInQR.ValidateExpireDate(model.InvoiceDate);
                                                    if (result == "")
                                                    {
                                                        result = ValidateVATInQR.ValidateVATIn(model.InvoiceNumberFull, model.InvoiceNumber, model.FGPengganti, "eFaktur", model.SupplierNPWP, model.InvoiceDate);
                                                        if (result == "")
                                                        {
                                                            #region Success Validasi
                                                            InputVATIn.save(model, SyncID);
                                                            SyncLog.InsertLog(URL, fileName, "QR_INF", "Insert VATIn Success");

                                                            foreach (resValidateFakturPmDetailTransaksi detail in VATInXML.detailTransaksi)
                                                            {
                                                                VATInDetailManualInputViewModel detailModel = new VATInDetailManualInputViewModel();

                                                                detailModel.UnitName = detail.nama;
                                                                detailModel.UnitPrice = detail.hargaSatuan;
                                                                detailModel.Quantity = detail.jumlahBarang;
                                                                detailModel.TotalPrice = detail.hargaTotal;
                                                                detailModel.Discount = detail.diskon;
                                                                detailModel.DPP = detail.dpp;
                                                                detailModel.PPN = detail.ppn;
                                                                detailModel.PPNBM = detail.ppnbm;
                                                                detailModel.TarifPPNBM = detail.tarifPpnbm;

                                                                try
                                                                {
                                                                    InputVATInDetail.save(detailModel, model.Id);
                                                                    SyncLog.InsertLog(URL, fileName, "QR_INF", "Insert VATIn Detail Success");
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    SyncLog.InsertLog(URL, fileName, "QR_ERR", ex.Message);
                                                                }
                                                            }
                                                            try
                                                            {
                                                                Syncdata.InsertSync(URL, SyncID);
                                                                SyncLog.InsertLog(URL, fileName, "QR_INF", "Insert SyncData Success");
                                                                Syncdata.InsertSyncDetail(model.Id + "|" + model.InvoiceNumber + "|" + model.InvoiceNumberFull + "|" +
                                                                        model.KDJenisTransaksi + "|" + model.FGPengganti + "|" + model.InvoiceDate + "|" + model.ExpireDate + "||" +
                                                                        model.SupplierNPWP + "|" + model.SupplierName + "|" + model.SupplierAddress + "|" + model.NPWPLawanTransaksi +
                                                                        model.NamaLawanTransaksi + "|" + model.AlamatLawanTransaksi + "|" + model.StatusApprovalXML + "|" + model.StatusFakturXML +
                                                                        "|" + model.VATBaseAmount + "|" + model.VATAmount + "|" + model.JumlahPPnBM + "|", SyncID);
                                                                SyncLog.InsertLog(URL, fileName, "QR_INF", "Insert SyncDataDetail Success");

                                                                UpdateQRStatus(URL, "S");
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                SyncLog.InsertLog(URL, fileName, "QR_ERR", ex.Message);
                                                                UpdateQRStatus(URL, "F");
                                                            }
                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            SyncLog.InsertLog(URL, fileName, "QR_ERR", result);
                                                            UpdateQRStatus(URL, "F");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        SyncLog.InsertLog(URL, fileName, "QR_ERR", result);
                                                        UpdateQRStatus(URL, "F");
                                                    }
                                                }
                                                else
                                                {
                                                    SyncLog.InsertLog(URL, fileName, "QR_ERR", "Cannot save with status Faktur Dibatalkan");
                                                    UpdateQRStatus(URL, "F");
                                                }
                                            }
                                            else
                                            {
                                                SyncLog.InsertLog(URL, fileName, "QR_ERR", result);
                                                UpdateQRStatus(URL, "F");
                                            }
                                            #endregion
                                        }
                                        catch (Exception ex)
                                        {
                                            SyncLog.InsertLog(URL, fileName, "QR_ERR", ex.Message);
                                            UpdateQRStatus(URL, "F");
                                        }
                                    }
                                    #endregion
                                }
                                #endregion
                            }
                        }
                        catch (Exception ex)
                        {
                            SyncLog.InsertLog(URL, fileName, "QR_ERR", ex.Message);
                            UpdateQRStatus(URL, "F");
                        }
                    }
                }
                cn.CloseConnection();
                cn.DisposeConnection();
            }
            catch (Exception ex)
            {
                SyncLog.InsertLog(URL, fileName, "QR_ERR", ex.Message);
                UpdateQRStatus(URL, "F");
            }
        }

        public static void InsertQR(string DJPLink, string fileName, string QRPDFFiles, string fail, string process)
        {
            try
            {
                #region
                var response = new HttpResponseMessage();
                resValidateFakturPm VATInXML = new resValidateFakturPm();

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
                    Uri serviceUri = new Uri(DJPLink, UriKind.Absolute);

                    response = client.GetAsync(serviceUri).Result;

                    var stream = new MemoryStream(response.Content.ReadAsByteArrayAsync().Result);

                    var serializer = new XmlSerializer(typeof(resValidateFakturPm));
                    using (var reader = XmlReader.Create(stream))
                    {
                        VATInXML = (resValidateFakturPm)serializer.Deserialize(reader);

                        if (string.IsNullOrEmpty(VATInXML.nomorFaktur))
                        {
                            if (VATInXML.statusApproval == "Faktur tidak Valid, Tidak ditemukan data di DJP")
                            {
                                SyncLog.InsertLog(DJPLink, fileName, "QR_ERR", "Faktur tidak valid pada server DJP");
                            }
                            else if (VATInXML.statusApproval == "Terjadi kesalahan sistem")
                            {
                                SyncLog.InsertLog(DJPLink, fileName, "QR_ERR", "Server DJP tidak dapat terkoneksi");
                            }
                        }
                        else
                        {
                            #region save
                            string InvoiceDate = VATInXML.tanggalFakturDateFormat.ToString("yyyy-MM-dd");
                            DateTime expdate = Convert.ToDateTime(InvoiceDate).AddDays(1).AddMonths(3).AddDays(-1);
                            DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);

                            VATInManualInputViewModel model = new VATInManualInputViewModel();

                            model.Id = Guid.NewGuid();
                            model.InvoiceNumber = VATInXML.nomorFaktur;
                            model.InvoiceNumberFull = VATInXML.nomorFaktur.FormatNomorFakturGabungan(VATInXML.kdJenisTransaksi, VATInXML.fgPengganti);
                            model.KDJenisTransaksi = VATInXML.kdJenisTransaksi;
                            model.FGPengganti = VATInXML.fgPengganti;
                            model.InvoiceDate = VATInXML.tanggalFakturDateFormat.FormatSQLDate();
                            model.ExpireDate = endOfMonth.ToString("yyyy-MM-dd");
                            model.SupplierNPWP = VATInXML.npwpPenjual.FormatNPWP();
                            model.SupplierName = VATInXML.namaPenjual;
                            model.SupplierAddress = VATInXML.alamatPenjual;
                            model.NPWPLawanTransaksi = VATInXML.npwpLawanTransaksi.FormatNPWP();
                            model.NamaLawanTransaksi = VATInXML.namaLawanTransaksi;
                            model.AlamatLawanTransaksi = VATInXML.alamatLawanTransaksi;
                            model.StatusApprovalXML = VATInXML.statusApproval;
                            model.StatusFakturXML = VATInXML.statusFaktur;
                            model.VATBaseAmount = VATInXML.jumlahDpp;
                            model.VATAmount = VATInXML.jumlahPpn;
                            model.JumlahPPnBM = VATInXML.jumlahPpnBm;

                            model.URL = DJPLink;
                            model.FileName = fileName;

                            model.VATInDetails = new List<VATInDetailManualInputViewModel>();
                            SyncID = Guid.NewGuid();
                            try
                            {
                                InputQR.save(model, SyncID);
                                SyncLog.InsertLog(DJPLink, fileName, "QR_INF", "Insert QR Success");

                                foreach (resValidateFakturPmDetailTransaksi detail in VATInXML.detailTransaksi)
                                {
                                    VATInDetailManualInputViewModel detailModel = new VATInDetailManualInputViewModel();
                                    detailModel.UnitName = detail.nama;
                                    detailModel.UnitPrice = detail.hargaSatuan;
                                    detailModel.Quantity = detail.jumlahBarang;
                                    detailModel.TotalPrice = detail.hargaTotal;
                                    detailModel.Discount = detail.diskon;
                                    detailModel.DPP = detail.dpp;
                                    detailModel.PPN = detail.ppn;
                                    detailModel.PPNBM = detail.ppnbm;
                                    detailModel.TarifPPNBM = detail.tarifPpnbm;
                                    try
                                    {
                                        InputQRDetail.save(detailModel, model.Id);
                                        SyncLog.InsertLog(DJPLink, fileName, "QR_INF", "Insert QR Detail Success");
                                    }
                                    catch (Exception ex)
                                    {
                                        SyncLog.InsertLog(DJPLink, fileName, "QR_ERR", ex.Message);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                SyncLog.InsertLog(DJPLink, fileName, "QR_ERR", ex.Message);
                                UpdateQRStatus(DJPLink, "F");

                                if (File.Exists(fail))
                                {
                                    File.Delete(fail);
                                }
                                File.Move(QRPDFFiles, fail);
                            }
                            finally
                            {
                                SyncLog.InsertLog(DJPLink, fileName, "QR_INF", "Success to read file, file moved to folder QR_Process");
                                if (File.Exists(process))
                                {
                                    File.Delete(process);
                                }
                                File.Move(QRPDFFiles, process);
                            }
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                SyncLog.InsertLog(DJPLink, fileName, "QR_ERR", ex.Message);
                //review 2019-02-11
                //ketika koneksi mati dapet error "One or more errors occurred." file nya gak usah di move ke fail, biar ketika konek bisa langsung dibaca lagi
                if (ex.Message != "One or more errors occurred.")
                {
                    if (File.Exists(fail))
                    {
                        File.Delete(fail);
                    }
                    File.Move(QRPDFFiles, fail);
                }
            }
        }

        public static void ValidateQR()
        {
            PathQR path = new PathQR();
            path = GetQRPath();
            //var dataTable = new DataTable();
            string pathSuccess = path.QRDestinationPath;
            string pathFail = path.QRFailPath;
            string pathProcess = path.QRProcessPath;
            string URL = "";
            string fileName = "";
            string fileNamePross = "";
            string datetime = DateTime.Now.ToString("yyyy-MM-ddTHH':'mm':'sszzz").Replace(":", "_").Replace("+", "_");
            DirectoryInfo info = new DirectoryInfo(pathProcess);
            FileInfo[] files = info.GetFiles().OrderBy(p => p.Name).ToArray();

            foreach (FileInfo file in files)
            {
                try
                {
                    fileNamePross = file.Name;
                    Connection cn = new Connection();
                    cn.CreateConnection();
                    cn.OpenConnection();

                    cn._sqlCommand.CommandText = "usp_GetQRUrlByFileName";
                    cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    cn._sqlCommand.Parameters.AddWithValue("@filename", fileNamePross);
                    SqlDataReader rd = cn._sqlCommand.ExecuteReader();
                   
                   // dataTable.Load(rd);
                    if (rd.HasRows)
                    {
                    
                    while (rd.Read())
                        {
                            VATInManualInputViewModel model = new VATInManualInputViewModel();
                            model.Id = Guid.NewGuid();
                            model.InvoiceNumber = rd[0].ToString();
                            model.InvoiceNumberFull = rd[1].ToString();
                            model.KDJenisTransaksi = rd[2].ToString();
                            model.FGPengganti = rd[3].ToString();
                            model.InvoiceDate = rd[4].ToString();
                            model.ExpireDate = rd[5].ToString();
                            model.SupplierNPWP = rd[6].ToString();
                            model.SupplierName = rd[7].ToString();
                            model.SupplierAddress = rd[8].ToString();
                            model.NPWPLawanTransaksi = rd[9].ToString();
                            model.StatusApprovalXML = rd[10].ToString();
                            model.StatusFakturXML = rd[11].ToString();
                            model.VATBaseAmount = Convert.ToDecimal(rd[12].ToString());
                            model.VATAmount = Convert.ToDecimal(rd[13].ToString());
                            model.JumlahPPnBM = Convert.ToDecimal(rd[14].ToString());
                            model.FileName = rd[16].ToString();
                            SyncID = Guid.NewGuid();

                            //tambahan di header yang disave ke table QR, tidak disave lagi ke vatin
                            URL = rd[15].ToString();
                            fileName = rd[16].ToString();
                            string QRUrlID = rd[17].ToString();

                            string QRFile = Path.GetFullPath(pathProcess + fileName);
                            string fail = Path.Combine(pathFail + fileName);
                            string failMove = Path.Combine(Path.ChangeExtension(pathFail + fileName, null) + "_" + datetime + ".pdf");
                            string success = Path.Combine(pathSuccess + fileName);
                            string successMove = Path.Combine(Path.ChangeExtension(pathFail + fileName, null) + "_" + datetime + ".pdf");

                            model.VATInDetails = new List<VATInDetailManualInputViewModel>();
                            try
                            {
                                #region Validasi
                                string result = ValidateVATInQR.ValidateNPWP(model.NPWPLawanTransaksi);
                                if (result == "")
                                {
                                    if (model.StatusFakturXML != "Faktur Dibatalkan")
                                    {
                                        result = ValidateVATInQR.ValidateExpireDate(model.InvoiceDate);
                                        if (result == "")
                                        {
                                            result = ValidateVATInQR.ValidateVATIn(model.InvoiceNumberFull, model.InvoiceNumber, model.FGPengganti, "eFaktur", model.SupplierNPWP, model.InvoiceDate);
                                            if (result == "")
                                            {
                                                #region Success Validasi
                                                InputVATIn.saveVatinQR(model, SyncID);
                                                SyncLog.InsertLog(URL, fileName, "QR_INF", "Insert VATIn Success");

                                                Connection conn = new Connection();
                                                conn.CreateConnection();
                                                conn.OpenConnection();

                                                conn._sqlCommand.CommandText = "usp_GetQRUrlDetail";
                                                conn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                                conn._sqlCommand.Parameters.AddWithValue("QRUrlID", QRUrlID);

                                                SqlDataReader reader = conn._sqlCommand.ExecuteReader();
                                                while (reader.Read())
                                                {

                                                    VATInDetailManualInputViewModel detailModel = new VATInDetailManualInputViewModel();

                                                    detailModel.UnitName = reader[0].ToString();
                                                    detailModel.UnitPrice = Convert.ToDecimal(reader[1].ToString());
                                                    detailModel.Quantity = Convert.ToDecimal(reader[2].ToString());
                                                    detailModel.TotalPrice = Convert.ToDecimal(reader[3].ToString());
                                                    detailModel.Discount = Convert.ToDecimal(reader[4].ToString());
                                                    detailModel.DPP = Convert.ToDecimal(reader[5].ToString());
                                                    detailModel.PPN = Convert.ToDecimal(reader[6].ToString());
                                                    detailModel.PPNBM = Convert.ToDecimal(reader[7].ToString());
                                                    detailModel.TarifPPNBM = Convert.ToDecimal(reader[8].ToString());

                                                    try
                                                    {
                                                        InputVATInDetail.save(detailModel, model.Id);
                                                        SyncLog.InsertLog(URL, fileName, "QR_INF", "Insert VATIn Detail Success");
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        SyncLog.InsertLog(URL, fileName, "QR_ERR", ex.Message);
                                                    }
                                                }
                                                try
                                                {
                                                    Syncdata.InsertSync(URL, SyncID);
                                                    SyncLog.InsertLog(URL, fileName, "QR_INF", "Insert SyncData Success");
                                                    Syncdata.InsertSyncDetail(model.Id + "|" + model.InvoiceNumber + "|" + model.InvoiceNumberFull + "|" +
                                                            model.KDJenisTransaksi + "|" + model.FGPengganti + "|" + model.InvoiceDate + "|" + model.ExpireDate + "||" +
                                                            model.SupplierNPWP + "|" + model.SupplierName + "|" + model.SupplierAddress + "|" + model.NPWPLawanTransaksi +
                                                            model.NamaLawanTransaksi + "|" + model.AlamatLawanTransaksi + "|" + model.StatusApprovalXML + "|" + model.StatusFakturXML +
                                                            "|" + model.VATBaseAmount + "|" + model.VATAmount + "|" + model.JumlahPPnBM + "|", SyncID);
                                                    SyncLog.InsertLog(URL, fileName, "QR_INF", "Insert SyncDataDetail Success");

                                                    UpdateQRStatus(URL, "S");

                                                    if (File.Exists(success))
                                                    {
                                                        try
                                                        {
                                                            File.Move(success, successMove);
                                                        }
                                                        catch (Exception)
                                                        {
                                                        }
                                                    }

                                                    File.Move(QRFile, success);
                                                }
                                                catch (Exception ex)
                                                {
                                                    SyncLog.InsertLog(URL, fileName, "QR_ERR", ex.Message);
                                                    UpdateQRStatus(URL, "F");

                                                    if (File.Exists(fail))
                                                    {
                                                        try
                                                        {
                                                            File.Move(fail, failMove);
                                                        }
                                                        catch (Exception)
                                                        {
                                                        }
                                                    }

                                                    File.Move(QRFile, fail);
                                                }
                                                conn.CloseConnection();
                                                conn.DisposeConnection();
                                                #endregion
                                            }
                                            else
                                            {
                                                SyncLog.InsertLog(URL, fileName, "QR_ERR", result);
                                                UpdateQRStatus(URL, "F");
                                                if (File.Exists(fail))
                                                {
                                                    try
                                                    {
                                                        File.Move(fail, failMove);
                                                    }
                                                    catch (Exception)
                                                    {
                                                    }
                                                    
                                                }
                                                File.Move(QRFile, fail);
                                            }
                                        }
                                        else
                                        {
                                            SyncLog.InsertLog(URL, fileName, "QR_ERR", result);
                                            UpdateQRStatus(URL, "F");

                                            if (File.Exists(fail))
                                            {
                                                try
                                                {
                                                    File.Move(fail, failMove);
                                                }
                                                catch (Exception)
                                                {
                                                }
                                               
                                            }
                                            File.Move(QRFile, fail);
                                        }
                                    }
                                    else
                                    {
                                        SyncLog.InsertLog(URL, fileName, "QR_ERR", "Cannot save with status Faktur Dibatalkan");
                                        UpdateQRStatus(URL, "F");

                                        if (File.Exists(fail))
                                        {
                                            try
                                            {
                                                File.Move(fail, failMove);
                                            }
                                            catch (Exception)
                                            {
                                            }
                                        }
                                        File.Move(QRFile, fail);
                                    }
                                }
                                else
                                {
                                    SyncLog.InsertLog(URL, fileName, "QR_ERR", result);
                                    UpdateQRStatus(URL, "F");

                                    if (File.Exists(fail))
                                    {
                                        try
                                        {
                                            File.Move(fail, failMove);
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                    File.Move(QRFile, fail);
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                SyncLog.InsertLog(URL, fileName, "QR_ERR", ex.Message);
                                UpdateQRStatus(URL, "F");

                                if (File.Exists(fail))
                                {
                                    try
                                    {
                                        File.Move(fail, failMove);
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }
                                File.Move(QRFile, fail);
                            }
                        }
                    cn.CloseConnection();
                    cn.DisposeConnection();
                    }
                    else
                    {
                        SyncLog.InsertLog("", fileNamePross, "QR_ERR", "Data Sudah di proses sebelumya atau data belum terverifikasi");
                        if (File.Exists(pathFail + fileNamePross))
                        {
                            try
                            {
                                File.Move(Path.GetFullPath(pathFail + fileNamePross), Path.ChangeExtension(pathFail + fileNamePross, null) + "_" + datetime + ".pdf");
                            }
                            catch (Exception)
                            {
                            }

                        }
                        try
                        {


                            File.Move(Path.GetFullPath(pathProcess + fileNamePross), pathFail + fileNamePross);
                        }
                        catch (Exception)
                        {


                        }
                    }



                }
                catch (Exception ex)
                {
                    SyncLog.InsertLog("", "", "QR_ERR", ex.Message);
                    //UpdateQRStatus(URL, "F");
                    //if (File.Exists(pathFail + fileName))
                    //{
                    //    try
                    //    {
                    //        File.Move(Path.GetFullPath(pathFail + fileName), Path.ChangeExtension(pathFail + fileName, null) + "_" + datetime + ".pdf");
                    //    }
                    //    catch (Exception)
                    //    {


                    //    }
                    //}
                    //try
                    //{


                    //    File.Move(Path.GetFullPath(pathProcess + fileName), pathFail + fileName);
                    //}
                    //catch (Exception)
                    //{


                    //}
                }
            }
        }

        public static void RenameQR(string fileName, string DJPLink, string renameInputQR, string renameOutputQR, string renameFailQR)
        {
            try
            {
                #region
                var response = new HttpResponseMessage();
                resValidateFakturPm VATInXML = new resValidateFakturPm();

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
                    Uri serviceUri = new Uri(DJPLink, UriKind.Absolute);

                    response = client.GetAsync(serviceUri).Result;

                    var stream = new MemoryStream(response.Content.ReadAsByteArrayAsync().Result);

                    var serializer = new XmlSerializer(typeof(resValidateFakturPm));
                    using (var reader = XmlReader.Create(stream))
                    {
                        VATInXML = (resValidateFakturPm)serializer.Deserialize(reader);

                        if (string.IsNullOrEmpty(VATInXML.nomorFaktur))
                        {
                            if (VATInXML.statusApproval == "Faktur tidak Valid, Tidak ditemukan data di DJP")
                            {
                                SyncLog.InsertLog(DJPLink, fileName, "RENAME_ERR", "Faktur tidak valid pada server DJP");
                            }
                            else if (VATInXML.statusApproval == "Terjadi kesalahan sistem")
                            {
                                SyncLog.InsertLog(DJPLink, fileName, "RENAME_ERR", "Server DJP tidak dapat terkoneksi");
                            }
                        }
                        else
                        {
                            #region Rename
                            string referensi = VATInXML.referensi;
                            if (referensi.Length > 4)
                            {
                                referensi = referensi.Substring(0, 4);
                            }

                            string newFileName = CleanFileName(
                                referensi + "_" +
                                VATInXML.namaLawanTransaksi + "_" +
                                VATInXML.tanggalFaktur + "_" +
                                VATInXML.referensi + "_" +
                                VATInXML.nomorFaktur.FormatNomorFakturGabungan(VATInXML.kdJenisTransaksi, VATInXML.fgPengganti) + ".pdf");

                            if (File.Exists(renameOutputQR + newFileName))
                            {
                                File.Delete(renameOutputQR + newFileName);
                            }
                            File.Move(renameInputQR + fileName, renameOutputQR + newFileName);
                            SyncLog.InsertLog(DJPLink, newFileName, "RENAME_INF", "Success to rename file, file moved to folder QR_Output");
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                SyncLog.InsertLog(DJPLink, fileName, "RENAME_ERR", ex.Message);

                if (ex.Message != "One or more errors occurred.")
                {
                    if (File.Exists(renameFailQR + fileName))
                    {
                        File.Delete(renameFailQR + fileName);
                    }
                    File.Move(renameInputQR + fileName, renameFailQR + fileName);
                }
            }
        }

        private static string CleanFileName(string fileName)
        {
            return Path.GetInvalidFileNameChars().Aggregate(fileName, (current, c) => current.Replace(c.ToString(), string.Empty));
        }
    }
}
