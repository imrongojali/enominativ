﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAM.Scheduler.QR
{
    public class PathQR
    {
        public string QRSourcePath { get; set; }
        public string QRDestinationPath { get; set; }
        public string QRFailPath { get; set; }
        public string QRProcessPath { get; set; }
    }
}
