﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAM.Scheduler
{
    class Connection
    {
        public static string connetionString = ConfigurationManager.ConnectionStrings["TAM_EFAKTURConnectionString"].ConnectionString;// "Data Source=BII-NB;Initial Catalog=TAM_EFAKTUR;User ID=sa;Password=sa";
        public static string connetionString2 = ConfigurationManager.ConnectionStrings["ELVIS_DBConnectionString"].ConnectionString;//"Data Source=.\\MSSQLSERVER2014;Initial Catalog=ELVIS_DB;User ID=sa;Password=sa";
        public SqlCommand _sqlCommand;

        public void CreateConnection()
        {
            SqlConnection _sqlConnection = new SqlConnection(connetionString);
            _sqlCommand = new SqlCommand();
            _sqlCommand.Connection = _sqlConnection;
        }
        public void CreateConnection2()
        {
            SqlConnection _sqlConnection = new SqlConnection(connetionString2);
            _sqlCommand = new SqlCommand();
            _sqlCommand.Connection = _sqlConnection;
        }
        public void OpenConnection()
        {
            _sqlCommand.Connection.Open();
        }
        public void CloseConnection()
        {
            _sqlCommand.Connection.Close();
        }
        public void DisposeConnection()
        {
            _sqlCommand.Connection.Dispose();
        }
        public void RefreshParam()
        {
            _sqlCommand.Parameters.Clear();
        }
    }
}
