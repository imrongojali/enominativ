﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Bytescout.PDFExtractor;
using Bytescout.BarCodeReader;
using System.Data.SqlClient;
using TAM.Scheduler;
using System.Configuration;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Net.Mail;
using System.Globalization;
using TAM.EFaktur.Web.Models.VATIn;
using TAM.EFaktur.Web.Models;
using TAM.Scheduler.OCR;
using TAM.Scheduler.QR;
using System.Net;
using System.Drawing.Imaging;
using System.Threading;
using System.Security.Cryptography;
using System.Drawing;
using TAM.Scheduler.EprocInvoice;
using TAM.Scheduler.MatchingData;
using TAM.EFaktur.Web.Models.VATOutSalesReturn;
using System.Diagnostics;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;


namespace TAM.Scheduler
{
    class Program
    {
        public static string SourcePath { get; set; }
        public static string DestinationPath { get; set; }
        public static string DestinationPathFail { get; set; }
        public static string DestinationBatch { get; set; }
        public static string Data { get; set; }
        public static Guid SyncID { get; set; }
        public static string QRPDFFiles { get; set; }
        public static string DJPLink { get; set; }
        public static string RenameInputQR { get; set; }
        public static string RenameOutputQR { get; set; }
        public static string RenameFailQR { get; set; }

        public static string eBupotSource { get; set; }
        public static string eBupotProcess { get; set; }
        public static string eBupotSuccess { get; set; }
        public static string eBupotFail { get; set; }
        public static string eBupotDFFiles { get; set; }
        public static string eBupotDFFilesProcess { get; set; }

        public static string PrepaidWHTSuccess { get; set; }
        public static string PrepaidWHTProcess { get; set; }
        public static string PrepaidWHTFail { get; set; }
        public static string PrepaidWHTFilesProcess { get; set; }

        public static Regex targetDataRegex = new Regex(@"\d{2}[-—]\d{6}[-—]\d{8}");

        private static string SourcePathGLOri;
        private static string PdfPath;
        private static string urlPdf;

        #region Parameters
        static log4net.ILog log;
        #endregion Parameters
        static void Main(string[] args)
        {

            StartLogging();
            string JobType = args[0];
            //string JobType = "GeneratePdfOri";
            if (JobType == "GETXML")
            {
                GetXML();
            }
            else if (JobType == "ScanOCR")
            {
                #region ScanOCR
                GetPath();

                //urutan 23,21,15,4(2)
                //jalan ke 23, terus cek nomor ke wht
                //kalo gak ada di wht, jalan ke method selanjutnya 21 -> 15

                CheckFilePDF23();

                BatchPDF();
                #endregion
            }
            else if (JobType == "ScanQR")
            {
                ScanQR();
            }
            else if (JobType == "NewScanQR")
            {
                NewScanQR();
            }
            else if (JobType == "ValidateScanQR")
            {
                ValidateScanQR();
            }
            else if (JobType == "RenameQR")
            {
                RenameQR();
            }
            else if (JobType == "Email")
            {
                #region
                List<DataRemainder> DataRemainder = new List<DataRemainder>(GetDataRemainder());
                if (DataRemainder.Count > 0)
                {
                    Subject = "[TAM ELVIS REMINDER] Reminder of to be expired PV tax invoice as " + DateTime.Now.ToString("dd/MM/yyyy hh:mm");
                    Body = CreateMailBody(DataRemainder);
                    IsHtml = true;
                    Send(DataRemainder);
                }
                #endregion
            }
            else if (JobType == "GenerateTxtTransitory")
            {
                #region GenerateTxtTransitory
                //get data transitory gl from sap
                string SourcePathGL = GetConfigValueByConfigKey("DestinationTXTTransitoryGL");
                string SourcePathGLSuccess = GetConfigValueByConfigKey("DestinationTXTTransitoryGLSuccess");
                string SourcePathGLFail = GetConfigValueByConfigKey("DestinationTXTTransitoryGLFail");
                string ftp = "ftp://" + ConfigurationManager.AppSettings["FTPPath"].ToString() + "/";
                string ftpFolder = ConfigurationManager.AppSettings["FTPFolder"].ToString();
                string ftpUser = ConfigurationManager.AppSettings["FTPUser"].ToString();
                string ftpPassword = ConfigurationManager.AppSettings["FTPPassword"].ToString();

                foreach (string fileName in Directory.GetFiles(SourcePathGL))
                {
                    string FileName = Path.GetFileName(fileName);
                    string NewFileName = Path.GetFileNameWithoutExtension(fileName) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
                    try
                    {
                        BulkLoadTransitoryGLData(fileName);
                        File.Move(SourcePathGL + FileName, SourcePathGLSuccess + NewFileName);
                        SyncLog.InsertLog("0", FileName, "INF", "Success load data Transitory GL");
                    }
                    catch (Exception ex)
                    {
                        File.Move(SourcePathGL + FileName, SourcePathGLFail + NewFileName);
                        SyncLog.InsertLog("0", FileName, "ERR", ex.Message);
                    }
                }

                if (CheckDataClearing())
                {
                    try
                    {
                        List<string[]> ListIdHeader = new List<string[]>(InsertTransitoryHeader());
                        foreach (var list in ListIdHeader)
                        {
                            InsertTransitoryDetail(list);

                            List<string[]> ListDtHeader = new List<string[]>(GetTransitoryHeader(list));
                            List<string[]> ListDtDetail = new List<string[]>(GetTransitoryDetail(list));
                            string Path = GetConfigValueByConfigKey("DestinationTXTTransitory");

                            if (ListDtDetail.Count > 0)
                            {
                                string FileName = PutTXT(ListDtHeader, ListDtDetail, Path, "EFB_TRANSITORY", list);
                                UpdateFileNameTransitoryHeader(FileName, list);
                                UploadFileToFTP(ftp + ftpFolder, Path + @"\" + FileName, ftpUser, ftpPassword);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        SyncLog.InsertLog("0", "GenerateTxtTransitory", "ERR", ex.Message);
                    }
                }
                #endregion
            }
            else if (JobType == "CheckTxtTransitory")
            {
                #region CheckTxtTransitory
                var UnproccessFileName = GetTransitoryUnproccessFileName();
                if (UnproccessFileName.Count > 0)
                {
                    List<string> FileName = new List<string>();
                    string SourcePath = GetConfigValueByConfigKey("DestinationTXTTransitoryResult");
                    string SourcePathSuccess = GetConfigValueByConfigKey("DestinationTXTTransitoryResultSuccess");
                    string SourcePathFail = GetConfigValueByConfigKey("DestinationTXTTransitoryResultFail");
                    foreach (string filePath in Directory.GetFiles(SourcePath))
                    {
                        FileName.Add(Path.GetFileName(filePath).Substring(0, 36));
                    }
                    if (FileName.Count > 0)
                    {
                        foreach (var item in UnproccessFileName)
                        {
                            string FindName = item.Substring(0, item.Length - 4);
                            //DownloadFileFTP(SourcePath, FindName); ga jadi, ftp langsung taruh file ke lokal EFB SourcePath
                            if (FileName.Contains(FindName))
                            {
                                string[] fileNames = Directory.GetFiles(SourcePath, FindName + "*.TXT", SearchOption.TopDirectoryOnly);
                                if (fileNames.Length != 0)
                                {
                                    FindName = Path.GetFileName(fileNames[0].ToString());
                                }

                                string text = File.ReadAllText(@SourcePath + FindName);
                                var splitText = text.Split('|');
                                string transitoryNO = splitText[63];
                                string errorMsg = splitText[68];

                                if (transitoryNO != "" && transitoryNO != null)
                                {
                                    UpdateTransitoryResult(item, transitoryNO);
                                    File.Move(SourcePath + FindName, SourcePathSuccess + FindName);
                                }
                                else
                                {
                                    UpdateTransitoryResult(item, "");
                                    File.Move(SourcePath + FindName, SourcePathFail + FindName);
                                    SyncLog.InsertLog("0", FindName, "ERR", errorMsg);
                                }

                            }
                        }
                    }
                }
                #endregion
            }
            else if (JobType == "SyncDataElvisVatin")
            {


                SyncDataElvisVatin();

            }
            else if (JobType == "SyncDataElvisWht")
            {
                #region SyncDataElvisWht
                SyncDataElvisWht();

                //get data pph22
                string SourcePathData = GetConfigValueByConfigKey("DestinationTXTPPh22Data");
                string SourcePathSuccess = GetConfigValueByConfigKey("DestinationTXTPPh22Success");
                string SourcePathFail = GetConfigValueByConfigKey("DestinationTXTPPh22Fail");

                foreach (string fileName in Directory.GetFiles(SourcePathData))
                {
                    string FileName = Path.GetFileName(fileName);
                    string NewFileName = Path.GetFileNameWithoutExtension(fileName) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
                    try
                    {
                        BulkLoadPPh22(fileName);
                        File.Move(SourcePathData + FileName, SourcePathSuccess + NewFileName);
                        SyncLog.InsertLog("0", FileName, "INF", "Success load PPh 22 from SAP");
                    }
                    catch (Exception ex)
                    {
                        File.Move(SourcePathData + FileName, SourcePathFail + NewFileName);
                        SyncLog.InsertLog("0", FileName, "ERR", "Failed load PPh 22 from SAP: " + ex.Message);
                    }
                }
                #endregion
            }
            else if (JobType == "ClearingDocument")
            {
                #region ClearingDocument
                //get data Clearing
                string SourcePathData = GetConfigValueByConfigKey("DestinationTXTClearingData");
                string SourcePathSuccess = GetConfigValueByConfigKey("DestinationTXTClearingSuccess");
                string SourcePathFail = GetConfigValueByConfigKey("DestinationTXTClearingFail");

                foreach (string fileName in Directory.GetFiles(SourcePathData))
                {
                    string FileName = Path.GetFileName(fileName);
                    string NewFileName = Path.GetFileNameWithoutExtension(fileName) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
                    try
                    {
                        BulkLoadClearingDocument(fileName);
                        File.Move(SourcePathData + FileName, SourcePathSuccess + NewFileName);
                        SyncLog.InsertLog("0", FileName, "INF", "Success load Clearing Document");
                    }
                    catch (Exception ex)
                    {
                        File.Move(SourcePathData + FileName, SourcePathFail + NewFileName);
                        SyncLog.InsertLog("0", FileName, "ERR", "Failed load Clearing Document: " + ex.Message);
                    }
                }
                #endregion
            }
            else if (JobType == "EprocInvoiceVat")
            {
                EprocInvoiceVatin.EprocInvoiceDataVatin();
            }
            else if (JobType == "EprocInvoiceWht")
            {
                EprocInvoiceWHT.EprocInvoiceDataWHT();
            }
            else if (JobType == "MatchingDataWHT")
            {
                eBupotArchiving();
                eBupotArchivingProcess();
                // EprocInvoiceWHT.EprocInvoiceDataWHT();

            }
            else if (JobType == "PrepaidWHT")
            {
                PrepaidWHTTaxType("PREPAIDWHT22");
                PrepaidWHTTaxType("PREPAIDWHT23");
                PrepaidWHTTaxType("PREPAIDWHT42");
            }

            //Add by hendry untuk CheckLog jika ada yg error notif
            else if (JobType == "ErrorLogSync")
            {
                CekErrorLogAndUpdateToTableNotif();
            }
            else if (JobType == "NotifSync")
            {
                CekNotifApps();
            }

            //Add by Hendry untuk Kirim Email Bulk (untuk notif juga)
            else if (JobType == "SendEmailNotif")
            {
                CekEmailAndSendMail();
            }

            //add ridwan//
            else if (JobType == "GeneratePdfOri")
            {
                GetListPdfOri();
            }
        }
        //add ridwan//
        public static void GetListPdfOri()
        {
            SourcePathGLOri = GetConfigValueByConfigKey("UrlTemplateGenerateNotaVATOutSalesRetur");
            PdfPath = GetConfigValueByConfigKey("UrlTempVATOutSalesReturPdf");
            urlPdf = GetConfigValueByConfigKey("UrlTemplateGenerateNotaVATOutSalesRetur");

            List<string> id = new List<string>();

            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_Get_Id_Ori";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.CommandTimeout = 0;
            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            while (rd.Read())
            {
                id.Add(rd[0].ToString());
            }
            cn.CloseConnection();
            cn.DisposeConnection();

            int j = 0;
            foreach (var i in id)
            {
                getdatasp(new Guid(i.ToString()));
                j++;
            }
        }

        public static string getdatasp(Guid getid)
        {
            string Value = "";

            try
            {
                List<string> ListId = new List<string>();

                Connection cn = new Connection();
                cn.CreateConnection();
                cn.OpenConnection();

                cn._sqlCommand.CommandText = "Usp_Get_list_PDfOri";
                cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                //cn._sqlCommand.Parameters.AddWithValue("@Id", "A59A0804-80CE-4782-919F-7DF97C2C8085");
                cn._sqlCommand.Parameters.AddWithValue("@id", getid);
                cn._sqlCommand.CommandTimeout = 0;
                SqlDataReader rd = cn._sqlCommand.ExecuteReader();
                while (rd.Read())
                {
                    ListId.Add(rd[0].ToString());
                    ListId.Add(rd[1].ToString());
                    ListId.Add(rd[2].ToString());
                    ListId.Add(rd[3].ToString());
                    ListId.Add(rd[4].ToString());
                    ListId.Add(rd[5].ToString());
                    ListId.Add(rd[6].ToString());
                    ListId.Add(rd[7].ToString());
                    ListId.Add(rd[8].ToString());
                    ListId.Add(rd[9].ToString());
                    ListId.Add(rd[10].ToString());
                    ListId.Add(rd[11].ToString());
                    ListId.Add(rd[12].ToString());
                    ListId.Add(rd[13].ToString());
                    ListId.Add(rd[14].ToString());
                    ListId.Add(rd[15].ToString());
                    ListId.Add(rd[16].ToString());
                    ListId.Add(rd[17].ToString());
                    ListId.Add(rd[18].ToString());
                    ListId.Add(rd[19].ToString());
                }
                cn.CloseConnection();
                cn.DisposeConnection();

                var NoFaktur = ListId[1];
                var charsToRemove = new string[] { "/" };
                foreach (var e in charsToRemove)
                {
                    NoFaktur = NoFaktur.Replace(e, string.Empty);
                }

                string fullPath = string.Empty;
                var fileName = NoFaktur;
                var Pdffilename = fileName;
                //cek Dok PDF ori//
                string fileConf = SourcePathGLOri + Pdffilename + ".pdf";

                if (!System.IO.File.Exists(fileConf) && ListId[16].StartsWith("Lexus") && ListId[15] == "Not Exist")
                {
                   
                    var fullPathpdf = PdfPath;
                    PdfDocument pdfDocument = PdfReader.Open(fullPathpdf, PdfDocumentOpenMode.Modify);
                    PdfPage page = pdfDocument.Pages[0];
                    XGraphics gfx = XGraphics.FromPdfPage(page);
                    XFont font = new XFont("Arial", 7);
                    XFont fontNo = new XFont("Arial", 8);
                    XFont fontHeader = new XFont("Arial", 9);
                    XFont fontbody = new XFont("Arial", 10);
                    decimal hargatotal = Convert.ToDecimal(ListId[13]);
                    decimal PPn = Convert.ToDecimal(ListId[7]);
                    decimal PPnb = Convert.ToDecimal(ListId[8]);
                    decimal hargasatuan = Convert.ToDecimal(ListId[14]);


                    string Tanggalfaktur = Convert.ToDateTime(ListId[10]).ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
                    string TanggalRetur = Convert.ToDateTime(ListId[2]).ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
                    string Tglreturgab = "," + " " + TanggalRetur;

                    //header
                    gfx.DrawString(ListId[1], fontHeader, XBrushes.Black, new XRect(260, 33, page.Width, page.Height), XStringFormats.TopLeft);
                    gfx.DrawString(ListId[9], fontHeader, XBrushes.Black, new XRect(259, -342.5, page.Width, page.Height), XStringFormats.CenterLeft);
                    gfx.DrawString(Tanggalfaktur, fontHeader, XBrushes.Black, new XRect(392, 48.5, page.Width, page.Height), XStringFormats.TopLeft);

                    //Pembeli                                
                    gfx.DrawString(ListId[3], fontHeader, XBrushes.Black, new XRect(180, -308, page.Width, page.Height), XStringFormats.CenterLeft);
                    gfx.DrawString(rd == null ||
                        string.IsNullOrEmpty(ListId[18]) ? "" : (ListId[18])
                        , fontHeader, XBrushes.Black, new XRect(179, -292, page.Width, page.Height), XStringFormats.CenterLeft);
                    gfx.DrawString(rd == null ||
                        string.IsNullOrEmpty(ListId[19]) ? "" : (ListId[19])
                        , fontHeader, XBrushes.Black, new XRect(179, -282, page.Width, page.Height), XStringFormats.CenterLeft);

                    gfx.DrawString(ListId[4], fontHeader, XBrushes.Black, new XRect(180, -264, page.Width, page.Height), XStringFormats.CenterLeft);

                    // penjual
                    gfx.DrawString("PT.TOYOTA-ASTRA MOTOR", fontHeader, XBrushes.Black, new XRect(180, -228, page.Width, page.Height), XStringFormats.CenterLeft);
                    gfx.DrawString("JALAN JENDERAL SUDIRMAN NO. 5 KARET TENGSIN/TANAH ABANG  JAKARTA PUSAT", fontHeader, XBrushes.Black, new XRect(179, -213, page.Width, page.Height), XStringFormats.CenterLeft);
                    gfx.DrawString("DKI JAKARTA RAYA", fontHeader, XBrushes.Black, new XRect(179, -203, page.Width, page.Height), XStringFormats.CenterLeft);
                    gfx.DrawString("02.116.115.3-092.000", fontHeader, XBrushes.Black, new XRect(180, -179, page.Width, page.Height), XStringFormats.CenterLeft);

                    //detail//

                    gfx.DrawString("1", fontbody, XBrushes.Black, new XRect(18, -124, page.Width, page.Height), XStringFormats.CenterLeft);
                    gfx.DrawString(ListId[17], fontbody, XBrushes.Black, new XRect(100, -124 , page.Width, page.Height), XStringFormats.CenterLeft);
                    ////gfx.DrawString(vATOutResult.KodeObjek, fontbody, XBrushes.Black, new XRect(45, -110 + (_i * 25), page.Width, page.Height), XStringFormats.CenterLeft);
                    //gfx.DrawString(ListId[20] == null ||
                    // string.IsNullOrEmpty(ListId[20]) ? "" : ListId[20],
                    // fontHeader, XBrushes.Black, new XRect(21, -125, page.Width, page.Height), XStringFormats.CenterLeft);
                    gfx.DrawString(ListId[12], fontbody, XBrushes.Black, new XRect(250, -124 , page.Width, page.Height), XStringFormats.CenterLeft);
                    gfx.DrawString(ListId[11], fontbody, XBrushes.Black, new XRect(100, -113 , page.Width, page.Height), XStringFormats.CenterLeft);
                    gfx.DrawString(hargasatuan.FormatViewCurrencyiNA(), fontbody, XBrushes.Black, new XRect(-190, 266 , page.Width, page.Height), XStringFormats.TopRight);
                    gfx.DrawString(hargatotal.FormatViewCurrencyiNA(), fontbody, XBrushes.Black, new XRect(-20, 266 , page.Width, page.Height), XStringFormats.TopRight);

                    //under//
                    gfx.DrawString(hargatotal.FormatViewCurrencyiNA(), fontbody, XBrushes.Black, new XRect(-20, 554.5, page.Width, page.Height), XStringFormats.TopRight);
                    gfx.DrawString(PPn.FormatViewCurrencyiNA(), fontbody, XBrushes.Black, new XRect(-20, 571.5, page.Width, page.Height), XStringFormats.TopRight);
                    gfx.DrawString(PPn.FormatViewCurrencyiNA(), fontbody, XBrushes.Black, new XRect(-20, 588.5, page.Width, page.Height), XStringFormats.TopRight);
                    gfx.DrawString(Tglreturgab, fontHeader, XBrushes.Black, new XRect(-20, 617, page.Width, page.Height), XStringFormats.TopRight);

                    var nomordocretur = Pdffilename;
                    pdfDocument.Save(urlPdf + nomordocretur + ".pdf");
                    cn.CreateConnection();
                    cn.OpenConnection();

                    cn._sqlCommand.CommandText = "Update_list_PDfOri";
                    cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    cn._sqlCommand.Parameters.AddWithValue("@id", getid);
                    cn._sqlCommand.CommandTimeout = 0;
                    cn._sqlCommand.ExecuteNonQuery();
                    cn.CloseConnection();
                }

                else if (System.IO.File.Exists(fileConf) && ListId[15] == "Not Exist" || ListId[15].ToString() == "")
                {
                    cn.CreateConnection();
                    cn.OpenConnection();
                    cn._sqlCommand.CommandText = "Update_list_PDfOri";
                    cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    cn._sqlCommand.Parameters.AddWithValue("@id", getid);
                    cn._sqlCommand.CommandTimeout = 0;
                    cn._sqlCommand.ExecuteNonQuery();
                    cn.CloseConnection();
                }
                else if (!System.IO.File.Exists(fileConf) && ListId[15].ToString() == "" || ListId[15] =="Exist")
                {
                    cn.CreateConnection();
                    cn.OpenConnection();
                    cn._sqlCommand.CommandText = "Update_list_PDfOri_NotExist";
                    cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    cn._sqlCommand.Parameters.AddWithValue("@id", getid);
                    cn._sqlCommand.CommandTimeout = 0;
                    cn._sqlCommand.ExecuteNonQuery();
                    cn.CloseConnection();
                }
            }
            catch (Exception)
            {
                
            }

            return Value;
        }

        public static void CekEmailAndSendMail()
        {

            string Id = "";
            string[] MailTos = new string[0];
            string EmailBody = "";


            string now;
            now = DateTime.Now.ToString("yyyy-MM-dd");

            //cek dulu
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_GetEMailNotificationsList";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.CommandTimeout = 0;
            cn._sqlCommand.ExecuteNonQuery();


            List<EmailModel> EmailList = new List<EmailModel>();
            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            while (rd.Read())
            {
                var message = new MailMessage();

                EmailModel emailItem = new EmailModel();
                MailTos = new string[0];
                //dataErrorLogTXT.Add(rd[0].ToString() + "|" + rd[1].ToString());
                Id = rd[0].ToString();
                MailTos = rd[2].ToString().Split(',');
                EmailBody = rd[5].ToString();
                message.Subject = rd[3].ToString();
                message.Body = EmailBody;
                message.IsBodyHtml = true;
                //message.From = rd[1].ToString();
                foreach (var mto in MailTos)
                {
                    message.To.Add(mto);

                }

                emailItem.Id = Id;
                emailItem.msg = message;

                Console.WriteLine(Id);
                EmailList.Add(emailItem);
            }

            cn.CloseConnection();
            cn.DisposeConnection();


            try
            {



                foreach (var item in EmailList)
                {
                    using (var smtp = new SmtpClient())
                    {
                        smtp.Send(item.msg);
                    }


                    Connection cnUpdate = new Connection();
                    cnUpdate.CreateConnection();
                    cnUpdate.OpenConnection();

                    cnUpdate._sqlCommand.CommandText = "usp_Update_tbl_email_notifications";
                    cnUpdate._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    cnUpdate._sqlCommand.Parameters.AddWithValue("@id", item.Id);
                    cnUpdate._sqlCommand.Parameters.AddWithValue("@isSend", 1);
                    cnUpdate._sqlCommand.Parameters.AddWithValue("@ModifiedOn", now);
                    cnUpdate._sqlCommand.Parameters.AddWithValue("@ModifiedBy", "System");
                    cnUpdate._sqlCommand.CommandTimeout = 0;
                    cnUpdate._sqlCommand.ExecuteNonQuery();

                    Console.WriteLine(item.Id);


                    SqlDataReader rdUpdate = cnUpdate._sqlCommand.ExecuteReader();
                    if (rdUpdate.Read())
                    {
                        //dataErrorLogTXT.Add(rd[0].ToString() + "|" + rd[1].ToString());
                    }

                    SyncLog.InsertLog(String.Join(", ", MailTos), "Send Email OK", "INFO : ", "OK");

                    cnUpdate.CloseConnection();
                    cnUpdate.DisposeConnection();
                }



            }
            catch (Exception ex)
            {
                //Console.WriteLine("Exception is: " + ex.Message);
                if (ex.InnerException != null)
                {
                    SyncLog.InsertLog(String.Join(",", MailTos), "Email: " + Subject, "ERR : ", ex.InnerException.ToString());
                }
                else
                {
                    SyncLog.InsertLog(String.Join(",", MailTos), "Email: " + Subject, "ERR : ", ex.Message);
                }
                Console.WriteLine("InnerException is: {0}", ex.InnerException);

            }
        }

        //private static List<string> dataErrorLogTXT = new List<string>();
        public static void CekErrorLogAndUpdateToTableNotif()
        {
            //string now;
            //now = DateTime.Now.ToString("yyyy-MM-dd");

            //cek dulu
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_GetErrorLogNotifForEmail";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            //cn._sqlCommand.Parameters.AddWithValue("@dateFrom", now);
            //cn._sqlCommand.Parameters.AddWithValue("@dateTo", NULL);
            cn._sqlCommand.CommandTimeout = 0;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            if (rd.Read())
            {
                //dataErrorLogTXT.Add(rd[0].ToString() + "|" + rd[1].ToString());
            }

            cn.CloseConnection();
            cn.DisposeConnection();

        }

        public static void CekNotifApps()
        {

            //cek dulu
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_GenerateEmailForAll";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.CommandTimeout = 0;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            if (rd.Read())
            {
                //dataErrorLogTXT.Add(rd[0].ToString() + "|" + rd[1].ToString());
            }

            cn.CloseConnection();
            cn.DisposeConnection();


        }

        #region SCAN OCR 23
        private static void CheckFilePDF23()
        {
            RectangleF extractionRectangle = new RectangleF(183.8f, 85.5f, 279.0f, 40.5f);

            Regex targetDataRegex = new Regex(@"\d{2}[-—]\d{6}[-—]\d{8}");

            const bool showProgress = false;

            string nomor = "";
            string dateFile = DateTime.Now.ToString("ddMMyyyyHHmm").Replace(":", "").Replace(" ", "");
            string FileName = "";
            string status = "";

            foreach (string file in Directory.GetFiles(SourcePath))
            {
                Connection cn = new Connection();
                try
                {
                    FileName = Path.GetFileName(file);

                    if (Path.GetExtension(file).ToLower() == ".pdf")
                    {
                        // Try OCRAnalyzer at 200 DPI 
                        int pdfRenderingResolution = 200;

                        string text = AnalyzeAndGetText(file, extractionRectangle, pdfRenderingResolution, showProgress);

                        if (text.Contains("?")) text = text.Replace("?", "2");

                        Match match = targetDataRegex.Match(text);

                        // If failed, re-try at 300 DPI 
                        if (!match.Success)
                        {
                            pdfRenderingResolution = 300;

                            text = AnalyzeAndGetText(file, extractionRectangle, pdfRenderingResolution, showProgress);

                            if (text.Contains("?")) text = text.Replace("?", "2");

                            match = targetDataRegex.Match(text);//pindah kesini
                        }

                        //match = targetDataRegex.Match(text);
                        nomor = match.Value;

                        if (match.Success) //match.Success
                        {
                            if (nomor.Substring(0, 1) != "2")
                            {
                                var stringBuilder = new StringBuilder(nomor);
                                stringBuilder.Remove(0, 1);
                                stringBuilder.Insert(0, "2");

                                nomor = stringBuilder.ToString();
                            }
                            if (nomor.Contains("—")) nomor = nomor.Replace("—", "-");

                            string statusNomor = OCRRepository.CekNomorBP(nomor);

                            if (statusNomor == "FAIL")
                            {
                                CheckFilePDF21(FileName);
                            }
                            else
                            {
                                #region sukses

                                if (File.Exists(SourcePath + FileName))//cek file asli
                                {
                                    string result = OCRRepository.ValidateNumber(nomor);

                                    if (result == "0" || nomor.Contains("DEMO")) //Demo di gagalin buat test
                                    {
                                        #region Input Data OCR
                                        OCRRepository.inputOCR(nomor, SourcePath, FileName);
                                        #endregion

                                        #region Cek Nomor Bukti Potong
                                        OCRRepository.CekBuktiPotong(nomor);
                                        #endregion

                                        #region Ambil Status Data
                                        //cn.RefreshParam();
                                        cn.CreateConnection();
                                        cn.OpenConnection();

                                        cn._sqlCommand.CommandText = "usp_GetPDFStatus";
                                        cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                        cn._sqlCommand.Parameters.AddWithValue("@Nomor", Convert.ToString(nomor));
                                        cn._sqlCommand.CommandTimeout = 0;
                                        cn._sqlCommand.ExecuteNonQuery();

                                        SqlDataReader rd = cn._sqlCommand.ExecuteReader();
                                        rd.Read();

                                        status = rd[0].ToString();

                                        cn.CloseConnection();
                                        cn.DisposeConnection();
                                        #endregion

                                        if (status != "Data Sesuai")
                                        {
                                            if (File.Exists(DestinationPathFail + nomor + ".pdf") == true)
                                            {
                                                SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR ERR", "Nomor Bukti Potong :" + " " + nomor + " " + "Sudah ada di Folder PDF_WHT_Failed, File tidak dapat di pindahkan");
                                                //replace file here 

                                            }
                                            else
                                            {
                                                File.Move(SourcePath + FileName, DestinationPathFail + nomor + "_" + dateFile + ".pdf");
                                                SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR ERR", "Nomor Bukti Potong :" + " " + nomor + " " + "Tidak di temukan di data Withholding, File di pindahkan ke folder PDF_WHT_Failed");
                                            }
                                        }
                                        else
                                        {
                                            #region Update Path
                                            cn.RefreshParam();
                                            cn.CreateConnection();
                                            cn.OpenConnection();

                                            cn._sqlCommand.CommandText = "usp_UpdatePDFPath";
                                            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                            cn._sqlCommand.Parameters.AddWithValue("@path", Convert.ToString(DestinationPath));
                                            cn._sqlCommand.Parameters.AddWithValue("@Nomor", Convert.ToString(nomor));
                                            cn._sqlCommand.CommandTimeout = 0;
                                            cn._sqlCommand.ExecuteNonQuery();

                                            cn.CloseConnection();
                                            cn.DisposeConnection();
                                            #endregion

                                            if (File.Exists(DestinationPath + nomor + ".pdf") == true)
                                            {
                                                File.Delete(DestinationPath + nomor + ".pdf");
                                                File.Move(SourcePath + FileName, DestinationPath + nomor + ".pdf");
                                                SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR INF", "Nomor Bukti Potong :" + " " + nomor + " " + "Data Sesuai, File sudah di pindahkan ke folder PDF_WHT_Success");
                                            }
                                            else
                                            {
                                                File.Move(SourcePath + FileName, DestinationPath + nomor + ".pdf");
                                                SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR INF", "Nomor Bukti Potong :" + " " + nomor + " " + "Data Sesuai, File sudah di pindahkan ke folder PDF_WHT_Success");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        File.Move(SourcePath + FileName, DestinationPathFail + dateFile + "_" + FileName);
                                        SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR ERR", "Nomor Bukti Potong :" + " " + nomor + " " + "Data sudah pernah di SCAN, File di pindahkan ke folder PDF_WHT_FAILED");
                                    }
                                }
                                else
                                {
                                    SyncLog.InsertLog(nomor, FileName, "WHT OCR ERR", "File PDF :" + " " + FileName + " " + "Tidak ada di Folder PDF_WHT_OCR");
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            if (nomor.Contains("—")) nomor = nomor.Replace("—", "-");

                            string statusNomor = OCRRepository.CekNomorBP(nomor);

                            if (statusNomor == "FAIL")
                            {
                                CheckFilePDF21(FileName);
                            }
                            //File.Move(SourcePath + FileName, DestinationPathFail + dateFile + "_" + FileName);
                            //SyncLog.InsertLog(nomor, FileName, "WHT OCR ERR", "File PDF :" + " " + FileName + " " + "Tidak Terbaca oleh System, File di pindahkan ke folder PDF_WHT_FAILED");
                        }
                    }
                    else if (Path.GetExtension(file) == ".txt" || Path.GetExtension(file) == ".db")
                    {
                        //
                    }
                    else
                    {
                        File.Move(SourcePath + FileName, DestinationPathFail + FileName);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("CheckFilePDF: " + ex.Message);
                    //extractor.Dispose();
                    File.Move(SourcePath + FileName, DestinationPathFail + dateFile + "_" + FileName);
                    SyncLog.InsertLog(FileName, FileName, "WHT OCR ERR", ex.Message + " Nomor : " + nomor + " File Name : " + FileName + ", File Dipindahkan ke Folder PDF_WHT_FAILED");
                }
            }
        }

        private static string AnalyzeAndGetText(string inputFile, RectangleF extractionRectangle, int pdfRenderingResolution, bool showProgress)
        {
            int pageIndex = 0;

            using (OCRAnalyzer ocrAnalyzer = new OCRAnalyzer("lalitya.pramudita@emeriocorp.com", "C892-0088-F68C-DC9E-D87D-F8EF-97F"))
            {
                if (showProgress)
                    ocrAnalyzer.ProgressChanged += (object sender, string message, double progress, ref bool cancel) =>
                    {
                        Console.WriteLine(message);
                    };

                ocrAnalyzer.LoadDocumentFromFile(inputFile);

                ocrAnalyzer.OCRResolution = pdfRenderingResolution;

                ocrAnalyzer.OCRLanguageDataFolder = ConfigurationManager.AppSettings["OCRLanguageDataFolder"].ToString();

                if (!extractionRectangle.IsEmpty)
                    ocrAnalyzer.SetExtractionArea(extractionRectangle);

                OCRAnalysisResults analysisResults = ocrAnalyzer.AnalyzeByOCRConfidence(pageIndex);

                // Apply analysis result to TextExtractor and perform final OCR
                using (TextExtractor extractor = new TextExtractor("support@bytescout.com", "3226-E7D3-92BE-B8A9-D4D"))
                {
                    extractor.LoadDocumentFromFile(inputFile);

                    extractor.OCRResolution = pdfRenderingResolution;
                    extractor.OCRMode = OCRMode.TextFromImagesAndVectorsOnly;
                    extractor.OCRLanguageDataFolder = ConfigurationManager.AppSettings["OCRLanguageDataFolder"].ToString();
                    extractor.OCRLanguage = "ind";

                    ocrAnalyzer.ApplyResults(analysisResults, extractor);

                    if (!extractionRectangle.IsEmpty)
                        extractor.SetExtractionArea(extractionRectangle);

                    return extractor.GetTextFromPage(pageIndex);
                }
            }
        }

        #endregion

        #region SCAN OCR 21 & 15
        public static void CheckFilePDF21(string FileName)
        {
            string dateFile = DateTime.Now.ToString("ddMMyyyyHHmm").Replace(":", "").Replace(" ", "");
            string status;
            string nomor = "";
            string statusNomor = "";

            Connection cn = new Connection();
            try
            {
                #region scan OCR
                string path = SourcePath + "output.txt";

                string str = byteScout21(0.5f, FileName); //str = Regex.Replace(str, @"[^\d-.]", "");
                if (str == "")
                {
                    str = byteScout21(0.4f, FileName);
                }

                if (str.Length > 27 && str.Length < 90)
                {
                    #region Untuk PPh 21
                    var lines = File.ReadAllLines(path); //Untuk PPh 21
                    string firstchar = lines[8]; //lines[12];
                    string lastchar = lines[14];
                    string thirdchar = lines[16];
                    string firstnumber = Regex.Replace(firstchar, @"[^\d]", "").Trim();
                    string lastnumber = Regex.Replace(lastchar, @"[^\d]", "");
                    string thirdnumber = Regex.Replace(thirdchar, @"[^\d]", "");

                    if (firstnumber.Length == 13)
                    {
                        nomor = firstnumber + lastnumber;
                        nomor = nomor.Insert(1, ".").Insert(3, "-").Insert(6, ".").Insert(9, "-");
                    }
                    else if (lastnumber.Length == 1)
                    {
                        string sublast = firstnumber.Substring(0, 1);
                        string midnumber = firstnumber.Substring(1, 2);
                        firstnumber = firstnumber.Substring(3, 7);

                        nomor = sublast + lastnumber + midnumber + thirdnumber + firstnumber;
                        nomor = nomor.Insert(1, ".").Insert(3, "-").Insert(6, ".").Insert(9, "-");
                    }
                    else if (firstnumber.Length == 1)
                    {
                        nomor = firstnumber;
                        nomor = nomor.Insert(1, ".").Insert(3, "-").Insert(6, ".").Insert(9, "-");
                    }
                    else
                    {
                        string sub = firstnumber.Substring(0, 1);

                        firstnumber = firstnumber.Substring(1, 7);

                        nomor = sub + lastnumber + firstnumber;

                        nomor = nomor.Insert(1, ".").Insert(3, "-").Insert(6, ".").Insert(9, "-");
                    }
                    #endregion
                }
                else
                {
                    if (str != "")
                    {
                        #region
                        if (str.Length > 30)
                        {
                            str = str.Substring(0, 28).Trim();
                        }
                        else
                        {
                            str = str.Substring(0, str.Length).Trim();
                        }
                        nomor = str.Substring(str.LastIndexOf(" ") + 1).Trim();

                        if (nomor.Contains("/") || nomor.Contains("I") || nomor.Contains("l"))
                        {
                            nomor = nomor.Replace("/", "-");
                            nomor = nomor.Replace("I", "-");
                            nomor = nomor.Replace("l", "-");
                        }
                        if (nomor.Contains("."))
                        {
                            nomor = nomor.Replace(".", "-");
                        }
                        if (nomor.Contains("—"))
                        {
                            nomor = nomor.Replace("—", "-");
                        }
                        if (nomor.Contains("~"))
                        {
                            nomor = nomor.Replace("~", "-");
                        }
                        if (nomor.Contains("O"))
                        {
                            nomor = nomor.Replace("O", "0");
                        }
                        if (nomor.Contains("&"))
                        {
                            nomor = nomor.Replace("&", "8");
                        }
                        if (nomor.Contains(":"))
                        {
                            nomor = nomor.Replace(":", "");
                        }
                        if (nomor.Contains("*"))
                        {
                            nomor = nomor.Replace("*", "");
                        }
                        if (nomor.Contains("!"))//tambahan baru 201901014
                        {
                            nomor = nomor.Replace("!", "1");
                        }
                        if (nomor.Contains("'"))
                        {
                            nomor = nomor.Replace("'", "");
                        }
                        if (nomor.Contains("’"))
                        {
                            nomor = nomor.Replace("’", "");
                        }
                        if (nomor.Contains("]"))
                        {
                            nomor = nomor.Replace("]", "1");
                        }
                        #endregion
                    }
                }
                Match match = targetDataRegex.Match(nomor);

                if (match.Success)
                {
                    if (nomor.Substring(0, 1) != "2")
                    {
                        var stringBuilder = new StringBuilder(nomor);
                        stringBuilder.Remove(0, 1);
                        stringBuilder.Insert(0, "2");

                        nomor = stringBuilder.ToString();
                    }
                }

                if (nomor != "" && nomor != null)
                {

                    if (nomor.Contains("—")) nomor = nomor.Replace("—", "-");

                    statusNomor = OCRRepository.CekNomorBP(nomor);

                    if (statusNomor == "FAIL")
                    {
                        CheckFilePDF15(FileName);
                    }
                    else
                    {
                        #region sukses
                        if (File.Exists(SourcePath + FileName))//cek file asli
                        {
                            string result = OCRRepository.ValidateNumber(nomor);

                            if (result == "0" || nomor.Contains("DEMO")) //Demo di gagalin buat test
                            {
                                #region Input Data OCR
                                OCRRepository.inputOCR(nomor, SourcePath, FileName);
                                #endregion

                                #region Cek Nomor Bukti Potong
                                OCRRepository.CekBuktiPotong(nomor);
                                #endregion

                                #region Ambil Status Data
                                //cn.RefreshParam();
                                cn.CreateConnection();
                                cn.OpenConnection();

                                cn._sqlCommand.CommandText = "usp_GetPDFStatus";
                                cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                cn._sqlCommand.Parameters.AddWithValue("@Nomor", Convert.ToString(nomor));
                                cn._sqlCommand.CommandTimeout = 0;
                                cn._sqlCommand.ExecuteNonQuery();

                                SqlDataReader rd = cn._sqlCommand.ExecuteReader();
                                rd.Read();

                                status = rd[0].ToString();

                                cn.CloseConnection();
                                cn.DisposeConnection();
                                #endregion

                                if (status != "Data Sesuai")
                                {
                                    if (File.Exists(DestinationPathFail + nomor + ".pdf") == true)
                                    {
                                        SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR ERR", "Nomor Bukti Potong :" + " " + nomor + " " + "Sudah ada di Folder PDF_WHT_Failed, File tidak dapat di pindahkan");
                                        //replace file here 

                                    }
                                    else
                                    {
                                        File.Move(SourcePath + FileName, DestinationPathFail + nomor + "_" + dateFile + ".pdf");
                                        SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR ERR", "Nomor Bukti Potong :" + " " + nomor + " " + "Tidak di temukan di data Withholding, File di pindahkan ke folder PDF_WHT_Failed");
                                    }
                                }
                                else
                                {
                                    #region Update Path
                                    cn.RefreshParam();
                                    cn.CreateConnection();
                                    cn.OpenConnection();

                                    cn._sqlCommand.CommandText = "usp_UpdatePDFPath";
                                    cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                    cn._sqlCommand.Parameters.AddWithValue("@path", Convert.ToString(DestinationPath));
                                    cn._sqlCommand.Parameters.AddWithValue("@Nomor", Convert.ToString(nomor));
                                    cn._sqlCommand.CommandTimeout = 0;
                                    cn._sqlCommand.ExecuteNonQuery();

                                    cn.CloseConnection();
                                    cn.DisposeConnection();
                                    #endregion

                                    if (File.Exists(DestinationPath + nomor + ".pdf") == true)
                                    {
                                        File.Delete(DestinationPath + nomor + ".pdf");
                                        File.Move(SourcePath + FileName, DestinationPath + nomor + ".pdf");
                                        SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR INF", "Nomor Bukti Potong :" + " " + nomor + " " + "Data Sesuai, File sudah di pindahkan ke folder PDF_WHT_Success");
                                    }
                                    else
                                    {
                                        File.Move(SourcePath + FileName, DestinationPath + nomor + ".pdf");
                                        SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR INF", "Nomor Bukti Potong :" + " " + nomor + " " + "Data Sesuai, File sudah di pindahkan ke folder PDF_WHT_Success");
                                    }
                                }
                            }
                            else
                            {
                                File.Move(SourcePath + FileName, DestinationPathFail + dateFile + "_" + FileName);
                                SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR ERR", "Nomor Bukti Potong :" + " " + nomor + " " + "Data sudah pernah di SCAN, File di pindahkan ke folder PDF_WHT_FAILED");
                            }
                        }
                        else
                        {
                            SyncLog.InsertLog(nomor, FileName, "WHT OCR ERR", "File PDF :" + " " + FileName + " " + "Tidak ada di Folder PDF_WHT_OCR");
                        }
                        #endregion
                    }
                }
                else
                {
                    //File.Move(SourcePath + FileName, DestinationPathFail + dateFile + "_" + FileName);
                    //SyncLog.InsertLog(nomor, FileName, "WHT OCR ERR", "File PDF :" + " " + FileName + " " + "Tidak Terbaca oleh System, File di pindahkan ke folder PDF_WHT_FAILED");

                    if (nomor.Contains("—")) nomor = nomor.Replace("—", "-");

                    statusNomor = OCRRepository.CekNomorBP(nomor);

                    if (statusNomor == "FAIL")
                    {
                        CheckFilePDF15(FileName);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                log.Error("CheckFilePDF: " + ex.Message);
                //extractor.Dispose();
                File.Move(SourcePath + FileName, DestinationPathFail + dateFile + "_" + FileName);
                SyncLog.InsertLog(FileName, FileName, "WHT OCR ERR", ex.Message + " Nomor : " + nomor + " File Name : " + FileName + ", File Dipindahkan ke Folder PDF_WHT_FAILED");
            }
        }

        private static string validateNomor(string str, string path)
        {
            string nomor = "";
            if (str.Length > 27 && str.Length < 90)
            {
                #region Untuk PPh 21
                var lines = File.ReadAllLines(path); //Untuk PPh 21
                //1.3- BACA DEPANNYA
                string firstchar = lines[12];
                string lastchar = lines[14];
                string thirdchar = lines[16];
                string firstnumber = Regex.Replace(firstchar, @"[^\d]", "").Trim();
                string lastnumber = Regex.Replace(lastchar, @"[^\d]", "");
                string thirdnumber = Regex.Replace(thirdchar, @"[^\d]", "");

                if (firstnumber.Length == 1)
                {
                    nomor = firstnumber + lastnumber;
                    nomor = nomor.Insert(1, ".").Insert(3, "-").Insert(6, ".").Insert(9, "-");
                }
                else if (lastnumber.Length == 1)
                {
                    string sublast = firstnumber.Substring(0, 1);
                    string midnumber = firstnumber.Substring(1, 2);
                    firstnumber = firstnumber.Substring(3, 7);

                    nomor = sublast + lastnumber + midnumber + thirdnumber + firstnumber;
                    nomor = nomor.Insert(1, ".").Insert(3, "-").Insert(6, ".").Insert(9, "-");
                }
                else if (firstnumber.Length == 13)
                {
                    nomor = firstnumber;
                    nomor = nomor.Insert(1, ".").Insert(3, "-").Insert(6, ".").Insert(9, "-");
                }
                else
                {
                    string sub = firstnumber.Substring(0, 1);

                    firstnumber = firstnumber.Substring(1, 7);

                    nomor = sub + lastnumber + firstnumber;

                    nomor = nomor.Insert(1, ".").Insert(3, "-").Insert(6, ".").Insert(9, "-");
                }
                #endregion
            }
            else
            {
                #region
                if (str.Length > 30)
                {
                    str = str.Substring(0, 28).Trim();
                }
                else
                {
                    str = str.Substring(0, str.Length).Trim();
                }
                nomor = str.Substring(str.LastIndexOf(" ") + 1).Trim();

                if (nomor.Contains("/") || nomor.Contains("I") || nomor.Contains("l"))
                {
                    nomor = nomor.Replace("/", "-");
                    nomor = nomor.Replace("I", "-");
                    nomor = nomor.Replace("l", "-");
                }
                if (nomor.Contains("."))
                {
                    nomor = nomor.Replace(".", "-");
                }
                if (nomor.Contains("—"))
                {
                    nomor = nomor.Replace("—", "-");
                }
                if (nomor.Contains("~"))
                {
                    nomor = nomor.Replace("~", "-");
                }
                if (nomor.Contains("O"))
                {
                    nomor = nomor.Replace("O", "0");
                }
                if (nomor.Contains("&"))
                {
                    nomor = nomor.Replace("&", "8");
                }
                if (nomor.Contains(":"))
                {
                    nomor = nomor.Replace(":", "");
                }
                if (nomor.Contains("*"))
                {
                    nomor = nomor.Replace("*", "");
                }
                //if (nomor.Contains("!"))//tambahan baru 201901014
                //{
                //    nomor = nomor.Replace("!", "1");
                //}
                //if (nomor.Contains("'"))
                //{
                //    nomor = nomor.Replace("'", "");
                //}
                //if (nomor.Contains("’"))
                //{
                //    nomor = nomor.Replace("’", "");
                //}
                //if (nomor.Contains("]"))
                //{
                //    nomor = nomor.Replace("]", "1");
                //}

                #endregion
            }

            return nomor;
        }

        private static string byteScout21(float Gamma, string FileName)
        {
            TextExtractor extractor = new TextExtractor();

            extractor.RegistrationName = "lalitya.pramudita@emeriocorp.com";
            extractor.RegistrationKey = "C892-0088-F68C-DC9E-D87D-F8EF-97F";

            // Load sample PDF document
            extractor.LoadDocumentFromFile(SourcePath + FileName);

            extractor.GetTextFromPage(0);
            // Enable Optical Character Recognition (OCR)
            // in .Auto mode (SDK automatically checks if needs to use OCR or not)
            extractor.OCRMode = OCRMode.Auto;

            // Set the location of "tessdata" folder containing language data files
            //extractor.OCRLanguageDataFolder = ConfigurationManager.AppSettings["OCRLanguageDataFolderDebug"].ToString();
            extractor.OCRLanguageDataFolder = ConfigurationManager.AppSettings["OCRLanguageDataFolder"].ToString();

            // Set OCR language
            extractor.OCRLanguage = "eng"; // "eng" for english, "deu" for German, "fra" for French, "spa" for Spanish etc - according to files in /tessdata
                                           // Find more language files at https://github.com/tesseract-ocr/tessdata/tree/3.04.00

            // Set PDF document rendering resolution
            extractor.OCRResolution = 300;

            //Remove noise
            extractor.OCRImagePreprocessingFilters.AddMedian();

            // Apply Contrast
            extractor.OCRImagePreprocessingFilters.AddContrast(70); //default 80

            // Apply Gamma Correction
            extractor.OCRImagePreprocessingFilters.AddGammaCorrection(Gamma); // default 0.5f);

            // Save extracted text to file
            extractor.SaveTextToFile(SourcePath + "output.txt");

            string path = SourcePath + "output.txt";

            StringBuilder buffer = new StringBuilder();

            string num = File.ReadAllText(path); //Buat Rename NOMOR jadi Nomor
            num = num.Replace("Nomor PER-53/PJ/2009", "");
            num = num.Replace("Nomor PER—53/PJ/2009", "");
            num = num.Replace("NOMOR", "Nomor");
            File.WriteAllText(path, num);

            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.Peek() >= 0)
                {
                    string strs = sr.ReadLine();
                    if (Regex.IsMatch(strs, "Nomor"))
                        buffer.Append(strs + @"");
                }
            }
            string str = buffer.ToString().Trim();
            extractor.Dispose();

            return str;
        }

        public static void BatchPDF()
        {
            Connection cn = new Connection();
            foreach (string fileName in Directory.GetFiles(DestinationBatch))
            {
                string FileName = Path.GetFileNameWithoutExtension(fileName);
                int count;

                cn.CreateConnection();
                cn.OpenConnection();

                cn._sqlCommand.CommandText = "usp_GetPDFBatch";
                cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                cn._sqlCommand.Parameters.AddWithValue("@Nomor", Convert.ToString(FileName));
                cn._sqlCommand.Parameters.AddWithValue("@DestinationPath", Convert.ToString(DestinationPath));
                cn._sqlCommand.Parameters.AddWithValue("@SourcePath", Convert.ToString(DestinationBatch));
                cn._sqlCommand.CommandTimeout = 0;
                //cn._sqlCommand.ExecuteNonQuery();

                SqlDataReader rd = cn._sqlCommand.ExecuteReader();
                rd.Read();

                count = Convert.ToInt32(rd[0]);

                cn.CloseConnection();
                cn.DisposeConnection();

                if (count == 1)
                {
                    if (File.Exists(DestinationBatch + FileName + ".pdf") == true)
                    {
                        if (File.Exists(DestinationPath + FileName + ".pdf"))
                        {
                            File.Delete(DestinationPath + FileName + ".pdf");
                            File.Move(DestinationBatch + FileName + ".pdf", DestinationPath + FileName + ".pdf");
                            SyncLog.InsertLog(FileName, fileName, "WHT OCR ERR", "Nomor Bukti Potong :" + " " + FileName + " " + "Nomor sesuai,File sukses di pindahkan ke folder PDF_WHT_SUCCESS");
                        }
                        else
                        {
                            File.Move(DestinationBatch + FileName + ".pdf", DestinationPath + FileName + ".pdf");
                            SyncLog.InsertLog(FileName, fileName, "WHT OCR INF", "Nomor Bukti Potong :" + " " + FileName + " " + "Nomor sesuai,File sukses di pindahkan ke folder PDF_WHT_SUCCESS");
                        }
                    }
                }
                else
                {
                    SyncLog.InsertLog(FileName, fileName, "WHT OCR ERR", "Nomor Bukti Potong :" + " " + FileName + " " + "Nomor masih belum sesuai dengan data Withholding");
                }
            }
        }

        public static void GetPath()
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_BatchPDF";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.CommandTimeout = 0;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            rd.Read();

            SourcePath = rd[0].ToString();
            DestinationPath = rd[1].ToString();
            DestinationPathFail = rd[2].ToString();
            DestinationBatch = rd[3].ToString();

            cn.CloseConnection();
            cn.DisposeConnection();
        }

        public static void CheckFilePDF15(string FileName)
        {
            string dateFile = DateTime.Now.ToString("ddMMyyyyHHmm").Replace(":", "").Replace(" ", "");
            string status;
            string nomor = "";
            string statusNomor = "";

            Connection cn = new Connection();
            try
            {
                #region scan OCR

                // Create Bytescout.PDFExtractor.TextExtractor instance
                string path = SourcePath + "output.txt";

                string str = byteScout15(0.2f, FileName); //str = Regex.Replace(str, @"[^\d-.]", "");
                if (str == "")
                {
                    str = byteScout15(0.1f, FileName);
                }

                if (str.Length > 27 && str.Length < 90)
                {
                    #region Untuk PPh 21

                    nomor = Regex.Replace(str, @"[^\d-.]", "");
                    //var lines = File.ReadAllLines(path); //Untuk PPh 21
                    //string firstchar = lines[11];
                    //string lastchar = lines[14];
                    //string thirdchar = lines[16];
                    //string firstnumber = Regex.Replace(firstchar, @"[^\d]", "").Trim();
                    //string lastnumber = Regex.Replace(lastchar, @"[^\d]", "");
                    //string thirdnumber = Regex.Replace(thirdchar, @"[^\d]", "");

                    //if (firstnumber.Length == 13)
                    //{
                    //    nomor = firstnumber + lastnumber;
                    //    nomor = nomor.Insert(1, ".").Insert(3, "-").Insert(6, ".").Insert(9, "-");
                    //}
                    //else if (lastnumber.Length == 1)
                    //{
                    //    string sublast = firstnumber.Substring(0, 1);
                    //    string midnumber = firstnumber.Substring(1, 2);
                    //    firstnumber = firstnumber.Substring(3, 7);

                    //    nomor = sublast + lastnumber + midnumber + thirdnumber + firstnumber;
                    //    nomor = nomor.Insert(1, ".").Insert(3, "-").Insert(6, ".").Insert(9, "-");
                    //}
                    //else if (firstnumber.Length == 1)
                    //{
                    //    nomor = firstnumber;
                    //    nomor = nomor.Insert(1, ".").Insert(3, "-").Insert(6, ".").Insert(9, "-");
                    //}
                    //else
                    //{
                    //    string sub = firstnumber.Substring(0, 1);

                    //    firstnumber = firstnumber.Substring(1, 7);

                    //    nomor = sub + lastnumber + firstnumber;

                    //    nomor = nomor.Insert(1, ".").Insert(3, "-").Insert(6, ".").Insert(9, "-");
                    //}
                    #endregion
                }
                else
                {
                    if (str != "")
                    {
                        #region

                        if (str.Contains("."))
                        {
                            str = str.Replace(".", "");
                        }
                        if (str.Length > 30)
                        {
                            str = str.Substring(0, 28).Trim();
                        }
                        else
                        {
                            str = str.Substring(0, str.Length).Trim();
                        }
                        nomor = str.Substring(str.LastIndexOf(" ") + 1).Trim();

                        if (nomor.Contains("/") || nomor.Contains("I") || nomor.Contains("l"))
                        {
                            nomor = nomor.Replace("/", "-");
                            nomor = nomor.Replace("I", "-");
                            nomor = nomor.Replace("l", "-");
                        }
                        if (nomor.Contains("—"))
                        {
                            nomor = nomor.Replace("—", "-");
                        }
                        if (nomor.Contains("~"))
                        {
                            nomor = nomor.Replace("~", "-");
                        }
                        if (nomor.Contains("O"))
                        {
                            nomor = nomor.Replace("O", "0");
                        }
                        if (nomor.Contains("&"))
                        {
                            nomor = nomor.Replace("&", "8");
                        }
                        if (nomor.Contains(":"))
                        {
                            nomor = nomor.Replace(":", "");
                        }
                        if (nomor.Contains("*"))
                        {
                            nomor = nomor.Replace("*", "");
                        }
                        if (nomor.Contains("!"))//tambahan baru 201901014
                        {
                            nomor = nomor.Replace("!", "1");
                        }
                        if (nomor.Contains("'"))
                        {
                            nomor = nomor.Replace("'", "");
                        }
                        if (nomor.Contains("’"))
                        {
                            nomor = nomor.Replace("’", "");
                        }
                        if (nomor.Contains("]"))
                        {
                            nomor = nomor.Replace("]", "1");
                        }
                        #endregion
                    }
                }

                Match match = targetDataRegex.Match(nomor);

                if (match.Success)
                {
                    if (nomor.Substring(0, 1) != "2")
                    {
                        var stringBuilder = new StringBuilder(nomor);
                        stringBuilder.Remove(0, 1);
                        stringBuilder.Insert(0, "2");

                        nomor = stringBuilder.ToString();
                    }
                }

                if (nomor != "" && nomor != null)
                {
                    if (nomor.Contains("—")) nomor = nomor.Replace("—", "-");

                    statusNomor = OCRRepository.CekNomorBP(nomor);

                    if (statusNomor == "FAIL")
                    {
                        if (File.Exists(DestinationPathFail + nomor + ".pdf") == true)
                        {
                            SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR ERR", "Nomor Bukti Potong :" + " " + nomor + " " + "Sudah ada di Folder PDF_WHT_Failed, File tidak dapat di pindahkan");
                        }
                        else
                        {
                            File.Move(SourcePath + FileName, DestinationPathFail + nomor + "_" + dateFile + ".pdf");
                            SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR ERR", "Nomor Bukti Potong :" + " " + nomor + " " + "Tidak di temukan di data Withholding, File di pindahkan ke folder PDF_WHT_Failed");
                        }
                    }
                    else
                    {
                        #region Sukses
                        if (File.Exists(SourcePath + FileName))//cek file asli
                        {
                            string result = OCRRepository.ValidateNumber(nomor);

                            if (result == "0" || nomor.Contains("DEMO")) //Demo di gagalin buat test
                            {
                                #region Input Data OCR
                                OCRRepository.inputOCR(nomor, SourcePath, FileName);
                                #endregion

                                #region Cek Nomor Bukti Potong
                                OCRRepository.CekBuktiPotong(nomor);
                                #endregion

                                #region Ambil Status Data
                                //cn.RefreshParam();
                                cn.CreateConnection();
                                cn.OpenConnection();

                                cn._sqlCommand.CommandText = "usp_GetPDFStatus";
                                cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                cn._sqlCommand.Parameters.AddWithValue("@Nomor", Convert.ToString(nomor));
                                cn._sqlCommand.CommandTimeout = 0;
                                cn._sqlCommand.ExecuteNonQuery();

                                SqlDataReader rd = cn._sqlCommand.ExecuteReader();
                                rd.Read();

                                status = rd[0].ToString();

                                cn.CloseConnection();
                                cn.DisposeConnection();
                                #endregion

                                if (status != "Data Sesuai")
                                {
                                    if (File.Exists(DestinationPathFail + nomor + ".pdf") == true)
                                    {
                                        SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR ERR", "Nomor Bukti Potong :" + " " + nomor + " " + "Sudah ada di Folder PDF_WHT_Failed, File tidak dapat di pindahkan");
                                        //replace file here 

                                    }
                                    else
                                    {
                                        File.Move(SourcePath + FileName, DestinationPathFail + nomor + "_" + dateFile + ".pdf");
                                        SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR ERR", "Nomor Bukti Potong :" + " " + nomor + " " + "Tidak di temukan di data Withholding, File di pindahkan ke folder PDF_WHT_Failed");
                                    }
                                }
                                else
                                {
                                    #region Update Path
                                    cn.RefreshParam();
                                    cn.CreateConnection();
                                    cn.OpenConnection();

                                    cn._sqlCommand.CommandText = "usp_UpdatePDFPath";
                                    cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                    cn._sqlCommand.Parameters.AddWithValue("@path", Convert.ToString(DestinationPath));
                                    cn._sqlCommand.Parameters.AddWithValue("@Nomor", Convert.ToString(nomor));
                                    cn._sqlCommand.CommandTimeout = 0;
                                    cn._sqlCommand.ExecuteNonQuery();

                                    cn.CloseConnection();
                                    cn.DisposeConnection();
                                    #endregion

                                    if (File.Exists(DestinationPath + nomor + ".pdf") == true)
                                    {
                                        File.Delete(DestinationPath + nomor + ".pdf");
                                        File.Move(SourcePath + FileName, DestinationPath + nomor + ".pdf");
                                        SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR INF", "Nomor Bukti Potong :" + " " + nomor + " " + "Data Sesuai, File sudah di pindahkan ke folder PDF_WHT_Success");
                                    }
                                    else
                                    {
                                        File.Move(SourcePath + FileName, DestinationPath + nomor + ".pdf");
                                        SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR INF", "Nomor Bukti Potong :" + " " + nomor + " " + "Data Sesuai, File sudah di pindahkan ke folder PDF_WHT_Success");
                                    }
                                }
                            }
                            else
                            {
                                File.Move(SourcePath + FileName, DestinationPathFail + dateFile + "_" + FileName);
                                SyncLog.InsertLog(nomor, nomor + ".pdf", "WHT OCR ERR", "Nomor Bukti Potong :" + " " + nomor + " " + "Data sudah pernah di SCAN, File di pindahkan ke folder PDF_WHT_FAILED");
                            }
                        }
                        else
                        {
                            SyncLog.InsertLog(nomor, FileName, "WHT OCR ERR", "File PDF :" + " " + FileName + " " + "Tidak ada di Folder PDF_WHT_OCR");
                        }
                        #endregion
                    }
                }
                else
                {
                    File.Move(SourcePath + FileName, DestinationPathFail + dateFile + "_" + FileName);
                    SyncLog.InsertLog(nomor, FileName, "WHT OCR ERR", "File PDF :" + " " + FileName + " " + "Tidak Terbaca oleh System, File di pindahkan ke folder PDF_WHT_FAILED");
                }
                #endregion
            }
            catch (Exception ex)
            {
                log.Error("CheckFilePDF: " + ex.Message);
                //extractor.Dispose();
                File.Move(SourcePath + FileName, DestinationPathFail + dateFile + "_" + FileName);
                SyncLog.InsertLog(FileName, FileName, "WHT OCR ERR", ex.Message + " Nomor : " + nomor + " File Name : " + FileName + ", File Dipindahkan ke Folder PDF_WHT_FAILED");
            }
        }

        private static string byteScout15(float Gamma, string FileName)
        {
            TextExtractor extractor = new TextExtractor();

            extractor.RegistrationName = "lalitya.pramudita@emeriocorp.com";
            extractor.RegistrationKey = "C892-0088-F68C-DC9E-D87D-F8EF-97F";

            // Load sample PDF document
            extractor.LoadDocumentFromFile(SourcePath + FileName);

            extractor.GetTextFromPage(0);

            extractor.OCRMode = OCRMode.Auto;

            extractor.OCRLanguageDataFolder = ConfigurationManager.AppSettings["OCRLanguageDataFolder"].ToString();

            extractor.OCRLanguage = "eng"; // "eng" for english, "deu" for German, "fra" for French, "spa" for Spanish etc - according to files in /tessdata
                                           // Find more language files at https://github.com/tesseract-ocr/tessdata/tree/3.04.00

            extractor.OCRResolution = 300;

            //Remove noise
            extractor.OCRImagePreprocessingFilters.AddMedian();

            // Apply Contrast
            extractor.OCRImagePreprocessingFilters.AddContrast(80);

            // Apply Gamma Correction
            extractor.OCRImagePreprocessingFilters.AddGammaCorrection(Gamma);

            // Save extracted text to file
            extractor.SaveTextToFile(SourcePath + "output.txt");

            string path = SourcePath + "output.txt";

            StringBuilder buffer = new StringBuilder();

            string num = File.ReadAllText(path); //Buat Rename NOMOR jadi Nomor
            num = num.Replace("Nomor PER-53/PJ/2009", "");
            num = num.Replace("Nomor PER—53/PJ/2009", "");
            num = num.Replace("NOMOR", "Nomor");
            File.WriteAllText(path, num);

            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.Peek() >= 0)
                {
                    string strs = sr.ReadLine();
                    if (Regex.IsMatch(strs, "Nomor"))
                        buffer.Append(strs + @"");
                }
            }
            string str = buffer.ToString().Trim();
            extractor.Dispose();

            return str;
        }

        #endregion

        #region FTP
        public static void UploadFileToFTP(string FtpPath, string filePath, string username, string password)
        {
            try
            {
                var fileName = Path.GetFileName(filePath);
                var request = (FtpWebRequest)WebRequest.Create(FtpPath + fileName);

                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                using (var fileStream = File.OpenRead(filePath))
                {
                    using (var requestStream = request.GetRequestStream())
                    {
                        fileStream.CopyTo(requestStream);
                        requestStream.Close();
                    }
                }

                var response = (FtpWebResponse)request.GetResponse();
                response.Close();
            }
            catch (WebException ex)
            {
                throw new Exception((ex.Response as FtpWebResponse).StatusDescription);
            }

        }
        protected static void DownloadFileFTP(string PathResult, string FileName)
        {
            //string fileName = "FTN0001.A00001.SAPIE01LXS2.TXT";
            string ftp = "ftp://" + ConfigurationManager.AppSettings["FTPPath"].ToString() + "/";
            string ftpFolder = ConfigurationManager.AppSettings["FTPFolder"].ToString();
            string ftpUser = ConfigurationManager.AppSettings["FTPUser"].ToString();
            string ftpPassword = ConfigurationManager.AppSettings["FTPPassword"].ToString();
            try
            {
                //Create FTP Request.
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftp + ftpFolder + FileName);
                request.Method = WebRequestMethods.Ftp.DownloadFile;

                //Enter FTP Server credentials.
                request.Credentials = new NetworkCredential(ftpUser, ftpPassword);
                request.UsePassive = true;
                request.UseBinary = true;
                request.EnableSsl = false;

                //Fetch the Response and read it into a MemoryStream object.
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (Stream fileStream = new FileStream(PathResult + FileName, FileMode.CreateNew))
                    {
                        responseStream.CopyTo(fileStream);
                    }
                }
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode != FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    //log.Error("DownloadFileFTP: " + ex.Message);
                    SyncLog.InsertLog(FileName, FileName, "ERR : ", ex.Message);
                }
            }
        }
        #endregion

        #region Logging
        private static void StopLogging()
        {
            //log.Info("Services Stop at : " + DateTime.Now.ToString());
            //log = null;
        }

        private static void StartLogging()
        {
            log4net.Config.XmlConfigurator.Configure();
            log = log4net.LogManager.GetLogger(typeof(Program));
            log.Info("Services Run at : " + DateTime.Now.ToString());
        }
        #endregion Logging        

        #region Scan VATIn
        public static void GetXML()
        {
            try
            {
                Connection cn = new Connection();
                cn.CreateConnection();
                cn.OpenConnection();

                cn._sqlCommand.CommandText = "usp_GetDJPURL";
                cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                cn._sqlCommand.CommandTimeout = 0;

                SqlDataReader rd = cn._sqlCommand.ExecuteReader();
                while (rd.Read())
                {
                    Data = rd[0].ToString();
                    if (Data != "")
                    {
                        Console.WriteLine(Data);
                        try
                        {
                            var response = new HttpResponseMessage();
                            TAM.Scheduler.resValidateFakturPm VATInXML = new resValidateFakturPm();
                            string serviceUrl = Data;

                            using (HttpClient client = new HttpClient())
                            {
                                #region Body
                                client.DefaultRequestHeaders.Accept.Clear();
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
                                Uri serviceUri = new Uri(serviceUrl, UriKind.Absolute);

                                XmlDocument doc = new XmlDocument();
                                response = client.GetAsync(serviceUri).Result;

                                var stream = new System.IO.MemoryStream(response.Content.ReadAsByteArrayAsync().Result);

                                var serializer = new XmlSerializer(typeof(resValidateFakturPm));
                                using (var reader = XmlReader.Create(stream))
                                {
                                    VATInXML = (resValidateFakturPm)serializer.Deserialize(reader);
                                    if (string.IsNullOrEmpty(VATInXML.nomorFaktur))
                                    {
                                        SyncLog.InsertLog(Data, "||", "ERR", "URL Not Found");
                                    }

                                    string InvoiceDate = VATInXML.tanggalFakturDateFormat.ToString("yyyy-MM-dd");
                                    DateTime expdate = Convert.ToDateTime(InvoiceDate).AddDays(1).AddMonths(3).AddDays(-1);
                                    DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);

                                    #region Map XML Model to ViewModel
                                    VATInManualInputViewModel model = new VATInManualInputViewModel();

                                    model.Id = Guid.NewGuid();
                                    model.InvoiceNumber = VATInXML.nomorFaktur;
                                    model.InvoiceNumberFull = VATInXML.nomorFaktur.FormatNomorFakturGabungan(VATInXML.kdJenisTransaksi, VATInXML.fgPengganti);
                                    model.KDJenisTransaksi = VATInXML.kdJenisTransaksi;
                                    model.FGPengganti = VATInXML.fgPengganti;
                                    model.InvoiceDate = VATInXML.tanggalFakturDateFormat.FormatSQLDate();
                                    model.ExpireDate = endOfMonth.ToString("yyyy-MM-dd");
                                    model.SupplierNPWP = VATInXML.npwpPenjual.FormatNPWP();
                                    model.SupplierName = VATInXML.namaPenjual;
                                    model.SupplierAddress = VATInXML.alamatPenjual;
                                    model.NPWPLawanTransaksi = VATInXML.npwpLawanTransaksi.FormatNPWP();
                                    model.NamaLawanTransaksi = VATInXML.namaLawanTransaksi;
                                    model.AlamatLawanTransaksi = VATInXML.alamatLawanTransaksi;
                                    model.StatusApprovalXML = VATInXML.statusApproval;
                                    model.StatusFakturXML = VATInXML.statusFaktur;
                                    model.VATBaseAmount = VATInXML.jumlahDpp;
                                    model.VATAmount = VATInXML.jumlahPpn;
                                    model.JumlahPPnBM = VATInXML.jumlahPpnBm;
                                    model.VATInDetails = new List<VATInDetailManualInputViewModel>();
                                    SyncID = Guid.NewGuid();
                                    try
                                    {
                                        //ValidateVATInInvoice.Validate(model.InvoiceNumberFull);
                                        string result = "";
                                        result = ValidateVATInInvoice.Validate(model.InvoiceNumberFull);
                                        if (result != "Number Already Exist")
                                        {
                                            InputVATIn.save(model, SyncID);
                                            SyncLog.InsertLog(Data, "Data Dari Link", "INF", "Insert VATIn Success");

                                            foreach (resValidateFakturPmDetailTransaksi detail in VATInXML.detailTransaksi)
                                            {
                                                VATInDetailManualInputViewModel detailModel = new VATInDetailManualInputViewModel();

                                                detailModel.UnitName = detail.nama;
                                                detailModel.UnitPrice = detail.hargaSatuan;
                                                detailModel.Quantity = detail.jumlahBarang;
                                                detailModel.TotalPrice = detail.hargaTotal;
                                                detailModel.Discount = detail.diskon;
                                                detailModel.DPP = detail.dpp;
                                                detailModel.PPN = detail.ppn;
                                                detailModel.PPNBM = detail.ppnbm;
                                                detailModel.TarifPPNBM = detail.tarifPpnbm;

                                                try
                                                {
                                                    InputVATInDetail.save(detailModel, model.Id);
                                                    SyncLog.InsertLog(Data, "Data Dari Link", "INF", "Insert VATIn Detail Success");
                                                }
                                                catch (Exception ex)
                                                {
                                                    SyncLog.InsertLog(Data, "||", "ERR", ex.Message);
                                                }
                                            }

                                            try
                                            {
                                                Syncdata.InsertSync(Data, SyncID);
                                                SyncLog.InsertLog(Data, "Data Dari Link", "INF", "Insert SyncData Success");
                                                Syncdata.InsertSyncDetail(model.Id + "|" + model.InvoiceNumber + "|" + model.InvoiceNumberFull + "|" +
                                                        model.KDJenisTransaksi + "|" + model.FGPengganti + "|" + model.InvoiceDate + "|" + model.ExpireDate + "||" +
                                                        model.SupplierNPWP + "|" + model.SupplierName + "|" + model.SupplierAddress + "|" + model.NPWPLawanTransaksi +
                                                        model.NamaLawanTransaksi + "|" + model.AlamatLawanTransaksi + "|" + model.StatusApprovalXML + "|" + model.StatusFakturXML +
                                                        "|" + model.VATBaseAmount + "|" + model.VATAmount + "|" + model.JumlahPPnBM + "|", SyncID);
                                                SyncLog.InsertLog(Data, "Data Dari Link", "INF", "Insert SyncDataDetail Success");

                                                UpdateURLStatus.UpdateStatus(serviceUrl, "S");
                                            }
                                            catch (Exception ex)
                                            {
                                                SyncLog.InsertLog(Data, "||", "ERR", ex.Message);
                                            }
                                        }
                                        else
                                        {
                                            SyncLog.InsertLog(Data, "||", "ERR", "Tax Invoice Number Already Exist");
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        SyncLog.InsertLog(Data, "||", "ERR", ex.Message);
                                        //log here
                                    }
                                    #endregion
                                }
                                #endregion
                            }
                        }
                        catch (Exception ex)
                        {
                            SyncLog.InsertLog(Data, "||", "ERR", ex.Message);
                            //log here
                        }
                    }
                }
                cn.CloseConnection();
                cn.DisposeConnection();
            }
            catch (Exception ex)
            {
                SyncLog.InsertLog(Data, "||", "ERR", ex.Message);
            }
        }
        #endregion

        #region Email

        public static string Subject { get; set; }
        public static string Body { get; set; }
        public static bool IsHtml { get; set; }
        public static object MasterConfigRepository { get; private set; }

        public static void Send(List<DataRemainder> param)
        {
            var message = new MailMessage();
            message.Subject = Subject;
            message.Body = Body;
            message.IsBodyHtml = IsHtml;

            List<string> UserEmail = new List<string>();
            foreach (DataRemainder data in param)
            {
                if (data.PVCreatorEmail != null && data.PVCreatorEmail != "")
                {
                    UserEmail.Add(data.PVCreatorEmail);
                }
                if (data.PVCreatorDeptHeadEmail != null && data.PVCreatorDeptHeadEmail != "")
                {
                    UserEmail.Add(data.PVCreatorDeptHeadEmail);
                }
            }
            var emails = UserEmail.Distinct().ToList();
            var emails2 = GetFinanceTaxEmail();
            var allemails = emails.Union(emails2);
            foreach (var item in allemails)
            {
                if (IsValidEmail(item))
                {
                    message.To.Add(new MailAddress(item));
                }
            }

            try
            {
                using (var smtp = new SmtpClient())
                {
                    smtp.Send(message);
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Exception is: " + ex.Message);
                if (ex.InnerException != null)
                {
                    //Console.WriteLine("InnerException is: {0}", ex.InnerException);
                    SyncLog.InsertLog(String.Join(";", allemails), "Email: " + Subject, "ERR : ", ex.InnerException.ToString());
                }
                else
                {
                    SyncLog.InsertLog(String.Join(";", allemails), "Email: " + Subject, "ERR : ", ex.Message);
                }
                //Console.ReadLine();
            }
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                SyncLog.InsertLog(email, "Email: " + Subject, "ERR", "Email is not valid.");
                return false;
            }
        }

        public static string CreateMailBody(List<DataRemainder> param)
        {
            string body = "";

            body = "<b>Dear Bapak dan Ibu,</b><br>";
            body += "<br>";
            body += "Anda memiliki {xx} Faktur Pajak yang sudah mendekati masa expired<br>";
            body += "Untuk detail sebagai berikut:<br>";
            body += "<br>";
            body += "<table width=\"100%\" border=\"1\" cellpadding=\"5\" style=\"border-collapse:collapse;\">";
            body += "<tr border=\"1\" bgcolor=\"#cccccc\">";
            body += "<th>No</th><th>PV Number</th><th>Invoice Status</th><th>PV Creator</th><th>Remaining Days</th><th>No. Faktur Pajak</th><th>Tanggal Faktur Pajak</th><th>DPP</th><th>PPN</th>";
            body += "</tr>";

            foreach (DataRemainder data in param)
            {
                body += "<tr border=\"1\">";
                body += "<td align=\"center\">" + data.No + "</td>";
                body += "<td align=\"center\"><a href=\"" + ConfigurationManager.AppSettings["urlElvis"].ToString() + "\">" + data.PvNumber + "</a></td>";
                body += "<td align=\"center\">" + data.InvoiceStatus + "</td>";
                body += "<td align=\"center\">" + data.PvCreator + "</td>";
                body += "<td align=\"center\" style=\"color:red;\"><b>" + data.RemainingDays + " days</b></td>";
                body += "<td align=\"center\">" + data.NoFakturPajak + "</td>";
                body += "<td align=\"center\">" + data.TglFakturPajak.ToString("dd/MM/yyyy") + "</td>";
                //body += "<td align=\"right\">" + int.Parse(data.Dpp.ToString()).ToString("0,0", CultureInfo.CreateSpecificCulture("el-GR")) + "</td>";
                //body += "<td align=\"right\">" + int.Parse(data.Ppn.ToString()).ToString("0,0", CultureInfo.CreateSpecificCulture("el-GR")) + "</td>";
                body += "<td align=\"right\">" + data.Dpp.ToString("N0") + "</td>";
                body += "<td align=\"right\">" + data.Ppn.ToString("N0") + "</td>";
                body += "</tr>";
            }

            body += "</table>";

            body = body.Replace("{xx}", param.Count.ToString());

            return body;
        }

        public static List<DataRemainder> GetDataRemainder()
        {
            List<DataRemainder> dts = new List<DataRemainder>();
            int Nomor = 1;

            Connection cn = new Connection();
            cn.CreateConnection2();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "sp_GetEmailRemainder";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.CommandTimeout = 0;
            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            while (rd.Read())
            {
                DataRemainder dt = new DataRemainder()
                {
                    No = Nomor.ToString(),
                    PvNumber = rd[0].ToString(),
                    InvoiceStatus = rd[1].ToString(),
                    PvCreator = rd[9].ToString(),
                    RemainingDays = rd[2].ToString(),
                    NoFakturPajak = rd[3].ToString(),
                    TglFakturPajak = DateTime.Parse(rd[4].ToString()),
                    Dpp = Double.Parse(rd[5].ToString()),
                    Ppn = Double.Parse(rd[6].ToString()),
                    PVCreatorEmail = rd[10].ToString(),
                    PVCreatorDeptHeadEmail = rd[11].ToString()
                };
                Nomor++;
                dts.Add(dt);
            }

            cn.CloseConnection();
            cn.DisposeConnection();

            return dts;
        }

        public static List<string> GetFinanceTaxEmail()
        {
            List<string> emails = new List<string>();

            Connection cn = new Connection();
            cn.CreateConnection2();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "sp_GetEmailFinanceTax";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.CommandTimeout = 0;
            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            while (rd.Read())
            {
                emails.Add(rd[0].ToString());
            }

            cn.CloseConnection();
            cn.DisposeConnection();

            return emails;
        }

        public class DataRemainder
        {
            public string No { get; set; }
            public string PvNumber { get; set; }
            public string InvoiceStatus { get; set; }
            public string PvCreator { get; set; }
            public string RemainingDays { get; set; }
            public string NoFakturPajak { get; set; }
            public DateTime TglFakturPajak { get; set; }
            public Double Dpp { get; set; }
            public Double Ppn { get; set; }
            public string PVCreatorEmail { get; set; }
            public string PVCreatorDeptHeadEmail { get; set; }
        }

        #endregion

        #region GenerateTxtTransitory
        public static bool CheckDataClearing()
        {
            bool Stt = false;

            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "efb_sp_CheckDataClearing";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.CommandTimeout = 0;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            if (rd.Read())
            {
                Stt = true;
            }

            cn.CloseConnection();
            cn.DisposeConnection();

            return Stt;
        }

        public static List<string[]> InsertTransitoryHeader()
        {
            List<string[]> dts = new List<string[]>();

            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "efb_sp_Insert_TB_R_TransitoryData_H";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.CommandTimeout = 0;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();

            int fieldCount = rd.FieldCount;

            while (rd.Read())
            {
                string[] row = new string[fieldCount];

                for (int i = 0; i < fieldCount; i++)
                {
                    row[i] = rd[i].ToString();
                }
                dts.Add(row);
            }

            cn.CloseConnection();
            cn.DisposeConnection();

            return dts;
        }

        public static void InsertTransitoryDetail(string[] param)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "efb_sp_Insert_TB_R_TransitoryData_D";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@TransitoryHeaderId", param[1]);
            cn._sqlCommand.Parameters.AddWithValue("@TransitoryHeaderDocDate", param[0]);
            cn._sqlCommand.CommandTimeout = 0;
            cn._sqlCommand.ExecuteReader();

            cn.CloseConnection();
            cn.DisposeConnection();
        }

        public static List<string[]> GetTransitoryHeader(string[] param)
        {
            List<string[]> dts = new List<string[]>();

            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "efb_sp_GetTransitoryDataHeader";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@TransitoryHeaderId", param[1]);
            cn._sqlCommand.CommandTimeout = 0;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();

            int fieldCount = rd.FieldCount;

            while (rd.Read())
            {
                string[] row = new string[fieldCount];

                for (int i = 0; i < fieldCount; i++)
                {
                    row[i] = rd[i].ToString();
                }
                dts.Add(row);
            }

            cn.CloseConnection();
            cn.DisposeConnection();

            return dts;
        }

        public static List<string[]> GetTransitoryDetail(string[] param)
        {
            List<string[]> dts = new List<string[]>();

            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "efb_sp_GetTransitoryDataDetail";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@TransitoryHeaderId", param[1]);
            cn._sqlCommand.CommandTimeout = 0;
            SqlDataReader rd = cn._sqlCommand.ExecuteReader();

            int fieldCount = rd.FieldCount;

            while (rd.Read())
            {
                string[] row = new string[fieldCount];

                for (int i = 0; i < fieldCount; i++)
                {
                    row[i] = rd[i].ToString();
                }
                dts.Add(row);
            }

            cn.CloseConnection();
            cn.DisposeConnection();

            return dts;
        }

        public static string PutTXT(List<string[]> ListArrHeader, List<string[]> ListArrDetail, string Folder, string FileName, string[] param)
        {
            string fileName =
                        FileName + "_" + param[0].Substring(0, 6)
                        + "_" + DateTime.Now.ToString("yyyyMMddHHmmss")
                        + ".txt";

            string fullPath = Path.Combine(@Folder, fileName);

            try
            {
                var csv = new StringBuilder();

                foreach (string[] arrayString in ListArrHeader)
                {
                    string newLine = string.Join("|", arrayString.Where(a => a != null)) + Environment.NewLine;

                    csv.Append(newLine);
                }

                foreach (string[] arrayString in ListArrDetail)
                {
                    string newLine = string.Join("|", arrayString.Where(a => a != null)) + Environment.NewLine;

                    csv.Append(newLine);
                }

                File.WriteAllText(fullPath, csv.ToString());
            }
            catch (Exception ex)
            {
                SyncLog.InsertLog(fileName, fileName, "ERR : ", ex.Message);
            }

            return fileName;
        }

        public static void UpdateFileNameTransitoryHeader(string fileName, string[] param)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "efb_sp_UpdateFileName_TB_R_TransitoryData_H";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@FileName", fileName);
            cn._sqlCommand.Parameters.AddWithValue("@TransitoryHeaderId", param[1]);
            cn._sqlCommand.CommandTimeout = 0;

            cn._sqlCommand.ExecuteReader();

            cn.CloseConnection();
            cn.DisposeConnection();
        }

        public static void BulkLoadTransitoryGLData(string PathFileName)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "efb_sp_load_data_transitory_gl";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@PathFileName", PathFileName);
            cn._sqlCommand.CommandTimeout = 0;

            cn._sqlCommand.ExecuteReader();

            cn.CloseConnection();
            cn.DisposeConnection();
        }
        #endregion GenerateTxtTransitory

        #region CheckTxtTransitory
        public static List<string> GetTransitoryUnproccessFileName()
        {
            List<string> filenames = new List<string>();

            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "efb_sp_GetTransitoryUnproccessFileName";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.CommandTimeout = 0;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            while (rd.Read())
            {
                filenames.Add(rd[0].ToString());
            }

            cn.CloseConnection();
            cn.DisposeConnection();

            return filenames;
        }

        public static void UpdateTransitoryResult(string FileName, string TransitoryNo)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "efb_sp_UpdateTransitoryResult";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@FileName", FileName);
            cn._sqlCommand.Parameters.AddWithValue("@TransitoryNo", TransitoryNo);
            cn._sqlCommand.CommandTimeout = 0;
            cn._sqlCommand.ExecuteReader();

            cn.CloseConnection();
            cn.DisposeConnection();
        }
        #endregion CheckTxtTransitory

        #region SyncDataElvisVatin
        public static void SyncDataElvisVatin()
        {

            try
            {

                Connection cn = new Connection();
                cn.CreateConnection();
                cn.OpenConnection();

                cn._sqlCommand.CommandText = "efb_sp_ELVIS_VATIN_DataSync_Batch";
                cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                cn._sqlCommand.CommandTimeout = 0;
                cn._sqlCommand.ExecuteReader();

                cn.CloseConnection();
                cn.DisposeConnection();

            }
            catch (Exception ex)
            {


                SyncLog.InsertLog("-", "SyncDataElvisVatin", "ERR", ex.Message);
            }
        }
        #endregion SyncDataElvisVatin

        #region SyncDataElvisWht
        public static void SyncDataElvisWht()
        {
            try
            {
                Connection cn = new Connection();
                cn.CreateConnection();
                cn.OpenConnection();

                cn._sqlCommand.CommandText = "efb_sp_ELVIS_WHT_DataSync_Batch";
                cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                cn._sqlCommand.CommandTimeout = 0;
                cn._sqlCommand.ExecuteReader();

                cn.CloseConnection();
                cn.DisposeConnection();
            }
            catch (Exception ex)
            {
                SyncLog.InsertLog("-", "SyncDataElvisWht", "ERR", ex.Message);
            }
        }
        #endregion SyncDataElvisWht

        #region BulkLoadPPH22
        public static void BulkLoadPPh22(string PathFileName)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "efb_sp_load_data_pph_22";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@PathFileName", PathFileName);
            cn._sqlCommand.CommandTimeout = 0;

            cn._sqlCommand.ExecuteReader();

            cn.CloseConnection();
            cn.DisposeConnection();
        }
        #endregion

        #region BulkLoadClearingDocument
        public static void BulkLoadClearingDocument(string PathFileName)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "efb_sp_load_data_clearing_document";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@PathFileName", PathFileName);
            cn._sqlCommand.CommandTimeout = 0;
            cn._sqlCommand.ExecuteReader();

            cn.CloseConnection();
            cn.DisposeConnection();
        }
        #endregion

        #region ScanQR
        public static void ScanQR()
        {
            PathQR path = new PathQR();
            path = ScanQRRepostitory.GetQRPath();

            string pathSource = path.QRSourcePath;
            string pathSuccess = path.QRDestinationPath;
            string pathFail = path.QRFailPath;

            DirectoryInfo info = new DirectoryInfo(pathSource);
            FileInfo[] files = info.GetFiles().OrderBy(p => p.Name).ToArray();
            string fileName = "";
            int countResult = 0;

            foreach (FileInfo file in files)
            {
                fileName = file.Name;
                QRPDFFiles = Path.GetFullPath(pathSource + fileName);

                string fail = Path.Combine(pathFail + fileName);
                string success = Path.Combine(pathSuccess + fileName);

                // Create and activate Bytescout.BarCodeReader.Reader instance
                Reader barcodeReader = new Reader("1_DEVELOPER_LICENSE_FOR_DESKTOP_DEVELOPER_LICENSE_2D_BARCODES_ONLY_DESKTOP_APPS_ONLY_MAHENDRA.SONDAY@EMERIOCORP.COM_SUPPORT_AND_UPDATES_UNTIL_APRIL_25_2020", "E702-4936-D399-2BC9-8239-D07D-27B");

                barcodeReader.BarcodeTypesToFind.QRCode = true;
                barcodeReader.PDFReadingMode = PDFReadingMode.ExtractEmbeddedImagesOnly;
                //barcodeReader.MaxNumberOfBarcodesPerDocument = 1;
                //barcodeReader.MaxNumberOfBarcodesPerPage = 1;
                barcodeReader.FastMode = true;
                barcodeReader.GetPdfPageCount(QRPDFFiles);

                FoundBarcode[] barcodes = barcodeReader.ReadFrom(QRPDFFiles);

                try
                {
                    foreach (FoundBarcode barcode in barcodes)
                    {
                        DJPLink = barcode.Value.ToString();

                        if (DJPLink != "" || DJPLink != null)
                        {
                            // DJPLink = DJPLink.Replace(">TRIAL VERSION EXPIRES 90 DAYS AFTER INSTALLATION<", "");
                            ScanQRRepostitory.InsertQRUrl(QRPDFFiles, DJPLink);//insert DJP URL to DB                          

                            if (File.Exists(success))
                                File.Delete(success);

                            File.Move(QRPDFFiles, success);
                            SyncLog.InsertLog(DJPLink, fileName, "QR_INF", "Success Read DJP URL file moved to " + success);

                            countResult++;
                        }
                        else
                        {
                            if (File.Exists(fail))
                                File.Delete(fail);

                            File.Move(QRPDFFiles, fail);
                            SyncLog.InsertLog(DJPLink, fileName, "QR_ERR", "Barcode failed to read by system, file moved to " + fail);
                        }
                    }
                }
                catch (Exception ex)
                {
                    File.Move(QRPDFFiles, fail);
                    SyncLog.InsertLog(DJPLink, fileName, "QR_ERR", "Error: " + ex.Message + " Read DJP URL File moved to " + fail);
                }
            }
            if (countResult > 0)
            {
                //ScanQRRepostitory.InsertVATIn();
            }
        }
        #endregion

        #region New ScanQR

        public static void NewScanQR()
        {
            GetQRPath();

            string fileName = "";
            DirectoryInfo info = new DirectoryInfo(SourcePath);
            FileInfo[] files = info.GetFiles().OrderBy(p => p.Name).ToArray();

            foreach (FileInfo file in files)
            {
                fileName = file.Name;
                QRPDFFiles = Path.GetFullPath(SourcePath + fileName);

                string fail = Path.Combine(DestinationPathFail + fileName);
                string process = Path.Combine(DestinationBatch + fileName);

                try
                {
                    //tambah try catch 20190227
                    //case dari mas hadid, Could not read document footer: Damaged PDF document.
                    Reader barcodeReader = new Reader("1_DEVELOPER_LICENSE_FOR_DESKTOP_DEVELOPER_LICENSE_2D_BARCODES_ONLY_DESKTOP_APPS_ONLY_MAHENDRA.SONDAY@EMERIOCORP.COM_SUPPORT_AND_UPDATES_UNTIL_APRIL_25_2020", "E702-4936-D399-2BC9-8239-D07D-27B");

                    // barcodeReader.PDFRenderingResolution = 200;
                    barcodeReader.ImagePreprocessingFilters.AddScale(0.75d);


                    barcodeReader.BarcodeTypesToFind.QRCode = true;

                    barcodeReader.FastMode = true;

                    var _indexPages = barcodeReader.GetPdfPageCount(QRPDFFiles);
                    var indpages = _indexPages > 0 ? _indexPages - 1 : 0;
                    FoundBarcode[] barcodesDataPage;
                    var _page = 0;
                    for (int i = indpages; i >= 0; i--)
                    {
                        barcodesDataPage = barcodeReader.ReadFrom(QRPDFFiles, i, _indexPages);
                        if (barcodesDataPage.Count() > 0) //validasi kalo barcode nya gak kebaca
                        {
                            _page = barcodesDataPage[0].Page;
                            break;
                        }


                    }


                    FoundBarcode[] barcodes = barcodeReader.ReadFrom(QRPDFFiles, _page, _indexPages);

                    if (barcodes.Count() > 0) //validasi kalo barcode nya gak kebaca
                    {
                        try
                        {
                            foreach (FoundBarcode barcode in barcodes)
                            {
                                DJPLink = barcode.Value.ToString();

                                if (DJPLink != "" || DJPLink != null)
                                {
                                    // DJPLink = DJPLink.Replace(">TRIAL VERSION EXPIRES 90 DAYS AFTER INSTALLATION<", "");
                                    ScanQRRepostitory.InsertQR(DJPLink, fileName, QRPDFFiles, fail, process);
                                }
                                else
                                {
                                    SyncLog.InsertLog(DJPLink, fileName, "QR_ERR", "Fail to read file, QR not recognize, file moved to folder QR_Failed");
                                    //ScanQRRepostitory.UpdateQRStatus(DJPLink, "F");

                                    if (File.Exists(fail))
                                    {
                                        File.Delete(fail);
                                    }
                                    File.Move(QRPDFFiles, fail);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            SyncLog.InsertLog(DJPLink, fileName, "QR_ERR", ex.Message);
                            //ScanQRRepostitory.UpdateQRStatus(DJPLink, "F");

                            if (File.Exists(fail))
                            {
                                File.Delete(fail);
                            }
                            File.Move(QRPDFFiles, fail);
                        }
                    }

                    else
                    {

                        SyncLog.InsertLog("", fileName, "QR_ERR", "File DJP tidak terbaca oleh system, File di pindahkan ke folder QR_Failed");
                        if (File.Exists(fail))
                        {
                            File.Delete(fail);
                        }
                        File.Move(QRPDFFiles, fail);
                    }

                }
                catch (Exception)
                {
                    SyncLog.InsertLog("", fileName, "QR_ERR", "Fail to read file, QR not recognize, file moved to folder QR_Failed");

                    if (File.Exists(fail))
                    {
                        File.Delete(fail);
                    }
                    File.Move(QRPDFFiles, fail);
                }
            }
        }

        public static void ValidateScanQR()
        {
            ScanQRRepostitory.ValidateQR();
        }

        #endregion


        #region Rename QR

        private static void RenameQR()
        {
            GetQRPath();

            string fileName = "";
            DirectoryInfo info = new DirectoryInfo(RenameInputQR);
            FileInfo[] files = info.GetFiles().OrderBy(p => p.Name).ToArray();

            foreach (FileInfo file in files)
            {
                fileName = file.Name;
                QRPDFFiles = Path.GetFullPath(RenameInputQR + fileName);

                string fail = Path.Combine(RenameFailQR + fileName);

                try
                {
                    //https://secure.bytescout.com/dashboard
                    Reader barcodeReader = new Reader("1_DEVELOPER_LICENSE_FOR_DESKTOP_DEVELOPER_LICENSE_2D_BARCODES_ONLY_DESKTOP_APPS_ONLY_MAHENDRA.SONDAY@EMERIOCORP.COM_SUPPORT_AND_UPDATES_UNTIL_APRIL_25_2020", "E702-4936-D399-2BC9-8239-D07D-27B");
                    barcodeReader.BarcodeTypesToFind.QRCode = true;
                    //barcodeReader.PDFReadingMode = PDFReadingMode.ExtractEmbeddedImagesOnly;
                    //barcodeReader.MaxNumberOfBarcodesPerDocument = 1;
                    //barcodeReader.MaxNumberOfBarcodesPerPage = 1;
                    barcodeReader.FastMode = true;
                    var _indexPages = barcodeReader.GetPdfPageCount(QRPDFFiles);
                    var indpages = _indexPages > 0 ? _indexPages - 1 : 0;
                    FoundBarcode[] barcodesDataPage;
                    var _page = 0;
                    for (int i = indpages; i >= 0; i--)
                    {
                        barcodesDataPage = barcodeReader.ReadFrom(QRPDFFiles, i, _indexPages);
                        if (barcodesDataPage.Count() > 0) //validasi kalo barcode nya gak kebaca
                        {
                            _page = barcodesDataPage[0].Page;
                            break;
                        }

                    }
                    FoundBarcode[] barcodes = barcodeReader.ReadFrom(QRPDFFiles, _page, _indexPages);
                    if (barcodes.Count() > 0)
                    {
                        try
                        {
                            foreach (FoundBarcode barcode in barcodes)
                            {
                                DJPLink = barcode.Value.ToString();

                                if (DJPLink != "" || DJPLink != null)
                                {
                                    // DJPLink = DJPLink.Replace(">TRIAL VERSION EXPIRES 90 DAYS AFTER INSTALLATION<", "");
                                    ScanQRRepostitory.RenameQR(fileName, DJPLink, RenameInputQR, RenameOutputQR, RenameFailQR);
                                }
                                else
                                {
                                    SyncLog.InsertLog(DJPLink, fileName, "RENAME_ERR", "Fail to read file, QR not recognize, file moved to folder Failed");
                                    if (File.Exists(fail))
                                    {
                                        File.Delete(fail);
                                    }
                                    File.Move(QRPDFFiles, fail);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            SyncLog.InsertLog(DJPLink, fileName, "RENAME_ERR", ex.Message);

                            if (File.Exists(fail))
                            {
                                File.Delete(fail);
                            }
                            File.Move(QRPDFFiles, fail);
                        }
                    }
                    else
                    {
                        SyncLog.InsertLog("", fileName, "RENAME_ERR", "File DJP tidak terbaca oleh system, File di pindahkan ke folder Failed");
                        if (File.Exists(fail))
                        {
                            File.Delete(fail);
                        }
                        File.Move(QRPDFFiles, fail);
                    }
                }
                catch (Exception)
                {
                    SyncLog.InsertLog("", fileName, "QR_ERR", "Fail to read file, QR not recognize, file moved to folder Failed");

                    if (File.Exists(fail))
                    {
                        File.Delete(fail);
                    }
                    File.Move(QRPDFFiles, fail);
                }
            }
        }

        #endregion

        public static void GetQRPath()
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_Get_QRPDFPath";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.CommandTimeout = 0;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            rd.Read();

            SourcePath = rd[0].ToString(); //QRSourcePath
            DestinationPath = rd[1].ToString(); //QRDestinationPath
            DestinationPathFail = rd[2].ToString(); //QRFailPath
            DestinationBatch = rd[3].ToString(); //QRProcessPath
            RenameOutputQR = rd[4].ToString();
            RenameInputQR = rd[5].ToString();
            RenameFailQR = rd[6].ToString();

            cn.CloseConnection();
            cn.DisposeConnection();
        }

        #region data ebupot
        public static void eBupotArchiving()
        {
            GeteBupotNumber();
        }
        #endregion

        #region data ebupot
        public static void eBupotArchivingProcess()
        {
            GetEbupot();

            string ProcessfileName = "";



            DirectoryInfo info = new DirectoryInfo(eBupotProcess);
            FileInfo[] filesProcess = info.GetFiles().OrderBy(p => p.Name).ToArray();
            if (filesProcess.Count() > 0)
            {
                foreach (FileInfo fileProcess in filesProcess)
                {
                    ProcessfileName = fileProcess.Name;
                    eBupotDFFilesProcess = Path.GetFullPath(eBupotProcess + ProcessfileName);



                    string Success = Path.Combine(eBupotSuccess + ProcessfileName);
                    string fail = Path.Combine(eBupotFail + ProcessfileName);

                    try
                    {
                        string extension = Path.GetExtension(eBupotDFFilesProcess);

                        int isSuccess = MatchingDataWHT.SaveMatchingDataWHT(ProcessfileName.Replace(extension, ""), eBupotSource, "021161153092000_" + ProcessfileName, eBupotSuccess, ProcessfileName, eBupotSource);
                        if (isSuccess == 1)
                        {
                            if (File.Exists(Success))
                            {
                                File.Delete(Success);
                            }
                            File.Move(eBupotDFFilesProcess, Success);
                            SyncLog.InsertLog("1", ProcessfileName, "eBupot_INF", "Process Success");
                        }
                        else
                        {
                            if (File.Exists(fail))
                            {
                                File.Delete(fail);
                            }
                            File.Move(eBupotDFFilesProcess, fail);
                            SyncLog.InsertLog("1", ProcessfileName, "eBupot_ERR", "eBupot number not found");
                        }
                        //File.Move(SourcePathGL + FileName, SourcePathGLSuccess + NewFileName);


                    }
                    catch (Exception ex)
                    {
                        SyncLog.InsertLog("1", eBupotProcess, "eBupot_ERR", ex.Message);

                        if (File.Exists(fail))
                        {
                            File.Delete(fail);
                        }
                        File.Move(eBupotDFFilesProcess, fail);
                    }
                }
            }
        }

        public static void PrepaidWHTTaxType(String Taxtype)
        {

            GetPrepaidWHT(Taxtype);
            string ProcessfileName = "";



            DirectoryInfo info = new DirectoryInfo(PrepaidWHTProcess);
            FileInfo[] filesProcess = info.GetFiles().OrderBy(p => p.Name).ToArray();
            if (filesProcess.Count() > 0)
            {
                foreach (FileInfo fileProcess in filesProcess)
                {
                    ProcessfileName = fileProcess.Name;
                    PrepaidWHTFilesProcess = Path.GetFullPath(PrepaidWHTProcess + ProcessfileName);



                    string Success = Path.Combine(PrepaidWHTSuccess + ProcessfileName);
                    string fail = Path.Combine(PrepaidWHTFail + ProcessfileName);

                    try
                    {
                        string extension = Path.GetExtension(PrepaidWHTFilesProcess);

                        int isSuccess = PrepaidWHT.PrepaidWHT.SavePrepaidWHT(ProcessfileName.Replace(extension, ""), PrepaidWHTProcess, ProcessfileName, PrepaidWHTSuccess, ProcessfileName, fail);
                        if (isSuccess == 1)
                        {
                            if (File.Exists(Success))
                            {
                                File.Delete(Success);
                            }
                            File.Move(PrepaidWHTFilesProcess, Success);
                            File.Delete(fail);
                            SyncLog.InsertLog("1", ProcessfileName, "Prepaid_WHT_INF", "Process Success");
                        }
                        else
                        {
                            if (File.Exists(fail))
                            {
                                File.Delete(fail);
                            }
                            File.Move(PrepaidWHTFilesProcess, fail);
                            string errmsg = "";
                            if (isSuccess == 0)
                            {
                                errmsg = "nomor bukti potong tidak ada";
                            }
                            else if (isSuccess == 2)
                            {
                                errmsg = "Bukti Potong bukan Tax Article 22, 23 dan 4 Ayat 2";
                            }
                            SyncLog.InsertLog("1", ProcessfileName, "Prepaid_WHT_ERR", errmsg);
                        }
                        //File.Move(SourcePathGL + FileName, SourcePathGLSuccess + NewFileName);


                    }
                    catch (Exception ex)
                    {
                        SyncLog.InsertLog("1", fail, "Prepaid_WHT_ERR", ex.Message);

                        if (File.Exists(fail))
                        {
                            File.Delete(fail);
                        }
                        File.Move(PrepaidWHTFilesProcess, fail);
                    }
                }
            }
        }


        public static void GeteBupotNumber()
        {
            try
            {
                GetEbupot();

                try
                {




                    string SourcefileName = "";

                    DirectoryInfo info = new DirectoryInfo(eBupotSource);
                    FileInfo[] filesSource = info.GetFiles().OrderBy(p => p.Name).ToArray();
                    if (filesSource.Count() > 0)
                    {
                        foreach (FileInfo fileSource in filesSource)
                        {
                            SourcefileName = fileSource.Name;
                            eBupotDFFiles = Path.GetFullPath(eBupotSource + SourcefileName);


                            string process = Path.Combine(eBupotProcess + SourcefileName);
                            if (!File.Exists(process))
                            {
                                File.Move(eBupotDFFiles, process);
                            }

                            //File.Move(SourcePathGL + FileName, SourcePathGLSuccess + NewFileName);

                        }

                    }
                }
                catch (Exception ex)
                {

                    SyncLog.InsertLog("0", "", "eBupot_ERR", ex.Message);
                    throw;
                }

                Connection cn = new Connection();
                cn.CreateConnection();
                cn.OpenConnection();

                cn._sqlCommand.CommandText = "usp_GeteBupotNumber";
                cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader rd = cn._sqlCommand.ExecuteReader();
                while (rd.Read())
                {



                    try
                    {


                        try
                        {




                            string processfileName = "";
                            string NewFileName = "";
                            DirectoryInfo info = new DirectoryInfo(eBupotProcess);
                            FileInfo[] processSource = info.GetFiles().OrderBy(p => p.Name).ToArray();
                            if (processSource.Count() > 0)
                            {
                                foreach (FileInfo fileprocess in processSource)
                                {
                                    processfileName = fileprocess.Name;
                                    eBupotDFFiles = Path.GetFullPath(eBupotProcess + processfileName);


                                    if (processfileName.Contains(rd["eBupotNumber"].ToString()) == true)
                                    {
                                        NewFileName = rd["eBupotNumber"].ToString() + ".pdf";
                                        string process = Path.Combine(eBupotProcess + NewFileName);
                                        if (File.Exists(process))
                                        {
                                            File.Delete(process);
                                        }
                                        File.Move(eBupotDFFiles, process);

                                        if (File.Exists(eBupotDFFiles))
                                        {
                                            File.Delete(eBupotDFFiles);
                                        }
                                        // File.Move(SourcePathGL + FileName, SourcePathGLSuccess + NewFileName);
                                        SyncLog.InsertLog("1", NewFileName, "eBupot_INF", "Process Success");
                                    }
                                    //else
                                    //{
                                    //    SyncLog.InsertLog("0", SourcefileName, "eBupot_ERR", "fail");

                                    //    if (File.Exists(fail))
                                    //    {
                                    //        File.Delete(fail);
                                    //    }
                                    //    File.Move(eBupotDFFiles, fail);
                                    //}



                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            SyncLog.InsertLog("0", "", "eBupot_ERR", ex.Message);
                            throw;
                        }






                    }
                    catch (Exception ex)
                    {
                        SyncLog.InsertLog("0", "", "eBupot_ERR", ex.Message);

                    }
                }
                cn.CloseConnection();
                cn.DisposeConnection();
            }
            catch (Exception ex)
            {
                SyncLog.InsertLog("1", eBupotProcess, "eBupot_ERR", ex.Message);

            }
        }
        #endregion

        public static void GetEbupot()
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_Get_eBupotPath";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.CommandTimeout = 0;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            rd.Read();

            eBupotSource = rd["eBupotSource"].ToString();
            eBupotProcess = rd["eBupotProcess"].ToString();
            eBupotSuccess = rd["eBupotSuccess"].ToString();
            eBupotFail = rd["eBupotFail"].ToString();


            cn.CloseConnection();
            cn.DisposeConnection();
        }

        public static string GetConfigValueByConfigKey(string ConfigKey)
        {
            string Value = "";

            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "efb_sp_GetConfigValueByConfigKey";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@ConfigKey", ConfigKey);
            cn._sqlCommand.CommandTimeout = 0;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            if (rd.Read())
            {
                Value = rd[0].ToString(); ;
            }

            cn.CloseConnection();
            cn.DisposeConnection();

            return Value;
        }

        public static void GetPrepaidWHT(string TypeTax)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_Get_PrepaidWHTPath";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@TypeTax", TypeTax);
            cn._sqlCommand.CommandTimeout = 0;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            rd.Read();


            PrepaidWHTProcess = rd["PrepaidWHTProcess"].ToString();
            PrepaidWHTSuccess = rd["PrepaidWHTSuccess"].ToString();
            PrepaidWHTFail = rd["PrepaidWHTFail"].ToString();


            cn.CloseConnection();
            cn.DisposeConnection();
        }

        private class EmailModel
        {
            public string Id { get; set; }
            public MailMessage msg { get; set; }
        }
    }
}
