﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAM.Scheduler;
using System.Data.SqlClient;
using System.Configuration;

namespace TAM.Scheduler
{
    public class Config
    {
        public static void GetPath()
        {
            Connection cn = new Connection();
            string SourcePathParam = "";

            SqlCommand cmd = new SqlCommand("Select ConfigValue from TB_M_ConfigurationData where ConfigKey = 'SourcePDFPPH'");
            SqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                SourcePathParam = rd.GetString(0);
            }
        }
    }
}
