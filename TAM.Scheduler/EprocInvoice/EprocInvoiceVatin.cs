﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.VATIn;
using TAM.Scheduler.OCR;
using TAM.Scheduler.QR;

namespace TAM.Scheduler.EprocInvoice
{
    class EprocInvoiceVatin
    {
        public static Guid SyncID { get; set; }

        public static string saveDataEprocInvoice(VATInManualInputViewEprocModel model, Guid SyncID)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_InsertCustom_TB_R_VATIn_Eproc";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@Id", model.Id);
            cn._sqlCommand.Parameters.AddWithValue("@TransactionId", model.TransactionId);
            cn._sqlCommand.Parameters.AddWithValue("@SyncId", SyncID);
            cn._sqlCommand.Parameters.AddWithValue("@KDJenisTransaksi", model.KDJenisTransaksi);
            cn._sqlCommand.Parameters.AddWithValue("@FGPengganti", model.FGPengganti);
            cn._sqlCommand.Parameters.AddWithValue("@NomorFaktur", model.InvoiceNumber);
            cn._sqlCommand.Parameters.AddWithValue("@NomorFakturGabungan", model.InvoiceNumberFull);
            cn._sqlCommand.Parameters.AddWithValue("@TanggalFaktur", model.InvoiceDate);
            //cn._sqlCommand.Parameters.AddWithValue("@ExpireDate", Convert.ToDateTime(model.ExpireDate));
            cn._sqlCommand.Parameters.AddWithValue("@NPWPPenjual", model.SupplierNPWP);
            cn._sqlCommand.Parameters.AddWithValue("@NamaPenjual", model.SupplierName);
            cn._sqlCommand.Parameters.AddWithValue("@AlamatPenjual", model.SupplierAddress);
            cn._sqlCommand.Parameters.AddWithValue("@JumlahDPP", model.VATBaseAmount);
            cn._sqlCommand.Parameters.AddWithValue("@JumlahPPN", model.VATAmount);
            cn._sqlCommand.Parameters.AddWithValue("@JumlahPPNBM", model.JumlahPPnBM);
            cn._sqlCommand.Parameters.AddWithValue("@StatusApprovalXML", model.StatusApprovalXML);
            cn._sqlCommand.Parameters.AddWithValue("@StatusFakturXML", model.StatusFakturXML);
            cn._sqlCommand.Parameters.AddWithValue("@FakturType", "eFaktur");
            cn._sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
            //cn._sqlCommand.Parameters.AddWithValue("@CreatedBy", model.CreatedByPV);

            //cn._sqlCommand.Parameters.AddWithValue("@NomorInvoice", model.NomorInvoice);
            //cn._sqlCommand.Parameters.AddWithValue("@StatusInvoice", model.StatusInvoice);
            //cn._sqlCommand.Parameters.AddWithValue("@CreatedByPV", model.CreatedByPV);
            //cn._sqlCommand.Parameters.AddWithValue("@StatusEPROC", model.StatusEPROC);
            //cn._sqlCommand.Parameters.AddWithValue("@PVNumber", model.PVNumber);
            //cn._sqlCommand.Parameters.AddWithValue("@PostingDate", model.PostingDate);

            //cn._sqlCommand.Parameters.AddWithValue("@SAPDocNumber", model.SAPDocNumber);
            //cn._sqlCommand.Parameters.AddWithValue("@TurnOverAmount", model.TurnOverAmount);

            //model.NomorInvoice = dataModal.InvoiceNumber;
            //model.StatusInvoice = dataModal.StatusInvoice;
            //model.StatusEPROC = dataModal.StatusEPROC;
            //model.PostingDate = dataModal.PostingDate;
            //model.PVNumber = dataModal.PVNumber; 
            //model.CreatedByPV = dataModal.InvoiceSubmittedBy;


            cn._sqlCommand.Parameters.AddWithValue("@VATInDetail", createDetailTable(model.VATInEprocDetails));
           
            SqlParameter OnActionData = new SqlParameter("@OnActionData", SqlDbType.VarChar,10);
            OnActionData.Direction = System.Data.ParameterDirection.Output;
            cn._sqlCommand.Parameters.Add(OnActionData);
            cn._sqlCommand.ExecuteNonQuery(); //record masuk

            string _OnActionData = cn._sqlCommand.Parameters["@OnActionData"].Value.ToString();
            //cn._sqlCommand.ExecuteReader();

            cn.CloseConnection();
            cn.DisposeConnection();

            return _OnActionData;

            //SqlParameter retval = sqlcomm.Parameters.Add("@b", SqlDbType.Bit);
            //retval.Direction = ParameterDirection.ReturnValue;
            //sqlcomm.ExecuteNonQuery();
            //string retunvalue = Convert.ToBoolean(sqlcomm.Parameters["@b"].Value);
            // return OnActionData;


        }

        public static void saveDataEprocInvoiceNonEfaktur(VATInManualInputViewEprocModel model, Guid SyncID)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_InsertCustom_TB_R_VATIn_Eproc_NonEfaktur";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@Id", model.Id);
            cn._sqlCommand.Parameters.AddWithValue("@TransactionId", model.TransactionId);
            cn._sqlCommand.Parameters.AddWithValue("@SyncId", SyncID);
            cn._sqlCommand.Parameters.AddWithValue("@KDJenisTransaksi", "01"); // model.KDJenisTransaksi);
            cn._sqlCommand.Parameters.AddWithValue("@FGPengganti", model.FGPengganti);
            cn._sqlCommand.Parameters.AddWithValue("@NomorFaktur", model.InvoiceNumber);
            cn._sqlCommand.Parameters.AddWithValue("@NomorFakturGabungan", model.InvoiceNumber);// model.InvoiceNumberFull);
            cn._sqlCommand.Parameters.AddWithValue("@TanggalFaktur", model.TaxInvoiceDate);
            cn._sqlCommand.Parameters.AddWithValue("@ExpireDate", model.ExpireDate);
            cn._sqlCommand.Parameters.AddWithValue("@NPWPPenjual", model.SupplierNPWP);
            cn._sqlCommand.Parameters.AddWithValue("@NamaPenjual", model.SupplierName);
            cn._sqlCommand.Parameters.AddWithValue("@AlamatPenjual", model.SupplierAddress);
            cn._sqlCommand.Parameters.AddWithValue("@JumlahDPP", model.VATBaseAmount);
            cn._sqlCommand.Parameters.AddWithValue("@JumlahPPN", model.VATAmount);
            cn._sqlCommand.Parameters.AddWithValue("@JumlahPPNBM", 0); // model.JumlahPPnBM);
            cn._sqlCommand.Parameters.AddWithValue("@StatusApprovalXML", ""); // model.StatusApprovalXML);
            cn._sqlCommand.Parameters.AddWithValue("@StatusFakturXML", ""); // model.StatusFakturXML);
            cn._sqlCommand.Parameters.AddWithValue("@FakturType", "Non eFaktur");
            cn._sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now); // model.SendDateTime);
            cn._sqlCommand.Parameters.AddWithValue("@CreatedBy", model.CreatedByPV);
            cn._sqlCommand.Parameters.AddWithValue("@NomorInvoice", model.NomorInvoice);
            cn._sqlCommand.Parameters.AddWithValue("@StatusInvoice", model.StatusInvoice);
            cn._sqlCommand.Parameters.AddWithValue("@CreatedByPV", model.CreatedByPV);
            cn._sqlCommand.Parameters.AddWithValue("@StatusEPROC", model.StatusEPROC);
            cn._sqlCommand.Parameters.AddWithValue("@PVNumber", model.PVNumber);
            cn._sqlCommand.Parameters.AddWithValue("@PostingDate", model.PostingDate);
            cn._sqlCommand.Parameters.AddWithValue("@OnActionData","");
            cn._sqlCommand.Parameters.AddWithValue("@SAPDocNumber", model.SAPDocNumber);
            cn._sqlCommand.Parameters.AddWithValue("@TurnOverAmount", model.TurnOverAmount);
            cn._sqlCommand.ExecuteReader();
            cn.CloseConnection();
            cn.DisposeConnection();
        }

        //public static void saveDataDetailEprocInvoice(VATInDetailManualInputViewEprocModel detail, Guid model)
        //{
        //    Connection cn = new Connection();
        //    cn.CreateConnection();
        //    cn.OpenConnection();

        //    cn._sqlCommand.CommandText = "usp_InsertCustom_TB_R_VATInDetail";
        //    cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
        //    cn._sqlCommand.Parameters.AddWithValue("@Id", Guid.NewGuid());
        //    cn._sqlCommand.Parameters.AddWithValue("@VATInId", model);
        //    cn._sqlCommand.Parameters.AddWithValue("@NamaBarang", detail.UnitName);
        //    cn._sqlCommand.Parameters.AddWithValue("@HargaSatuan", detail.UnitPrice);
        //    cn._sqlCommand.Parameters.AddWithValue("@JumlahBarang", detail.Quantity);
        //    cn._sqlCommand.Parameters.AddWithValue("@HargaTotal", detail.TotalPrice);
        //    cn._sqlCommand.Parameters.AddWithValue("@Diskon", detail.Discount);
        //    cn._sqlCommand.Parameters.AddWithValue("@DPP", detail.DPP);
        //    cn._sqlCommand.Parameters.AddWithValue("@PPN", detail.PPN);
        //    cn._sqlCommand.Parameters.AddWithValue("@TarifPPNBM", detail.TarifPPNBM);
        //    cn._sqlCommand.Parameters.AddWithValue("@PPNBM", detail.PPNBM);
        //    cn._sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
        //    cn._sqlCommand.Parameters.AddWithValue("@CreatedBy", "System");
        //    cn._sqlCommand.ExecuteReader();
        //    cn.CloseConnection();
        //    cn.DisposeConnection();
        //}

        public static DataTable createDetailTable(IList<VATInDetailManualInputViewEprocModel> _vATInDetailManualInputViewEprocModel)
        {
            var table = new DataTable();
            table.Columns.Add("tpid", typeof(Guid));
            table.Columns.Add("NamaBarang", typeof(string));
            table.Columns.Add("HargaSatuan", typeof(decimal));
            table.Columns.Add("JumlahBarang", typeof(decimal));
            table.Columns.Add("HargaTotal", typeof(decimal));
            table.Columns.Add("Diskon", typeof(decimal));
            table.Columns.Add("DPP", typeof(decimal));
            table.Columns.Add("PPN", typeof(decimal));
            table.Columns.Add("TarifPPNBM", typeof(decimal));
            table.Columns.Add("PPNBM", typeof(decimal));
            table.Columns.Add("CreatedOn", typeof(DateTime));
            table.Columns.Add("CreatedBy", typeof(string));
            foreach (var val in _vATInDetailManualInputViewEprocModel)
            {
                DataRow dr = table.NewRow();
                dr["tpid"] = val.tpid;
                dr["NamaBarang"] = val.UnitName;
                dr["HargaSatuan"] = val.UnitPrice;
                dr["JumlahBarang"] = val.Quantity;
                dr["HargaTotal"] = val.TotalPrice;
                dr["Diskon"] = val.Discount;
                dr["DPP"] = val.DPP;
                dr["PPN"] = val.PPN;
                dr["TarifPPNBM"] = val.TarifPPNBM;
                dr["PPNBM"] = val.PPNBM;
                dr["CreatedOn"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                dr["CreatedBy"] = "System";
                table.Rows.Add(dr);
            }



            return table;
        }

        public static void InsertEproc(VATInManualInputViewEprocModel dataModal, string id, Guid SyncID, string DJPLink)
        {
            try
            {
                #region
                var response = new HttpResponseMessage();
                resValidateFakturPm VATInXML = new resValidateFakturPm();
                VATInManualInputViewEprocModel model = new VATInManualInputViewEprocModel();
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
                    Uri serviceUri = new Uri(DJPLink, UriKind.Absolute);

                    response = client.GetAsync(serviceUri).Result;

                    var stream = new MemoryStream(response.Content.ReadAsByteArrayAsync().Result);

                    var serializer = new XmlSerializer(typeof(resValidateFakturPm));
                    using (var reader = XmlReader.Create(stream))
                    {
                        VATInXML = (resValidateFakturPm)serializer.Deserialize(reader);

                        if (string.IsNullOrEmpty(VATInXML.nomorFaktur))
                        {
                            if (VATInXML.statusApproval == "Faktur tidak Valid, Tidak ditemukan data di DJP")
                            {
                                SyncLog.InsertLog(DJPLink, "", "EPROC_VATIN_ERR", "Faktur tidak valid pada server DJP");
                                SyncLog.InsertLogEprocInvoice(id, "Faktur tidak valid pada server DJP");
                            }
                            else if (VATInXML.statusApproval == "Terjadi kesalahan sistem")
                            {
                                SyncLog.InsertLog(DJPLink, id, "EPROC_VATIN_ERR", "Server DJP tidak dapat terkoneksi");
                                SyncLog.InsertLogEprocInvoice(id, "Server DJP tidak dapat terkoneksi");
                            }
                        }
                        else
                        {
                            #region save
                            string InvoiceDate = VATInXML.tanggalFakturDateFormat.ToString("yyyy-MM-dd");
                            DateTime expdate = Convert.ToDateTime(InvoiceDate).AddDays(1).AddMonths(3).AddDays(-1);
                            DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);

                            string expireDateQR = endOfMonth.ToString("yyyy-MM-dd");

                            model.TransactionId = new Guid(id);
                            model.Id = Guid.NewGuid();
                            model.InvoiceNumber = VATInXML.nomorFaktur;
                            model.InvoiceNumberFull = VATInXML.nomorFaktur.FormatNomorFakturGabungan(VATInXML.kdJenisTransaksi, VATInXML.fgPengganti);
                            model.KDJenisTransaksi = VATInXML.kdJenisTransaksi;
                            model.FGPengganti = VATInXML.fgPengganti;
                            model.InvoiceDate = VATInXML.tanggalFakturDateFormat.FormatSQLDate();
                            //model.ExpireDate = dataModal.ExpireDate; //endOfMonth.ToString("yyyy-MM-dd");
                            model.SupplierNPWP = VATInXML.npwpPenjual.FormatNPWP();
                            model.SupplierName = VATInXML.namaPenjual;
                            model.SupplierAddress = VATInXML.alamatPenjual;
                            model.NPWPLawanTransaksi = VATInXML.npwpLawanTransaksi.FormatNPWP();
                            model.NamaLawanTransaksi = VATInXML.namaLawanTransaksi;
                            model.AlamatLawanTransaksi = VATInXML.alamatLawanTransaksi;
                            model.StatusApprovalXML = VATInXML.statusApproval;
                            model.StatusFakturXML = VATInXML.statusFaktur;
                            model.VATBaseAmount = VATInXML.jumlahDpp;
                            model.VATAmount = VATInXML.jumlahPpn;
                            model.JumlahPPnBM = VATInXML.jumlahPpnBm;

                            model.URL = DJPLink; 

                            //model.NomorInvoice = dataModal.InvoiceNumber;
                            //model.StatusInvoice = dataModal.StatusInvoice;
                            //model.StatusEPROC = dataModal.StatusEPROC;
                            //model.PostingDate = dataModal.PostingDate;
                            //model.PVNumber = dataModal.PVNumber; 
                            //model.CreatedByPV = dataModal.InvoiceSubmittedBy;
                            try
                            { 
                                #region Validasi 
                                string result = ValidateVATInQR.ValidateNPWP(model.NPWPLawanTransaksi);
                                if (result == "")
                                {
                                    if (model.StatusFakturXML != "Faktur Dibatalkan")
                                    {
                                        result = ValidateVATInQR.ValidateExpireDate(model.InvoiceDate);
                                        if (result == "")
                                        {
                                            //result = ValidateVATInQR.ValidateVATIn(model.InvoiceNumberFull, model.InvoiceNumber, model.FGPengganti, "eFaktur", model.SupplierNPWP, model.InvoiceDate);
                                            //if (result == "")
                                            //{
                                                #region Success Validasi 
                                                foreach (resValidateFakturPmDetailTransaksi detail in VATInXML.detailTransaksi)
                                                {
                                                    VATInDetailManualInputViewEprocModel detailModel = new VATInDetailManualInputViewEprocModel();
                                                    detailModel.tpid = Guid.NewGuid();
                                                    detailModel.UnitName = detail.nama;
                                                    detailModel.UnitPrice = detail.hargaSatuan;
                                                    detailModel.Quantity = detail.jumlahBarang;
                                                    detailModel.TotalPrice = detail.hargaTotal;
                                                    detailModel.Discount = detail.diskon;
                                                    detailModel.DPP = detail.dpp;
                                                    detailModel.PPN = detail.ppn;
                                                    detailModel.PPNBM = detail.ppnbm;
                                                    detailModel.TarifPPNBM = detail.tarifPpnbm;
                                                    try
                                                    {
                                                        model.VATInEprocDetails.Add(detailModel);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        SyncLog.InsertLog(DJPLink, id, "EPROC_VATIN_ERR", ex.Message);
                                                        SyncLog.InsertLogEprocInvoice(id, ex.Message);
                                                }
                                                }
                                                string dat = "";
                                                dat = saveDataEprocInvoice(model, SyncID);
                                                SyncLog.InsertLog(DJPLink, id, "EPROC_VATIN_INF", "Insert EPROC Success");
                                               
                                                #endregion
                                            //}
                                            //else
                                            //{
                                            //    SyncLog.InsertLogEprocInvoice(id, dataModal.PVNumber, dataModal.InvoiceNumber);
                                            //    SyncLog.InsertLog(DJPLink, id, "EPROC_VATIN_ERR", result);
                                            //}
                                        }
                                        else
                                        {
                                            SyncLog.InsertLogEprocInvoice(id, result);
                                            SyncLog.InsertLog(DJPLink, id, "EPROC_VATIN_ERR", result);

                                        }
                                    }
                                    else
                                    {
                                        SyncLog.InsertLogEprocInvoice(id, "Cannot save with status Faktur Dibatalkan");
                                        SyncLog.InsertLog(DJPLink, id, "EPROC_VATIN_ERR", "Cannot save with status Faktur Dibatalkan");
                                    } 
                                }
                                else
                                {
                                    SyncLog.InsertLogEprocInvoice(id, result);
                                    SyncLog.InsertLog(DJPLink, id, "EPROC_VATIN_ERR", result);
                                }
                                #endregion 
                            }
                            catch (Exception ex)
                            {
                                SyncLog.InsertLogEprocInvoice(id, ex.Message);
                                SyncLog.InsertLog(DJPLink, id, "EPROC_VATIN_ERR", ex.Message); 
                            } 
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {  
                SyncLog.InsertLog(DJPLink, id, "EPROC_VATIN_ERR", ex.Message);
                SyncLog.InsertLogEprocInvoice(id, ex.Message);
                //if (ex.Message != "One or more errors occurred.")
                //{
                //    SyncLog.InsertLogEprocInvoice(id, dataModal.PVNumber, dataModal.InvoiceNumber);
                //} 
            }
        }

        public static int EprocInvoiceDataVatinPaging(int pageNumber, int pageSize)
        {
            int cdata = 0;
           
            try
            {
                Connection cn = new Connection();
                cn.CreateConnection();
                cn.OpenConnection();

                cn._sqlCommand.CommandText = "usp_GetEprocInvoiceVATIN";
                cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                cn._sqlCommand.Parameters.AddWithValue("@PageNumber", pageNumber);
                cn._sqlCommand.Parameters.AddWithValue("@PageSize", pageSize);
                SqlDataReader rd = cn._sqlCommand.ExecuteReader();
                while (rd.Read())
                {

                    try
                    {
                        cdata = Convert.ToInt32(rd["c"].ToString());
                    }
                    catch (Exception ex)
                    {
                        SyncLog.InsertLog("", "CountData1", "EPROC_VATIN_ERR", ex.Message);
                    }
                }
                cn.CloseConnection();
                cn.DisposeConnection();

                return cdata;
            }
            catch (Exception ex)
            {
                SyncLog.InsertLog("", "CountData2", "EPROC_VATIN_ERR", ex.Message);
                return 0;
            }
        }

        public static void EprocInvoiceDataVatin()
        {

            int vatintotal = 0;
            vatintotal= EprocInvoiceDataVatinPaging(1, 1);
            int _pageNumber = 1;
            int _pageSize=10;
            int _page = 0;
            int _pagemid = 0;
            _page = vatintotal / _pageSize;
            _pagemid = (vatintotal % _pageSize) > 0 ? 1:0 ;
            _pageNumber = _page + _pagemid;

            for (int i = 1; i <= _pageNumber; i++)
            {
                string URL = "";
                string TransactionId = "";

                try
                {
                    Connection cn = new Connection();
                    cn.CreateConnection();
                    cn.OpenConnection();

                    cn._sqlCommand.CommandText = "usp_GetEprocInvoiceVATIN";
                    cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    cn._sqlCommand.Parameters.AddWithValue("@PageNumber", 1);
                    cn._sqlCommand.Parameters.AddWithValue("@PageSize", _pageSize);
                    SqlDataReader rd = cn._sqlCommand.ExecuteReader();
                    while (rd.Read())
                    {
                        VATInManualInputViewEprocModel model = new VATInManualInputViewEprocModel();

                        model.Id = Guid.NewGuid();
                        SyncID = Guid.NewGuid();
                        URL = rd["URLVATVerification"].ToString();
                        TransactionId = rd["TransactionId"].ToString();

                        model.TransactionId = new Guid(TransactionId);

                        model.InvoiceNumber = rd["VendorInvoiceNumber"].ToString();
                       
                        model.PVNumber = rd["InvoiceCode"].ToString();
                       

                        try
                        {
                            string StatusFaktur = rd["VendorEFakturFlag"].ToString();
                            // eFaktur 1 || 0 non e-Faktur
                            if (StatusFaktur == "True")
                                InsertEproc(model, TransactionId, SyncID, URL);
                            else
                                InsertEprocNonEfaktur(model, TransactionId, SyncID);
                        }
                        catch (Exception ex)
                        {
                            SyncLog.InsertLog(URL, TransactionId, "EPROC_VATIN_ERR", ex.Message);
                            SyncLog.InsertLogEprocInvoice(TransactionId, ex.Message);
                        }
                    }
                    cn.CloseConnection();
                    cn.DisposeConnection();
                }
                catch (Exception ex)
                {
                    SyncLog.InsertLog(URL, TransactionId, "EPROC_VATIN_ERR", ex.Message);
                    SyncLog.InsertLogEprocInvoice(TransactionId, ex.Message);
                }
            }
        }

        public static void InsertEprocNonEfaktur(VATInManualInputViewEprocModel model, string TransactionId, Guid SyncID)
        { 
            try
            {
                Connection cn = new Connection();
                cn.CreateConnection();
                cn.OpenConnection();

                cn._sqlCommand.CommandText = "usp_InsertCustom_TB_R_VATIn_Eproc_NonEfaktur";
                cn._sqlCommand.CommandType = CommandType.StoredProcedure;
                //cn._sqlCommand.Parameters.AddWithValue("@Id", model.Id);
                cn._sqlCommand.Parameters.AddWithValue("@TransactionId", model.TransactionId);
                //cn._sqlCommand.Parameters.AddWithValue("@SyncId", SyncID);
                //cn._sqlCommand.Parameters.AddWithValue("@FGPengganti", model.FGPengganti);
                //cn._sqlCommand.Parameters.AddWithValue("@NomorFaktur", model.InvoiceNumber);
                //cn._sqlCommand.Parameters.AddWithValue("@NomorFakturGabungan", model.InvoiceNumber);
                //cn._sqlCommand.Parameters.AddWithValue("@TanggalFaktur", model.TaxInvoiceDate);
                //cn._sqlCommand.Parameters.AddWithValue("@ExpireDate", model.ExpireDate);
                //cn._sqlCommand.Parameters.AddWithValue("@NPWPPenjual", model.SupplierNPWP);
                //cn._sqlCommand.Parameters.AddWithValue("@NamaPenjual", model.SupplierName);
                //cn._sqlCommand.Parameters.AddWithValue("@AlamatPenjual", model.SupplierAddress);
                //cn._sqlCommand.Parameters.AddWithValue("@JumlahDPP", model.VATBaseAmount);
                //cn._sqlCommand.Parameters.AddWithValue("@JumlahPPN", model.VATAmount); 
                //cn._sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                //cn._sqlCommand.Parameters.AddWithValue("@CreatedBy", model.CreatedByPV);
                //cn._sqlCommand.Parameters.AddWithValue("@NomorInvoice", model.NomorInvoice);
                //cn._sqlCommand.Parameters.AddWithValue("@StatusInvoice", model.StatusInvoice);
                //cn._sqlCommand.Parameters.AddWithValue("@CreatedByPV", model.CreatedByPV);
                //cn._sqlCommand.Parameters.AddWithValue("@StatusEPROC", model.StatusEPROC);
                //cn._sqlCommand.Parameters.AddWithValue("@PVNumber", model.PVNumber);
                //cn._sqlCommand.Parameters.AddWithValue("@PostingDate", model.PostingDate);
                //cn._sqlCommand.Parameters.AddWithValue("@SAPDocNumber", model.SAPDocNumber);
                //cn._sqlCommand.Parameters.AddWithValue("@TurnOverAmount", model.TurnOverAmount);
                cn._sqlCommand.ExecuteReader();
                cn.CloseConnection();
                cn.DisposeConnection();

                SyncLog.InsertLog("", TransactionId, "EPROC_VATIN_NON_EFAKTUR_INF", "Insert EPROC Success");
                try
                {
                  //  Syncdata.InsertSync("", SyncID);
                    SyncLog.InsertLog("", TransactionId, "EPROC_VATIN_NON_EFAKTUR_INF", "Insert SyncData Success");
                }
                catch (Exception ex)
                {
                    SyncLog.InsertLog("", TransactionId, "EPROC_VATIN_NON_EFAKTUR_ERR", ex.Message);
                    SyncLog.InsertLogEprocInvoice(TransactionId, ex.Message);
                }
            }
            catch (Exception ex)
            {
                SyncLog.InsertLog("", TransactionId, "EPROC_VATIN_NON_EFAKTUR_ERR", ex.Message);
                SyncLog.InsertLogEprocInvoice(TransactionId, ex.Message);
            }
        }
    }
}