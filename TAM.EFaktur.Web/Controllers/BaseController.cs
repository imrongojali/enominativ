﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.Master_HRIS;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class BaseController : PageController
    {
        #region Protected Methods
        protected string TempUploadFolder
        {
            get 
            {
                return Path.Combine(MasterConfigRepository.Instance.GetByConfigKey("TempUploadFolder").ConfigValue);
                
            }
        }
       
        protected string TempDownloadFolder
        {
            get 
            { 
                return Path.Combine(MasterConfigRepository.Instance.GetByConfigKey("TempDownloadFolder").ConfigValue); 
            }
        }

        protected string TempZipFolder
        {
            get
            {
              //  return Path.Combine(@"D:\EFB_PATH\Zipdownload\");
                return Path.Combine(MasterConfigRepository.Instance.GetByConfigKey("TempZipFolder").ConfigValue);
            }
        }

        protected string VirtualDirectoryFilePrintURL
        {
            get
            {
                return Path.Combine(MasterConfigRepository.Instance.GetByConfigKey("VirtualDirectoryFilePrintURL").ConfigValue);
            }
        }

        protected string TempPrintFolder
        {
            get
            {
                return Path.Combine(MasterConfigRepository.Instance.GetByConfigKey("TempPrintFolder").ConfigValue);
            }
        }

        protected string EncodeFile(string fileName)
        {
            return Convert.ToBase64String(System.IO.File.ReadAllBytes(fileName));
        }

        protected string StampFileName(string fileName, Nullable<DateTime> stampTime = null)
        {
            if (!stampTime.HasValue)
                stampTime = DateTime.Now;

            return stampTime.Value.FormatFullDate(Formatting.FILE_TIMESTAMP) + fileName;
        }

        public FileResult DownloadTemp(string filePath)
        {
            //try
            //{
                byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
                string fileName = Path.GetFileName(filePath);
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            //}
            //catch
            //{
            //    return File(byte, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            //}
           
        }

        public FileResult DownloadTemp2(string filePath)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            string fileName = Path.GetFileName(filePath);
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            return File(fileBytes,  "application/vnd.ms-excel", fileName);
           // return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Grid.xlsx");
        }

        protected void UploadWholeFile(HttpRequestBase request, List<UploadResultViewModel> statuses, string storageRoot, Nullable<DateTime> stampTime = null)
        {
            for (int i = 0; i < request.Files.Count; i++)
            {
                var file = request.Files[i];
                var fileName = StampFileName(file.FileName, stampTime);
                var fullPath = Path.Combine(storageRoot, Path.GetFileName(fileName));
               // var fullPath = Path.Combine(@"D:\Interface\Temp\Upload", Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);

                file.SaveAs(fullPath);

                statuses.Add(new UploadResultViewModel()
                {
                    name = file.FileName,
                    size = file.ContentLength,
                    type = file.ContentType,
                    extension = extension,
                    thumbnail_url = @"data:image/png;base64," + EncodeFile(fullPath),
                    delete_type = "GET",
                });
            }
        }

        protected void UploadPartialFile(string fileName, HttpRequestBase request, List<UploadResultViewModel> statuses)
        {
            if (request.Files.Count != 1) throw new HttpRequestValidationException("Attempt to upload chunked file containing more than one fragment per request");
            var file = request.Files[0];
            var inputStream = file.InputStream;

            var fullName = Path.Combine(TempUploadFolder, Path.GetFileName(StampFileName(fileName)));
            var extension = Path.GetExtension(fileName);

            using (var fs = new FileStream(fullName, FileMode.Append, FileAccess.Write))
            {
                var buffer = new byte[1024];

                var l = inputStream.Read(buffer, 0, 1024);
                while (l > 0)
                {
                    fs.Write(buffer, 0, l);
                    l = inputStream.Read(buffer, 0, 1024);
                }
                fs.Flush();
                fs.Close();
            }
            statuses.Add(new UploadResultViewModel()
            {
                name = fileName,
                size = file.ContentLength,
                type = file.ContentType,
                extension = extension,
                thumbnail_url = @"data:image/png;base64," + EncodeFile(fullName),
                delete_type = "GET",
            });
        }

        protected User CurrentLogin
        {
            get
            {
                return Lookup.Get<User>();
            }
        }

        protected HRIS CurrentHRIS
        {
            get
            {
                return Lookup.Get<HRIS>();
            }
        }

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }

        #endregion

        [HttpPost]
        public virtual ActionResult UploadFiles()
        {
            var r = new List<UploadResultViewModel>();

            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;

                if (string.IsNullOrEmpty(headers["X-File-Name"]))
                {
                    UploadWholeFile(Request, statuses, TempUploadFolder);
                }
                else
                {
                    UploadPartialFile(headers["X-File-Name"], Request, statuses);
                }
                Result result = new Result();
                result.ResultCode = true;
                result.ResultDesc = "File has been uploaded successfully";
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";

                //JsonResult result = Json(statuses);
                //result.ContentType = "text/plain";

                return jsonObj;
            }

            return Json(r);
        }

    }
}
