﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using TAM.EFaktur.Web.Models;
using System.IO;
using System.Text;
using System.Diagnostics;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.Master_Company_Details;

namespace TAM.EFaktur.Web.Controllers
{
    public static class XlsHelper
    {
        public static readonly string EXT = ".xls";
        public static readonly string EXTX = ".xlsx";
        public static readonly string EXTPDF = ".pdf";

        public static string PutExcel(List<string[]> ListArr, object o, string TempDownloadFolder, string XlsName = null, bool ForceName = false)
        {
            try
            {
                HSSFWorkbook workbook = GlobalFunction.Instance.CreateObjectToSheet(ListArr, o);// call function execute report 
                string fileName = XlsName;
                if (!ForceName)
                {
                      fileName = (string.IsNullOrEmpty(XlsName) ? o.GetType().Name : XlsName)
                                            + "_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss")
                                            + EXT;
                }
                
                if (!EXT.Equals(Path.GetExtension(fileName)))
                {
                    fileName = Path.ChangeExtension(fileName, EXT);
                }
                string fullPath = Path.Combine(@TempDownloadFolder, fileName);
                using (var exportData = new MemoryStream())// binding to streamreader
                {
                    workbook.Write(exportData);
                    using (FileStream fileStream = new FileStream(fullPath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        exportData.Position = 0;
                        exportData.CopyTo(fileStream);
                    }
                }

                return fullPath;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public static string PutExcelDRKBReport(List<string[]> ListArr, object o, string TempDownloadFolder, string XlsName = null, bool ForceName = false)
        {
            try
            {
                XSSFWorkbook workbook = GlobalFunction.Instance.CreateObjectToSheetDRKBReport(ListArr, o);// call function execute report 
                string fileName = XlsName + " Report";
                string fileNamePdf = XlsName + " Report";

                if (!ForceName)
                {
                    fileName = (string.IsNullOrEmpty(XlsName) ? o.GetType().Name : XlsName)
                                          + "_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss")
                                          + EXTX;
                }

                if (!EXTX.Equals(Path.GetExtension(fileName)))
                {
                    fileName = Path.ChangeExtension(fileName, EXTX);
                }
                if (!EXTPDF.Equals(Path.GetExtension(fileNamePdf)))
                {
                    fileNamePdf = Path.ChangeExtension(fileNamePdf, EXTPDF);
                }
                string fullPath = Path.Combine(@TempDownloadFolder, fileName);
                string fullPathPdf = Path.Combine(@TempDownloadFolder, fileNamePdf);
                //using (var exportData = new MemoryStream())// binding to streamreader
                //{
                //    workbook.Write(exportData);
                //    using (FileStream fileStream = new FileStream(fullPath, FileMode.Create, FileAccess.ReadWrite))
                //    {
                //        exportData.Position = 0;
                //        exportData.CopyTo(fileStream);
                //    }
                //}
                using (FileStream stream = new FileStream(fullPath, FileMode.Create, FileAccess.Write))
                {
                    workbook.Write(stream);

                }

                return fullPath;

            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public static string PutExcelDRKBOriginal(List<string[]> ListArr, object o, string TempDownloadFolder, string XlsName = null, bool ForceName = false, bool isPDF = false)
        {
            try
            {
                XSSFWorkbook workbook = GlobalFunction.Instance.CreateObjectToSheetDRKBOriginal(ListArr, o);// call function execute report 
                string fileName = XlsName + " Original";
                string fileNamePdf = XlsName + " Original";
                if (!ForceName)
                {
                    fileName = (string.IsNullOrEmpty(XlsName) ? o.GetType().Name : XlsName)
                                          + "_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss")
                                          + EXTX;
                }

                if (!EXTX.Equals(Path.GetExtension(fileName)))
                {
                    fileName = Path.ChangeExtension(fileName, EXTX);
                }
                if (!EXTPDF.Equals(Path.GetExtension(fileNamePdf)))
                {
                    fileNamePdf = Path.ChangeExtension(fileNamePdf, EXTPDF);
                }
                string fullPath = Path.Combine(@TempDownloadFolder, fileName);
                string fullPathPdf = Path.Combine(@TempDownloadFolder, fileNamePdf);
                //using (var exportData = new MemoryStream())// binding to streamreader
                //{
                //    workbook.Write(exportData);
                //    using (FileStream fileStream = new FileStream(fullPath, FileMode.Create, FileAccess.ReadWrite))
                //    {
                //        exportData.Position = 0;
                //        exportData.CopyTo(fileStream);
                //    }
                //}
                using (FileStream stream = new FileStream(fullPath, FileMode.Create, FileAccess.Write))
                {
                    workbook.Write(stream);

                }
 
                    return fullPath;

                 
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public static string PutExcelEbupot(List<string[]> ListArr, List<string[]> ListArrRef, object o, string TempDownloadFolder, string XlsName = null, string jenisPajak = null)
        {
            try
            {
                //Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
                //excelApp.Visible = false;
                //string fullPath2 = @"D:\EFB_PATH\Download\021161153092000_23_20190814_111214.xls";
                //string fullPath3 = @"D:\EFB_PATH\Download\021161153092000_23_20190814_111214_imron.xls";
                //Microsoft.Office.Interop.Excel.Workbook eWorkbook = excelApp.Workbooks.Open(fullPath2, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                //eWorkbook.SaveAs(fullPath3, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                //eWorkbook.Close(false, Type.Missing, Type.Missing);

                //string configebupot = MasterConfigRepository.Instance.GetByConfigKey("TempEbupotFolder").ConfigValue;
                //XLWorkbook wb = new XLWorkbook(configebupot);
                //wb.Worksheet("23").Cell(1, 5).Value = 3;
                //wb.Worksheet("23").Cell(2, 5).Value = "BBB";
                //wb.Worksheet("23").Cell(3, 5).SetValue("ASDASDASDASD");
                ////wb.AddWorksheet("CCC");
                //wb.SaveAs("D:\\bbb.xls");   

                //Workbook workbookSave = new Workbook();
                HSSFWorkbook workbook = GlobalFunction.Instance.CreateObjectToSheetEbupot(ListArr, ListArrRef, o, jenisPajak);// call function execute report 
                //string fileNametem =
                //        (string.IsNullOrEmpty(XlsName) ? o.GetType().Name : XlsName)
                //        + "_" + jenisPajak + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss")+""
                //        + EXT;
                string fileName =
                      (string.IsNullOrEmpty(XlsName) ? o.GetType().Name : XlsName)
                      + "_" + jenisPajak + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss")
                      + EXT;
                if (!EXT.Equals(Path.GetExtension(fileName)))
                {
                    fileName = Path.ChangeExtension(fileName, EXT);
                }
                string fullPath = Path.Combine(@TempDownloadFolder, fileName);
                // string fullpathtemp = Path.Combine(@TempDownloadFolder, fileName + "Temp");


                using (var exportData = new MemoryStream())// binding to streamreader
                {

                    workbook.Write(exportData);

                    using (FileStream fileStream = new FileStream(fullPath, FileMode.Create, FileAccess.ReadWrite))
                    {

                        exportData.Position = 0;

                        exportData.CopyTo(fileStream);
                    }


                }
                //using (var stream = new FileStream(fullPath, FileMode.OpenOrCreate, FileAccess.Write))
                //{
                //    workbook.Write(stream);
                //}

                //    try { 
                //    ProcessStartInfo startInfo = new ProcessStartInfo(@"D:\EFB_PATH\DLL\Debug\ConsoleAppExel.exe");
                //    //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                //        startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;

                //        // Start with one argument.
                //        // Output of ArgsEcho:
                //        //  [0]=/a   

                //        string fullPath2 = @"D:\EFB_PATH\Download\021161153092000_23_15-07-2019_113520.xls";
                //    string fullPath3 = @"D:\EFB_PATH\Download\021161153092000_23_15-07-2019_113520_gojalitest.xls";
                //    string genArgs = fullPath2 + " " + fullPath3;
                //    startInfo.Arguments = genArgs;
                //    Process.Start(startInfo);

                //        string arguments = genArgs;  // /t "D:\\Temp_Report.pdf" "\\TestPrinter_one"  

                //        string executable = @"D:\EFB_PATH\DLL\Debug\ConsoleAppExel.exe";


                //    int waitingTime = 18000;


                //        ProcessStartInfo starter = new ProcessStartInfo(executable, arguments);
                //        starter.CreateNoWindow = true;
                //        starter.WindowStyle = ProcessWindowStyle.Hidden;
                //        starter.UseShellExecute = false;
                //        Process process = new Process();
                //        process.StartInfo = starter;

                //        process.Start();

                //        process.WaitForExit(waitingTime); // hold the process for some seconds so that PDF   
                //                                          //files can be sent   
                //        process.CloseMainWindow();
                //        process.Kill();
                //        starter = null;
                //        process = null;
                //    }
                //catch (Exception e)
                //{

                //    throw e;
                //}
                //byte[] byteArray = File.ReadAllBytes(@"D:\021161153092000_23_20190807_182032.xls");
                //using (MemoryStream stream = new MemoryStream())
                //{
                //    stream.Write(byteArray, 0, (int)byteArray.Length);
                //    using (SpreadsheetDocument spreadsheetDoc = SpreadsheetDocument.Open(stream, true))
                //    {
                //        // Do work here
                //    }
                //    File.WriteAllBytes(@"D:\021161153092000_23_20190807_182032333.xls", stream.ToArray());
                //}

                //Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
                //excelApp.Visible = false;
                //string fullPath2 = @"D:\EFB_PATH\Download\021161153092000_23_20190808_173544.xls";
                //Microsoft.Office.Interop.Excel.Workbook eWorkbook = excelApp.Workbooks.Open(fullPath2, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                //eWorkbook.SaveAs(fullPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                //eWorkbook.Close(false, Type.Missing, Type.Missing);
                //workbookSave.SaveAs(fullPath);
                // File.Delete(fullpathtemp);



                return fullPath;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public static void ExportToExcel(HSSFWorkbook workbook, string fileName)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                workbook.Write(memoryStream);
                memoryStream.Flush();

                try
                {
                    HttpResponse response = HttpContext.Current.Response;
                    response.ClearContent();
                    response.ClearHeaders();
                    response.Buffer = true;
                    response.ContentType = "application/vnd.ms-excel";
                    response.AddHeader("Content-Length", memoryStream.Length.ToString());
                    response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
                    response.BinaryWrite(memoryStream.GetBuffer());
                    response.Flush();
                    response.End();
                }
                catch
                {
                    // Do nothing, error expected due to Flush();
                }
            }
        }

        internal static string PutExcel(List<string[]> listArr, CompanyOriginalViewModel companyOriginalViewModel, object tempDownloadFolder, string v)
        {
            throw new NotImplementedException();
        }

        public static string PutExcelWithTitle(List<string[]> ListArr, object o, string TempDownloadFolder, string Title, string XlsName = null)
        {
            try
            {
                HSSFWorkbook workbook = GlobalFunction.Instance.CreateObjectToSheetWithTitle(ListArr, o, Title);// call function execute report 
                string fileName =
                        (string.IsNullOrEmpty(XlsName) ? o.GetType().Name : XlsName)
                        + "_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss")
                        + EXT;
                if (!EXT.Equals(Path.GetExtension(fileName)))
                {
                    fileName = Path.ChangeExtension(fileName, EXT);
                }
                string fullPath = Path.Combine(@TempDownloadFolder, fileName);
                using (var exportData = new MemoryStream())// binding to streamreader
                {
                    workbook.Write(exportData);
                    using (FileStream fileStream = new FileStream(fullPath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        exportData.Position = 0;
                        exportData.CopyTo(fileStream);
                    }
                }

                return fullPath;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

    }

    public static class CSVHelper
    {
        public static readonly string EXT = ".csv";

        public static string PutCSV(List<string[]> ListArr, object o, string TempDownloadFolder, string CsvName = null)
        {
            try
            {
                string fileName =
                        (string.IsNullOrEmpty(CsvName) ? o.GetType().Name : CsvName)
                        + "_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss")
                        + EXT;
                if (!EXT.Equals(Path.GetExtension(fileName)))
                {
                    fileName = Path.ChangeExtension(fileName, EXT);
                }
                string fullPath = Path.Combine(@TempDownloadFolder, fileName);

                var csv = new StringBuilder();

                foreach (string[] arrayString in ListArr)
                {
                    string newLine = string.Join(";", arrayString.Where(a => a != null)) + Environment.NewLine;

                    csv.Append(newLine);
                }

                File.WriteAllText(fullPath, csv.ToString());

                return fullPath;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }

   
}