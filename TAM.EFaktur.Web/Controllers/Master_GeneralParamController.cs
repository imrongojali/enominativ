﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.Master_GeneralParam;
using TAM.EFaktur.Web.Models.Master_User;

namespace TAM.EFaktur.Web.Controllers
{
    public class Master_GeneralParamController : BaseController
    {
        //
        // GET: /Master_GeneralParam/
        bool isFullAccess { get; set; }

        [HttpGet]
        public JsonResult GetGeneralParamDropdown(string ParamType = "")
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetGeneralParamDropdown(ParamType);
                
            }
            catch (Exception e)
            {
                
                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        //ADD AD FilterColor
        //[HttpGet]
        //public JsonResult GetGeneralParamDropdownColor(string ParamType = "")
        //{
        //    List<DropdownColor> model = new List<DropdownColor>();
        //    try
        //    {
        //        model = GeneralParamRepository.Instance.GetGeneralParamDropdownColor(ParamType);
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    return Json(model, JsonRequestBehavior.AllowGet);
        //}
        //END
        [HttpGet]
        public JsonResult GetGeneralParamDropdownByParamCode(string ParamType = "")
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetGeneralParamDropdownByParamCode(ParamType);

            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetGeneralParamDropdownByCode()
        {
            List<DropdownViewByCodeModel> model = new List<DropdownViewByCodeModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetGeneralParamDropdownByCode();

            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetGeneralParamDropdownMAPCode()
        {
            List<DropdownViewByCodeModel> model = new List<DropdownViewByCodeModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetGeneralParamDropdownMAPCode();

            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetKdTransaksi(int ParamType)
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetKDTransaksi(ParamType);
            }
            catch (Exception e)
            {
                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetBusinessUnitByDivisionName(string DivisionName = "")
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetBusinessUnitByDivisionName(DivisionName);

            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetUserListDropdown()
        {
            string username = CurrentLogin.Username;

            #region Cek Wht Full Access Role
            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("WHTFullAccessRole");//new by role


            string[] WHTFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            if (config != null)
            {
                WHTFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (WHTFullAccessRole.Contains(item))
                    {
                        isFullAccess = true;
                    }
                }
            }
            #endregion

            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = UserRepository.Instance.GetUserListDropdown(username,isFullAccess);
            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetPvCreateByDropdown()
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetPvCreateByDropdown();

            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetTaxArticleByDropdown()
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetTaxArticleByDropdown();

            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public JsonResult GetJenisPajakByDropdown()
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetJenisPajakByDropdown();

            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetJenisPajakByDropdownByParam()
        {
            List<DropdownModel> model = new List<DropdownModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetJenisPajakByDropdownByParam();

            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult GetSPTTypeByDropdown()
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetSPTTypeByDropdown();

            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetPVCreatedByByDropdown()
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetPVCreatedByByDropdown();
            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetKjsMapCodeDropdownFromTB_M_JenisPajak()
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetKjsMapCodeDropdownFromTB_M_JenisPajak();
            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetGlAccountDropdownFromTB_M_JenisPajak()
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetGlAccountDropdownFromTB_M_JenisPajak();
            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetTaxArticleDropdownFromTB_M_JenisPajak()
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetTaxArticleDropdownFromTB_M_JenisPajak();
            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetSptTypeDropdownFromTB_M_JenisPajak()
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetSptTypeDropdownFromTB_M_JenisPajak();
            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetColorParamDropDown()
        {
            List<DropdownParamColor> model = new List<DropdownParamColor>();
            try
            {
                model = GeneralParamRepository.Instance.GetColorParamDropDown();
            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

       

        #region Register
        [HttpGet]
        public JsonResult GetApprovalStatusByDropdown()
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetApprovaStatusByDropDown();
            }
            catch (Exception e)
            {
                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSupplierCountryCodeByDropdown()
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetSupplierCountryCodeByDropDown();
            }
            catch (Exception e)
            {
                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetRecordedByByDropdown()
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetRecordedByByDropDown();
            }
            catch (Exception e)
            {
                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetFRecordedByByDropdown()
        {
            List<DropdownViewModel> model = new List<DropdownViewModel>();
            try
            {
                model = GeneralParamRepository.Instance.GetFRecordedByByDropDown();
            }
            catch (Exception e)
            {
                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #endregion 

    }
}
