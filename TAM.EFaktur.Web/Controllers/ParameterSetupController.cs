﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.ParameterSetup;

namespace TAM.EFaktur.Web.Controllers
{
    public class ParameterSetupController : BaseController
    {
        //
        // GET: /ParameterSetup/
        protected override void Startup()
        {
            Settings.Title = "Parameter Setup";
            bool isFullAccess = false;

            ViewBag.IsFullAccess = isFullAccess;
        }
     
        public ActionResult PopupMultipleSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupMultipleSearch");
        }

        public ActionResult PopupTextAreaSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupTextAreaSearch");
        }
        public ActionResult AddParameterSetup()
        {
            
            return PartialView();
        }
        [HttpPost]
        public ActionResult GetParameterSetupList(FormCollection collection, int sortBy, bool sortDirection, int page, int size)
        {
            var data = collection["data"];
            var model = new ParameterSetupSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<ParameterSetupSearchParamViewModel>(data);

            Paging pg = new Paging
            (
                ParameterSetupRepository.Instance.Count(model),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["ParameterSetupListDashboard"] = ParameterSetupRepository.Instance.GetList(model, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData);

            return PartialView("_ParameterSetupGrid");
        }

        public JsonResult Insert(FormCollection collection)
        {
            var data = collection["data"];
            var model = new ParameterSetupParamModel();
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                model = new JavaScriptSerializer().Deserialize<ParameterSetupParamModel>(data);
            }
            try
            {
               
                result = ParameterSetupRepository.Instance.Save(model, CurrentLogin.Username); //TODO: Later change  to real user
            }
            catch (Exception e)
            {
                e.LogToApp("Parameter Setup", MessageType.ERR, CurrentLogin.Username);
                result.ResultCode = false;
                result.ResultDesc = e.Message;
            }
            //}
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Delete(FormCollection collection, Guid Id)
        {
            var data = collection["data"];
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "Data has ben deleted"
            };

          
            try
            {

                result = ParameterSetupRepository.Instance.Delete(Id, CurrentLogin.Username); //TODO: Later change  to real user
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("Parameter Setup Delete", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
            //}
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GenerateDownloadFile(FormCollection collection, bool checkAll)
        {


            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };

            
            try
            {
                string fullPath = string.Empty;
                int totalDataCount = 0;
                int totalDataFailed = 0;
                var searchParam = collection["data"];
                var paramIdList = collection["paramIdList"];
                var searchParamModel = new ParameterSetupSearchParamViewModel();
                List<Guid> _paramIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<ParameterSetupSearchParamViewModel>(searchParam);
                    _paramIdList = ParameterSetupRepository.Instance.GetIdBySearchParam(searchParamModel);
                }
                else
                {
                    _paramIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(paramIdList);
                }

                List<string[]> ListArr = new List<string[]>();

               
                #region xls report
               
                    String[] header = new string[]
                    { "Module Name", "Parameter Code", "Parameter Name", "Parameter Value", "Deadline H+",
                      "Field Name", "Business Unit" ,"Color", "Icon", "Color Status",
                      "Transaction Code","Category Name","Category 1", "Category 2","Category 3", "Category 4", "Category 5"
                    }; //for header 


                    ListArr.Add(header);


                    var list = ParameterSetupRepository.Instance.GetXlsReportById(_paramIdList);
                    foreach (ParameterSetupOriginalViewModel obj in list)
                    {
                        String[] myArr = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field16,
                            obj.Field17
                          
                         };
                        ListArr.Add(myArr);
                    }
                    totalDataCount = ListArr.Count() - 1;
                    totalDataFailed = _paramIdList.Count() - totalDataCount;
                    result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    result.ResultDesc += totalDataFailed + " data failed downloaded";
                    fullPath = XlsHelper.PutExcel(ListArr, new ParameterSetupDashBoardViewModel(), TempDownloadFolder, "ParameterSetup");
                
                #endregion

                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("Parameter Setup Download ", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
                throw e;
            }
        }

        public FileResult DownloadTempxls(string filePath)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            string fileName = Path.GetFileName(filePath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpGet]
        public JsonResult GetParamById(Guid Id)
        {
            try
            {
                ParameterSetupViewModelById oData = new ParameterSetupViewModelById();
                oData = ParameterSetupRepository.Instance.GetParameterById(Id); //TODO: Later change  to real user
                return Json(oData, JsonRequestBehavior.AllowGet);
               
            }
            catch (Exception e)
            {
                Result result = new Result();
                
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("Get Parameter Setup", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
            //}
            
        }
         

    }
}
