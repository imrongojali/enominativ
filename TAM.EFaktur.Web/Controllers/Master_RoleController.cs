﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_Role;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class Master_RoleController : PageController
    {
        //
        // GET: /Master_Role/

        public Master_RoleController()
        {
            Settings.Title = "Maintain-Role";
        }

        public ActionResult PopupCreateRole()
        {
            return PartialView("_PopupCreateRole");
        }
        public ActionResult PopupUpdateRole(Guid Id)
        {

            ViewData["UpdateRole"] = MasterRoleRepository.Instance.GetById(Id);
            return PartialView("_PopupUpdateRole");
        }

        public ActionResult PopupDeleteRole(Guid Id)
        {

            ViewData["DeleteRole"] = MasterRoleRepository.Instance.GetById(Id);
            return PartialView("_PopupDeleteRole");
        }

        public ActionResult PopupMultipleSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupMultipleSearch");
        }

        [HttpPost]
        public ActionResult GetMasterRoleList(FormCollection collection, int sortBy, bool sortDirection, int page, int size)
        {
            var data = collection["data"];
            var model = new MasterRoleDashboardSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<MasterRoleDashboardSearchParamViewModel>(data);

            Paging pg = new Paging
            (
                MasterRoleRepository.Instance.Count(model),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["MasterRoleListDashboard"] = MasterRoleRepository.Instance.GetList(model, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData);

            return PartialView("_SimpleGrid");
        }

        [HttpPost]
        public JsonResult Insert(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<MasterRoleCreateUpdate>(data);



                    result = MasterRoleRepository.Instance.Insert(model, "Handika Satwika R", DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<MasterRoleCreateUpdate>(data);



                    result = MasterRoleRepository.Instance.Update(model, "Handik Satwika R", DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //Delete Tampilan Data
        [HttpPost]
        public JsonResult Delete(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<MasterRoleCreateUpdate>(data);



                    result = MasterRoleRepository.Instance.Delete(model, "Handik Satwika R", DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult GetRoleDropdown()
        {


            List<DropdownViewModel> model = new List<DropdownViewModel>();

            model = MasterRoleRepository.Instance.GetRoleDropdown();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRoleDropdownList()
        {


            List<DropdownViewModel> model = new List<DropdownViewModel>();

            model = RoleRepository.Instance.GetRoleDropdownList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

    }
}
