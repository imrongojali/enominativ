﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.DRKB;
using Toyota.Common.Web.Platform;
using TAM.EFaktur.Web.Models.Master_Company_Details;

namespace TAM.EFaktur.Web.Controllers
{
    public class DRKBController : BaseController
    {
        //
        // GET: /DRKBDashboard/

        protected override void Startup()
        {
            Settings.Title = "DRKB Report";
            bool isFullAccess = false;
            string divisionFullAccess = string.Empty;
            if (!CurrentHRIS.DivisionName.ToLower().Contains("lexus"))
            {
                isFullAccess = true;
            }
            ViewBag.IsFullAccess = isFullAccess;
        }
        
        #region Popup
        public ActionResult PopupMultipleSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupMultipleSearch");
        }

        public ActionResult PopupTextAreaSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupTextAreaSearch");
        }

        public ActionResult PopupUploadApprovalStatus()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateApprovalStatus");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadApprovalStatus");
        }

        public ActionResult PopupUploadTaxInvoice()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateTaxInvoiceDRKB");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadTaxInvoice");
        }

        public ActionResult PopupUploadPDFFile()
        {
            return PartialView("_PopupUploadPDFFile");
        }
        #endregion

        [HttpPost]
        public ActionResult GetDRKBList(FormCollection collection, string sortBy, bool sortDirection, int page, int size)
        {
            var data = collection["data"];
            var model = new DRKBDashboardSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<DRKBDashboardSearchParamViewModel>(data);

            Paging pg = new Paging
            (
                DRKBRepository.Instance.Count(model,CurrentHRIS.DivisionName),
                page,
                size
            );
            ViewData["Paging"] = pg;
            try
            {
                ViewData["DRKBListDashboard"] = DRKBRepository.Instance.GetList(model, CurrentHRIS.DivisionName, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData);

            }
            catch (Exception e)
            {

                throw new Exception (e.Message);
            }

            return PartialView("_SimpleGrid");
        }

        [HttpPost]
        public JsonResult GeneratePrintFile(FormCollection collection, bool checkAll)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for printing is generated successfully"
            };
            string fullName = string.Empty;
            string fullPath = string.Empty;
            try
            {
                var searchParam = collection["data"];
                var DRKBIdLists = collection["DRKBIdList"];
                var searchParamModel = new DRKBDashboardSearchParamViewModel();
                List<String> DRKBIdList = new List<String>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<DRKBDashboardSearchParamViewModel>(searchParam);
                    DRKBIdList = DRKBRepository.Instance.GetIdBySearchParam(searchParamModel,CurrentHRIS.DivisionName);
                }
                else
                {
                    DRKBIdList = new JavaScriptSerializer().Deserialize<List<String>>(DRKBIdLists);
                }

                var filePaths = DRKBRepository.Instance.GetPDFUrlById(DRKBIdList);
                fullName = GlobalFunction.MergePDFFiles(filePaths, TempPrintFolder, CurrentLogin.Username);
                if (!string.IsNullOrEmpty(fullName))
                {
                    fullPath = VirtualDirectoryFilePrintURL + fullName;
                }
                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("DRKB Print e-Faktur", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GenerateDownloadFile(FormCollection collection, string type, bool checkAll, string totalData)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download is generated successfully"
            };
            string fullPath = string.Empty;
            int totalDataCount = 0;
            int totalDataFailed = 0;
            int totalCheckGrid = 0;
            int totalDownloadCSV = 0;
            int TotalDownloadCSVFailed = 0;

            try
            {
                var searchParam = collection["data"];
                var DRKBIdLists = collection["DRKBIdList"];
                var searchParamModelInit = new DRKBDashboardSearchParamViewModel();
                List<String> DRKBIdList = new List<String>();

                if ( int.Parse(totalData) > 3000)
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Total row can not more than 3000";
                    return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
                }


                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModelInit = new JavaScriptSerializer().Deserialize<DRKBDashboardSearchParamViewModel>(searchParam);
                    DRKBIdList = DRKBRepository.Instance.GetIdBySearchParam(searchParamModelInit, CurrentHRIS.DivisionName);
                }
                else
                {
                    searchParamModelInit = new JavaScriptSerializer().Deserialize<DRKBDashboardSearchParamViewModel>(searchParam);
                    DRKBIdList = new JavaScriptSerializer().Deserialize<List<String>>(DRKBIdLists);
                }

                //ambil default jakarta
                var Company = CompanyRepository.Instance.GetByCode("");
                if (Company == null)
                {
                    Company = CompanyRepository.Instance.GetByCode("J");
                }

               

                totalDataCount = DRKBIdList.Count();
                result.ResultDesc = totalDataCount + " data successfully downloaded";
                List<string[]> ListArr = new List<string[]>();// array for choose data
                if (searchParamModelInit.TanggalFakturFrom == "" || searchParamModelInit.TanggalFakturFrom == null)
                {
                    searchParamModelInit.TanggalFakturFrom = "01-01-1900";
                }
                if (searchParamModelInit.TanggalFakturTo == "" || searchParamModelInit.TanggalFakturTo == null)
                {
                    searchParamModelInit.TanggalFakturTo= "31-12-9999";
                }

                string filename = "DRKB_" + searchParamModelInit.TanggalFakturFrom.Replace("-","") + " to "+ searchParamModelInit.TanggalFakturTo.Replace("-", "");
                 

                string FilterDateHeader = "";
                 if (searchParamModelInit.TanggalFakturFrom == "01-01-1900" && searchParamModelInit.TanggalFakturTo == "31-12-9999"  )
                {
                    FilterDateHeader = "";
                }
                else if (searchParamModelInit.TanggalFakturFrom != "01-01-1900" && searchParamModelInit.TanggalFakturTo != "31-12-9999" )
                {
                    FilterDateHeader = searchParamModelInit.TanggalFakturFrom.FormatViewDate("MMMM dd, yyyy") + " to " + searchParamModelInit.TanggalFakturTo.FormatViewDate("MMMM dd, yyyy");

                }
               
                #region xls type
                if (type == "xls")
                {
                    fullPath = GenerateXLSOriginal(Company, ListArr, FilterDateHeader, DRKBIdList, filename);
                }
                #endregion
                #region xls type
                else if (type == "xls_report")
                {
                    fullPath = GenerateXLSReport(Company, ListArr, FilterDateHeader, DRKBIdList, filename);

                }
                #endregion
                #region report pdf
                else if (type == "pdf")
                {
                    fullPath = GeneratePDFOriginal(Company, ListArr, FilterDateHeader, DRKBIdList, filename);


                }
                #endregion
                #region report pdf report
                else if (type == "pdf_report")
                {
                    fullPath = GeneratePDFReport(Company, ListArr, FilterDateHeader, DRKBIdList, filename);
                     

                }
                #endregion

                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("DRKB Download " + type, MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        private string GenerateXLSOriginal(Master_Company_detail company,List<string[]> ListArr, string FilterDateHeader, List<String> DRKBIdList, string filename)
        {
            //1
            String[] headerDate = new string[]
            {
                        FilterDateHeader
            }; //for header
            ListArr.Add(headerDate);

            //2
            String[] headerType = new string[]
            {
                        "IMPORTER / BRAND HOLDING AGENT / MANUFACTURING / DISTRIBUTOR/ DEALER / SUB DEALER / SHOWROOM"
            }; //for header
            ListArr.Add(headerType);


            //3
            String[] headerName = new string[]
            {
                        "NAME","" , ": "+company.Nama_pt
            }; //for header
            ListArr.Add(headerName);

            //4
            String[] headerAddress = new string[]
            {
                        "ADDRESS","" , ": "+company.alamat_pt
            }; //for header
            ListArr.Add(headerAddress);

            //5
            String[] headerNPWP = new string[]
            {
                        "NPWP","" , ": "+company.Npwp_pt
            }; //for header
            ListArr.Add(headerNPWP);

            //6
            String[] headerNoPKP = new string[]
            {
                        "NO. PENGUKUHAN PKP" ,"" , ": "+company.no_pengukuhan_pt
            }; //for header
            ListArr.Add(headerNoPKP);

            //7
            String[] lokasi = new string[]
            {
                        company.Lokasi
            }; //for header
            ListArr.Add(lokasi);

            //8
            String[] namaTTD = new string[]
            {
                        company.Nama_ttd
            }; //for header
            ListArr.Add(namaTTD);

            //9
            String[] jabatanTTD = new string[]
            {
                        company.Jabatan_ttd
            }; //for header
            ListArr.Add(jabatanTTD);

            //10
            String[] header = new string[]
            {
                        "No", "Business Unit", "Tax Invoice or Credit Note","Tax Invoice or Credit Note", "Customer","Customer", "Frame Number", "Engine Number","Merk Name", "Model", "Year", 
                        "Vehicle Selling Price","Vehicle Selling Price","Vehicle Selling Price", "Previous Luxury Tax Amount", "Remarks"
            }; //for header

            ListArr.Add(header);

            //11
            String[] header2 = new string[]
            {
                        "", "Business Unit","Tax Number", "Tax Date", "NPWP", "Name", "Frame Number", "Engine Number","Merk Name", "Model", "Year",
                        "VAT Based Amount", "VAT Amount", "Luxury Tax Amount", "Previous Luxury Tax Amount" , "Remarks"
            }; //for header

            ListArr.Add(header2);

            var list = DRKBRepository.Instance.GetOriginalById(DRKBIdList);
            foreach (DRKBOriginalViewModel obj in list)
            {
                String[] myArr = new string[] {
                            "",
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                        };
                ListArr.Add(myArr);
            }
            return XlsHelper.PutExcelDRKBOriginal(ListArr, new DRKBDashboardViewModel(), TempDownloadFolder, filename, true );
            //return PdfHelper.PutExcelDRKBOriginal(ListArr, new DRKBDashboardViewModel(), "D:\\\\VAT\\", filename, true, isPDF);
        }

        private string GenerateXLSReport(Master_Company_detail company, List<string[]> ListArr, string FilterDateHeader, List<String> DRKBIdList, string filename)
        {
            //1
            String[] headerDate = new string[]
            {
                        FilterDateHeader
            }; //for header
            ListArr.Add(headerDate);

            //2
            String[] headerType = new string[]
            {
                        "IMPORTER / BRAND HOLDING AGENT / MANUFACTURING / DISTRIBUTOR/ DEALER / SUB DEALER / SHOWROOM"
            }; //for header
            ListArr.Add(headerType);

            //3
            String[] headerName = new string[]
            {
                        "NAME","" , ": "+company.Nama_pt
            }; //for header
            ListArr.Add(headerName);

            //4
            String[] headerAddress = new string[]
            {
                        "ADDRESS","" , ": "+company.alamat_pt
            }; //for header
            ListArr.Add(headerAddress);

            //5
            String[] headerNPWP = new string[]
            {
                        "NPWP","" , ": "+company.Npwp_pt
            }; //for header
            ListArr.Add(headerNPWP);

            //6
            String[] headerNoPKP = new string[]
            {
                        "NO. PENGUKUHAN PKP" ,"" , ": "+company.no_pengukuhan_pt
            }; //for header
            ListArr.Add(headerNoPKP);

            //7
            String[] lokasi = new string[]
            {
                        company.Lokasi
            }; //for header
            ListArr.Add(lokasi);

            //8
            String[] namaTTD = new string[]
            {
                        company.Nama_ttd
            }; //for header
            ListArr.Add(namaTTD);

            //9
            String[] jabatanTTD = new string[]
            {
                        company.Jabatan_ttd
            }; //for header
            ListArr.Add(jabatanTTD);

            //10
            String[] header = new string[]
            {
                        "Business Unit","Tax Invoice or Credit Note","Tax Invoice or Credit Note", "Customer","Customer", "Frame Number","Merk Name", "Model", "Year", 
                        "Vehicle Selling Price","Vehicle Selling Price", "Previous Luxury Tax Amount", "Remarks"
            }; //for header

            ListArr.Add(header);

            //11
            String[] header2 = new string[]
            {
                        "Business Unit", "Tax Number", "Tax Date", "NPWP", "Name", "Frame Number","Merk Name", "Model", "Year",
                        "VAT Based Amount", "VAT Amount", "Previous Luxury Tax Amount" , "Remarks"
            }; //for header

            ListArr.Add(header2);


            var list = DRKBRepository.Instance.GetOriginalById(DRKBIdList);
            foreach (DRKBOriginalViewModel obj in list)
            {
                String[] myArr = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            //obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            //obj.Field13,
                            obj.Field14,
                            obj.Field15
                        };
                ListArr.Add(myArr);
            }
            return XlsHelper.PutExcelDRKBReport(ListArr, new DRKBDashboardViewModel(), TempDownloadFolder, filename, true);
            //return PdfHelper.PutExcelDRKBReport(ListArr, new DRKBDashboardViewModel(), "D:\\\\VAT\\", filename, true, isPDF);
           

        }

        private string GeneratePDFOriginal(Master_Company_detail company, List<string[]> ListArr, string FilterDateHeader, List<String> DRKBIdList, string filename)
        {
            //1
            String[] headerDate = new string[]
            {
                        FilterDateHeader
            }; //for header
            ListArr.Add(headerDate);

            //2
            String[] headerType = new string[]
            {
                        "IMPORTER / BRAND HOLDING AGENT / MANUFACTURING / DISTRIBUTOR/ DEALER / SUB DEALER / SHOWROOM"
            }; //for header
            ListArr.Add(headerType);


            //3
            String[] headerName = new string[]
            {
                        "NAME",": " + company.Nama_pt
            }; //for header
            ListArr.Add(headerName);

            //4
            String[] headerAddress = new string[]
            {
                        "ADDRESS",": " + company.alamat_pt
            }; //for header
            ListArr.Add(headerAddress);

            //5
            String[] headerNPWP = new string[]
            {
                        "NPWP",": " + company.Npwp_pt
            }; //for header
            ListArr.Add(headerNPWP);

            //6
            String[] headerNoPKP = new string[]
            {
                        "NO. PENGUKUHAN PKP" ,": " + company.no_pengukuhan_pt
            }; //for header
            ListArr.Add(headerNoPKP);

            //7
            String[] lokasi = new string[]
            {
                        company.Lokasi
            }; //for header
            ListArr.Add(lokasi);

            //8
            String[] namaTTD = new string[]
            {
                        company.Nama_ttd
            }; //for header
            ListArr.Add(namaTTD);

            //9
            String[] jabatanTTD = new string[]
            {
                        company.Jabatan_ttd
            }; //for header
            ListArr.Add(jabatanTTD);

            //10
            String[] header = new string[]
            {
                        "No", "Business Unit", "Tax Invoice or Credit Note","Tax Invoice or Credit Note", "Customer","Customer", "Frame Number", "Engine Number","Merk Name", "Model", "Year",
                        "Vehicle Selling Price","Vehicle Selling Price","Vehicle Selling Price", "Previous Luxury Tax Amount", "Remarks"
            }; //for header

            ListArr.Add(header);

            //11
            String[] header2 = new string[]
            {
                        "", "Business Unit","Tax Number", "Tax Date", "NPWP", "Name", "Frame Number", "Engine Number" ,"Merk Name","Model", "Year",
                        "VAT Based Amount", "VAT Amount", "Luxury Tax Amount", "Previous Luxury Tax Amount" , "Remarks"
            }; //for header

            ListArr.Add(header2);

            var list = DRKBRepository.Instance.GetOriginalById(DRKBIdList);
            foreach (DRKBOriginalViewModel obj in list)
            {
                String[] myArr = new string[] {
                            "",
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11.FormatNumberIna(),
                            obj.Field12.FormatNumberIna(),
                            obj.Field13.FormatNumberIna(),
                            obj.Field14.FormatNumberIna(),
                            obj.Field15,
                        };
                ListArr.Add(myArr);
            }
            return PdfHelper.PutExcelDRKBOriginal(ListArr, new DRKBDashboardViewModel(), TempDownloadFolder, filename, true);
            //return PdfHelper.PutExcelDRKBOriginal(ListArr, new DRKBDashboardViewModel(), "D:\\\\VAT\\", filename, true, isPDF);
        }

        private string GeneratePDFReport(Master_Company_detail company, List<string[]> ListArr, string FilterDateHeader, List<String> DRKBIdList, string filename)
        {
            //1
            String[] headerDate = new string[]
            {
                        FilterDateHeader
            }; //for header
            ListArr.Add(headerDate);

            //2
            String[] headerType = new string[]
            {
                        "IMPORTER / BRAND HOLDING AGENT / MANUFACTURING / DISTRIBUTOR/ DEALER / SUB DEALER / SHOWROOM"
            }; //for header
            ListArr.Add(headerType);

            //3
            String[] headerName = new string[]
            {
                        "NAME",": " + company.Nama_pt
            }; //for header
            ListArr.Add(headerName);

            //4
            String[] headerAddress = new string[]
            {
                        "ADDRESS",": " + company.alamat_pt
            }; //for header
            ListArr.Add(headerAddress);

            //5
            String[] headerNPWP = new string[]
            {
                        "NPWP",": " + company.Npwp_pt
            }; //for header
            ListArr.Add(headerNPWP);

            //6
            String[] headerNoPKP = new string[]
            {
                        "NO. PENGUKUHAN PKP" ,": " + company.no_pengukuhan_pt
            }; //for header
            ListArr.Add(headerNoPKP);

            //7
            String[] lokasi = new string[]
            {
                        company.Lokasi
            }; //for header
            ListArr.Add(lokasi);

            //8
            String[] namaTTD = new string[]
            {
                        company.Nama_ttd
            }; //for header
            ListArr.Add(namaTTD);

            //9
            String[] jabatanTTD = new string[]
            {
                        company.Jabatan_ttd
            }; //for header
            ListArr.Add(jabatanTTD);

            //10
            String[] header = new string[]
            {
                        "Business Unit","Tax Invoice or Credit Note","Tax Invoice or Credit Note", "Customer","Customer", "Frame Number","Merk Name", "Model", "Year",
                        "Vehicle Selling Price","Vehicle Selling Price", "Previous Luxury Tax Amount", "Remarks"
            }; //for header

            ListArr.Add(header);

            //11
            String[] header2 = new string[]
            {
                        "Business Unit", "Tax Number", "Tax Date", "NPWP", "Name", "Frame Number","Merk Name", "Model", "Year",
                        "VAT Based Amount", "VAT Amount", "Previous Luxury Tax Amount" , "Remarks"
            }; //for header

            ListArr.Add(header2);


            var list = DRKBRepository.Instance.GetOriginalById(DRKBIdList);
            foreach (DRKBOriginalViewModel obj in list)
            {
                String[] myArr = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            //obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11.FormatNumberIna(),
                            obj.Field12.FormatNumberIna(),
                            //obj.Field13.FormatNumberIna(),
                            obj.Field14.FormatNumberIna(),
                            obj.Field15
                        };
                ListArr.Add(myArr);
            }
            return PdfHelper.PutExcelDRKBReport(ListArr, new DRKBDashboardViewModel(), TempDownloadFolder, filename, true);
            //return PdfHelper.PutExcelDRKBReport(ListArr, new DRKBDashboardViewModel(), "D:\\\\VAT\\", filename, true, isPDF);


        }
    }
}
