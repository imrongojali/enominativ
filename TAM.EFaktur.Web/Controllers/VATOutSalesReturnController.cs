﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.VATOutSalesReturn;
using Toyota.Common.Web.Platform;
using System.Windows;
using System.Diagnostics;
using ICSharpCode.SharpZipLib.Zip;
using System.Web.Routing;
using TAM.EFaktur.Web.Models.ParameterSetup;
//using System.Net;
//using AttributeRouting.Web.Http;


namespace TAM.EFaktur.Web.Controllers
{
    public class VATOutSalesReturnController : BaseController/* , BaseRepository*/
    {
        //
        // GET: /VATOutSalesReturn/

        protected override void Startup()
        {
            Settings.Title = "VAT-Out Sales Return Dashboard";
            bool isFullAccess = false;
            string divisionFullAccess = string.Empty;
            if (!CurrentHRIS.DivisionName.ToLower().Contains("lexus"))
            {
                isFullAccess = true;
            }
            ViewBag.IsFullAccess = isFullAccess;
            try
            {
                ViewBag.Notif_Days = ParameterSetupRepository.Instance.GetParameterByCode("NOTIF_DAYS_FOR_VATOUT_SALES_RETURN").parameter_value; //TODO: Later change  to real user

            }
            catch (Exception)
            {

                ViewBag.Notif_Days = "0";

            }

        }
        #region popup
        public ActionResult PopupMultipleSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupMultipleSearch");
        }

        public ActionResult PopupTextAreaSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupTextAreaSearch");
        }

        public ActionResult PopupUploadApprovalStatus()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateApprovalStatusSalesReturn");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadApprovalStatus");
        }

        public ActionResult PopupUploadTransitoryStatus()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateTransitoryStatusSalesReturn");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadTransitoryStatus");
        }

        public ActionResult PopupUploadTaxInvoice()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateTaxInvoiceVATOutReturn");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadTaxInvoice");
        }
        public ActionResult PopupUploadTaxDataLexus()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateTaxDataLexusVATOutSalesRetur");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadTaxDataLexus");
        }

        public ActionResult PopupUploadPDFFile()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateTaxInvoiceVATOutReturn");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadEFakturFile");
        }

        public ActionResult PopupUploadCreditNote()
        {
            return PartialView("_PopupUploadCreditNote");
        }
        #endregion 
        public ActionResult GetVATOutSalesReturnList(FormCollection collection, int sortBy, bool sortDirection, int page, int size, string Id)
        {
            var data = collection["data"];
            var model = new VATOutSalesReturnSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<VATOutSalesReturnSearchParamViewModel>(data);

            //ADD AD 13112020
            if (model.EFakturStatus != "")
            {
                if (model.EFakturStatus != null)
                {
                    var _efaktur = model.EFakturStatus;
                    var _hasilreplace = _efaktur.Replace(";", "','");
                    string _color = "'" + _hasilreplace + "'";
                    model.EFakturStatus = _color;

                }
            }
            //END
            try
            {
                Paging pg = new Paging
                (
                    VATOutSalesReturnRepository.Instance.Count(model, CurrentHRIS.DivisionName,page,size,Id),
                    page,
                    size
                );
                ViewData["Paging"] = pg;
                ViewData["VATOutSalesReturnListDashboard"] = VATOutSalesReturnRepository.Instance.GetList(model, CurrentHRIS.DivisionName, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData, Id);
            }
            catch (Exception e)
            {
                //ViewData["ErrorMsg"] = e.Message;
                throw new Exception(e.Message);
            }
            return PartialView("_SimpleGrid");
        }

        [HttpPost]
        public JsonResult GenerateDownloadFile(FormCollection collection, string type, bool checkAll)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };
            string fullPath = string.Empty;
            //string filePaths = string.Empty;
            string filepath2 = string.Empty;
            string fullPathpdf = string.Empty;
            string filepath1 = string.Empty;
            int totalDataCount = 0;
            int totalDataFailed = 0;

            try
            {
                var searchParam = collection["data"];
                var vatOutSalesReturnIdList = collection["vatOutSalesReturnIdList"];
                var searchParamModel = new VATOutSalesReturnSearchParamViewModel();
                List<Guid> VATOutSalesReturnIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<VATOutSalesReturnSearchParamViewModel>(searchParam);
                    VATOutSalesReturnIdList = VATOutSalesReturnRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName);
                }
                else
                {
                    VATOutSalesReturnIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(vatOutSalesReturnIdList);
                }

                totalDataCount = VATOutSalesReturnIdList.Count();
                result.ResultDesc = totalDataCount + " data successfully downloaded";

                List<string[]> ListArr = new List<string[]>(); // array for the data

                #region xls type

                if (type == "xls")
                {
                    try
                    {
                        String[] header = new string[]
                      {"DA_NUMBER", "BUSINESS_UNIT","CA_DAC_NUMBER", "RK", "NPWP", "NAMA",
                      //CR
                      "ALAMAT",
                      //
                      "KD_JENIS_TRANSAKSI", "FG_PENGGANTI", "NOMOR_FAKTUR", "TANGGAL_FAKTUR",
                      "NOMOR_DOKUMEN_RETUR","TANGGAL_RETUR","MASA_PAJAK_RETUR","TAHUN_PAJAK_RETUR",
                      //CR
                      "KODE_OBJEK",
                      //
                        //ad field ridwan
                        "NAMA_BARANG","FRAME_NO","ENGINE_NO","MODEL_TYPE","HARGA_SATUAN",
                        "JUMLAH_BARANG","HARGA_TOTAL","DISKON",
                        "NILAI_RETUR_DPP","NILAI_RETUR_PPN","NILAI_RETUR_PPNBM","PPNBM_LINI_SEBELUMNYA",
                      };

                        ListArr.Add(header);
                        var list = VATOutSalesReturnRepository.Instance.GetOriginalById(VATOutSalesReturnIdList);
                        foreach (VATOutSalesReturnOriginalViewModel obj in list)
                        {
                            String[] myArr = new string[] {
                                obj.Field1,
                                obj.Field2,
                                obj.Field3,
                                obj.Field4,
                                obj.Field5,
                                obj.Field6,
                                //CR
                                obj.Field33,
                                //
                                obj.Field7,
                                obj.Field8,
                                obj.Field9,
                                obj.Field10,
                                obj.Field11,
                                obj.Field12,
                                obj.Field13,
                                obj.Field14,
                                //CR
                                obj.Field34,
                                //
                                obj.Field15,
                                obj.Field16,
                                obj.Field17,
                                obj.Field18,
                                obj.Field19,
                                obj.Field20,
                                obj.Field21,
                                obj.Field22,
                                obj.Field23,
                                obj.Field24,
                                obj.Field25,
                                obj.Field26
                               };
                            ListArr.Add(myArr);
                        }

                        fullPath = XlsHelper.PutExcel(ListArr, new VATOutSalesReturnOriginalViewModel(), TempDownloadFolder, "VATOutSalesReturn");
                    }
                    catch (Exception e)
                    {
                        throw new TimeoutException("Load too much data");
                    }
                }
                #endregion

                #region report
                else if (type == "report")
                {

                    try
                    {
                        String[] header = new string[]
                   {"TRANSACTION_DATA_FILE", "CREDIT_NOTE_NUMBER","RETURN_DATE", "RETURN_TAX_INVOICE_NUMBER", "CANCEL_DA/CA_NUMBER", "RETURN_DA",
                      "TAX_INVOICE/DA_DATE", "CUSTOMER_NPWP", "CUSTOMER_NAME", "RETURN_VAT_BASED_AMOUNT","RETURN_VAT_AMOUNT","BUSINESS UNIT","RECEIVE_FILE_DATE",
                      "RECEIVE_FILE_TIME","DOWNLOAD_STATUS","DOWNLOAD_DATE","DOWNLOAD_TIME","RECORD_STATUS","RECORD_DATE","APPROVAL_STATUS","APPROVAL_DATE",
                      //add ridwan
                       "CREDIT_NOTE_STATUS","TRANSITORY_STATUS","TRANSITORY_NUMBER","TRANSITORY_DATE","BATCH_FILE_NAME",
                       "CREDITNOTE_ORIGINAL",
                        "CREDITNOTE_SIGNOFF","CREDITNOTE_DGT"
                   }; //for header

                        ListArr.Add(header);

                        var list = VATOutSalesReturnRepository.Instance.GetReportExcelById(VATOutSalesReturnIdList);
                        foreach (VATOutSalesReturnReportExcelViewModel obj in list)
                        {
                            String[] myArr = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field16,
                            obj.Field17,
                            obj.Field18,
                            obj.Field19,
                            obj.Field20,
                            obj.Field21,
                            obj.Field22,
                            obj.Field23,
                            obj.Field24,
                            obj.Field25,
                            obj.Field26,
                            obj.Field27,
                            obj.Field28,
                            obj.Field29,
                        };
                            ListArr.Add(myArr);
                        }
                        fullPath = XlsHelper.PutExcel(ListArr, new VATOutSalesReturnOriginalViewModel(), TempDownloadFolder, "VATOutSalesReturn");
                    }
                    catch (Exception e)
                    {
                        throw new TimeoutException("Load too much data");
                    }
                }
                #endregion
                #region csv type
                else if (type == "csv")
                {
                    String[] header = new string[]
                    { "RK", "NPWP","NAMA","KD_JENIS_TRANSAKSI", "FG_PENGGANTI", "NOMOR_FAKTUR", "TANGGAL_FAKTUR",
                      "NOMOR_DOKUMEN_RETUR", "TANGGAL_RETUR","MASA_PAJAK_RETUR", "TAHUN_PAJAK_RETUR", "NILAI_RETUR_DPP","NILAI_RETUR_PPN","NILAI_RETUR_PPNBM"
                    }; //for header FK

                    ListArr.Add(header);

                    var list = VATOutSalesReturnRepository.Instance.GetCSVById(VATOutSalesReturnIdList);
                    foreach (VATOutSalesReturnCSVViewModel obj in list)
                    {
                        String[] myArr = new string[] {
                        obj.Field1,
                        obj.Field2,
                        obj.Field3,
                        obj.Field4,
                        obj.Field5,
                        obj.Field6,
                        obj.Field7,
                        obj.Field8,
                        obj.Field9,
                        obj.Field10,
                        obj.Field11,
                        obj.Field12,
                        obj.Field13,
                        obj.Field14,

                    };
                        ListArr.Add(myArr);
                    }
                    totalDataCount = ListArr.Count() - 1;
                    totalDataFailed = VATOutSalesReturnIdList.Count() - totalDataCount;
                    result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    result.ResultDesc += totalDataFailed + " data failed downloaded";
                    fullPath = CSVHelper.PutCSV(ListArr, new VATOutSalesReturnViewModel(), TempDownloadFolder, "VATOutSalesReturn");
                    VATOutSalesReturnRepository.Instance.UpdateFlagCSV(VATOutSalesReturnIdList, fullPath, CurrentLogin.Username);
                }
                #endregion

                #region pdf type to zip
                else if (type == "pdf")
                {
                    var filePaths = VATOutSalesReturnRepository.Instance.GetPDFUrlById(VATOutSalesReturnIdList);
                    fullPath = GlobalFunction.GenerateZipFile(filePaths, TempZipFolder);
                }
                #endregion

                #region print pdf original
                #endregion

                #region Download PDF 
                else if (type == "pdfori")
                {
                    Int32 totalfile = 0;
                    var ListZip = new List<string>();
                    var list = VATOutSalesReturnRepository.Instance.getlistVATreturn(VATOutSalesReturnIdList);
                    totalfile = 0;
                    foreach (var obj in list)
                    {
                        if (obj.CreditNoteOriginal.ToUpper() == "EXIST")
                        {
                            String[] myArr = new string[] { obj.Nomor };
                            string Nomor = obj.Nomor;
                            var str = Nomor;
                            var charstToRemove = new string[] { "/" };
                            foreach (var c in charstToRemove)
                            {
                                str = str.Replace(c, string.Empty);
                            }
                            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateGenerateNotaVATOutSalesRetur");

                            string fileConf = config.ConfigValue + str + ".pdf";
                            try
                            {
                                if (System.IO.File.Exists(fileConf))
                                {
                                    totalfile += 1;
                                    ListZip.Add(fileConf);
                                }
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                    if (totalfile > 1)
                    {
                        fullPath = GlobalFunction.GenerateZipFileDownloadCreditNote(ListZip, TempZipFolder);
                    }
                    else if (totalfile == 1)
                    {

                        string fileConfsingle = ListZip.FirstOrDefault();
                        //string fileConfsingle = configsingle.ConfigValue + nofaktur.Replace("/", "") + ".pdf";
                        fullPath = fileConfsingle;
                    }
                    totalDataFailed = VATOutSalesReturnIdList.Count() - totalfile;
                    if (totalfile <= 0)
                    {
                        result.ResultDesc = "File Not Exist";
                        result.ResultCode = false;
                    }
                    else
                    {
                        result.ResultDesc = totalfile + " data successfully downloaded <br/>";
                        result.ResultDesc += totalDataFailed + " data failed downloaded";
                    }
                }
                else if (type == "pdfttd")
                {
                    Int32 totalfile = 0;
                    var ListZip = new List<string>();
                    var list = VATOutSalesReturnRepository.Instance.getlistVATreturn(VATOutSalesReturnIdList);
                    totalfile = 0;
                    foreach (var obj in list)
                    {
                        if (obj.CreditNoteTTD.ToUpper() == "EXIST")
                        {
                            String[] myArr = new string[] { obj.Nomor };
                            string Nomor = obj.Nomor;

                            var str = Nomor;
                            var charstToRemove = new string[] { "/" };
                            foreach (var c in charstToRemove)
                            {
                                str = str.Replace(c, string.Empty);
                            }
                            Config config = MasterConfigRepository.Instance.GetByConfigKey("FilePrintCreditnoteTTD");
                            string fileConf = config.ConfigValue + str + ".pdf";
                            try
                            {
                                if (System.IO.File.Exists(fileConf))
                                {
                                    totalfile += 1;
                                    ListZip.Add(fileConf);
                                }
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                    if (totalfile > 1)
                    {
                        fullPath = GlobalFunction.GenerateZipFileDownloadCreditNote(ListZip, TempZipFolder);
                    }
                    else if (totalfile == 1)
                    {

                        string fileConfsingle = ListZip.FirstOrDefault();
                        //string fileConfsingle = configsingle.ConfigValue + nofaktur.Replace("/", "") + ".pdf";
                        fullPath = fileConfsingle;
                    }
                    totalDataFailed = VATOutSalesReturnIdList.Count() - totalfile;
                    if (totalfile <= 0)
                    {
                        result.ResultDesc = "File Not Exist";
                        result.ResultCode = false;
                    }
                    else
                    {
                        result.ResultDesc = totalfile + " data successfully downloaded <br/>";
                        result.ResultDesc += totalDataFailed + " data failed downloaded";
                    }
                }
                else if (type == "pdfdgt")
                {
                    Int32 totalfile = 0;
                    var ListZip = new List<string>();
                    var list = VATOutSalesReturnRepository.Instance.getlistVATreturn(VATOutSalesReturnIdList);
                    totalfile = 0;
                    foreach (var obj in list)
                    {
                        if (obj.CreditNoteDGT.ToUpper() == "EXIST")
                        {
                            String[] myArr = new string[] { obj.Nomor };
                            string Nomor = obj.Nomor;

                            var str = Nomor;
                            var charstToRemove = new string[] { "/" };
                            foreach (var c in charstToRemove)
                            {
                                str = str.Replace(c, string.Empty);
                            }
                            Config config = MasterConfigRepository.Instance.GetByConfigKey("FilePrintCreditnoteDgt");
                            string fileConf = config.ConfigValue + str + ".pdf";
                            try
                            {
                                if (System.IO.File.Exists(fileConf))
                                {
                                    totalfile += 1;
                                    ListZip.Add(fileConf);
                                }
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                    if (totalfile > 1)
                    {
                        fullPath = GlobalFunction.GenerateZipFileDownloadCreditNote(ListZip, TempZipFolder);
                    }
                    else if (totalfile == 1)
                    {

                        string fileConfsingle = ListZip.FirstOrDefault();
                        //string fileConfsingle = configsingle.ConfigValue + nofaktur.Replace("/", "") + ".pdf";
                        fullPath = fileConfsingle;
                    }
                    totalDataFailed = VATOutSalesReturnIdList.Count() - totalfile;
                    if (totalfile <= 0)
                    {
                        result.ResultDesc = "File Not Exist";
                        result.ResultCode = false;
                    }
                    else
                    {
                        result.ResultDesc = totalfile + " data successfully downloaded <br/>";
                        result.ResultDesc += totalDataFailed + " data failed downloaded";
                    }
                }
                #endregion
                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                if (e.GetType().Equals(typeof(TimeoutException)))
                    result.ResultDesc = e.LogToApp("VATOutSalesReturn Download " + type, MessageType.ERR, CurrentLogin.Username).InnerException.Message;
                else
                    result.ResultDesc = e.LogToApp("VATOutSalesReturn Download " + type, MessageType.ERR, CurrentLogin.Username).Message;

                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);

            }
        }
        [HttpPost]
        public ActionResult UploadVATOutSalesReturn()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);

                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT Out

                    result = VATOutSalesReturnRepository.Instance.ExcelTaxInvoiceInsert(fullPath, CurrentLogin.Username);
                    #endregion
                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }
        [HttpPost]
        public ActionResult UploadVATOutSalesReturnLexus()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);

                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT Out

                    result = VATOutSalesReturnRepository.Instance.ExcelTaxInvoiceInsertLexus(fullPath, CurrentLogin.Username);
                    #endregion
                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }
        [HttpPost]
        public ActionResult UploadApprovalStatus()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                //var fullPath = Path.Combine(@"D:\Interface\Temp\Upload", Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);
                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        // UploadWholeFile(Request, statuses, @"D:\Interface\Temp\Upload", now);
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT Out

                    result = VATOutSalesReturnRepository.Instance.ExcelApprovalStatusUpdate(fullPath, CurrentLogin.Username);
                    #endregion

                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel File Type allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }
        [HttpPost]
        public ActionResult UploadTransitoryStatus()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                //var fullPath = Path.Combine(@"D:\Interface\Temp\Upload", Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);
                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                       // UploadWholeFile(Request, statuses, @"D:\Interface\Temp\Upload", now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT Out

                    result = VATOutSalesReturnRepository.Instance.ExcelTransitoryStatusUpdate(fullPath, CurrentLogin.Username);
                    #endregion

                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel File Type allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }
        [HttpPost]
        public ActionResult UploadFilesData()
        {
            Result result = new Result();
            string _datas = Request["data"];
            JavaScriptSerializer oSerializer = new JavaScriptSerializer();
            var datas = new List<CreditNoteModel>();
            if (!string.IsNullOrEmpty(_datas))
                datas = oSerializer.Deserialize<List<CreditNoteModel>>(_datas);

            result = VATOutSalesReturnRepository.Instance.uploadfilepdfcreditnote(datas, Request.Files, Request.Browser.Browser.ToUpper());
           

            return Json(result, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult GeneratePrintFile(FormCollection collection, bool checkAll, string type)
        {
            Result result = new Result()
            {
                ResultCode = true,
                ResultDesc = "Download generated successfully"
            };
            string fullPath = string.Empty;
            string filepath2 = string.Empty;
            string fullPathpdf = string.Empty;
            string filepath1 = string.Empty;
            int totalDataCount = 0;
            int totalDataFailed = 0;
            //List<string> fullPath = new List<string>();
            var docFile = new List<string>();
            try
            {
                var searchParam = collection["data"];
                var vatOutSalesReturnIdList = collection["vatOutSalesReturnIdList"];
                var searchParamModel = new VATOutSalesReturnSearchParamViewModel();
                List<Guid> VATOutSalesReturnIdList = new List<Guid>();

                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<VATOutSalesReturnSearchParamViewModel>(searchParam);
                    VATOutSalesReturnIdList = VATOutSalesReturnRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName);
                }
                else

                {
                    VATOutSalesReturnIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(vatOutSalesReturnIdList);
                }

                if (type == "PdfSignOff")
                {
                    Int32 totalfile = 0;
                    var ListZip = new List<string>();
                    var list = VATOutSalesReturnRepository.Instance.getlistVATreturn(VATOutSalesReturnIdList);
                    totalfile = 0;
                    foreach (var obj in list)
                    {
                        if (obj.CreditNoteTTD.ToUpper() == "EXIST")
                        {
                            String[] myArr = new string[] { obj.Nomor };
                            string Nomor = obj.Nomor;
                            var str = Nomor;
                            var charstToRemove = new string[] { "/" };
                            foreach (var c in charstToRemove)
                            {
                                str = str.Replace(c, string.Empty);
                            }
                            Config config = MasterConfigRepository.Instance.GetByConfigKey("FilePrintCreditnoteTTD");
                            //   if 
                            string fileConf = config.ConfigValue + str + ".pdf";
                            try
                            {
                                if (System.IO.File.Exists(fileConf))
                                {
                                    totalfile += 1;
                                    ListZip.Add(fileConf);
                                }
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                    if (totalfile > 1)
                    {
                        fullPath = fullPath = ListZip.ToString();
                    }
                    else if (totalfile <= 1)
                    {

                        string fileConfsingle = ListZip.FirstOrDefault();
                        //string fileConfsingle = configsingle.ConfigValue + nofaktur.Replace("/", "") + ".pdf";
                        fullPath = fileConfsingle;
                    }
                    totalDataFailed = VATOutSalesReturnIdList.Count() - totalfile;
                    if (totalfile <= 0)
                    {
                        result.ResultDesc = "File Not Exist";
                        result.ResultCode = false;
                    }
                    else
                    {
                        result.ResultDesc = totalfile + " data successfully downloaded <br/>";
                        result.ResultDesc += totalDataFailed + " data failed downloaded";
                    }
                    return Json(result.AsDynamic(ListZip), JsonRequestBehavior.AllowGet);
                }

                if (type == "PdfDgt")
                {
                    Int32 totalfile = 0;
                    var ListZip = new List<string>();
                    var list = VATOutSalesReturnRepository.Instance.getlistVATreturn(VATOutSalesReturnIdList);
                    totalfile = 0;
                    foreach (var obj in list)
                    {
                        if (obj.CreditNoteDGT.ToUpper() == "EXIST")
                        {
                            String[] myArr = new string[] { obj.Nomor };
                            string Nomor = obj.Nomor;
                            var str = Nomor;
                            var charstToRemove = new string[] { "/" };
                            foreach (var c in charstToRemove)
                            {
                                str = str.Replace(c, string.Empty);
                            }
                            Config config = MasterConfigRepository.Instance.GetByConfigKey("FilePrintCreditnoteDGT");
                            //   if 
                            string fileConf = config.ConfigValue + str + ".pdf";
                            try
                            {
                                if (System.IO.File.Exists(fileConf))
                                {
                                    totalfile += 1;
                                    ListZip.Add(fileConf);
                                }
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                    if (totalfile > 1)
                    {
                        fullPath = fullPath = ListZip.ToString();
                    }
                    else if (totalfile <= 1)
                    {

                        string fileConfsingle = ListZip.FirstOrDefault();
                        //string fileConfsingle = configsingle.ConfigValue + nofaktur.Replace("/", "") + ".pdf";
                        fullPath = fileConfsingle;
                    }
                    totalDataFailed = VATOutSalesReturnIdList.Count() - totalfile;
                    if (totalfile <= 0)
                    {
                        result.ResultDesc = "File Not Exist";
                        result.ResultCode = false;
                    }
                    else
                    {
                        result.ResultDesc = totalfile + " data successfully downloaded <br/>";
                        result.ResultDesc += totalDataFailed + " data failed downloaded";
                    }
                    return Json(result.AsDynamic(ListZip), JsonRequestBehavior.AllowGet);

                }
                else if (type == "PdfOriginal")
                {
                    Int32 totalfile = 0;
                    var ListZip = new List<string>();
                    var list = VATOutSalesReturnRepository.Instance.getlistVATreturn(VATOutSalesReturnIdList);
                    totalfile = 0;
                    foreach (var obj in list)
                    {
                        if (obj.CreditNoteOriginal.ToUpper() == "EXIST")
                        {
                            String[] myArr = new string[] { obj.Nomor };
                            string Nomor = obj.Nomor;
                            var str = Nomor;
                            var charstToRemove = new string[] { "/" };
                            foreach (var c in charstToRemove)
                            {
                                str = str.Replace(c, string.Empty);
                            }
                            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateGenerateNotaVATOutSalesRetur");
                            //   if 
                            string fileConf = config.ConfigValue + str + ".pdf";
                            try
                            {
                                if (System.IO.File.Exists(fileConf))
                                {
                                    totalfile += 1;
                                    ListZip.Add(fileConf);
                                }
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                    if (totalfile > 1)
                    {
                        ListZip.ToString();
                    }
                    else if (totalfile <= 1)
                    {

                        string fileConfsingle = ListZip.FirstOrDefault();
                        //string fileConfsingle = configsingle.ConfigValue + nofaktur.Replace("/", "") + ".pdf";
                        fullPath = fileConfsingle;
                    }
                    totalDataFailed = VATOutSalesReturnIdList.Count() - totalfile;
                    if (totalfile <= 0)
                    {
                        result.ResultDesc = "File Not Exist";
                        result.ResultCode = false;
                    }
                    else
                    {
                        result.ResultDesc = totalfile + " data successfully downloaded <br/>";
                        result.ResultDesc += totalDataFailed + " data failed downloaded";
                    }
                    return Json(result.AsDynamic(ListZip), JsonRequestBehavior.AllowGet);
                }
                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                //hardcodemessagesementara
                var pesan = e.Message;
                var replacefile = pesan.Replace(pesan, "Could not find file");
                if (replacefile == "Could not find file")
                {
                    return Json(result.AsDynamic(null), JsonRequestBehavior.AllowGet);
                }
                //batas
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("VATOut Print e-Faktur", MessageType.ERR, CurrentLogin.Username).Message;
            }
            return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
        }
        // ridwan//
        //[HttpRoute("~/api/GetAllTodos")]
        [HttpGet]
        public JsonResult GetListAPI(FormCollection collection)
        {
            Result result = new Result();
            string fullPath = string.Empty;
            string Message = "Succes";


            try
            {
                var vatOutSalesReturnIdList = "[\"7b9529a2-e25a-421e-a495-3a5452a6c1c8\"]";
                List<Guid> VATOutSalesReturnIdList = new List<Guid>();
                {
                    VATOutSalesReturnIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(vatOutSalesReturnIdList);
                }
                {
                    Config PdfPath = MasterConfigRepository.Instance.GetByConfigKey("UrlTempVATOutSalesReturPdf");


                    var fullPathpdf = PdfPath.ConfigValue;
                    fullPath = fullPathpdf;
                    var list = VATOutSalesReturnRepository.Instance.GetListAPI(VATOutSalesReturnIdList);
                    foreach (var obj in list)
                    {
                        PdfDocument pdfDocument = PdfReader.Open(fullPath, PdfDocumentOpenMode.Modify);
                        String[] myArr = new string[] {
                        //Header
                        obj.Nomor,
                        obj.Tanggal_Retur,
                        obj.Tanggal,
                        obj.Faktur_pajak,
                        //End
                        obj.Alamat_Pembeli,
                        obj.NamaCustomer,
                        obj.NPWPCustomer,

                        //paling bawah
                        obj.Jumlah_Harga_Jual,
                        obj.PPn,
                        obj.PPNBM,
                        obj.Nama_Barang
                        };

                        string Nomor = obj.Nomor;
                        string TanggalRetur = Convert.ToDateTime(obj.Tanggal_Retur).ToString("dd-MM-yyyy");
                        string Tanggal = Convert.ToDateTime(obj.Tanggal).ToString("dd-MM-yyyy");
                        string Faktur_pajak = obj.Faktur_pajak;
                        //End
                        //batastes
                        string alamatpembeli = obj.Alamat_Pembeli;
                        string NamaCustomer = obj.NamaCustomer;
                        string NPWPCustomer = obj.NPWPCustomer;
                        //penjual
                        string alamatpembeliPenj = obj.Alamat_Pembeli;
                        string NamaCustomerPenj = obj.NamaCustomer;
                        string NPWPCustomerPenJ = obj.NPWPCustomer;
                        //end

                        //TableBawah
                        string Jumlah_Harga_Jual = obj.Jumlah_Harga_Jual;
                        string PPn = obj.PPn;
                        string PPNBM = obj.PPNBM;
                        //end
                        PdfPage page = pdfDocument.Pages[0];
                        XGraphics gfx = XGraphics.FromPdfPage(page);
                        XFont font = new XFont("Arial", 7);
                        XFont fontHeader = new XFont("Arial", 9);
                        XFont fontbody = new XFont("Arial", 10);
                        //header//
                        gfx.DrawString(Nomor, fontHeader, XBrushes.Black, new XRect(-15, -358.5, page.Width, page.Height), XStringFormats.Center);
                        gfx.DrawString(Faktur_pajak, fontHeader, XBrushes.Black, new XRect(247, -341, page.Width, page.Height), XStringFormats.CenterLeft);
                        gfx.DrawString(Tanggal, fontHeader, XBrushes.Black, new XRect(120, -341, page.Width, page.Height), XStringFormats.Center);
                        //end
                        //Pembeli
                        gfx.DrawString(NamaCustomer, font, XBrushes.Black, new XRect(180, -308.5, page.Width, page.Height), XStringFormats.CenterLeft);
                        gfx.DrawString(alamatpembeli, font, XBrushes.Black, new XRect(180, -292, page.Width, page.Height), XStringFormats.CenterLeft);
                        gfx.DrawString(NPWPCustomer, font, XBrushes.Black, new XRect(180, -263.5, page.Width, page.Height), XStringFormats.CenterLeft);

                        gfx.DrawString("PT.TOYOTA - ASTRA MOTOR", font, XBrushes.Black, new XRect(180, -229, page.Width, page.Height), XStringFormats.CenterLeft);
                        gfx.DrawString("JALAN JENDERAL SUDIRMAN NO. 5 KARET TENGSIN/TANAH ABANG Alamat JAKARTA PUSAT DKI JAKARTA RAYA", font, XBrushes.Black, new XRect(180, -211.5, page.Width, page.Height), XStringFormats.CenterLeft);
                        gfx.DrawString(NPWPCustomerPenJ, font, XBrushes.Black, new XRect(180, -178.5, page.Width, page.Height), XStringFormats.CenterLeft);

                        gfx.DrawString("1", fontbody, XBrushes.Black, new XRect(18, -110, page.Width, page.Height), XStringFormats.CenterLeft);
                        gfx.DrawString("Toyota Fortuner", fontbody, XBrushes.Black, new XRect(100, -110, page.Width, page.Height), XStringFormats.CenterLeft);
                        gfx.DrawString("1", fontbody, XBrushes.Black, new XRect(250, -110, page.Width, page.Height), XStringFormats.CenterLeft);
                        gfx.DrawString(Jumlah_Harga_Jual, fontbody, XBrushes.Black, new XRect(320, -110, page.Width, page.Height), XStringFormats.CenterLeft);
                        gfx.DrawString(Jumlah_Harga_Jual, fontbody, XBrushes.Black, new XRect(475, -110, page.Width, page.Height), XStringFormats.CenterLeft);

                        gfx.DrawString(Jumlah_Harga_Jual, fontbody, XBrushes.Black, new XRect(475, -228, page.Width, page.Height), XStringFormats.BottomLeft);
                        gfx.DrawString(Jumlah_Harga_Jual, fontbody, XBrushes.Black, new XRect(475, -210, page.Width, page.Height), XStringFormats.BottomLeft);

                        var nomordocretur = Nomor;
                        var charsToRemove = new string[] { "/", "-" };
                        foreach (var c in charsToRemove)
                        {
                            nomordocretur = nomordocretur.Replace(c, string.Empty);
                        }
                        Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateGenerateNotaVATOutSalesRetur");
                        var urlPdf = config.ConfigValue;
                        pdfDocument.Save(urlPdf + nomordocretur + ".pdf");
                        string file = urlPdf + nomordocretur + ".pdf";
                        this.RedirectToAction(file);
                        System.Diagnostics.Process.Start(file);

                    }
                }
                return Json(result.AsDynamic(Message), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("VATOut Print e-Faktur", MessageType.ERR, CurrentLogin.Username).Message;
            }
            return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteVATOutSalesReturn(FormCollection collection, bool checkAll)
        {
            

            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "Delete successfully"
            };

            try
            {
                var searchParam = collection["data"];
                var vatOutSalesReturnIdList = collection["vatOutSalesReturnIdList"];
                var searchParamModel = new VATOutSalesReturnSearchParamViewModel();
                List<Guid> VATOutIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<VATOutSalesReturnSearchParamViewModel>(searchParam);
                    VATOutIdList = VATOutSalesReturnRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName);
                }
                else
                {

                    VATOutIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(vatOutSalesReturnIdList);
                }

                foreach (var g in VATOutIdList)
                {
                    string _pdfurl = VATOutSalesReturnRepository.Instance.GetById(g).PdfUrl;

                    VATOutSalesReturnRepository.Instance.Delete(g, CurrentHRIS.DivisionName);

                    if (Convert.ToString(_pdfurl) != "")
                    {
                        try
                        {


                            if (System.IO.File.Exists(_pdfurl))
                            {
                                System.IO.File.Delete(_pdfurl);
                            }
                        }
                        catch (Exception)
                        {


                        }
                    }
                }

                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);


            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("VAT Out Sales Delete Return", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }


    }
}

