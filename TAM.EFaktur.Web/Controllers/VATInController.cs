﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.VATIn;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class VATInController : BaseController
    {
        //
        // GET: /VATIn/

        bool isFullAccess { get; set; }

        protected override void Startup()
        {
            Settings.Title = "VAT-In Dashboard";
            
            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("VATInFullAccessRole"); //new by role

            string[] VATInFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();
           
            if (config != null)
            {
                VATInFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (VATInFullAccessRole.Contains(item))//abc;acc;abb
                    {
                        isFullAccess = true;
                        break;
                    }
                }
                
            }
            ViewBag.IsFullAccess = isFullAccess;
        }

        #region PopUp
        public ActionResult PopupMultipleSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupMultipleSearch");
        }

        public ActionResult PopupTextAreaSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupTextAreaSearch");
        }

        public ActionResult PopupUploadApprovalStatus()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateApprovalStatus");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadApprovalStatus");
        }

        public ActionResult PopupUploadTransitoryStatus()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateTransitoryStatus");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadTransitoryStatus");
        }

        public ActionResult PopupUploadEFakturFile()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateTaxInvoiceVATIn");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadEFakturFile");
        }

        public ActionResult PopupUploadEFakturFilePIB()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateTaxInvoiceVATInPIB");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadEFakturFilePIB");
        }

        public ActionResult PopupUploadMasaPajak()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateMasaPajak");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadMasaPajak");
        }

        #endregion

        [HttpPost]
        public ActionResult GetVATInList(FormCollection collection, int sortBy, bool sortDirection, int page, int size)
        {

            #region cek isFullAccess
            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("VATInFullAccessRole"); //new by role

            string[] VATInFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            if (config != null)
            {
                VATInFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (VATInFullAccessRole.Contains(item))//abc;acc;abb
                    {
                        isFullAccess = true;
                        break;
                    }
                }

            }
            #endregion

            var data = collection["data"];
            var model = new VATInDashboardSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<VATInDashboardSearchParamViewModel>(data);
            
            Paging pg = new Paging
            (
                VATInRepository.Instance.Count(model, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["VATInListDashboard"] = VATInRepository.Instance.GetList(model, CurrentHRIS.DivisionName, CurrentLogin.Username, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData, isFullAccess);

            return PartialView("_SimpleGrid");
        }

        [HttpPost]
        public JsonResult DeleteVATIn(FormCollection collection, bool checkAll)
        {
            #region cek isFullAccess
            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("VATInFullAccessRole"); //new by role

            string[] VATInFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            if (config != null)
            {
                VATInFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (VATInFullAccessRole.Contains(item))//abc;acc;abb
                    {
                        isFullAccess = true;
                        break;
                    }
                }

            }
            #endregion

            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "Delete successfully"
            };

            try
            {
                var searchParam = collection["data"];
                var vatInIdList = collection["vatInIdList"];
                var searchParamModel = new VATInDashboardSearchParamViewModel();
                List<Guid> VATInIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<VATInDashboardSearchParamViewModel>(searchParam);
                    VATInIdList = VATInRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess);
                }
                else
                {
                    VATInIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(vatInIdList);
                }

                result = VATInRepository.Instance.ValidateVATInTransitoryStatusUpdated(VATInIdList);
                if (!result.ResultCode)
                {
                    return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
                }
                result = VATInRepository.Instance.DeleteVATInById(VATInIdList, CurrentLogin.Username);

                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("VATIn Delete", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GenerateDownloadFile(FormCollection collection, string type, bool checkAll)
        {
            #region cek isFullAccess
            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("VATInFullAccessRole"); //new by role

            string[] VATInFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            if (config != null)
            {
                VATInFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (VATInFullAccessRole.Contains(item))//abc;acc;abb
                    {
                        isFullAccess = true;
                        break;
                    }
                }

            }
            #endregion

            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };
            string fullPath = string.Empty;
            string fullPathnonEFaktur = string.Empty;
            string fullPathnonEFakturother = string.Empty;
            string fullPathZIP = string.Empty;

            int totalDataCount = 0;
            int totalDataFailed = 0;
            try
            {
                var searchParam = collection["data"];
                var vatInIdList = collection["vatInIdList"];
                var searchParamModel = new VATInDashboardSearchParamViewModel();
                List<Guid> VATInIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<VATInDashboardSearchParamViewModel>(searchParam);
                    VATInIdList = VATInRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess);
                }
                else
                {
                    VATInIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(vatInIdList);
                }

                List<string[]> ListArr = new List<string[]>();// array for choose data
                List<string[]> ListArrnonEFaktur = new List<string[]>();
                List<string[]> ListArrnonEFakturother = new List<string[]>();

                #region xls type


                if (type == "xls")
                {
                    String[] header = new string[]
                    { "FM", "KD_JENIS_TRANSAKSI", "FG_PENGGANTI", "NOMOR_FAKTUR", "TANGGAL_FAKTUR",
                      "NPWP_PENJUAL", "NAMA_PENJUAL", "ALAMAT_PENJUAL","TURNOVER","JUMLAH_DPP",
                      "JUMLAH_PPN", "JUMLAH_PPNBM", "OF", "NAMA","HARGA_SATUAN", "JUMLAH_BARANG",
                      "HARGA_TOTAL","DISKON", "DPP", "PPN", "TARIF_PPNBM",
                      "PPNBM",
                      "STATUS_INVOICE", "NOMOR_INVOICE", "NOMOR_PV", "PV_CREATED_BY", "NOMOR_DOC_SAP",
                      "TANGGAL_POSTING", "VAT_BASED_GL_ACCOUNT", "DESKRIPSI_APPROVAL"
                    }; //for header 


                    ListArr.Add(header);


                    var list = VATInRepository.Instance.GetXlsById(VATInIdList);
                    foreach (VATInOriginalViewModel obj in list)
                    {
                        String[] myArr = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field30,
                            obj.Field9,

                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field16,
                            obj.Field17,
                            obj.Field18,
                            obj.Field19,
                            obj.Field20,
                            obj.Field21,

                            obj.Field22,
                            obj.Field23,
                            obj.Field24,
                            obj.Field25,
                            obj.Field26,
                            obj.Field27,
                            obj.Field28,
                            obj.Field29
                         };
                        ListArr.Add(myArr);
                    }
                    totalDataCount = VATInIdList.Count();
                    result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    fullPath = XlsHelper.PutExcel(ListArr, new VATInDashboardViewModel(), TempDownloadFolder, "VATIn");
                    fullPathZIP = fullPath;
                }
                #endregion
                #region report excel
                else if (type == "report")
                {
                    String[] header = new string[]
                    { "Tax Invoice Number", "Tax Invoice For ELVIS", "Tax Date For ELVIS", "Invoice Status", "Supplier Name", "Supplier NPWP", "Supplier Address", "VAT Based Amount", "VAT Amount", "PPnBM", "Tax Invoice Date",
                      "Invoice Number", "Tax Period Month", "Tax Period Year", "Days To Expired", "Tax Invoice Type", "Remark", "Record Date",
                      "Recorded By", "Receive File Date", "Receive File Time", "PV Number", "PV Created By", "SAP Doc Number", "Posting Date",
                      "Download Status", "Download Date", "Record Status", "Approval Status", "Approval Date", "Approval Description", "Transitory Status",
                      "Transitory Number", "Transitory Date", "VAT Based GL Account", "Link DJP", "Batch File Name"
                    }; //for header

                    ListArr.Add(header);

                    var list = VATInRepository.Instance.GetReportExcelById(VATInIdList);
                    foreach (VATInReportExcelViewModel obj in list)
                    {
                        String[] myArr = new string[] {
                            obj.Field1,
                            obj.Field36,
                            obj.Field37,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field35,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field16,
                            obj.Field17,
                            obj.Field18,
                            obj.Field19,
                            obj.Field20,
                            obj.Field21,
                            obj.Field22,
                            obj.Field23,
                            obj.Field24,
                            obj.Field25,
                            obj.Field26,
                            obj.Field27,
                            obj.Field28,
                            obj.Field29,
                            obj.Field30,
                            obj.Field31,
                            obj.Field32,
                            obj.Field33,
                            obj.Field34
                        };
                        ListArr.Add(myArr);
                    }
                    totalDataCount = VATInIdList.Count();
                    result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    fullPath = XlsHelper.PutExcel(ListArr, new VATInDashboardViewModel(), TempDownloadFolder, "VATInReport");
                    fullPathZIP = fullPath;
                }
                #endregion
                #region csv type

                else if (type == "csv")
                {
                    #region eFaktur
                    String[] header = new string[]
                    {"FM", "KD_JENIS_TRANSAKSI","FG_PENGGANTI", "NOMOR_FAKTUR", "MASA_PAJAK", "TAHUN_PAJAK",
                      "TANGGAL_FAKTUR", "NPWP", "NAMA", "ALAMAT_LENGKAP","JUMLAH_DPP","JUMLAH_PPN",
                      "JUMLAH_PPNBM","IS_CREDITABLE",
                    }; //for header

                    ListArr.Add(header);

                    var list = VATInRepository.Instance.GetCSVById(VATInIdList);
                    foreach (VATInCSVViewModel obj in list)
                    {
                        String[] myArr = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14
                        };
                        ListArr.Add(myArr);
                    }

                    if (list.Count > 0)
                    {
                        List<Guid> id = new List<Guid>();
                        foreach (VATInCSVViewModel obj in list)
                        {
                            id.Add(obj.VATInId);
                        }

                        fullPath = CSVHelper.PutCSV(ListArr, new VATInDashboardViewModel(), TempDownloadFolder, "VATIn");
                        VATInRepository.Instance.UpdateFlagCSV(id, fullPath, CurrentLogin.Username);
                    }
                    #endregion

                    #region non eFaktur

                    String[] headernoneFaktur = new string[]
                    {"DK_DM", "JENIS_TRANSAKSI","JENIS_DOKUMEN", "KD_JENIS_TRANSAKSI", "FG_PENGGANTI", "NOMOR_DOK_LAIN_GANTI",
                        "NOMOR_DOK_LAIN", "TANGGAL_DOK_LAIN", "MASA_PAJAK", "TAHUN_PAJAK","NPWP","NAMA",
                        "ALAMAT_LENGKAP","JUMLAH_DPP","JUMLAH_PPN","JUMLAH_PPNBM","KETERANGAN","TGL_APPROVAL",
                    }; //for header

                    ListArrnonEFaktur.Add(headernoneFaktur);

                    var listnonefaktur = VATInRepository.Instance.GetCSVnonEFakturById(VATInIdList);
                    foreach (VATInCSVViewModel obj in listnonefaktur)
                    {
                        String[] myArrnonefaktur = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field16,
                            obj.Field17,
                            obj.Field18,
                            };
                        ListArrnonEFaktur.Add(myArrnonefaktur);
                    }

                    if (listnonefaktur.Count > 0)
                    {
                        List<Guid> id = new List<Guid>();
                        foreach (VATInCSVViewModel obj in listnonefaktur)
                        {
                            id.Add(obj.VATInId);
                        }

                        fullPathnonEFaktur = CSVHelper.PutCSV(ListArrnonEFaktur, new VATInDashboardViewModel(), TempDownloadFolder, "VATIn_nonEFaktur");
                        VATInRepository.Instance.UpdateFlagCSV(id, fullPathnonEFaktur, CurrentLogin.Username);
                    }
                    #endregion

                    #region non eFaktur dok lain

                    String[] headernoneFakturother = new string[]
                    {"DK_DM", "JENIS_TRANSAKSI","JENIS_DOKUMEN", "KD_JENIS_TRANSAKSI", "FG_PENGGANTI", "NOMOR_DOK_LAIN_GANTI",
                        "NOMOR_DOK_LAIN", "TANGGAL_DOK_LAIN", "MASA_PAJAK", "TAHUN_PAJAK","NPWP","NAMA",
                        "ALAMAT_LENGKAP","JUMLAH_DPP","JUMLAH_PPN","JUMLAH_PPNBM","KETERANGAN","TGL_APPROVAL",
                    }; //for header

                    ListArrnonEFakturother.Add(headernoneFakturother);

                    var listnonefakturother = VATInRepository.Instance.GetCSVnonEFakturOtherById(VATInIdList);
                    foreach (VATInCSVViewModel obj in listnonefakturother)
                    {
                        String[] myArrnonefakturother = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field16,
                            obj.Field17,
                            obj.Field18,
                            };
                        ListArrnonEFakturother.Add(myArrnonefakturother);
                    }

                    if (listnonefakturother.Count > 0)
                    {
                        List<Guid> id = new List<Guid>();
                        foreach (VATInCSVViewModel obj in listnonefakturother)
                        {
                            id.Add(obj.VATInId);
                        }

                        fullPathnonEFakturother = CSVHelper.PutCSV(ListArrnonEFakturother, new VATInDashboardViewModel(), TempDownloadFolder, "VATIn_nonEFaktur_DokLain");
                        VATInRepository.Instance.UpdateFlagCSV(id, fullPathnonEFakturother, CurrentLogin.Username);
                    }
                    #endregion

                    totalDataCount = (ListArr.Count() - 1) + (ListArrnonEFaktur.Count() - 1) + (ListArrnonEFakturother.Count() - 1);
                    totalDataFailed = VATInIdList.Count() - totalDataCount;
                    result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    result.ResultDesc += totalDataFailed + " data failed downloaded";

                    List<string> filePaths = new List<string>();
                    filePaths.Add(fullPath);
                    filePaths.Add(fullPathnonEFaktur);
                    filePaths.Add(fullPathnonEFakturother);
                    fullPathZIP = GlobalFunction.GenerateZipFile(filePaths, TempZipFolder);

                    var countFilePaths = filePaths.Where(s => s != "").Count();
                    if (countFilePaths > 1)
                    {
                        fullPathZIP = GlobalFunction.GenerateZipFile(filePaths, TempZipFolder);
                    }else
                    {
                        if (!string.IsNullOrEmpty(fullPath)) fullPathZIP = fullPath;
                        else if (!string.IsNullOrEmpty(fullPathnonEFaktur)) fullPathZIP = fullPathnonEFaktur;
                        else fullPathZIP = fullPathnonEFakturother;
                    }
                }
                #endregion
                return Json(result.AsDynamic(fullPathZIP), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("VATIn Download " + type, MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UploadVATIn()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);

                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT Out

                    result = VATInRepository.Instance.ExcelTaxInvoiceInsert(fullPath, CurrentLogin.Username);
                    #endregion
                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        public ActionResult UploadVATInPIB()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);

                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT Out

                    result = VATInRepository.Instance.ExcelTaxInvoiceInsertPIB(fullPath, CurrentLogin.Username);
                    #endregion
                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        [HttpPost]
        public ActionResult UploadApprovalStatus()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
               // var fullPath = Path.Combine(@"D:\Interface\Temp\Upload", Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);
                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT In

                    result = VATInRepository.Instance.ExcelApprovalStatusUpdate(fullPath, CurrentLogin.Username);
                    #endregion

                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        [HttpPost]
        public ActionResult UploadTransitoryStatus()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);
                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT In

                    result = VATInRepository.Instance.ExcelTransitoryStatusUpdate(fullPath, CurrentLogin.Username);
                    #endregion

                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        public ActionResult UploadMasaPajak()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);
                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT In

                    result = VATInRepository.Instance.ExcelMasaPajakUpdate(fullPath, CurrentLogin.Username);
                    #endregion

                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        [HttpPost]
        public ActionResult SyncVATIn()
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "Sync Success"
            };

            try
            {
                result = VATInRepository.Instance.SyncVATIn();
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("VATIn Sync Failed", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }
    }
}