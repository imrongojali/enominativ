﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Log_Sync;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class Log_SyncController : PageController
    {
        //
        // GET: /Log_Sync/

        public Log_SyncController()
        {
            Settings.Title = "Synchronization Log";
        }

        [HttpPost]
        public ActionResult GetLogSyncList(FormCollection collection, int sortBy, bool sortDirection, int page, int size)
        {
            var data = collection["data"];
            var model = new LogSyncDashboardSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<LogSyncDashboardSearchParamViewModel>(data);

            Paging pg = new Paging
            (
                LogSyncRepository.Instance.Count(model),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["LogSyncListDashboard"] = LogSyncRepository.Instance.GetList(model, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData);

            return PartialView("_SimpleGrid");
        }
    }
}
