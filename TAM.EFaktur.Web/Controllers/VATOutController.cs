﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.ParameterSetup;
using TAM.EFaktur.Web.Models.VATOut;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class VATOutController : BaseController
    {
        //
        // GET: /VATOutDashboard/
        bool isFullAccess { get; set; }
        protected override void Startup()
        {
            Settings.Title = "VAT-Out Dashboard";
            bool isFullAccess = false;
            string divisionFullAccess = string.Empty;
            if (!CurrentHRIS.DivisionName.ToLower().Contains("lexus"))
            {
                isFullAccess = true;
            }
            ViewBag.IsFullAccess = isFullAccess;
            try
            {
                ViewBag.Notif_Days = ParameterSetupRepository.Instance.GetParameterByCode("NOTIF_DAYS_FOR_VATOUT_SALES").parameter_value; //TODO: Later change  to real user

            }
            catch (Exception)
            {

                ViewBag.Notif_Days = "0";
                ;
            }


        }

        #region Popup
        public ActionResult PopupMultipleSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupMultipleSearch");
        }

        public ActionResult PopupTextAreaSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupTextAreaSearch");
        }

        public ActionResult PopupUploadApprovalStatus()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateApprovalStatus");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadApprovalStatus");
        }

        public ActionResult PopupUploadTransitoryStatus()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateTransitoryStatus");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadTransitoryStatus");
        }

        public ActionResult PopupUploadTaxInvoice()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateTaxInvoiceVATOut");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadTaxInvoice");
        }
        //add 12102020 pop up upload tax invoice motor vehicle 
        public ActionResult PopupUploadTaxDataMotorVehicle()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateTaxDataMotorVehicleVATOut");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";            
            return PartialView("_PopupUploadTaxDataMotorVehicle");            
        }

        public ActionResult PopupUploadPDFFile()
        {
            return PartialView("_PopupUploadPDFFile");
        }
        #endregion

        [HttpPost]
        public ActionResult GetVATOutList(FormCollection collection, string sortBy, bool sortDirection, int page, int size, string Id )
        {
            var data = collection["data"];
            var model = new VATOutDashboardSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<VATOutDashboardSearchParamViewModel>(data);

            //ADD AD 13112020
            if (model.EFakturStatus != ""  )
            {
                if(model.EFakturStatus != null)
                {
                    var _efaktur = model.EFakturStatus;
                    var _hasilreplace = _efaktur.Replace(";", "','");
                    string _color = "'" + _hasilreplace + "'";
                    model.EFakturStatus = _color;

                }
                
            }
            //END
            try
            {
                Paging pg = new Paging
                (
                    VATOutRepository.Instance.Count(model, CurrentHRIS.DivisionName, page,size,Id),
                    page,
                    size
                );
                ViewData["Paging"] = pg;
                ViewData["VATOutListDashboard"] = VATOutRepository.Instance.GetList(model, CurrentHRIS.DivisionName, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData, Id);

            }
            catch (Exception e)
            {                
                throw new Exception(e.Message);
            }
         
            return PartialView("_SimpleGrid");
        }

        [HttpPost]
        public JsonResult GeneratePrintFile(FormCollection collection, bool checkAll)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for printing is generated successfully"
            };
            string fullName = string.Empty;
            string fullPath = string.Empty;
            try
            {
                var searchParam = collection["data"];
                var vatOutIdList = collection["vatOutIdList"];
                var searchParamModel = new VATOutDashboardSearchParamViewModel();
                List<Guid> VATOutIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<VATOutDashboardSearchParamViewModel>(searchParam);
                    VATOutIdList = VATOutRepository.Instance.GetIdBySearchParam(searchParamModel,CurrentHRIS.DivisionName);
                }
                else
                {
                    VATOutIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(vatOutIdList);
                }

                var filePaths = VATOutRepository.Instance.GetPDFUrlById(VATOutIdList);
                fullName = GlobalFunction.MergePDFFiles(filePaths, TempPrintFolder, CurrentLogin.Username);
                if (!string.IsNullOrEmpty(fullName))
                {
                    fullPath = VirtualDirectoryFilePrintURL + fullName;
                }
                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("VATOut Print e-Faktur", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GenerateDownloadFile(FormCollection collection, string type, bool checkAll)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download is generated successfully"
            };
            string fullPath = string.Empty;
            int totalDataCount = 0;
            int totalDataFailed = 0;
            int totalCheckGrid = 0;
            int totalDownloadCSV = 0;
            int TotalDownloadCSVFailed = 0;

            try
            {
                var searchParam = collection["data"];
                var vatOutIdList = collection["vatOutIdList"];
                var searchParamModel = new VATOutDashboardSearchParamViewModel();
                List<Guid> VATOutIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<VATOutDashboardSearchParamViewModel>(searchParam);
                    VATOutIdList = VATOutRepository.Instance.GetIdBySearchParam(searchParamModel,CurrentHRIS.DivisionName);
                }
                else
                {
                    VATOutIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(vatOutIdList);
                }
                totalDataCount = VATOutIdList.Count();
                result.ResultDesc = totalDataCount + " data successfully downloaded";
                List<string[]> ListArr = new List<string[]>();// array for choose data
                
                #region xls type
                if (type == "xls")
                {
                    String[] header = new string[] 
                    { "DA_NUMBER", "BUSINESS_UNIT", "FK", "KD_JENIS_TRANSAKSI", "FG_PENGGANTI", 
                      "NOMOR_FAKTUR", "MASA_PAJAK", "TAHUN_PAJAK", "TANGGAL_FAKTUR", "NPWP", 
                      "NAMA", "ALAMAT_LENGKAP", "JUMLAH_DPP", "JUMLAH_PPN", "JUMLAH_PPNBM",
                      "ID_KETERANGAN_TAMBAHAN", "FG_UANG_MUKA", "UANG_MUKA_DPP", "UANG_MUKA_PPN", "UANG_MUKA_PPNBM",
                      "REFERENSI", "OF", "KODE_OBJEK", "NAMA_BARANG", "FRAME_NO" , "ENGINE_NO", "MODEL_TYPE" , "HARGA_SATUAN", "JUMLAH_BARANG", "HARGA_TOTAL", "DISKON", "DPP", "PPN",
                      "TARIF_PPNBM", "PPNBM", "PPNBM_LINI_SEBELUMNYA"
                    }; //for header

                    ListArr.Add(header);

                    var list = VATOutRepository.Instance.GetOriginalById(VATOutIdList);
                    foreach (VATOutOriginalViewModel obj in list)
                    {
                        String[] myArr = new string[] { 
                            obj.Field1, 
                            obj.Field2, 
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field16,
                            obj.Field17,
                            obj.Field18,
                            obj.Field19,
                            obj.Field20,
                            obj.Field21,
                            obj.Field22,
                            obj.Field23,
                            obj.Field24,
                            obj.Field25,
                            obj.Field26,
                            obj.Field27,
                            obj.Field28,
                            obj.Field29,
                            obj.Field30,
                            obj.Field31,
                            obj.Field32,
                            obj.Field33,
                            obj.Field34,
                            obj.Field35,
                            obj.Field36,
                        };
                        ListArr.Add(myArr);
                    }
                    fullPath = XlsHelper.PutExcel(ListArr, new VATOutDashboardViewModel(), TempDownloadFolder, "VATOut");
                    //Test di local, add By DA
                    //fullPath = XlsHelper.PutExcel(ListArr, new VATOutDashboardViewModel(), "D:\\\\EFB_PATH\\Download", "VATOut");
                }
                #endregion
                #region report excel
                else if (type == "report")
                {
                    String[] header = new string[] 
                    { "Transaction Data File", "Tax Invoice Number", "Debit Advice Number", "Customer NPWP", "Customer Name", 
                      "VAT Based Amount", "VAT Amount", "Business Unit", "Tax Invoice Date/Debit Advice Date", "Receive File Date", 
                      "Receive File Time", "Record Status", "Record Date", "Download Status", "Download Date",
                      "Download Time", "Approval Status", "Approval Date", "Transitory Status", "Transitory Number", "Transitory Date", "Batch File Name", "E-Faktur PDF File Status"
                      , "E-Faktur Status"
                    }; //for header

                    ListArr.Add(header);
                    //List<VATOutReportExcelViewModel> list = new List<VATOutReportExcelViewModel>();
                    //if (!string.IsNullOrEmpty(searchParam) && checkAll)
                    //{
                    //    list = VATOutRepository.Instance.GetReportExcelAllByIdSearchParam(searchParamModel, CurrentHRIS.DivisionName);
                    //}
                    //else
                    //{
                    //    list = VATOutRepository.Instance.GetReportExcelById(VATOutIdList);
                    //}
                    var list = VATOutRepository.Instance.GetReportExcelById(VATOutIdList);
                    foreach (VATOutReportExcelViewModel obj in list)
                    {
                        String[] myArr = new string[] { 
                            obj.Field1, 
                            obj.Field2, 
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field16,
                            obj.Field17,
                            obj.Field18,
                            obj.Field19,
                            obj.Field20,
                            obj.Field21,
                            obj.Field22,
                            obj.Field23,
                            obj.Field24,
                        };
                        ListArr.Add(myArr);
                    }
                    fullPath = XlsHelper.PutExcel(ListArr, new VATOutDashboardViewModel(), TempDownloadFolder, "VATOutReport");
                    //fullPath = XlsHelper.PutExcel(ListArr, new VATOutDashboardViewModel(), "D:\\\\EFB_PATH\\Download", "VATOutReport");
                }
                #endregion
                #region csv type
                else if (type == "csv")
                {
                    String[] headerFK = new string[] 
                    { "FK", "KD_JENIS_TRANSAKSI", "FG_PENGGANTI", "NOMOR_FAKTUR", "MASA_PAJAK", 
                      "TAHUN_PAJAK", "TANGGAL_FAKTUR", "NPWP", "NAMA", "ALAMAT_LENGKAP", 
                      "JUMLAH_DPP", "JUMLAH_PPN", "JUMLAH_PPNBM", "ID_KETERANGAN_TAMBAHAN", "FG_UANG_MUKA",
                      "UANG_MUKA_DPP", "UANG_MUKA_PPN", "UANG_MUKA_PPNBM", "REFERENSI"
                    }; //for header FK

                    String[] headerLT = new string[] 
                    { "LT", "NPWP", "NAMA", "JALAN", "BLOK", 
                      "NOMOR", "RT", "RW", "KECAMATAN", "KELURAHAN", 
                      "KABUPATEN", "PROPINSI", "KODE_POS", "NOMOR_TELEPON", null,
                      null, null, null, null
                    }; //for header LT

                    String[] headerOF = new string[] 
                    { "OF", "KD_OBJEK", "NAMA", "HARGA_SATUAN", "JUMLAH_BARANG", 
                      "HARGA_TOTAL", "DISKON", "DPP", "PPN", "TARIF_PPNBM", 
                      "PPNBM", null, null, null, null,
                      null, null, null, null
                    }; //for header OF


                    ListArr.Add(headerFK);
                    ListArr.Add(headerLT);
                    ListArr.Add(headerOF);

                    var list = VATOutRepository.Instance.GetCSVById(VATOutIdList);
                    foreach (VATOutCSVViewModel obj in list)
                    {
                        String[] myArr = new string[] { 
                            obj.Field1, 
                            obj.Field2, 
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field16,
                            obj.Field17,
                            obj.Field18,
                            obj.Field19,
                        };
                        ListArr.Add(myArr);
                    }
                    //totalDataCount = ListArr.Count() - 3;
                    //totalDataFailed = VATOutIdList.Count() - totalDataCount;
                    //totalCheckGrid = totalDataCount - totalDataFailed;
                    totalDownloadCSV = VATOutRepository.Instance.GetCountCSVById(VATOutIdList);
                    TotalDownloadCSVFailed = VATOutIdList.Count() - totalDownloadCSV;
                    result.ResultDesc = totalDownloadCSV + " data successfully downloaded <br/>";
                    result.ResultDesc += TotalDownloadCSVFailed + " data failed downloaded";
                    fullPath = CSVHelper.PutCSV(ListArr, new VATOutDashboardViewModel(), TempDownloadFolder, "VATOut");
                    VATOutRepository.Instance.UpdateFlagCSV(VATOutIdList, fullPath, CurrentLogin.Username);
                }
                #endregion
                #region pdf type to zip
                else if (type == "pdf")
                {
                    var filePaths = VATOutRepository.Instance.GetPDFUrlById(VATOutIdList);
                    fullPath = GlobalFunction.GenerateZipFile(filePaths, TempZipFolder);
                }
                #endregion

                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("VATOut Download " + type, MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UploadVATOut()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);

                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT Out

                    result = VATOutRepository.Instance.ExcelTaxInvoiceInsert(fullPath, CurrentLogin.Username);                    
                    #endregion
                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        //add 12102020 untuk upload tax invoice motor vehicle
        public ActionResult UploadVATOutMotorVehicle()
        {
          
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));                
                //var fullPath = Path.Combine("D:\\\\Interface\\Temp\\Upload\\", Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);

                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT Out
                    result = VATOutRepository.Instance.ExcelTaxInvoiceInsertMotorVehicle(fullPath, CurrentLogin.Username);
                    //save username pakai Test Dina, user name ga kebaca karna tidak login
                    //result = VATOutRepository.Instance.ExcelTaxInvoiceInsertMotorVehicle(fullPath, "Test Dev");
                    #endregion
                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        [HttpPost]
        public ActionResult UploadApprovalStatus()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);
                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT Out

                    result = VATOutRepository.Instance.ExcelApprovalStatusUpdate(fullPath, CurrentLogin.Username);
                    #endregion

                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel File Type allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }


        [HttpPost]
        public ActionResult UploadTransitoryStatus()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);
                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT Out

                    result = VATOutRepository.Instance.ExcelTransitoryStatusUpdate(fullPath, CurrentLogin.Username);
                    #endregion

                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel File Type allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        // Add delete by DA
        public JsonResult DeleteVATOutSales(FormCollection collection, bool checkAll)
        {
            //#region Cek VAT Out Full Access Role
            //int countArr = CurrentLogin.Roles.Count();

            //List<string> RoleId = new List<string>();

            //for (int i = 0; i < countArr; i++)
            //{
            //    RoleId.Add(CurrentLogin.Roles[i].Id);
            //}

            //Config config = MasterConfigRepository.Instance.GetByConfigKey("VATOutFullAccessRole");//new by role


            //string[] VATOutFullAccessRole = { "" };
            //string[] RoleIds = RoleId.ToArray();

            //if (config != null)
            //{
            //    VATOutFullAccessRole = config.ConfigValue.Split(';');
            //    foreach (var item in RoleIds)
            //    {
            //        if (VATOutFullAccessRole.Contains(item))
            //        {
            //            isFullAccess = true;
            //        }
            //    }
            //}
            //#endregion

            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "Delete successfully"
            };

            try
            {
                var searchParam = collection["data"];
                var vatOutIdList = collection["vatOutIdList"];
                var searchParamModel = new VATOutDashboardSearchParamViewModel();
                List<Guid> VATOutIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<VATOutDashboardSearchParamViewModel>(searchParam);
                    VATOutIdList = VATOutRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName);
                }
                else
                {
                  
                    VATOutIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(vatOutIdList);
                }

                foreach (var g in VATOutIdList)
                {
                    string _pdfurl = VATOutRepository.Instance.GetById(g).PdfUrl;

                    VATOutRepository.Instance.DeleteVatOutSales(g);

                    if (Convert.ToString(_pdfurl) != "")
                    {
                        try
                        {

                        
                        if (System.IO.File.Exists(_pdfurl))
                        {
                            System.IO.File.Delete(_pdfurl);
                        }
                        }
                        catch (Exception)
                        {

                           
                        }
                    }
                }

                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);

              
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("VAT Out Sales Delete", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        #region Old Upload Code
        //public JsonResult Upload()
        //{
        //    try
        //    {
        //        HttpPostedFileBase fileRequest = Request.Files[0] as HttpPostedFileBase;
        //        if (fileRequest != null && fileRequest.ContentLength > 0)
        //        {
        //            var fileName = Path.GetFileName(fileRequest.FileName);

        //            string ext = Path.GetExtension(fileName);

        //            if (ext.Equals(".xls") || ext.Equals(".xlsx"))
        //            {
        //                var path = Path.Combine(Server.MapPath("~/Content/UploadFile"), fileName);
        //                fileRequest.SaveAs(path);

        //                if (System.IO.File.Exists(fileName)) System.IO.File.Delete(fileName);

        //                var message = VATOutRepository.Instance.ExcelTaxInvoiceInsert(path, CurrentLogin.Username);
        //                return Json(message);
        //            }
        //            else
        //            {
        //                return Json(new { success = true, error = "Error in opening Excel because Type File NOT xls or NOT xlsx !!! " });
        //            }
        //        }
        //        else
        //        {
        //            return Json(new { success = true, error = "File is empty" });
        //        }
        //    }
        //    catch (Exception x)
        //    {
        //        return Json(new { success = false, error = x.Message });
        //    }
        //}
        #endregion

    }
}
