﻿
using System;
using System.Collections.Generic;
using System.IO;

using System.Linq;

using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;

using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.ParameterSetup;
using TAM.EFaktur.Web.Models.WHT;


namespace TAM.EFaktur.Web.Controllers
{
    //[HandleError]
    public class WHTController : BaseController
    {
        //
        // GET: /WHT/
        bool isFullAccess { get; set; }
        protected override void Startup()
        {
            Settings.Title = "Withholding Tax Dashboard";


            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("WHTFullAccessRole");//new by role


            string[] WHTFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            if (config != null)
            {
                WHTFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (WHTFullAccessRole.Contains(item))
                    {
                        isFullAccess = true;
                    }
                }
            }
            ViewBag.IsFullAccess = isFullAccess;
            try
            {
                ViewBag.Notif_Days = ParameterSetupRepository.Instance.GetParameterByCode("NOTIF_DAYS_FOR_WHT").parameter_value; //TODO: Later change  to real user

            }
            catch (Exception)
            {

                ViewBag.Notif_Days = "0";
            }
      
        }

        #region PopUp
        public ActionResult PopupMultipleSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupMultipleSearch");
        }

        public ActionResult PopupTextAreaSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupTextAreaSearch");
        }

        public ActionResult PopupUploadApprovalStatus()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateApprovalStatusWHT");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadApprovalStatus");
        }

        //by imron
        public ActionResult PopupUploadEbupotApproval()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateApprovalStatusWHTEBupot");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadEBupotApproval");
        }

        public ActionResult PopupUploadEBupotFile()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateEBupotFile");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadEBupotFile");
        }

        public ActionResult PopupUploadNIK()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateNIK");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadNIK");
        }

        public ActionResult PopupUploadWHTEBupotFile()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateUploadEBupot");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadWHTEBupotFile");
        }

        //-------
        public ActionResult PopupUploadTransitoryStatus()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateTransitoryStatus");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadTransitoryStatus");
        }

        public ActionResult PopupUploadEFakturFile()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateTaxInvoiceVATIn");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadEFakturFile");
        }

        public ActionResult PopupUploadWHTFile()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateUploadWHT");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadWHTFile");
        }
        #endregion

        [HttpPost]
        public ActionResult GetWHTList(FormCollection collection, int sortBy, bool sortDirection, int page, int size, string Id)
        {

            #region Cek Wht Full Access Role
            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("WHTFullAccessRole");//new by role


            string[] WHTFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            if (config != null)
            {
                WHTFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (WHTFullAccessRole.Contains(item))
                    {
                        isFullAccess = true;
                    }
                }
            }
            #endregion

            var data = collection["data"];
            var model = new WHTDashboardSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<WHTDashboardSearchParamViewModel>(data);

            //ADD AD 13112020
            if (model.EFakturStatus != "")
            {
                if (model.EFakturStatus != null)
                {
                    var _efaktur = model.EFakturStatus;
                    var _hasilreplace = _efaktur.Replace(";", "','");
                    string _color = "'" + _hasilreplace + "'";
                    model.EFakturStatus = _color;
                    //model.EFakturStatus = "'yellow'";

                }

            }
            //END
            try
            {
                Paging pg = new Paging
               (
                   WHTRepository.Instance.Count(model, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess,page,size,Id),
                   page,
                   size
               );
                    ViewData["Paging"] = pg;
                    ViewData["WHTListDashboard"] = WHTRepository.Instance.GetList(model, CurrentHRIS.DivisionName, CurrentLogin.Username, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData, isFullAccess, Id);
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
           

            return PartialView("_SimpleGrid");
        }

        [HttpPost]
        public JsonResult DeleteWHT(FormCollection collection, bool checkAll)
        {
            #region Cek Wht Full Access Role
            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("WHTFullAccessRole");//new by role


            string[] WHTFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            if (config != null)
            {
                WHTFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (WHTFullAccessRole.Contains(item))
                    {
                        isFullAccess = true;
                    }
                }
            }
            #endregion

            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "Delete successfully"
            };

            try
            {
                var searchParam = collection["data"];
                var whtIdList = collection["whtIdList"];
                var searchParamModel = new WHTDashboardSearchParamViewModel();
                List<Guid> WHTIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<WHTDashboardSearchParamViewModel>(searchParam);
                    WHTIdList = WHTRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess);
                }
                else
                {
                    WHTIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(whtIdList);
                }

                //result = WHTRepository.Instance.ValidateVATInTransitoryStatusUpdated(WHTIdList);
                //if (!result.ResultCode)
                //{
                //    return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
                //}
                result = WHTRepository.Instance.DeleteWHTById(WHTIdList, CurrentLogin.Username);

                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("WHT Delete", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GenerateDownloadFile(FormCollection collection, string type, bool checkAll)
        {
            #region Cek Wht Full Access Role
            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("WHTFullAccessRole");//new by role


            string[] WHTFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            if (config != null)
            {
                WHTFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (WHTFullAccessRole.Contains(item))
                    {
                        isFullAccess = true;
                    }
                }
            }
            #endregion

            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };

            string fullPath = string.Empty;
            int totalDataCount = 0;
            int totalDataFailed = 0;
            try
            {
                var searchParam = collection["data"];
                var whtIdList = collection["whtIdList"];
                var searchParamModel = new WHTDashboardSearchParamViewModel();
                List<Guid> WHTIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<WHTDashboardSearchParamViewModel>(searchParam);
                    WHTIdList = WHTRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess);
                }
                else
                {
                    WHTIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(whtIdList);
                }

                List<string[]> ListArr = new List<string[]>();

                #region xls
                if (type == "xls")
                {
                    String[] header = new string[]
                    { "Tax Article","WHT Number","Invoice Status","Supplier Name","Original Supplier Name",
                      "Supplier NPWP","Supplier Address","Accumulated WHT Based Amount","Turnover","WHT Based Amount","WHT Tarif","WHT Amount",
                      "Description","Invoice Date","Invoice Number","eBupot Reference No", "Tax Type","Map Code","SPT Type","WHT Date","Tax Period Month","Tax Period Year","Additional Information",
                      "PV Number","PV Created By","SAP Document Number","Posting Date",
                      "Download Status","Download Date","Record Status","Record Date","Receive File Date","Receive File Time","Approval Status","Approval Date",
                      "Approval Description","WHT Based GL Account","PDF WHT Status","Batch File Name","Efaktur Status"
                    }; //for header 


                    ListArr.Add(header);


                    var list = WHTRepository.Instance.GetXlsById(WHTIdList);
                    foreach (WHTOriginalViewModel obj in list)
                    {
                        var efakturstatus = obj.Field40.Split('|');
                        String[] myArr = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field34,
                            obj.Field35,
                            obj.Field37,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field36,
                            obj.Field11,
                            obj.Field38,
                            obj.Field12,
                            obj.Field39,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field16,
                            obj.Field17,
                            obj.Field18,
                            obj.Field19,
                            obj.Field20,
                            obj.Field21,
                            obj.Field22,
                            obj.Field23,
                            obj.Field24,
                            obj.Field25,
                            obj.Field26,
                            obj.Field27,
                            obj.Field28,
                            obj.Field29,
                            obj.Field30,
                            obj.Field31,
                            obj.Field32,
                            obj.Field33,
                            efakturstatus[1]
                         };
                        ListArr.Add(myArr);
                    }
                    totalDataCount = ListArr.Count() - 1;
                    totalDataFailed = WHTIdList.Count() - totalDataCount;
                    result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    result.ResultDesc += totalDataFailed + " data failed downloaded";
                    fullPath = XlsHelper.PutExcel(ListArr, new WHTDashboardViewModel(), TempDownloadFolder, "WHT");
                }
                #endregion
                #region xls report
                if (type == "xlsreport")
                {
                    String[] header = new string[]
                    { "Tax Article", "WHT Number", "Invoice Status", "Supplier Name", "Original Supplier Name",
                      "Supplier NPWP", "Accumulated WHT Based Amount" ,"WHT Based Amount", "WHT Tarif", "WHT Amount",
                      "Description","Invoice Date","Invoice Number","eBupot Reference No", "Tax Type","Map Code", "SPT Type", "WHT Date","Tax Period Month","Tax Period Year" ,"Additional Information",
                      "PV Number", "PV Created By", "SAP Document Number", "Posting Date",
                      "Download Status","Download Date","Record Status","Record Date","Receive File Date","Receive File Time","Approval Status","Approval Date",
                      "Approval Description","WHT Based GL Account","PDF WHT Status","Batch File Name", "Efaktur Status"
                    }; //for header 


                    ListArr.Add(header);


                    var list = WHTRepository.Instance.GetXlsReportById(WHTIdList);
                    foreach (WHTOriginalViewModel obj in list)
                    {
                        var efakturstatus = obj.Field40.Split('|');
                        String[] myArr = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field34,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field35,
                            obj.Field11,
                            obj.Field38,
                            obj.Field12,
                            obj.Field39,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field16,
                            obj.Field17,
                            obj.Field18,
                            obj.Field19,
                            obj.Field20,
                            obj.Field21,
                            obj.Field22,
                            obj.Field23,
                            obj.Field24,
                            obj.Field25,
                            obj.Field26,
                            obj.Field27,
                            obj.Field28,
                            obj.Field29,
                            obj.Field30,
                            obj.Field31,
                            obj.Field32,
                            obj.Field33,
                            efakturstatus[1]
                         };
                        ListArr.Add(myArr);
                    }
                    totalDataCount = ListArr.Count() - 1;
                    totalDataFailed = WHTIdList.Count() - totalDataCount;
                    result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    result.ResultDesc += totalDataFailed + " data failed downloaded";
                    fullPath = XlsHelper.PutExcel(ListArr, new WHTDashboardViewModel(), TempDownloadFolder, "WHT");
                }
                #endregion

                //button download existing, belum ditambahkan kondisi
                //add 28092020 menambahkan kondisi jika spt type dan tax article = 22, dan kondisinya sudah exist
                //balikin ke existing, 17112020

                #region pdf zip
                if (type == "pdf")
                {
                    //exist

                    //var filePaths = WHTRepository.Instance.GetPDFUrlById(WHTIdList);
                    //fullPath = GlobalFunction.GenerateZipFile(filePaths, TempZipFolder);
                    //var newPath = TempZipFolder + "\\WHT_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss") + ".zip";

                    //System.IO.File.Move(fullPath, newPath);
                    //fullPath = newPath;


                    //totalDataCount = filePaths.Count();
                    //totalDataFailed = WHTIdList.Count() - totalDataCount;

                    //result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    //result.ResultDesc += totalDataFailed + " data failed downloaded";

                    var filePaths = WHTRepository.Instance.GetPDFUrlById(WHTIdList);
                    fullPath = GlobalFunction.GenerateZipFile(filePaths, TempZipFolder);
                    var newPath = TempZipFolder + "\\WHT_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss") + ".zip";
                    //biar errornya ga lgsg ke log error 
                    if (!string.IsNullOrEmpty(fullPath))
                    {
                        System.IO.File.Move(fullPath, newPath);
                        fullPath = newPath;
                    }

                    totalDataCount = filePaths.Count();
                    totalDataFailed = WHTIdList.Count() - totalDataCount;

                    result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    result.ResultDesc += totalDataFailed + " data failed downloaded";
                }
                #endregion
                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("WHT Download " + type, MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
                throw e;
            }
        }

       // usp_GetWHTCSVValidationById
        [HttpPost]
        public ActionResult GetNoneBupotReferenceNo(FormCollection collection, string type, bool checkAll)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = ""
            };
            try
            {
                
                    var searchParam = collection["data"];
                    var whtIdList = collection["whtIdList"];
                    var searchParamModel = new WHTDashboardSearchParamViewModel();
                    List<Guid> WHTIdList = new List<Guid>();
                    if (!string.IsNullOrEmpty(searchParam) && checkAll)
                    {
                        searchParamModel = new JavaScriptSerializer().Deserialize<WHTDashboardSearchParamViewModel>(searchParam);
                        WHTIdList = WHTRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess);
                    }
                    else
                    {
                        WHTIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(whtIdList);
                    }

                var list= WHTRepository.Instance.GetCSVvalidation(WHTIdList);
                //var data = service.FindInternaMaintenancePartDeskription(partID).First();

                //string partDeskription = data.PartDeskription.ToString().TrimStart().TrimEnd();
                //string unit = data.Unit.ToString().TrimStart().TrimEnd();
                //decimal price = data.SellingPrice;
                return Json(new { ResultCode = list.FirstOrDefault().ResultCode, ResultDesc = list.FirstOrDefault().ResultDesc });
            }

            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("CSV Download " + type, MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
                throw e;

            }
        }

        [HttpPost]
        public ActionResult GetErrEbupotList(FormCollection collection, bool checkAll)
        {


            var searchParam = collection["data"];
            var whtIdList = collection["whtIdList"];
            var searchParamModel = new WHTDashboardSearchParamViewModel();
            List<Guid> WHTIdList = new List<Guid>();
            if (!string.IsNullOrEmpty(searchParam) && checkAll)
            {
                searchParamModel = new JavaScriptSerializer().Deserialize<WHTDashboardSearchParamViewModel>(searchParam);
                WHTIdList = WHTRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess);
            }
            else
            {
                WHTIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(whtIdList);
            }

            var _EbupotErrorList = WHTRepository.Instance.GetErrorDataList(WHTIdList);
            ViewData["EbupotErrorList"] = _EbupotErrorList.ToList();

            return PartialView("_ErrGrid");
        }

        [HttpPost]
        public ActionResult GetDownloadExistsEbupotList(FormCollection collection, bool checkAll)
        {


            var searchParam = collection["data"];
            var whtIdList = collection["whtIdList"];
            var searchParamModel = new WHTDashboardSearchParamViewModel();
            List<Guid> WHTIdList = new List<Guid>();
            if (!string.IsNullOrEmpty(searchParam) && checkAll)
            {
                searchParamModel = new JavaScriptSerializer().Deserialize<WHTDashboardSearchParamViewModel>(searchParam);
                WHTIdList = WHTRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess);
            }
            else
            {
                WHTIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(whtIdList);
            }

            var _DownloadExistsList = WHTRepository.Instance.GetDownloadEbupotDataList(WHTIdList);
            ViewData["DownloadExistsList"] = _DownloadExistsList.ToList();

            return PartialView("_DownloadExistsGrid");
        }


        [HttpPost]
        public ActionResult GetValidationDataebupot(FormCollection collection, string type, bool checkAll)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = ""
            };
            try
            {

                var searchParam = collection["data"];
                var whtIdList = collection["whtIdList"];
                var searchParamModel = new WHTDashboardSearchParamViewModel();
                List<Guid> WHTIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<WHTDashboardSearchParamViewModel>(searchParam);
                    WHTIdList = WHTRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess);
                }
                else
                {
                    WHTIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(whtIdList);
                }

                var _EbupotErrorList = WHTRepository.Instance.GetErrorDataList(WHTIdList);
                bool dataval =  (_EbupotErrorList.Count() > 0) ? true : false;
                return Json(new { ResultCode = dataval, ResultDesc = "Validation Ebupot"});
            }

            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("eBupot Download " + type, MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
                throw e;

            }
        }

        [HttpPost]
        public ActionResult ExistsDownloadAndApprovalStatus(FormCollection collection, string type, bool checkAll)
        {
            
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = ""
            };
            try
            {

                var searchParam = collection["data"];
                var whtIdList = collection["whtIdList"];
                var searchParamModel = new WHTDashboardSearchParamViewModel();
                List<Guid> WHTIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<WHTDashboardSearchParamViewModel>(searchParam);
                    WHTIdList = WHTRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess);
                }
                else
                {
                    WHTIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(whtIdList);
                }

                var list =  WHTRepository.Instance.GetDownloadEbupotDataList(WHTIdList);
                
                return Json(new { ResultCode = list.FirstOrDefault().ResultCode, ResultDesc = list.FirstOrDefault().ResultDesc });
            }

            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("eBupot Download " + type, MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
                throw e;

            }
        }

        [HttpPost]
        public JsonResult GenerateDownloadFileEbupot(FormCollection collection, string type, bool checkAll)
        {
            #region Cek Wht Full Access Role
            int countArr = CurrentLogin.Roles.Count();
           
            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("WHTFullAccessRole");//new by role
            string confignpwpcompany = MasterConfigRepository.Instance.GetByConfigKey("NpwpEbupotCompany").ConfigValue;

            string[] WHTFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            //if (config != null)
            //{
            //    WHTFullAccessRole = config.ConfigValue.Split(';');
            //    foreach (var item in RoleIds)
            //    {
            //        if (WHTFullAccessRole.Contains(item))
            //        {
            //            isFullAccess = true;
            //        }
            //    }
            //}
            isFullAccess = true;
            #endregion

            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };

            string fullPath23 = string.Empty;
            string fullPath26 = string.Empty;
            int totalDataCount = 0;
            int totalDataFailed = 0;
            try
            {
                var searchParam = collection["data"];
                var whtIdList = collection["whtIdList"];
                var searchParamModel = new WHTDashboardSearchParamViewModel();
                List<Guid> WHTIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<WHTDashboardSearchParamViewModel>(searchParam);
                    WHTIdList = WHTRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess);
                }
                else
                {
                    WHTIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(whtIdList);
                }
                #region xls23
               
                    var list23 = WHTRepository.Instance.GetXlsEBupot23ById(WHTIdList);
                    if (list23.Count > 0)
                    {
                        List<string[]> ListArr = new List<string[]>();
                        List<string[]> ListArrRef = new List<string[]>();




                        foreach (WHTEBupot23ViewModel obj in list23)
                        {
                            String[] myArr = new string[] {
                            obj.RowNum.ToString(),
                            obj.MasaPajak,
                            obj.TahunPajak,
                            obj.TglPemotongan.ToString("dd/MM/yyyy"),
                            obj.BerNPWP,
                            obj.NPWP,
                            obj.NIK,
                            obj.NomorTelp,
                            obj.KodeObjekPajak,
                            obj.PenandaTanganBPPengurus,
                            obj.PenghasilanBruto.ToString(),
                            obj.MendapatkanFasilitas,
                            obj.NomorSKB,
                            obj.NomorAturanDTP,
                            obj.NTPNDTP
                         };


                            String[] myArrRef = new string[] {
                            obj.RowNum.ToString(),
                            obj.JenisPajak,
                            obj.JenisDokumen,
                            obj.eBupotReferenceNo,
                            obj.InvoiceDate.ToString("dd/MM/yyyy")
                         };



                            ListArr.Add(myArr);
                            ListArrRef.Add(myArrRef);
                        }
                        totalDataCount = ListArr.Count() - 1;
                        totalDataFailed = WHTIdList.Count() - totalDataCount;
                        result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                        result.ResultDesc += totalDataFailed + " data failed downloaded";
                        fullPath23 = XlsHelper.PutExcelEbupot(ListArr, ListArrRef, new WHTEBupot23ViewModel(), TempDownloadFolder, confignpwpcompany, "23");
                    }
                    #endregion
                    #region xls26
                    var list26 = WHTRepository.Instance.GetXlsEBupot26ById(WHTIdList);
                    if (list26.Count > 0)
                    {
                        List<string[]> ListArr = new List<string[]>();
                        List<string[]> ListArrRef = new List<string[]>();



                        foreach (WHTEBupot26ViewModel obj in list26)
                        {
                            String[] myArr = new string[] {
                            obj.RowNum.ToString(),
                            obj.MasaPajak,
                            obj.TahunPajak,
                            obj.TglPemotongan.ToString("dd/MM/yyyy"),
                            obj.TIN,
                            obj.NamaWPTerpotong,
                            obj.TglLahirWPTerpotong.ToString("dd/MM/yyyy"),
                            obj.AlamatWPTerpotong,
                            obj.NoPasporWPTerpotong,
                            obj.NoKitasWPTerpotong,
                            obj.KodeNegara,
                            obj.KodeObjekPajak,
                             obj.PenandatanganBPPengurus,
                            obj.PenghasilanBruto.ToString(),
                            obj.PerkiraanPenghasilan,
                            obj.MendapatkanFasilitas,
                             obj.NomorTandaTerimaSKD,
                            obj.TarifSKD,
                            obj.NomorAturanDTP,
                            obj.NTPNDTP
                         };


                            String[] myArrRef = new string[] {
                            obj.RowNum.ToString(),
                            obj.JenisPajak,
                            obj.JenisDokumen,
                            obj.eBupotReferenceNo,
                            obj.InvoiceDate.ToString("dd/MM/yyyy")
                         };



                            ListArr.Add(myArr);
                            ListArrRef.Add(myArrRef);
                        }
                        totalDataCount = ListArr.Count() - 1;
                        totalDataFailed = WHTIdList.Count() - totalDataCount;
                        result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                        result.ResultDesc += totalDataFailed + " data failed downloaded";
                        fullPath26 = XlsHelper.PutExcelEbupot(ListArr, ListArrRef, new WHTEBupot26ViewModel(), TempDownloadFolder, confignpwpcompany, "26");
                    }
                    #endregion

                    return Json(new { ResultCode = true, ResultDesc = result.ResultDesc, fullPath23 = fullPath23, fullPath26 = fullPath26 }, JsonRequestBehavior.AllowGet);
                
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("eBupot Download " + type, MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
                throw e;
            }

        }

        //[HttpPost]
        //public JsonResult NextGenerateDownloadFileEbupot(FormCollection collection, string type, bool checkAll)
        //{
        //    #region Cek Wht Full Access Role
        //    int countArr = CurrentLogin.Roles.Count();

        //    List<string> RoleId = new List<string>();

        //    for (int i = 0; i < countArr; i++)
        //    {
        //        RoleId.Add(CurrentLogin.Roles[i].Id);
        //    }

        //    Config config = MasterConfigRepository.Instance.GetByConfigKey("WHTFullAccessRole");//new by role
        //    string confignpwpcompany = MasterConfigRepository.Instance.GetByConfigKey("NpwpEbupotCompany").ConfigValue;

        //    string[] WHTFullAccessRole = { "" };
        //    string[] RoleIds = RoleId.ToArray();

        //    if (config != null)
        //    {
        //        WHTFullAccessRole = config.ConfigValue.Split(';');
        //        foreach (var item in RoleIds)
        //        {
        //            if (WHTFullAccessRole.Contains(item))
        //            {
        //                isFullAccess = true;
        //            }
        //        }
        //    }
        //    #endregion

        //    Result result = new Result
        //    {
        //        ResultCode = true,
        //        ResultDesc = "File for download generated successfully"
        //    };

        //    string fullPath23 = string.Empty;
        //    string fullPath26 = string.Empty;
        //    int totalDataCount = 0;
        //    int totalDataFailed = 0;
        //    try
        //    {
        //        var searchParam = collection["data"];
        //        var whtIdList = collection["whtIdList"];
        //        var searchParamModel = new WHTDashboardSearchParamViewModel();
        //        List<Guid> WHTIdList = new List<Guid>();
        //        if (!string.IsNullOrEmpty(searchParam) && checkAll)
        //        {
        //            searchParamModel = new JavaScriptSerializer().Deserialize<WHTDashboardSearchParamViewModel>(searchParam);
        //            WHTIdList = WHTRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess);
        //        }
        //        else
        //        {
        //            WHTIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(whtIdList);
        //        }
        //        #region xls23
              
        //            var list23 = WHTRepository.Instance.GetXlsEBupot23ById(WHTIdList);
        //            if (list23.Count > 0)
        //            {
        //                List<string[]> ListArr = new List<string[]>();
        //                List<string[]> ListArrRef = new List<string[]>();




        //                foreach (WHTEBupot23ViewModel obj in list23)
        //                {
        //                    String[] myArr = new string[] {
        //                    obj.RowNum.ToString(),
        //                    obj.MasaPajak,
        //                    obj.TahunPajak,
        //                    obj.TglPemotongan.ToString("dd/MM/yyyy"),
        //                    obj.BerNPWP,
        //                    obj.NPWP,
        //                    obj.NIK,
        //                    obj.NomorTelp,
        //                    obj.KodeObjekPajak,
        //                    obj.PenandaTanganBPPengurus,
        //                    obj.PenghasilanBruto.ToString(),
        //                    obj.MendapatkanFasilitas,
        //                    obj.NomorSKB,
        //                    obj.NomorAturanDTP,
        //                    obj.NTPNDTP
        //                 };


        //                    String[] myArrRef = new string[] {
        //                    obj.RowNum.ToString(),
        //                    obj.JenisPajak,
        //                    obj.JenisDokumen,
        //                    obj.eBupotReferenceNo,
        //                    obj.InvoiceDate.ToString("dd/MM/yyyy")
        //                 };



        //                    ListArr.Add(myArr);
        //                    ListArrRef.Add(myArrRef);
        //                }
        //                totalDataCount = ListArr.Count() - 1;
        //                totalDataFailed = WHTIdList.Count() - totalDataCount;
        //                result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
        //                result.ResultDesc += totalDataFailed + " data failed downloaded";
        //                fullPath23 = XlsHelper.PutExcelEbupot(ListArr, ListArrRef, new WHTEBupot23ViewModel(), TempDownloadFolder, confignpwpcompany, "23");
        //            }
        //            #endregion
        //            #region xls26
        //            var list26 = WHTRepository.Instance.GetXlsEBupot26ById(WHTIdList);
        //            if (list26.Count > 0)
        //            {
        //                List<string[]> ListArr = new List<string[]>();
        //                List<string[]> ListArrRef = new List<string[]>();



        //                foreach (WHTEBupot26ViewModel obj in list26)
        //                {
        //                    String[] myArr = new string[] {
        //                    obj.RowNum.ToString(),
        //                    obj.MasaPajak,
        //                    obj.TahunPajak,
        //                    obj.TglPemotongan.ToString("dd/MM/yyyy"),
        //                    obj.TIN,
        //                    obj.NamaWPTerpotong,
        //                    obj.TglLahirWPTerpotong.ToString("dd/MM/yyyy"),
        //                    obj.AlamatWPTerpotong,
        //                    obj.NoPasporWPTerpotong,
        //                    obj.NoKitasWPTerpotong,
        //                    obj.KodeNegara,
        //                    obj.KodeObjekPajak,
        //                     obj.PenandatanganBPPengurus,
        //                    obj.PenghasilanBruto.ToString(),
        //                    obj.PerkiraanPenghasilan,
        //                    obj.MendapatkanFasilitas,
        //                     obj.NomorTandaTerimaSKD,
        //                    obj.TarifSKD,
        //                    obj.NomorAturanDTP,
        //                    obj.NTPNDTP
        //                 };


        //                    String[] myArrRef = new string[] {
        //                    obj.RowNum.ToString(),
        //                    obj.JenisPajak,
        //                    obj.JenisDokumen,
        //                    obj.eBupotReferenceNo,
        //                    obj.InvoiceDate.ToString("dd/MM/yyyy")
        //                 };



        //                    ListArr.Add(myArr);
        //                    ListArrRef.Add(myArrRef);
        //                }
        //                totalDataCount = ListArr.Count() - 1;
        //                totalDataFailed = WHTIdList.Count() - totalDataCount;
        //                result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
        //                result.ResultDesc += totalDataFailed + " data failed downloaded";
        //                fullPath26 = XlsHelper.PutExcelEbupot(ListArr, ListArrRef, new WHTEBupot26ViewModel(), TempDownloadFolder, confignpwpcompany, "26");
        //            }
        //            #endregion

        //            return Json(new { ResultCode = true, ResultDesc = result.ResultDesc, fullPath23 = fullPath23, fullPath26 = fullPath26, errorlog = 0, isDownload = 0 }, JsonRequestBehavior.AllowGet);
                
        //    }
        //    catch (Exception e)
        //    {
        //        result.ResultCode = false;
        //        result.ResultDesc = e.LogToApp("eBupot Download " + type, MessageType.ERR, CurrentLogin.Username).Message;
        //        return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
        //        throw e;
        //    }
        //}

        public FileResult DownloadTemp23(string filePath)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            string fileName = Path.GetFileName(filePath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public FileResult DownloadTemp26(string filePath)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            string fileName = Path.GetFileName(filePath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpPost]
        public JsonResult GenerateDownloadFileEbupot26(FormCollection collection, string type, bool checkAll)
        {
            #region Cek Wht Full Access Role
            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("WHTFullAccessRole");//new by role


            string[] WHTFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            if (config != null)
            {
                WHTFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (WHTFullAccessRole.Contains(item))
                    {
                        isFullAccess = true;
                    }
                }
            }
            #endregion

            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };

            string fullPath = string.Empty;
            
            int totalDataCount = 0;
            int totalDataFailed = 0;
            try
            {
                var searchParam = collection["data"];
                var whtIdList = collection["whtIdList"];
                var searchParamModel = new WHTDashboardSearchParamViewModel();
                List<Guid> WHTIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<WHTDashboardSearchParamViewModel>(searchParam);
                    WHTIdList = WHTRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess);
                }
                else
                {
                    WHTIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(whtIdList);
                }

                List<string[]> ListArr = new List<string[]>();
                List<string[]> ListArrRef = new List<string[]>();


                    var list = WHTRepository.Instance.GetXlsEBupot26ById(WHTIdList);
                    foreach (WHTEBupot26ViewModel obj in list)
                    {
                        String[] myArr = new string[] {
                            obj.RowNum.ToString(),
                            obj.MasaPajak,
                            obj.TahunPajak,
                            obj.TglPemotongan.ToString("dd/MM/yyyy"),
                            obj.TIN,
                            obj.NamaWPTerpotong,
                            obj.TglLahirWPTerpotong.ToString("dd/MM/yyyy"),
                            obj.AlamatWPTerpotong,
                            obj.NoPasporWPTerpotong,
                            obj.NoKitasWPTerpotong,
                            obj.KodeObjekPajak,
                            obj.PenghasilanBruto.ToString(),
                            obj.PerkiraanPenghasilan,
                            obj.MendapatkanFasilitas,
                            obj.TarifSKD,
                            obj.NomorAturanDTP,
                            obj.NTPNDTP
                         };


                        String[] myArrRef = new string[] {
                            obj.RowNum.ToString(),
                            obj.JenisPajak,
                            obj.JenisDokumen,
                            obj.eBupotReferenceNo,
                            obj.InvoiceDate.ToString("dd/MM/yyyy")
                         };



                        ListArr.Add(myArr);
                        ListArrRef.Add(myArrRef);
                    }
                    totalDataCount = ListArr.Count() - 1;
                    totalDataFailed = WHTIdList.Count() - totalDataCount;
                    result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    result.ResultDesc += totalDataFailed + " data failed downloaded";
                    fullPath = XlsHelper.PutExcelEbupot(ListArr, ListArrRef, new WHTEBupot26ViewModel(), TempDownloadFolder, "021161153092000", "26");
               


                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("eBupot 26 Download " + type, MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
                throw e;
            }
        }






        [HttpPost]
        public JsonResult GenerateDownloadFileESPT(FormCollection collection, string type, bool checkAll)
        {
            #region Cek Wht Full Access Role
            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("WHTFullAccessRole");//new by role


            string[] WHTFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            if (config != null)
            {
                WHTFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (WHTFullAccessRole.Contains(item))
                    {
                        isFullAccess = true;
                    }
                }
            }
            #endregion

            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };
            string fullPath22 = string.Empty;
            string fullPath23 = string.Empty;
            string fullPath15 = string.Empty;
            string fullPath2126 = string.Empty;
            string fullPath2326 = string.Empty;
            string fullPath42 = string.Empty;
            string fullPathBadan = string.Empty;
            string fullPathZIP = string.Empty;

            int totalDataCount = 0;
            int totalDataFailed = 0;
            try
            {
                var searchParam = collection["data"];
                var whtIdList = collection["whtIdList"];
                var searchParamModel = new WHTDashboardSearchParamViewModel();
                List<Guid> WHTIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<WHTDashboardSearchParamViewModel>(searchParam);
                    WHTIdList = WHTRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess);
                }
                else
                {
                    WHTIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(whtIdList);
                }

                List<string[]> ListArr22 = new List<string[]>();
                List<string[]> ListArr23 = new List<string[]>();
                List<string[]> ListArr15 = new List<string[]>();
                List<string[]> ListArr2126 = new List<string[]>();
                List<string[]> ListArr2326 = new List<string[]>();
                List<string[]> ListArr42 = new List<string[]>();
                List<string[]> ListArrBadan = new List<string[]>();

                #region csv type
                if (type == "csv")
                {
                    #region WHT 22
                    var list22 = WHTRepository.Instance.GetCSVById_WHT22(WHTIdList);
                    foreach (WHTCSVViewModel obj in list22)
                    {
                        String[] myArr = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field16,
                            obj.Field17,
                            obj.Field18,
                            obj.Field19,
                            obj.Field20,
                            obj.Field21,
                            obj.Field22,
                            obj.Field23,
                            obj.Field24,
                            obj.Field25,
                            obj.Field26,
                            obj.Field27,
                            obj.Field28,
                            obj.Field29,
                            obj.Field30,
                            obj.Field31,
                            obj.Field32,
                            obj.Field33,
                            obj.Field34,
                            obj.Field35,
                            obj.Field36,
                            obj.Field37,
                            obj.Field38,
                            obj.Field39,
                            obj.Field40,
                            obj.Field41,
                            obj.Field42,
                            obj.Field43,
                            obj.Field44,
                            obj.Field45,
                            obj.Field46,
                            obj.Field47,
                            obj.Field48,
                            obj.Field49,
                            obj.Field50,
                            obj.Field51,
                            obj.Field52,
                            obj.Field53,
                            obj.Field54,
                            obj.Field55,
                            obj.Field56
                        };
                        ListArr22.Add(myArr);
                    }
                    //totalDataCount = ListArr.Count() - 1;
                    //totalDataFailed = WHTIdList.Count() - totalDataCount;
                    //result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    //result.ResultDesc += totalDataFailed + " data failed downloaded";
                    if (list22.Count > 0)
                    {
                        List<Guid> id = new List<Guid>();
                        foreach (WHTCSVViewModel obj in list22)
                        {
                            id.Add(obj.WHTId);
                        }

                        fullPath22 = CSVHelper.PutCSV(ListArr22, new WHTDashboardViewModel(), TempDownloadFolder, "WHT22_22");
                        WHTRepository.Instance.UpdateFlagCSV(id, fullPath22, CurrentLogin.Username);
                    }
                    #endregion WHT22

                    #region WHT23
                    String[] header23 = new string[]
                   {
                        "Kode Form Bukti Potong","Masa Pajak","Tahun Pajak","Pembetulan","NPWP WP yang Dipotong","Nama WP yang Dipotong","Alamat WP yang Dipotong","Nomor Bukti Potong",
                        "Tanggal Bukti Potong","Nilai Bruto 1","Tarif 1","PPh Yang Dipotong  1","Nilai Bruto 2","Tarif 2","PPh Yang Dipotong  2","Nilai Bruto 3","Tarif 3","PPh Yang Dipotong  3",
                        "Nilai Bruto 4","Tarif 4","PPh Yang Dipotong  4","Nilai Bruto 5","Tarif 5","PPh Yang Dipotong  5","Nilai Bruto 6a/Nilai Bruto 6","Tarif 6a/Tarif 6","PPh Yang Dipotong  6a/PPh Yang Dipotong  6",
                        "Nilai Bruto 6b/Nilai Bruto 7","Tarif 6b/Tarif 7","PPh Yang Dipotong  6b/PPh Yang Dipotong  7","Nilai Bruto 6c/Nilai Bruto 8","Tarif 6c/Tarif 8","PPh Yang Dipotong  6c/PPh Yang Dipotong  8",
                        "Nilai Bruto 9","Tarif 9","PPh Yang Dipotong  9","Nilai Bruto 10","Perkiraan Penghasilan Netto10","Tarif 10","PPh Yang Dipotong  10","Nilai Bruto 11","Perkiraan Penghasilan Netto11",
                        "Tarif 11","PPh Yang Dipotong  11","Nilai Bruto 12","Perkiraan Penghasilan Netto12","Tarif 12","PPh Yang Dipotong  12","Nilai Bruto 13","Tarif 13","PPh Yang Dipotong  13","Kode Jasa 6d1 PMK-244/PMK.03/2008",
                        "Nilai Bruto 6d1","Tarif 6d1","PPh Yang Dipotong  6d1","Kode Jasa 6d2 PMK-244/PMK.03/2008","Nilai Bruto 6d2","Tarif 6d2","PPh Yang Dipotong  6d2","Kode Jasa 6d3 PMK-244/PMK.03/2008","Nilai Bruto 6d3","Tarif 6d3",
                        "PPh Yang Dipotong  6d3","Kode Jasa 6d4 PMK-244/PMK.03/2008","Nilai Bruto 6d4","Tarif 6d4","PPh Yang Dipotong  6d4","Kode Jasa 6d5 PMK-244/PMK.03/2008","Nilai Bruto 6d5","Tarif 6d5","PPh Yang Dipotong  6d5",
                        "Kode Jasa 6d6 PMK-244/PMK.03/2008","Nilai Bruto 6d6","Tarif 6d6","PPh Yang Dipotong  6d6","Jumlah Nilai Bruto ","Jumlah PPh Yang Dipotong"
                   };

                    ListArr23.Add(header23);

                    var list23 = WHTRepository.Instance.GetCSVById_WHT23(WHTIdList);
                    foreach (WHTCSVViewModel obj in list23)
                    {
                        String[] myArr = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field16,
                            obj.Field17,
                            obj.Field18,
                            obj.Field19,
                            obj.Field20,
                            obj.Field21,
                            obj.Field22,
                            obj.Field23,
                            obj.Field24,
                            obj.Field25,
                            obj.Field26,
                            obj.Field27,
                            obj.Field28,
                            obj.Field29,
                            obj.Field30,
                            obj.Field31,
                            obj.Field32,
                            obj.Field33,
                            obj.Field34,
                            obj.Field35,
                            obj.Field36,
                            obj.Field37,
                            obj.Field38,
                            obj.Field39,
                            obj.Field40,
                            obj.Field41,
                            obj.Field42,
                            obj.Field43,
                            obj.Field44,
                            obj.Field45,
                            obj.Field46,
                            obj.Field47,
                            obj.Field48,
                            obj.Field49,
                            obj.Field50,
                            obj.Field51,
                            obj.Field52,
                            obj.Field53,
                            obj.Field54,
                            obj.Field55,
                            obj.Field56,
                            obj.Field57,
                            obj.Field58,
                            obj.Field59,
                            obj.Field60,
                            obj.Field61,
                            obj.Field62,
                            obj.Field63,
                            obj.Field64,
                            obj.Field65,
                            obj.Field66,
                            obj.Field67,
                            obj.Field68,
                            obj.Field69,
                            obj.Field70,
                            obj.Field71,
                            obj.Field72,
                            obj.Field73,
                            obj.Field74,
                            obj.Field75,
                            obj.Field76,
                            obj.Field77

                        };
                        ListArr23.Add(myArr);
                    }
                    //totalDataCount = ListArr.Count() - 1;
                    //totalDataFailed = WHTIdList.Count() - totalDataCount;
                    //result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    //result.ResultDesc += totalDataFailed + " data failed downloaded";
                    if (list23.Count > 0)
                    {
                        List<Guid> id = new List<Guid>();
                        foreach (WHTCSVViewModel obj in list23)
                        {
                            id.Add(obj.WHTId);
                        }

                        fullPath23 = CSVHelper.PutCSV(ListArr23, new WHTDashboardViewModel(), TempDownloadFolder, "WHT23_2326");
                        WHTRepository.Instance.UpdateFlagCSV(id, fullPath23, CurrentLogin.Username);
                    }
                    #endregion WHT23

                    #region WHT 15
                    String[] header15 = new string[]
                  {
                        "Kode Form Bukti Potong / Kode Form Input PPh Yang Dibayar Sendiri","Masa Pajak","Tahun Pajak","Pembetulan","NPWP WP yang Dipotong"," Nama WP yang Dipotong","Alamat WP yang Dipotong","Nomor Bukti Potong / Nomor Urut Pada PPh Pasal 24 Yang Dapat Diperhitungkan / NTPP","Tanggal Bukti Potong / Tanggal SSP",
                        "Negara Sumber Penghasilan","Kode Option Penghasilan","Jumlah Bruto / Jumlah Penghasilan Pada Form Input Yang Dibayar Sendiri","Tarif  /  Jumlah Pajak Terutang yang dibayar di luar negeri","PPh Yang Dipotong  /  PPh Pasal 24 Yang Dapat Diperhitungkan / Jumlah PPh Pada Form Input Yang Dibayar Sendiri","Invoice / Keterangan"
                  };

                    ListArr15.Add(header15);
                    var list15 = WHTRepository.Instance.GetCSVById_WHT15(WHTIdList);
                    foreach (WHTCSVViewModel obj in list15)
                    {
                        String[] myArr = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15
                        };
                        ListArr15.Add(myArr);
                    }
                    if (list15.Count > 0)
                    {
                        List<Guid> id = new List<Guid>();
                        foreach (WHTCSVViewModel obj in list15)
                        {
                            id.Add(obj.WHTId);
                        }

                        fullPath15 = CSVHelper.PutCSV(ListArr15, new WHTDashboardViewModel(), TempDownloadFolder, "WHT15_15");
                        WHTRepository.Instance.UpdateFlagCSV(id, fullPath15, CurrentLogin.Username);
                    }
                    #endregion WHT 15

                    #region WHT 2126
                    String[] header2126 = new string[]
                    {
                       "Masa Pajak","Tahun Pajak","Pembetulan","Nomor Bukti Potong","NPWP","NIK","Nama","Alamat","WP Luar Negeri","Kode Negara",
                       "Kode Pajak","Jumlah Bruto","Jumlah DPP","Tanpa NPWP","Tarif","Jumlah PPh","NPWP Pemotong","Nama Pemotong","Tanggal Bukti Potong"

                    };

                    ListArr2126.Add(header2126);
                    var list2126 = WHTRepository.Instance.GetCSVById_WHT2126(WHTIdList);
                    foreach (WHTCSVViewModel obj in list2126)
                    {
                        String[] myArr = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field16,
                            obj.Field17,
                            obj.Field18,
                            obj.Field19
                        };
                        ListArr2126.Add(myArr);
                    }
                    if (list2126.Count > 0)
                    {
                        List<Guid> id = new List<Guid>();
                        foreach (WHTCSVViewModel obj in list2126)
                        {
                            id.Add(obj.WHTId);
                        }

                        fullPath2126 = CSVHelper.PutCSV(ListArr2126, new WHTDashboardViewModel(), TempDownloadFolder, "WHT21-26_2126");
                        WHTRepository.Instance.UpdateFlagCSV(id, fullPath2126, CurrentLogin.Username);
                    }
                    #endregion WHT 2126

                    #region WHT2326
                    String[] header2326 = new string[]
                   {
                        "Kode Form Bukti Potong","Masa Pajak","Tahun Pajak","Pembetulan","NPWP WP yang Dipotong","Nama WP yang Dipotong","Alamat WP yang Dipotong",
                        "Nomor Bukti Potong","Tanggal Bukti Potong","Nilai Bruto 1","Tarif 1","PPh Yang Dipotong  1","Nilai Bruto 2","Tarif 2","PPh Yang Dipotong  2",
                        "Nilai Bruto 3","Tarif 3","PPh Yang Dipotong  3","Nilai Bruto 4","Tarif 4","PPh Yang Dipotong  4","Nilai Bruto 5","Tarif 5","PPh Yang Dipotong  5",
                        "Nilai Bruto 6a/Nilai Bruto 6","Tarif 6a/Tarif 6","PPh Yang Dipotong  6a/PPh Yang Dipotong  6","Nilai Bruto 6b/Nilai Bruto 7","Tarif 6b/Tarif 7",
                        "PPh Yang Dipotong  6b/PPh Yang Dipotong  7","Nilai Bruto 6c/Nilai Bruto 8","Tarif 6c/Tarif 8","PPh Yang Dipotong  6c/PPh Yang Dipotong  8","Nilai Bruto 9",
                        "Tarif 9","PPh Yang Dipotong  9","Nilai Bruto 10","Perkiraan Penghasilan Netto10","Tarif 10","PPh Yang Dipotong  10","Nilai Bruto 11",
                        "Perkiraan Penghasilan Netto11","Tarif 11","PPh Yang Dipotong  11","Nilai Bruto 12",
                        "Perkiraan Penghasilan Netto12","Tarif 12","PPh Yang Dipotong  12","Nilai Bruto 13","Tarif 13","PPh Yang Dipotong  13","Kode Jasa 6d1 PMK-244/PMK.03/2008",
                        "Nilai Bruto 6d1","Tarif 6d1","PPh Yang Dipotong  6d1","Kode Jasa 6d2 PMK-244/PMK.03/2008","Nilai Bruto 6d2","Tarif 6d2","PPh Yang Dipotong  6d2",
                        "Kode Jasa 6d3 PMK-244/PMK.03/2008","Nilai Bruto 6d3","Tarif 6d3","PPh Yang Dipotong  6d3","Kode Jasa 6d4 PMK-244/PMK.03/2008","Nilai Bruto 6d4",
                        "Tarif 6d4","PPh Yang Dipotong  6d4","Kode Jasa 6d5 PMK-244/PMK.03/2008","Nilai Bruto 6d5","Tarif 6d5","PPh Yang Dipotong  6d5","Kode Jasa 6d6 PMK-244/PMK.03/2008",
                        "Nilai Bruto 6d6","Tarif 6d6","PPh Yang Dipotong  6d6","Jumlah Nilai Bruto ","Jumlah PPh Yang Dipotong"
                   };

                    ListArr2326.Add(header2326);
                    var list2326 = WHTRepository.Instance.GetCSVById_WHT2326(WHTIdList);
                    foreach (WHTCSVViewModel obj in list2326)
                    {
                        String[] myArr = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field16,
                            obj.Field17,
                            obj.Field18,
                            obj.Field19,
                            obj.Field20,
                            obj.Field21,
                            obj.Field22,
                            obj.Field23,
                            obj.Field24,
                            obj.Field25,
                            obj.Field26,
                            obj.Field27,
                            obj.Field28,
                            obj.Field29,
                            obj.Field30,
                            obj.Field31,
                            obj.Field32,
                            obj.Field33,
                            obj.Field34,
                            obj.Field35,
                            obj.Field36,
                            obj.Field37,
                            obj.Field38,
                            obj.Field39,
                            obj.Field40,
                            obj.Field41,
                            obj.Field42,
                            obj.Field43,
                            obj.Field44,
                            obj.Field45,
                            obj.Field46,
                            obj.Field47,
                            obj.Field48,
                            obj.Field49,
                            obj.Field50,
                            obj.Field51,
                            obj.Field52,
                            obj.Field53,
                            obj.Field54,
                            obj.Field55,
                            obj.Field56,
                            obj.Field57,
                            obj.Field58,
                            obj.Field59,
                            obj.Field60,
                            obj.Field61,
                            obj.Field62,
                            obj.Field63,
                            obj.Field64,
                            obj.Field65,
                            obj.Field66,
                            obj.Field67,
                            obj.Field68,
                            obj.Field69,
                            obj.Field70,
                            obj.Field71,
                            obj.Field72,
                            obj.Field73,
                            obj.Field74,
                            obj.Field75,
                            obj.Field76,
                            obj.Field77

                        };
                        ListArr2326.Add(myArr);
                    }
                    //totalDataCount = ListArr.Count() - 1;
                    //totalDataFailed = WHTIdList.Count() - totalDataCount;
                    //result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    //result.ResultDesc += totalDataFailed + " data failed downloaded";
                    if (list2326.Count > 0)
                    {
                        List<Guid> id = new List<Guid>();
                        foreach (WHTCSVViewModel obj in list2326)
                        {
                            id.Add(obj.WHTId);
                        }

                        fullPath2326 = CSVHelper.PutCSV(ListArr2326, new WHTDashboardViewModel(), TempDownloadFolder, "WHT26_2326");
                        WHTRepository.Instance.UpdateFlagCSV(id, fullPath2326, CurrentLogin.Username);
                    }
                    #endregion WHT2326

                    #region WHT42
                    String[] header42 = new string[]
                   {
                       "Kode Form Bukti Potong / Kode Form Input PPh Yang Dibayar Sendiri", "Masa Pajak", "Tahun Pajak", "Pembetulan",
                       "NPWP WP yang Dipotong", "Nama WP yang Dipotong", "Alamat WP yang Dipotong", "Nomor Bukti Potong / NTPN",
                       "Tanggal Bukti Potong / Tanggal SSP", "Jenis Hadiah Undian 1 / Lokasi Tanah dan atau Bangunan / Nama Obligasi",
                       "Kode Option Tempat Penyimpanan 1(Khusus F113310)",
                       "Jumlah Nilai Bruto 1 / Jumlah Nilai Nominal Obligasi Yg Diperdagangkan Di Bursa Efek / Jumlah Penghasilan Pada Form Input Yang Dibayar Sendiri",
                       "Tarif 1 / Tingkat Bunga per Tahun", "PPh Yang Dipotong  1 / Jumlah PPh Pada Form Input Yang Dibayar Sendiri",
                       "Jenis Hadiah Undian 2 / Nomor Seri Obligasi", "Kode Option Tempat Penyimpanan 2",
                       "Jumlah Nilai Bruto 2 / Jumlah Harga Perolehan Bersih(tanpa Bunga) Pada Obligasi Yg Diperdagangkan Di Bursa Efek",
                       "Tarif 2", "PPh Yang Dipotong  2", "Jenis Hadiah Undian 3", "Kode Option Tempat Penyimpanan 3",
                       "Jumlah Nilai Bruto 3 / Jumlah Harga Penjualan Bersih(tanpa Bunga) Pada Obligasi Yg Diperdagangkan Di Bursa Efek",
                       "Tarif 3", "PPh Yang Dipotong  3", "Jenis Hadiah Undian 4",
                       "Kode Option Tempat Penyimpanan 4 / Kode Option Perencanaan (1) atau Pengawasan(2) atau selainnya(0) untuk BP Jasa Konstruksi poin 4",
                       "Jumlah Nilai Bruto 4 / Jumlah Diskonto Pada Obligasi Yg Diperdagangkan Di Bursa Efek", "Tarif 4", "PPh Yang Dipotong  4",
                       "Jenis Hadiah Undian 5", "Kode Option Tempat Penyimpanan 5 / Kode Option Perencanaan (1) atau Pengawasan(2) atau selainnya(0) untuk BP Jasa Konstruksi poin 5",
                       "Jumlah Nilai Bruto 5 / Jumlah Bunga Pada Obligasi Yg Diperdagangkan Di Bursa Efek", "Tarif 5", "PPh Yang Dipotong  5",
                       "Jenis Hadiah Undian 6", "Jumlah Nilai Bruto 6 / Jumlah Total Bunga atau Diskonto Obligasi Yang Diperdagangkan",
                       "Tarif 6 / Tarif PPh Final Pada Obligasi Yang Diperdagangkan Di Bursa Efek", "PPh Yang Dipotong  6", "Jumlah Nilai Bruto 7",
                       "Tarif 7", "PPh Yang Dipotong 7", "Jenis Penghasilan 8", "Jumlah Nilai Bruto 8", "Tarif 8", "PPh Yang Dipotong 8",
                       "Jumlah PPh Yang Dipotong", "Tanggal Jatuh Tempo Obligasi", "Tanggal Perolehan Obligasi", "Tanggal Penjualan Obligasi",
                       "Holding Periode Obligasi(Hari)", "Time Periode Obligasi(Hari)"
                   };

                    ListArr42.Add(header42);
                    var list42 = WHTRepository.Instance.GetCSVById_WHT42(WHTIdList);
                    foreach (WHTCSVViewModel obj in list42)
                    {
                        String[] myArr = new string[] {
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field16,
                            obj.Field17,
                            obj.Field18,
                            obj.Field19,
                            obj.Field20,
                            obj.Field21,
                            obj.Field22,
                            obj.Field23,
                            obj.Field24,
                            obj.Field25,
                            obj.Field26,
                            obj.Field27,
                            obj.Field28,
                            obj.Field29,
                            obj.Field30,
                            obj.Field31,
                            obj.Field32,
                            obj.Field33,
                            obj.Field34,
                            obj.Field35,
                            obj.Field36,
                            obj.Field37,
                            obj.Field38,
                            obj.Field39,
                            obj.Field40,
                            obj.Field41,
                            obj.Field42,
                            obj.Field43,
                            obj.Field44,
                            obj.Field45,
                            obj.Field46,
                            obj.Field47,
                            obj.Field48,
                            obj.Field49,
                            obj.Field50,
                            obj.Field51,
                            obj.Field52
                        };
                        ListArr42.Add(myArr);
                    }
                    //totalDataCount = ListArr.Count() - 1;
                    //totalDataFailed = WHTIdList.Count() - totalDataCount;
                    //result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    //result.ResultDesc += totalDataFailed + " data failed downloaded";
                    if (list42.Count > 0)
                    {
                        List<Guid> id = new List<Guid>();
                        foreach (WHTCSVViewModel obj in list42)
                        {
                            id.Add(obj.WHTId);
                        }

                        fullPath42 = CSVHelper.PutCSV(ListArr42, new WHTDashboardViewModel(), TempDownloadFolder, "WHT4ayat2_4(2)");
                        WHTRepository.Instance.UpdateFlagCSV(id, fullPath42, CurrentLogin.Username);
                    }
                    #endregion WHT42

                    #region WHTBadan
                    String[] headerBadan = new string[]
                   {
                       "Jenis PPh yang dipotong", "Cara pembayaran", "Nomor bukti potong/pungut", "Jenis penghasilan",
                       "Objek pemotongan/ pemungutan", "PPh yang dipotong/ dipungut", "Tgl bukti potong/pungut", "NPWP pemotong/ pemungut",
                       "Nama pemotong/ pemungut", "Alamat pemotong/ pemungut", "Kode MAP/ iuran pembayaran", "NTPP", "Jumlah pembayaran",
                       "Tanggal setor"
                   };

                    ListArrBadan.Add(headerBadan);
                    var listBadan = WHTRepository.Instance.GetCSVById_WHTBadan(WHTIdList);
                    foreach (WHTCSVViewModel obj in listBadan)
                    {
                        String[] myArr = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14
                        };
                        ListArrBadan.Add(myArr);
                    }

                    if (listBadan.Count > 0)
                    {
                        List<Guid> id = new List<Guid>();
                        foreach (WHTCSVViewModel obj in listBadan)
                        {
                            id.Add(obj.WHTId);
                        }

                        fullPathBadan = CSVHelper.PutCSV(ListArrBadan, new WHTDashboardViewModel(), TempDownloadFolder, "WHT22-23_PPhBadan");
                        WHTRepository.Instance.UpdateFlagCSV(id, fullPathBadan, CurrentLogin.Username);
                    }
                    #endregion WHTBadan

                    totalDataCount = ListArr22.Count() + (ListArr23.Count() - 1) + (ListArr15.Count() - 1) + (ListArr2126.Count() - 1) + (ListArr2326.Count() - 1) + (ListArr42.Count() - 1) + (ListArrBadan.Count() - 1);
                    totalDataFailed = WHTIdList.Count() - totalDataCount;
                    result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    result.ResultDesc += totalDataFailed + " data failed downloaded";
                    //zip multiple file csv
                    List<string> filePaths = new List<string>();
                    filePaths.Add(fullPath22);
                    filePaths.Add(fullPath23);
                    filePaths.Add(fullPath15);
                    filePaths.Add(fullPath2126);
                    filePaths.Add(fullPath2326);
                    filePaths.Add(fullPath42);
                    filePaths.Add(fullPathBadan);

                    fullPathZIP = GlobalFunction.GenerateZipFileWHT(filePaths, TempZipFolder);

                    var countFilePaths = filePaths.Where(s => s != "").Count();
                    if (countFilePaths > 1)
                    {
                        fullPathZIP = GlobalFunction.GenerateZipFileWHT(filePaths, TempZipFolder);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(fullPath22)) fullPathZIP = fullPath22;
                        else if (!string.IsNullOrEmpty(fullPath23)) fullPathZIP = fullPath23;
                        else if (!string.IsNullOrEmpty(fullPath15)) fullPathZIP = fullPath15;
                        else if (!string.IsNullOrEmpty(fullPath2126)) fullPathZIP = fullPath2126;
                        else if (!string.IsNullOrEmpty(fullPath2326)) fullPathZIP = fullPath2326;
                        else if (!string.IsNullOrEmpty(fullPath42)) fullPathZIP = fullPath42;
                        else fullPathZIP = fullPathBadan;
                    }
                }
                #endregion

                return Json(result.AsDynamic(fullPathZIP), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("WHT Download ESPT " + type, MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UploadWHTFile()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);

                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT Out

                    result = WHTRepository.Instance.WHTFileInsert(fullPath, CurrentLogin.Username);
                    #endregion
                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        [HttpPost]
        public ActionResult UploadWHTEBupotFile()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);

                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT Out

                    result = WHTRepository.Instance.EBupotFileInsert(fullPath, CurrentLogin.Username);
                    #endregion
                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }


        [HttpPost]
        public ActionResult UploadApprovalStatus()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);
                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT In

                    result = WHTRepository.Instance.ExcelApprovalStatusUpdate(fullPath, CurrentLogin.Username);
                    #endregion

                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        [HttpPost]
        public ActionResult UploadEBupotApproval()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                //string TempUploadFolder = "\\" + "\\10.85.40.199\\Interface\\Temp\\Upload\\";
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);
                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT In 

                    result = WHTRepository.Instance.ExcelEBupotApprovalUpdate(fullPath, CurrentLogin.Username);
                    #endregion

                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        [HttpPost]
        public ActionResult UploadEbupotFile()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);
                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert eBupot

                    result = WHTRepository.Instance.ExcelEbupotFile(fullPath, CurrentLogin.Username);
                    #endregion

                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        [HttpPost]
        public ActionResult UploadNIK()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);
                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert eBupot

                    result = WHTRepository.Instance.ExcelNIK(fullPath, CurrentLogin.Username);
                    #endregion

                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        [HttpPost]
        public ActionResult UploadTransitoryStatus()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);
                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert VAT In

                    result = WHTRepository.Instance.ExcelTransitoryStatusUpdate(fullPath, CurrentLogin.Username);
                    #endregion

                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        [HttpPost]
        public JsonResult GeneratePrintFile(FormCollection collection, bool checkAll)
        {
            #region Cek Wht Full Access Role
            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("WHTFullAccessRole");//new by role


            string[] WHTFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            if (config != null)
            {
                WHTFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (WHTFullAccessRole.Contains(item))
                    {
                        isFullAccess = true;
                    }
                }
            }
            #endregion

            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for printing is generated successfully"
            };
            string fullName = string.Empty;
            string fullPath = string.Empty;
            try
            {
                var searchParam = collection["data"];
                var whtIdList = collection["WHTIdList"];
                var searchParamModel = new WHTDashboardSearchParamViewModel();
                List<Guid> WHTIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<WHTDashboardSearchParamViewModel>(searchParam);
                    WHTIdList = WHTRepository.Instance.GetIdBySearchParam(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess);
                }
                else
                {
                    WHTIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(whtIdList);
                }

                var filePaths = WHTRepository.Instance.GetPDFUrlById(WHTIdList);
                fullName = GlobalFunction.MergePDFFilesNameable(filePaths, TempPrintFolder, CurrentLogin.Username, "WHT");
                if (!string.IsNullOrEmpty(fullName))
                {
                    fullPath = VirtualDirectoryFilePrintURL + fullName;
                }
                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("WHT Print e-Faktur", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SyncWHT()
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "Sync Success"
            };

            try
            {
                result = WHTRepository.Instance.SyncWHT();
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("WHT Sync Failed", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }
    }
}

