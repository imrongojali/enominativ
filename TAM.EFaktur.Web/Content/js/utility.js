﻿var DEBUG = true;
var currencyFormat = "0,0.00";
var charMaskNoFaktur = "999.999-99.99999999";
var charMaskNPWP = "99.999.999.9-999.999";
var charMaskcurrency = "99.999.999";

function defaultFor(arg, val) {
    return typeof arg !== 'undefined' ? arg : val;
}

function checkLoginTimeout(r) {
    var timeout = ($(r).find('form#login-form').length > 0);
    if (timeout) {
        window.location.href = "/Login";
    }
    if (DEBUG) {
        console.log("session timeout:"+timeout);
    }
    return timeout;
}

function getDropdownData(URL, elementId, selectedValue) {
    $.ajax({
        type: "GET",
        url: URL,
        success: function (result) {
            if (result != null && result.length > 0) {
                $.each(result, function (i, item) {
                    var row = "<option value='" + item.Id + "'>"; //kolom ViewModel : Id
                    row += item.Name; //kolom ViewModel : Name
                    row += "</option>";
                    $('#' + elementId).append(row);
                });
                var lookupIdElement = '<input type="hidden" class="form-control" id="lookupId' + elementId + '" />';
                var lookupNameElement = '<input type="hidden" class="form-control" id="lookupName' + elementId + '" />';

                $('#' + elementId).parent().append(lookupIdElement).append(lookupNameElement);

                //set selectedValue
                defaultFor(selectedValue, "");
                $('#' + elementId).val(selectedValue);
                $('#' + elementId).multiselect({
                    dropLeft: true,
                    buttonWidth: '100%',
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    onChange: function (element, checked) {
                        var brands = $('#'+ elementId +' option:selected');
                        var selectedId = [];
                        var selectedName = [];
                        $(brands).each(function (index, brand) {
                            selectedName.push([$(this).text()]);
                            selectedId.push([$(this).val()]);
                            if (DEBUG) {
                                console.log(selectedId[index]);
                                console.log(selectedName[index]);
                            }
                        });
                        
                        $('#lookupId' + elementId).val(selectedId.join(';'));
                        $('#lookupName' + elementId).val(selectedName.join(';'));
                    }
                });

            }
            //else {
                //msgError("No dropdown data found");
            //} //20181012
            if (DEBUG) {
                console.log('Dropdown Data Loaded');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (DEBUG) {
                console.log("error: " + errorThrown);
            }
            msgError("An error has occured");
        },
    });
}

function getDropdownDataByText(URL, elementId, selectedValue) {
    $.ajax({
        type: "GET",
        url: URL,
        success: function (result) {
            if (result != null && result.length > 0) {
                $.each(result, function (i, item) {
                    var row = "<option value='" + item.Id.replace(' ', '-') + "'>"; //kolom ViewModel : Id
                    row += item.Name; //kolom ViewModel : Name
                    row += "</option>";
                    $('#' + elementId).append(row);
                });
                var lookupIdElement = '<input type="hidden" class="form-control" id="lookupId' + elementId + '" />';
                var lookupNameElement = '<input type="hidden" class="form-control" id="lookupName' + elementId + '" />';

                $('#' + elementId).parent().append(lookupIdElement).append(lookupNameElement);

                //set selectedValue
                defaultFor(selectedValue, "");
                $('#' + elementId).val(selectedValue);
                $('#' + elementId).multiselect({
                    dropLeft: true,
                    buttonWidth: '100%',
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    onChange: function (element, checked) {
                        var brands = $('#' + elementId + ' option:selected');
                        var selectedId = [];
                        var selectedName = [];
                        $(brands).each(function (index, brand) {
                            selectedName.push([$(this).text()]);
                            selectedId.push([$(this).val()]);
                            if (DEBUG) {
                                console.log(selectedId[index]);
                                console.log(selectedName[index]);
                            }
                        });

                        $('#lookupId' + elementId).val(selectedId.join(','));
                        $('#lookupName' + elementId).val(selectedName.join(','));
                    }
                });

            }
            //else {
            //msgError("No dropdown data found");
            //} //20181012
            if (DEBUG) {
                console.log('Dropdown Data Loaded');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (DEBUG) {
                console.log("error: " + errorThrown);
            }
            msgError("An error has occured");
        },
    });
}

function getDropdownlist(URL, elementId) {
    $.ajax({
        type: "GET",
        url: URL,
        success: function (response) {

            if (response.length > 0) {
                $('#' + elementId).html('');
                var options = '';
                options += '<option value="">--Select--</option>';
                for (var i = 0; i < response.length; i++) {
                    options += '<option value="' + response[i].Id + '">' + response[i].Name + '</option>';
                }
                $('#' + elementId).append(options);    
            }
            //else {
            //msgError("No dropdown data found");
            //} //20181012
            if (DEBUG) {
                console.log('Dropdown Data Loaded');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (DEBUG) {
                console.log("error: " + errorThrown);
            }
            msgError("An error has occured");
        },
    });
}


$(function () {
    // Initialize modal dialog
    // attach modal-container bootstrap attributes to links with .modal-link class.
    // when a link is clicked with these attributes, bootstrap will display the href content in a modal dialog.
    $('body').on('click', '.modal-link', function (e) {
        e.preventDefault();
        $(this).attr('data-target', '#modal-container');
        $(this).attr('data-toggle', 'modal');
    });
    // Attach listener to .modal-close-btn's so that when the button is pressed the modal dialog disappears
    $('body').on('click', '.modal-close-btn', function () {
        $('#modal-container').modal('hide');
    });
    //clear modal cache, so that new content can be loaded
    $('#modal-container').on('hidden.bs.modal', function () {
        $(this).removeData('bs.modal');
    });
    $('#CancelModal').on('click', function () {
        return false;
    });
});

$(document).ready(function () {
    //convert HTML element to a specific date/time format
    var dateNow = new Date();

    $("._datetimepicker").datetimepicker({
    }); // _datetimepicker class into full datetime type

    $("._datepicker").datetimepicker({ // _datepicker class into date type
        format: 'DD-MM-YYYY'
    });

    $("._datepicker-default").datetimepicker({ // _datepicker class into date type
        format: 'DD-MM-YYYY',
        defaultDate: dateNow
    });

    $("._timepicker").datetimepicker({ // _timepicker class into time type
        format: 'LT'
    });

    $("._timepicker-default").datetimepicker({ // _datepicker class into date type
        format: 'LT',
        defaultDate: dateNow
    });

    //masking format Nomor Faktur
    $("._mask-nofaktur").mask(charMaskNoFaktur);

    ///masking format NPWP
    $("._mask-npwp").mask(charMaskNPWP);

    $("._mask-currency").mask(currencyFormat);
});

function getDropdownColor(URL, elementId, selectedValue) {
    $.ajax({
        type: "GET",
        url: URL,
        success: function (response) {

            if (response.length > 0) {
                $('#' + elementId).html('');
                var options = '';
               
                for (var i = 0; i < response.length; i++) {
                    var _colorValur = '';
                    let result = ntc.name(response[i].Id);
                    if (response[i].Id == 'No Color' || response[i].Id == 'ALL') {
                        _colorValur = response[i].Id;
                    }
                    else {
                        _colorValur = result[1];
                    }
                   
                    options += "<li><a href='#'><span style='width:15px;height:15px;background-color:" + response[i].Id + ";display:inline-block;'></span><input type='hidden'  id='" + response[i].Id + "' value='" + response[i].Id + "' /> " + _colorValur + "</a></li>";
                }
              
                $('#' + elementId).append(options);
            }
            //else {
            //msgError("No dropdown data found");
            //} //20181012
            if (DEBUG) {
                console.log('Dropdown Data Loaded');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (DEBUG) {
                console.log("error: " + errorThrown);
            }
            msgError("An error has occured");
        },
    });
}