﻿USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetVATOutSalesReturnOriginalFileById]    Script Date: 29/09/2020 12:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetVATOutSalesReturnOriginalFileById] 
	@VATOutSalesReturnId VARCHAR(MAX)
AS
BEGIN
	SELECT ColumnData 
	INTO #tempSelectedVATOutSalesReturnId 
	FROM uf_SplitString(@VATOutSalesReturnId, ';')

	--SELECT 
	--	--ROW_NUMBER() OVER (ORDER BY VO.TanggalFaktur ASC) AS RowNum,
	--	RVO.Id AS VATOutSalesReturnId,
	--	RVO.DANumber as Field1,
	--	RVO.BusinessUnit as Field2,
	--	RVO.CADACNumber as Field3,
	--	'RK' as Field4,
	--	dbo.uf_Only_Numbers (RVO.NPWPCustomer) as Field5,
	--	RVO.NamaCustomer as Field6,
	--	KJT.KDJenisTransaksiCode as Field7,
	--	RVO.FGPengganti as Field8,
	--	RVO.NomorFaktur as Field9,
	--	ISNULL(CONVERT(VARCHAR, RVO.TanggalFaktur, 103), '') as Field10,
	--	RVO.NomorDokumenRetur as Field11,
	--	ISNULL(CONVERT(VARCHAR, RVO.TanggalRetur, 103), '') as Field12,
	--	RVO.MasaPajakRetur as Field13,
	--	RVO.TahunPajakRetur as Field14,
	--	CONVERT(VARCHAR, RVO.JumlahReturDPP)as Field15,
	--	CONVERT(VARCHAR, RVO.JumlahReturPPN)as Field16,
	--	CONVERT(VARCHAR, RVO.JumlahReturPPNBM)as Field17
	--	FROM [dbo].[TB_R_ReturnVATOut] RVO
	--	LEFT JOIN dbo.TB_M_GeneralParam GP ON GP.ParamCode= RVO.BusinessUnit
	--	LEFT JOIN dbo.TB_M_KDJenisTransaksi KJT ON KJT.Id=RVO.KDJenisTransaksiID
	--	WHERE RVO.Id IN (SELECT ColumnData From #tempSelectedVATOutSalesReturnId )
	--	ORDER BY RVO.BusinessUnit,RVO.TanggalFaktur,RVO.NPWPCustomer  

	--DROP TABLE #tempSelectedVATOutSalesReturnId

	--SELECT 
	--	--ROW_NUMBER() OVER (ORDER BY VO.TanggalFaktur ASC) AS RowNum,
	--	RVO.Id AS VATOutSalesReturnId,
	--	RVO.DANumber as Field1,
	--	RVO.BusinessUnit as Field2,
	--	RVO.CADACNumber as Field3,
	--	'RK' as Field4,
	--	dbo.uf_Only_Numbers (RVO.NPWPCustomer) as Field5,
	--	RVO.NamaCustomer as Field6,
	--	KJT.KDJenisTransaksiCode as Field7,
	--	RVO.FGPengganti as Field8,
	--	RVO.NomorFaktur as Field9,
	--	ISNULL(CONVERT(VARCHAR, RVO.TanggalFaktur, 103), '') as Field10,
	--	RVO.NomorDokumenRetur as Field11,
	--	ISNULL(CONVERT(VARCHAR, RVO.TanggalRetur, 103), '') as Field12,
	--	RVO.MasaPajakRetur as Field13,
	--	RVO.TahunPajakRetur as Field14,
	--	CONVERT(VARCHAR, RVO.JumlahReturDPP)as Field15,
	--	CONVERT(VARCHAR, RVO.JumlahReturPPN)as Field16,
	--	CONVERT(VARCHAR, RVO.JumlahReturPPNBM)as Field17,
	--	RVOD.NamaObjek as Field18,
	--	RVOD.HargaSatuan as Field19,
	--	RVOD.JumlahBarang as Field20,
	--	RVOD.HargaTotal as Field21
	--	FROM [dbo].[TB_R_ReturnVATOut] RVO
	--	LEFT JOIN dbo.TB_R_VATOutDetail RVOD ON RVOD.[VATOutId]= RVO.Id
	--	LEFT JOIN dbo.TB_M_GeneralParam GP ON GP.ParamCode= RVO.BusinessUnit
	--	LEFT JOIN dbo.TB_M_KDJenisTransaksi KJT ON KJT.Id=RVO.KDJenisTransaksiID
 --		ORDER BY RVO.BusinessUnit,RVO.TanggalFaktur,RVO.NPWPCustomer
	--DROP TABLE #tempSelectedVATOutSalesReturnId

END


