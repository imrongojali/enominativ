﻿Exec [usp_Insert_Upload_TB_M_Facility] @TaxFacCategory,@TaxFacNo,@SKBType,@SKBTypeOthers,@TaxFacDate
		,@SupplierName,@SupplierNPWP,@SupplierAddress,@SupplierPhoneNo
		,@SupplierCountry,@SupplierEstDate,@FacValidPeriodFrom,@FacValidPeriodTo
		,@FacDocSignerName,@FacDocSignerTitle,@RecordedDate,@RecordedBy,@MapCode
		,@UploadFile,@Seq,@PDFFacStatus, @ApprovalStatus, @ApprovalDate, @RegistrationNo