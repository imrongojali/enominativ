﻿Exec usp_GetApplicationLogListDashboard
	@SortBy,
	@SortDirection,
	@LogDateFrom,
	@LogDateTo,
	@LogTimeFrom,
	@LogTimeTo,
	@MessageType,
	@FromNumber,
	@ToNumber