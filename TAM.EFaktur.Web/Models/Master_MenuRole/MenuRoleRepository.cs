﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Master_MenuRole
{
    public class MenuRoleRepository : BaseRepository
    {

        #region Singleton
        private MenuRoleRepository() { }
        private static MenuRoleRepository instance = null;
        public static MenuRoleRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MenuRoleRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(MenuRoleDashboardSearchParamViewModel model)
        {
            dynamic args = new
            {
                RoleCode = model.RoleCode,
                MenuCode = model.MenuCode,
                MenuName = model.MenuName
            };

            return db.SingleOrDefault<int>("Master_MenuRole/usp_CountMenuRoleListDashboard", args);
        }
        #endregion

        #region Processing Data
        public List<MenuRoleDashboardViewModel> GetList(MenuRoleDashboardSearchParamViewModel model, int SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            dynamic args = new
            {
                RoleCode = model.RoleCode,
                MenuCode = model.MenuCode,
                MenuName = model.MenuName,
                ButtonName = model.ButtonName,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
            IEnumerable<MenuRoleDashboardViewModel> result = db.Query<MenuRoleDashboardViewModel>("Master_MenuRole/usp_GetRoleMenuListDashboard", args);
            return result.ToList();
        }

        public MenuRole GetById(Guid Id)
        {
            return db.SingleOrDefault<MenuRole>("Master_MenuRole/usp_GetRoleMenuByID", new { Id = Id });
        }


        #endregion

        public List<DropdownViewModel> GetMenuDropdown()
        {

            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_MenuRole/usp_DropDownMenu");
            return result.ToList();
        }


        //insert Data
        public Result Insert(Master_MenuRoleCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    Id = Guid.NewGuid(),
                    RoleId = model.RoleId,
                    MenuId = model.MenuId,
                    ButtonName = model.ButtonName,
                    EventDate = EventDate.FormatSQLDate(),
                    EventActor = EventActor
                };
                db.Execute("Master_MenuRole/usp_Insert_TB_M_RoleMenu", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        //Update Data
        public Result Update(Master_MenuRoleCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    Id = model.Id,
                    RoleId = model.RoleId,
                    MenuId = model.MenuId,
                    ButtonName = model.ButtonName,
                    EventDate = EventDate.FormatSQLDate(),
                    EventActor = EventActor

                };
                db.Execute("Master_MenuRole/usp_Update_TB_M_RoleMenu", args);
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        //Update Data
        public Result Delete(Master_MenuRoleCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    Id = model.Id,
                    RoleCode = model.RoleCode,
                    Menucode = model.MenuCode,
                    EventDate = EventDate.FormatSQLDate(),
                    EventActor = EventActor

                };
                db.Execute("Master_MenuRole/usp_Delete_TB_M_RoleMenu", args);
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }

        }
    }
}

