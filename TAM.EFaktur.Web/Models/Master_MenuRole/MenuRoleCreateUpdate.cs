﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_MenuRole
{
    public class MenuRoleCreateUpdate
    {
        public Guid Id { get; set; }
        public string RoleId { get; set; }
        public string MenuId { get; set; }
        public string ButtonName { get; set; }
        public string CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string RowStatus { get; set; }
    }
}