﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.ApplicationLog
{
    public class ApplicationLog : BaseRepository
    {
        public DateTime LoginDate { get; set; }
        public string MessageType { get; set; }
    }
}