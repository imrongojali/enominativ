﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_Company_Details
{
    public class CompanyCreateUpdate 
    {

        public int RowNum { get; set; }
        public Guid Id { get; set; }
        public string Lokasi { get; set; }
        public string Npwp_pt { get; set; }
        public string Nama_pt { get; set; }
        public string Npwp_ttd { get; set; }
        public string Nama_ttd { get; set; }
        public string Jabatan_ttd { get; set; }
        public string Image_ttd { get; set; }
        public string Image_cap { get; set; }
        public string Code { get; set; }
        public string alamat_pt { get; set; }
        public string no_pengukuhan_pt { get; set; }
       
        public DateTime EventDate { get; set; }
        public string EventActor { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }
        public HttpPostedFileBase ImageCap
        {
            get; set;
        }
        public DateTime Cut_Off_Start { get; set; }
        public DateTime Cut_Off_End { get; set; }
        //public List<Nullable<Guid>> RoleIds { get; set; }
    }
}