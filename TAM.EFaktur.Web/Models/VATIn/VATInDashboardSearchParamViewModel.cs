﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.VATIn
{
    public class VATInDashboardSearchParamViewModel
    {
        public string TanggalFakturFrom { get; set; } 
        public string TanggalFakturTo { get; set; }
        public string TanggalFileDiterimaFrom { get; set; } 
        public string TanggalFileDiterimaTo { get; set; }
        public string WaktuFileDiterimaFrom { get; set; } 
        public string WaktuFileDiterimaTo { get; set; }
        public string TransitoryDateFrom { get; set; } 
        public string TransitoryDateTo { get; set; }
        public string TanggalDownloadFrom { get; set; } 
        public string TanggalDownloadTo { get; set; }
        public string RecordTimeFrom { get; set; } 
        public string RecordTimeTo { get; set; }
        public string ApprovalTimeFrom { get; set; } 
        public string ApprovalTimeTo { get; set; }
        public string NomorFakturPajak { get; set; } 
        public string NPWPPenjual { get; set; } 
        public string NamaPenjual { get; set; }
        public string Perekam { get; set; } 
        public string FakturType { get; set; } 
        public string RecordStatus { get; set; } 
        public string TransitoryStatus { get; set; }
        public string DownloadStatus { get; set; } 
        public string ApprovalStatus { get; set; }
        public Nullable<Decimal> VATBaseAmount { get; set; } 
        public string VATBaseAmountOperator { get; set; }
        public Nullable<Decimal> VATAmount { get; set; } 
        public string VATAmountOperator { get; set; }
        public string BatchFileName { get; set; }
        public int page { get; set; }
        public int size { get; set; }
        public string PVNumber { get; set; }
        public string TransitoryNumber { get; set; }
        public string ApprovalDescription { get; set; }
        public string PostingDateFrom { get; set; }
        public string PostingDateTo { get; set; }
        public string RelatedGLAccount { get; set; }
        public string PVCreatedBy { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceStatus { get; set; }
        public string SAPDocNumber { get; set; }
        public string Remark { get; set; }
        public string TAXPeriodMonth { get; set; }
        public string TAXPeriodYear { get; set; }
        public string VATPPnBMOperator { get; set; }
        public Nullable<Decimal> VATPPnBM { get; set; }
        public string VATDppOperator { get; set; }
        public Nullable<Decimal> VATDpp { get; set; }
        public dynamic MapFromModel(bool isFullAccess = false, string DivisionName = "", string Username = "", int SortBy = 0, string SortDirection = "ASC", int FromNumber = 1, int ToNumber = 10)
        {
            return new
            {

                TanggalFakturFrom = this.TanggalFakturFrom.FormatDefaultDateWhenNullOrEmpty(),
                TanggalFakturTo = this.TanggalFakturTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                TanggalFileDiterimaFrom = this.TanggalFileDiterimaFrom.FormatDefaultDateWhenNullOrEmpty(),
                TanggalFileDiterimaTo = this.TanggalFileDiterimaTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                WaktuFileDiterimaFrom = this.WaktuFileDiterimaFrom.FormatDefaultTimeWhenNullOrEmpty(),
                WaktuFileDiterimaTo = this.WaktuFileDiterimaTo.FormatDefaultTimeWhenNullOrEmpty("TO"),
                TransitoryDateFrom = this.TransitoryDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                TransitoryDateTo = this.TransitoryDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                TanggalDownloadFrom = this.TanggalDownloadFrom.FormatDefaultDateWhenNullOrEmpty(),
                TanggalDownloadTo = this.TanggalDownloadTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                RecordTimeFrom = this.RecordTimeFrom.FormatDefaultDateWhenNullOrEmpty(),
                RecordTimeTo = this.RecordTimeTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                ApprovalTimeFrom = this.ApprovalTimeFrom.FormatDefaultDateWhenNullOrEmpty(),
                ApprovalTimeTo = this.ApprovalTimeTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                NomorFakturPajak = this.NomorFakturPajak,
                NPWPPenjual = this.NPWPPenjual,
                NamaPenjual = this.NamaPenjual,
                Perekam = this.Perekam,
                FakturType = this.FakturType,
                RecordStatus = this.RecordStatus,
                TransitoryStatus = this.TransitoryStatus,
                DownloadStatus = this.DownloadStatus,
                ApprovalStatus = this.ApprovalStatus,
                VATBaseAmount = this.VATBaseAmount,
                VATBaseAmountOperator = this.VATBaseAmountOperator,
                VATAmount = this.VATAmount,
                VATAmountOperator = this.VATAmountOperator,
                BatchFileName = this.BatchFileName,
                DivisionName = DivisionName,
                Username = Username,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber,
                PVNumber = this.PVNumber,
                TransitoryNumber = this.TransitoryNumber,
                ApprovalDescription = this.ApprovalDescription,
                PostingDateFrom = this.PostingDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                PostingDateTo = this.PostingDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                RelatedGLAccount = this.RelatedGLAccount,
                PVCreatedBy = this.PVCreatedBy,
                InvoiceNumber = this.InvoiceNumber,
                InvoiceStatus = this.InvoiceStatus,
                SAPDocNumber = this.SAPDocNumber,
                Remark = this.Remark,
                TAXPeriodMonth = this.TAXPeriodMonth,
                TAXPeriodYear = this.TAXPeriodYear,
                VATPPnBM = this.VATPPnBM,
                VATPPnBMOperator = this.VATPPnBMOperator,
                VATDpp = this.VATDpp,
                VATDppOperator = this.VATDppOperator,
                isFullAccess = isFullAccess,
            };
        }
    }
}