﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.VATIn
{
    public class VATInManualInputViewModel
    {
        public Guid Id { get; set; }
        public String InvoiceNumber { get; set; }
        public String InvoiceNumberFull { get; set; }
        public String KDJenisTransaksi { get; set; }
        public String FGPengganti { get; set; }
        public String InvoiceDate { get; set; }
        public String SupplierNPWP { get; set; }
        public String SupplierName { get; set; }
        public String SupplierAddress { get; set; }
        public String NPWPLawanTransaksi { get; set; }
        public String NamaLawanTransaksi { get; set; }
        public String AlamatLawanTransaksi { get; set; }
        public String StatusApprovalXML { get; set; }
        public String StatusFakturXML { get; set; } 
        public Nullable<Decimal> VATBaseAmount { get; set; }
        public Nullable<Decimal> VATAmount { get; set; }
        public Nullable<Decimal> JumlahPPnBM { get; set; }
        public List<VATInDetailManualInputViewModel> VATInDetails { get; set; }
        public String ExpireDate { get; set; }
        public String FakturType { get; set; }
        public String URL { get; set; }
        public String FileName{ get; set; }
    }
}