﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.VATIn
{
    public class VATInDetailRepository : BaseRepository
    {
        #region Singleton
        private VATInDetailRepository() { }
        private static VATInDetailRepository instance = null;
        public static VATInDetailRepository Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new VATInDetailRepository();
                }
                return instance;
            }
        }
        #endregion


    }
}