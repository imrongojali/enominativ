﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.VATIn
{
    public class VATInApprovalStatusViewModel
    {
        public string NomorFakturGabungan { get; set; }
        public string ApprovalStatus { get; set; }
        public Nullable<DateTime> ApprovalTime { get; set; }
        public DateTime RecordTime { get; set; }
        public string ApprovalDescription { get; set; }

        public dynamic MapFromModel(DateTime EventDate, string EventActor)
        {
            return new
            {
                NomorFakturGabungan = NomorFakturGabungan,
                ApprovalStatus = ApprovalStatus,
                ApprovalTime = ApprovalTime,
                RecordTime = RecordTime,
                EventDate = EventDate,
                EventActor = EventActor,
                ApprovalDescription = ApprovalDescription
            };
        }
    }
}