﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Log_Sync
{
    public class LogSyncCreateUpdate
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }
        public int RowId { get; set; }
        public string RowData { get; set; }
        public string MessageType { get; set; }
        public string Message { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }


        public LogSyncCreateUpdate CreateModel(string fileName, int rowId, string rowData, MessageType messageType, string message, string messageDetail = "None")
        {
            return new LogSyncCreateUpdate
            {
                Id = Guid.NewGuid(),
                FileName = fileName,
                Message = message + " \nDetail: " + messageDetail,
                MessageType = messageType.ToString(),
                RowId = rowId,
                RowData = rowData,
                CreatedOn = DateTime.Now,
                CreatedBy ="sistem"
               
            };
        }
    }
}