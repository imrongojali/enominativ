﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Log_Sync
{
    public class LogSyncDashboardViewModel
    {
        public int RowNum { get; set; }
        public Guid Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public int RowID { get; set; }
        public string RowData { get; set; }
        public string Message { get; set; }
        public string MessageType { get; set; }
        public string FileName { get; set; }
        public string CreatedBy { get; set; }
    }
}