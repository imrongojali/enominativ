﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Log_Sync
{
    public class LogSyncRepository : BaseRepository
    {
        #region Singleton
        private LogSyncRepository() { }
        private static LogSyncRepository instance = null;
        public static LogSyncRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LogSyncRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(LogSyncDashboardSearchParamViewModel model)
        {
            dynamic args = new
            {

               Id = model.Id,
               LogDateFrom = model.LogDateFrom.FormatDefaultDateWhenNullOrEmpty(),
               LogDateTo = model.LogDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
               LogTimeFrom = model.LogTimeFrom.FormatDefaultTimeWhenNullOrEmpty(),
               LogTimeTo = model.LogTimeTo.FormatDefaultTimeWhenNullOrEmpty("TO"),
               MessageType = model.MessageType
            };

            return db.SingleOrDefault<int>("SyncLog/usp_CountLogSyncListDashboard", args);
        }
        #endregion
        #region Processing Data

        public List<LogSyncDashboardViewModel> GetList(LogSyncDashboardSearchParamViewModel model, int SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            dynamic args = new
            {
                Id= model.Id,
                LogDateFrom = model.LogDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                LogDateTo = model.LogDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                LogTimeFrom = model.LogTimeFrom.FormatDefaultTimeWhenNullOrEmpty(),
                LogTimeTo = model.LogTimeTo.FormatDefaultTimeWhenNullOrEmpty("TO"),
                MessageType = model.MessageType,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
            IEnumerable<LogSyncDashboardViewModel> result = db.Query<LogSyncDashboardViewModel>("SyncLog/usp_GetLogSyncListDashboard", args);
            return result.ToList();
        }

        public LogSync GetById(Guid Id)
        {
            return db.SingleOrDefault<LogSync>("SyncLog/usp_GetSyncLogByID", new { Id = Id });
        }

        //insert Data
        public Result Insert(LogSyncCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    Id = model.Id,
                    FileName = model.FileName,
                    RowId = model.RowId,
                    RowData =  model.RowData,
                    MessageType = model.MessageType,
                    Message = model.Message,
                    EventDate = EventDate.FormatSQLDateTime(),
                    EventActor = EventActor
                };
                db.Execute("SyncLog/usp_Insert_TB_R_SyncLog", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

       
        #endregion
    }
}