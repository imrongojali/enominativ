﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.WHTSummary
{
    public class WHTSummaryReportExcelViewModel
    {
        public int MasaPajakBulan { get; set; }
        public int MasaPajakTahun { get; set; }
        public string KJSMapCode { get; set; }
        public string TaxArticle { get; set; }
        public string GLAccount { get; set; }
        public string ESPTType { get; set; }
        public Nullable<decimal> TaxPayment { get; set; }
        public Nullable<DateTime> TaxPaymentDate { get; set; }
        public Nullable<DateTime> TaxReportingDate { get; set; }
        public string NTPN { get; set; }
        public string NTTE { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<decimal> DPPAmountAdjustment { get; set; }
        public Nullable<decimal> PPHAmountAdjusment { get; set; }

        public dynamic MapFromModel(DateTime EventDate, string EventActor)
        {
            return new
            {
                MasaPajakBulan = MasaPajakBulan,
                MasaPajakTahun = MasaPajakTahun,
                KJSMapCode = KJSMapCode,
                TaxArticle = TaxArticle,
                GLAccount = GLAccount,
                ESPTType = ESPTType,
                TaxPayment = TaxPayment,
                TaxPaymentDate = TaxPaymentDate,
                TaxReportingDate = TaxReportingDate,
                NTPN = NTPN,
                NTTE = NTTE,
                CreatedOn = CreatedOn,
                CreatedBy = CreatedBy,
                DPPAmountAdjustment = DPPAmountAdjustment,
                PPHAmountAdjusment = PPHAmountAdjusment
            };
        }
    }
}