﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models.Log_Sync;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.SyncData;

namespace TAM.EFaktur.Web.Models.WHTSummary
{
    public class WHTSummaryRepository : BaseRepository
    {
        #region Singleton
        private WHTSummaryRepository() { }
        private static WHTSummaryRepository instance = null;
        public static WHTSummaryRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new WHTSummaryRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(WHTSummaryReportSearchParamViewModel model, string DivisionName, string Username)
        {
            try
            {
                return db.SingleOrDefault<int>("WHTSummary/efb_sp_CountSummaryReportList", model.MapFromModel(DivisionName, Username));
            }
            catch
            {
                throw;
            }

        }
        #endregion

        #region Processing Data
        public List<WHTSummaryReportViewModel> GetList(WHTSummaryReportSearchParamViewModel model, string DivisionName, string Username, int SortBy, string SortDirection)
        {
            IEnumerable<WHTSummaryReportViewModel> result = db.Query<WHTSummaryReportViewModel>("WHTSummary/efb_sp_SummaryReportList", model.MapFromModel(DivisionName, Username, SortBy, SortDirection));
            return result.ToList();
        }

        //public List<WHTSummaryDownloadViewModel> DownloadList(WHTSummaryReportSearchParamViewModel model, string DivisionName, string Username)
        //{
        //    IEnumerable<WHTSummaryDownloadViewModel> result = db.Query<WHTSummaryDownloadViewModel>("WHTSummary/efb_sp_DownloadSummaryReportList", model.MapFromModel(DivisionName, Username));
        //    return result.ToList();
        //}
        public List<WHTSummaryReportViewModel> DownloadList(WHTSummaryReportSearchParamViewModel model, string DivisionName, string Username)
        {
            IEnumerable<WHTSummaryReportViewModel> result = db.Query<WHTSummaryReportViewModel>("WHTSummary/efb_sp_SummaryReportList", model.MapFromModel(DivisionName, Username));
            return result.ToList();
        }

        public Result ExcelSummaryReportInsert(string path, string user)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = Formatting.UPLOAD_NOTIFICATION
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                WHTSummaryReportExcelViewModel whtSummary = new WHTSummaryReportExcelViewModel();
                var cellNum = 13; // 13 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();
                exceptionCellNum.Add(6);
                exceptionCellNum.Add(7);
                exceptionCellNum.Add(8);
                exceptionCellNum.Add(9);
                exceptionCellNum.Add(10);
                exceptionCellNum.Add(11);
                exceptionCellNum.Add(12);
                db.BeginTransaction();

                for (row = 3; row <= sheet.LastRowNum; row++) // Data row start at row 4, index 3 on excel template
                {
                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNum; i++)
                        {
                            sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                            pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        #endregion

                        if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        {
                            #region Cell Mapping & Insert to DB
                            //if (whtApprovalStatus.NomorFakturGabungan != sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim()) //ignore if duplicate
                            //{
                            whtSummary = new WHTSummaryReportExcelViewModel();

                            int MasaPajakBulan;
                            bool isNumMasaPajakBulan = int.TryParse(sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim(), out MasaPajakBulan);
                            if (isNumMasaPajakBulan)
                            {
                                whtSummary.MasaPajakBulan = MasaPajakBulan;
                            }
                            else
                            {
                                throw new Exception("Masa Pajak Bulan must be digit");
                            }

                            int MasaPajakTahun;
                            bool isNumMasaPajakTahun = int.TryParse(sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim(), out MasaPajakTahun);
                            if (isNumMasaPajakTahun)
                            {
                                whtSummary.MasaPajakTahun = MasaPajakTahun;
                            }
                            else
                            {
                                throw new Exception("Masa Pajak Tahun must be digit");
                            }

                            whtSummary.KJSMapCode = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();
                            whtSummary.TaxArticle = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();
                            whtSummary.GLAccount = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim();
                            whtSummary.ESPTType = sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim();

                            if (sheet.GetRow(row).GetCell(6).StringCellValue.ToString().Trim() != "" && sheet.GetRow(row).GetCell(6).StringCellValue.ToString().Trim() != null)
                            {
                                decimal TaxPayment;
                                bool isNumTaxPayment = decimal.TryParse(sheet.GetRow(row).GetCell(6).StringCellValue.ToString().Trim(), out TaxPayment);
                                if (isNumTaxPayment)
                                {
                                    whtSummary.TaxPayment = TaxPayment;
                                }
                                else
                                {
                                    throw new Exception("Tax Payment must be digit");
                                }
                            }

                            if (sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim() != "" && sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim() != null)
                            {
                                var taxPayDate = sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim().Split('/');
                                if (taxPayDate.Count() > 1) // when the excel column is a text type
                                {
                                    if (taxPayDate[0].Length == 2 && taxPayDate[1].Length == 2 && taxPayDate[2].Length == 4)
                                    {
                                        whtSummary.TaxPaymentDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        throw new Exception("Date Format must be: dd/mm/yyyy");
                                    }
                                }
                                else // when the excel column is a DateTime type
                                {
                                    if (sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim().Contains("-"))
                                    {
                                        var date = DateTime.ParseExact(sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Replace("-", "/").Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        whtSummary.TaxPaymentDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Replace("-","/").Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        whtSummary.TaxPaymentDate = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim()));
                                    }
                                }
                            }

                            if (sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim() != "" && sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim() != null)
                            {
                                var taxRprtDate = sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim().Split('/');
                                if (taxRprtDate.Count() > 1) // when the excel column is a text type
                                {
                                    if (taxRprtDate[0].Length == 2 && taxRprtDate[1].Length == 2 && taxRprtDate[2].Length == 4)
                                    {
                                        whtSummary.TaxReportingDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        throw new Exception("Date Format must be: dd/mm/yyyy");
                                    }
                                }
                                else // when the excel column is a DateTime type
                                {
                                    if (sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim().Contains("-"))
                                    {
                                        var date = DateTime.ParseExact(sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Replace("-", "/").Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        whtSummary.TaxReportingDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Replace("-","/").Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        whtSummary.TaxReportingDate = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim()));
                                    }
                                }
                            }

                            whtSummary.NTPN = sheet.GetRow(row).GetCell(9).StringCellValue.ToString().Trim();
                            whtSummary.NTTE = sheet.GetRow(row).GetCell(10).StringCellValue.ToString().Trim();
                            whtSummary.CreatedBy = user;
                            whtSummary.CreatedOn = DateTime.Now;

                            if (sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim() != "" && sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim() != null)
                            {
                                decimal DppAdjust;
                                bool isNumDppAdjust = decimal.TryParse(sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim(), out DppAdjust);
                                if (isNumDppAdjust)
                                {
                                    whtSummary.DPPAmountAdjustment = DppAdjust;
                                }
                                else
                                {
                                    throw new Exception("DPP Amount Adjustment must be digit");
                                }
                            }

                            if (sheet.GetRow(row).GetCell(12).StringCellValue.ToString().Trim() != "" && sheet.GetRow(row).GetCell(12).StringCellValue.ToString().Trim() != null)
                            {
                                decimal PphAdjust;
                                bool isNumPphAdjust = decimal.TryParse(sheet.GetRow(row).GetCell(12).StringCellValue.ToString().Trim(), out PphAdjust);
                                if (isNumPphAdjust)
                                {
                                    whtSummary.PPHAmountAdjusment = PphAdjust;
                                }
                                else
                                {
                                    throw new Exception("PPH Amount Adjustment must be digit");
                                }
                            }

                            result = db.SingleOrDefault<Result>("WHTSummary/usp_Insert_TB_R_SummaryReportPaymentData", whtSummary.MapFromModel(DateTime.Now, user));
                            //}
                            //else
                            //{
                            //    throw new Exception("Immediate duplicate rows of " + whtApprovalStatus.NomorFakturGabungan);
                            //}
                            #endregion

                            if (!result.ResultCode)
                            {
                                throw new Exception(result.ResultDesc);
                            }
                            else
                            {
                                ExceptionHandler.LogToSync(row - 2, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path,MessageType.INF);
                                rowSuccess++;
                            }
                        }
                        else
                        {
                            throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        }
                    }
                    catch (Exception e)
                    {
                        e.LogToSync(row - 2, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }

                db.CommitTransaction();
                result.ResultCode = true;
                result.ResultDesc = Formatting.UPLOAD_NOTIFICATION.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        #endregion

        #region Private Method
        #endregion
    }
}