﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.WHTSummary
{
    public class WHTSummaryReportSearchParamViewModel
    {
        public int Masa { get; set; } 
        public int Tahun { get; set; }
        public string KJSMapCode { get; set; }
        public string GLAccount { get; set; }
        public string GLDescription { get; set; }
        public string TaxArticle { get; set; }
        public string eSPTType { get; set; }
        //public int page { get; set; }
        //public int size { get; set; }
        public dynamic MapFromModel(string DivisionName = "", string Username = "", int SortBy = 0, string SortDirection = "ASC")
        {
            return new
            {
                SortBy = SortBy,
                SortDirection = SortDirection,
                Masa = this.Masa,
                Tahun = this.Tahun,
                KJSMapCode = this.KJSMapCode,
                GLAccount = this.GLAccount,
                GLDescription = this.GLDescription,
                TaxArticle = this.TaxArticle,
                eSPTType = this.eSPTType
            };
        }
    }
}