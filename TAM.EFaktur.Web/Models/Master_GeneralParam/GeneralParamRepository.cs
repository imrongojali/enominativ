﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_GeneralParam
{
    public class GeneralParamRepository : BaseRepository
    {
        #region Singleton
        private GeneralParamRepository() { }
        private static GeneralParamRepository instance = null;
        public static GeneralParamRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GeneralParamRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Get Dropdown

        public List<DropdownViewModel> GetGeneralParamDropdown(string ParamType)
        {
            dynamic args = new
            {
                ParamType = ParamType
            };
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetGeneralParamDropdownListByType", args);
            return result.ToList();
        }
        //ADD AD FilterColor
        //public List<DropdownColor> GetGeneralParamDropdownColor(string ParamType)
        //{
        //    dynamic args = new
        //    {
        //        ParamType = ParamType
        //    };
        //    IEnumerable<DropdownColor> result = db.Query<DropdownColor>("SELECT * FROM (SELECT VO.Id,VO.NomorFakturGabungan AS NomorFaktur,([TAM_EFAKTUR].dbo.[udf_GetColorFromParam]('TMS_VATOUTSALES', VO.NomorFakturGabungan, CONVERT(datetime, VO.TanggalFaktur), CONVERT(nvarchar(max), VO.ApprovalStatus), CASE ISNULL(VO.PdfUrl, '') WHEN '' THEN 'Not Exist' ELSE 'Exist' END, VO.TransitoryStatus, NULL)) as EFakturStatus FROM[dbo].[TB_R_VATOut] VO WHERE VO.RowStatus = 0) TBL where PARSENAME(REPLACE(EFakturStatus, '|', '.'), 3) <> 'All OK'", args);
        //    return result.ToList();
        //}
        //END
        public List<DropdownViewModel> GetGeneralParamDropdownByParamCode(string ParamType)
        {
            dynamic args = new
            {
                ParamType = ParamType
            };
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetGeneralParamDropdownListByParamCode", args);
            return result.ToList();
        }
        #endregion

        #region Get Dropdown Code

        public List<DropdownViewByCodeModel> GetGeneralParamDropdownByCode()
        {
           
            IEnumerable<DropdownViewByCodeModel> result = db.Query<DropdownViewByCodeModel>("Master_GeneralParam/usp_GetGeneralParamDropdownListByCode");
            return result.ToList();
        }

        public List<DropdownViewByCodeModel> GetGeneralParamDropdownMAPCode()
        {

            IEnumerable<DropdownViewByCodeModel> result = db.Query<DropdownViewByCodeModel>("Master_GeneralParam/usp_GetGeneralParamDropdownListMAPCode");
            return result.ToList();
        }
        #endregion

        #region Dropdown DDL
        public List<DropdownViewModel> GetKDTransaksi(int ParamType)
        {
            dynamic args = new
            {
                ParamType = ParamType
            };
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetKDTransaksiDropDown",args);
            return result.ToList();
        }
        #endregion

        #region Get Dropdown

        public List<DropdownViewModel> GetBusinessUnitByDivisionName(string DivisionName)
        {
            dynamic args = new
            {
                DivisionName = DivisionName
            };
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetBusinessUnitByDivisionName", args);
            return result.ToList();
        }
        #endregion

        #region VAT-In Dashboard
        public List<DropdownViewModel> GetPvCreateByDropdown()
        {
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetPvCreateByDropdownList");
            return result.ToList();
        }
        #endregion

        #region WHT Dashboard
        public List<DropdownViewModel> GetTaxArticleByDropdown()
        {
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetTaxArticleByDropdownlist");
            return result.ToList();
        }
        public List<DropdownViewModel> GetJenisPajakByDropdown()
        {
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetJenisPajakByDropdownlist");
            return result.ToList();
        }

        public List<DropdownModel> GetJenisPajakByDropdownByParam()
        {
            IEnumerable<DropdownModel> result = db.Query<DropdownModel>("Master_GeneralParam/usp_GetJenisPajakByDropdownlistByParam");
            return result.ToList();
        }
        public List<DropdownViewModel> GetSPTTypeByDropdown()
        {
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetSPTTypeByDropdownlist");
            return result.ToList();
        }
        public List<DropdownViewModel> GetPVCreatedByByDropdown()
        {
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetPVCreatedByByDropdownlist");
            return result.ToList();
        }
        #endregion

        #region Tax Summary Report
        public List<DropdownViewModel> GetKjsMapCodeDropdownFromTB_M_JenisPajak()
        {
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetKjsMapCodeDropdownFromTB_M_JenisPajak");
            return result.ToList();
        }
        public List<DropdownViewModel> GetGlAccountDropdownFromTB_M_JenisPajak()
        {
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetGlAccountDropdownFromTB_M_JenisPajak");
            return result.ToList();
        }
        public List<DropdownViewModel> GetTaxArticleDropdownFromTB_M_JenisPajak()
        {
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetTaxArticleDropdownFromTB_M_JenisPajak");
            return result.ToList();
        }
        public List<DropdownViewModel> GetSptTypeDropdownFromTB_M_JenisPajak()
        {
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetSptTypeDropdownFromTB_M_JenisPajak");
            return result.ToList();
        }
        #endregion


        #region Register
        public List<DropdownViewModel> GetApprovaStatusByDropDown()
        {
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetApprovalStatusByDropdownlist");
            return result.ToList();
        }

        public List<DropdownViewModel> GetSupplierCountryCodeByDropDown()
        {
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetPvCreateByDropdownList");
            return result.ToList();
        }

        public List<DropdownViewModel> GetRecordedByByDropDown()
        {
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetPvCreateByDropdownList");
            return result.ToList();
        }
        public List<DropdownViewModel> GetFRecordedByByDropDown()
        {
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetRecordedByByDropdownlist");
            return result.ToList();
        }
        public List<DropdownParamColor> GetColorParamDropDown()
        {
            IEnumerable<DropdownParamColor> result = db.Query<DropdownParamColor>("Master_GeneralParam/usp_GetGeneralParamDropdownColorList");
            return result.ToList();
        }
        #endregion
    }
}