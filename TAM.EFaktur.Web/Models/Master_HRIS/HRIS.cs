﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_HRIS
{
    [Serializable]
    public class HRIS
    {
        public Int32 sKey { get; set; }
        public String NoReg { get; set; }
        public String EmployeeName { get; set; }
        public String DivisionName { get; set; }
        public String DepartmentName { get; set; }
        public String SectionName { get; set; }
        public String Title { get; set; }
        public DateTime scd_start { get; set; }
        public DateTime scd_end { get; set; }
    }
}