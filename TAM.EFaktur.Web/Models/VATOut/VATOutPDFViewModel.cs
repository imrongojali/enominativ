﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.VATOut
{
    public class VATOutPDFViewModel : BaseModel
    {
        public Guid Id { get; set; }       
        public string NomorFaktur { get; set; }
        public string BusinessUnit { get; set; }
        public string DANumber { get; set; }
        public string NomorFakturGabungan { get; set; }
        public DateTime TanggalFaktur { get; set; }
        public string NamaCustomer { get; set; }
        public string AlamatLawanTransaksi { get; set; }
        public string NPWPCustomer { get; set; }

        public string KodeObjek { get; set; }
        public string NamaObjek { get; set; }
        public decimal JumlahBarang { get; set; }
        public decimal HargaSatuan { get; set; }
        public decimal HargaTotal { get; set; }
        public decimal JumlahDPP { get; set; }

        public decimal JumlahPPN { get; set; }




    }


    }

