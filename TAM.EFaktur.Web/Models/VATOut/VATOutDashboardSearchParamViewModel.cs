﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.VATOut
{
    public class VATOutDashboardSearchParamViewModel
    {
        public string TanggalFakturFrom { get; set; }
        public string TanggalFakturTo { get; set; }
        public string TanggalFileDiterimaFrom { get; set; }
        public string TanggalFileDiterimaTo { get; set; }
        public string WaktuFileDiterimaFrom { get; set; }
        public string WaktuFileDiterimaTo { get; set; }
        public string TanggalDownloadFrom { get; set; }
        public string TanggalDownloadTo { get; set; }
        public string WaktuDownloadFrom { get; set; }
        public string WaktuDownloadTo { get; set; }
        public string RecordTimeFrom { get; set; }
        public string RecordTimeTo { get; set; }
        public string ApprovalTimeFrom { get; set; }
        public string ApprovalTimeTo { get; set; }
        public string TaxInvoiceStatus { get; set; }
        public string TransitoryStatus { get; set; }
        public string TransitoryNumber { get; set; }
        public string TransitoryDateFrom { get; set; }
        public string TransitoryDateTo { get; set; }
  
        public string BusinessUnit { get; set; }
        public string TransactionDataFile { get; set; }
        public string NomorFakturPajak { get; set; }
        public string DANumber { get; set; }
        public string NPWPCustomer { get; set; }
        public string NamaCustomer { get; set; }
        public Nullable<Decimal> VATBaseAmount { get; set; }
        public string VATBaseAmountOperator { get; set; }
        public Nullable<Decimal> VATAmount { get; set; }
        public string VATAmountOperator { get; set; }
        public string RecordStatus { get; set; }
        public string DownloadStatus { get; set; }
        public string ApprovalStatus { get; set; }
        public string BatchFileName { get; set; }
        public string PDFFileStatus { get; set; }
        public int page { get; set; }
        public int size { get; set; }

        //ADD AD
        public string EFakturStatus { get; set; }
        //END

        public dynamic MapFromModel(string DivisionName, string SortBy = "VO.BusinessUnit,VO.TanggalFaktur,VO.NPWPCustomer", string SortDirection = "ASC", int FromNumber = 1, int ToNumber = 10, string Id = "")
        {
            return new
            {
                SortBy = SortBy,
                SortDirection = SortDirection,
                TanggalFakturFrom = this.TanggalFakturFrom.FormatDefaultDateWhenNullOrEmpty(),
                TanggalFakturTo = this.TanggalFakturTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                TanggalFileDiterimaFrom = this.TanggalFileDiterimaFrom.FormatDefaultDateWhenNullOrEmpty(),
                TanggalFileDiterimaTo = this.TanggalFileDiterimaTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                WaktuFileDiterimaFrom = this.WaktuFileDiterimaFrom.FormatDefaultTimeWhenNullOrEmpty(),
                WaktuFileDiterimaTo = this.WaktuFileDiterimaTo.FormatDefaultTimeWhenNullOrEmpty("TO"),
                TanggalDownloadFrom = this.TanggalDownloadFrom.FormatDefaultDateWhenNullOrEmpty(),
                TanggalDownloadTo = this.TanggalDownloadTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                WaktuDownloadFrom = this.WaktuDownloadFrom.FormatDefaultTimeWhenNullOrEmpty(),
                WaktuDownloadTo = this.WaktuDownloadTo.FormatDefaultTimeWhenNullOrEmpty("TO"),
                RecordTimeFrom = this.RecordTimeFrom.FormatDefaultDateWhenNullOrEmpty(),
                RecordTimeTo = this.RecordTimeTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                ApprovalTimeFrom = this.ApprovalTimeFrom.FormatDefaultDateWhenNullOrEmpty(),
                ApprovalTimeTo = this.ApprovalTimeTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                TaxInvoiceStatus = this.TaxInvoiceStatus,
                TransitoryStatus = this.TransitoryStatus,
                TransitoryNumber = this.TransitoryNumber,
                TransitoryDateFrom = this.TransitoryDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                TransitoryDateTo = this.TransitoryDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                BusinessUnit = this.BusinessUnit,
                TransactionDataFile = this.TransactionDataFile,
                NomorFakturPajak = this.NomorFakturPajak,
                DANumber = this.DANumber,
                NPWPCustomer = this.NPWPCustomer,
                NamaCustomer = this.NamaCustomer,
                VATBaseAmount = this.VATBaseAmount,
                VATBaseAmountOperator = this.VATBaseAmountOperator,
                VATAmount = this.VATAmount,
                VATAmountOperator = this.VATAmountOperator,
                RecordStatus = this.RecordStatus,
                DownloadStatus = this.DownloadStatus,
                ApprovalStatus = this.ApprovalStatus,
                BatchFileName = this.BatchFileName,
                PDFFileStatus = this.PDFFileStatus,
                EFakturStatus = this.EFakturStatus,
                //EFakturStatus = EFakturStatus,
                DivisionName = DivisionName,
                FromNumber = FromNumber,
                ToNumber = ToNumber,
                
                Id = Id
            };
        }

    }
}