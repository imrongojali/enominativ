﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.VATOut
{
    public class VATOutReportExcelViewModel
    {
        public Guid VATOutId { get; set; }
        public String Field1 { get; set; }
        public String Field2 { get; set; }
        public String Field3 { get; set; }
        public String Field4 { get; set; }
        public String Field5 { get; set; }
        public String Field6 { get; set; }
        public String Field7 { get; set; }
        public String Field8 { get; set; }
        public String Field9 { get; set; }
        public String Field10 { get; set; }
        public String Field11 { get; set; }
        public String Field12 { get; set; }
        public String Field13 { get; set; }
        public String Field14 { get; set; }
        public String Field15 { get; set; }
        public String Field16 { get; set; }
        public String Field17 { get; set; }
        public String Field18 { get; set; }
        public String Field19 { get; set; }
        public String Field20 { get; set; }
        public String Field21 { get; set; }
        public String Field22 { get; set; }
        public String Field23 { get; set; }
        public String Field24 { get; set; }
    }
}