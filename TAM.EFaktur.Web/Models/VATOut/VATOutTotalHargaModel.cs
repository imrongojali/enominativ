﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.VATOut
{
    public class VATOutTotalHargaModel : BaseModel
    {
        public Guid Id { get; set; }
        public string NoFaktur { get; set; }
        public string DANumber { get; set; }
       
        public Decimal HargaTotal { get; set; }

        public Decimal JumlahDPP { get; set; }

        public Decimal JumlahPPN { get; set; }


    }


    }

