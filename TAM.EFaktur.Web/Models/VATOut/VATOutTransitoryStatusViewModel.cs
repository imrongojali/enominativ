﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.VATOut
{
    public class VATOutTransitoryStatusViewModel
    {
        public string NomorFakturGabungan { get; set; }
        public Nullable<DateTime> TransitoryDate { get; set; }
        public string TransitoryStatus { get; set; }
        public string TransitoryNumber { get; set; }

        public dynamic MapFromModel(DateTime EventDate, string EventActor)
        {
            return new
            {
                NomorFakturGabungan = NomorFakturGabungan,
                TransitoryDate = TransitoryDate,
                TransitoryStatus = TransitoryStatus,
                TransitoryNumber = TransitoryNumber,
                EventDate = EventDate,
                EventActor = EventActor

            };

        }
    }
}