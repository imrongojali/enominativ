﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using TAM.EFaktur.Web.Models.SyncData;
using TAM.EFaktur.Web.Models.Log_Sync;
using System.Globalization;
using NPOI.SS.Formula.Functions;
using System.Xml;
using NPOI.HSSF.EventUserModel.DummyRecord;
using System.Runtime.InteropServices;
using Microsoft.Ajax.Utilities;
using TAM.EFaktur.Web.Models.Master_Config;
using NPOI.XSSF.UserModel.Helpers;
using TAM.EFaktur.Web.Controllers;
using Dapper;
using System.Data;

namespace TAM.EFaktur.Web.Models.VATOut
{
    public class VATOutRepository : BaseRepository
    {
        #region Singleton
        private VATOutRepository() { }
        private static VATOutRepository instance = null;
        public static VATOutRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new VATOutRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(VATOutDashboardSearchParamViewModel model, string DivisionName, int page, int size, string Id)
        {
            try
            {
                return db.SingleOrDefault<int>("VATOut/usp_CountVATOutListDashboard", model.MapFromModel(DivisionName, null, null, page, size, Id));
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Processing Data
        public List<Guid> GetIdBySearchParam(VATOutDashboardSearchParamViewModel model, string DivisionName)
        {
            IEnumerable<Guid> result = db.Query<Guid>("VATOut/usp_GetVATOutIdBySearchParam", model.MapFromModel(DivisionName));
            return result.ToList();
        }

        public List<VATOutReportExcelViewModel> GetReportExcelById(List<Guid> VATOutIdList)
        {
            dynamic args = new
            {
                VATOutIdList = string.Join(";", VATOutIdList)
            };
            IEnumerable<VATOutReportExcelViewModel> result = db.Query<VATOutReportExcelViewModel>("VATOut/usp_GetVATOutReportExcelById", args);
            return result.ToList();
        }

        //public List<VATOutReportExcelViewModel> GetReportExcelAllByIdSearchParam(VATOutDashboardSearchParamViewModel model, string DivisionName)
        //{
        //    IEnumerable<VATOutReportExcelViewModel> result = db.Query<VATOutReportExcelViewModel>("VATOut/usp_GetVATOutReportExcelAllBySearchParam", model.MapFromModel(DivisionName));
        //    return result.ToList();
        //}

        public List<VATOutCSVViewModel> GetCSVById(List<Guid> VATOutIdList)
        {
            dynamic args = new
            {
                VATOutIdList = string.Join(";", VATOutIdList)
            };
            IEnumerable<VATOutCSVViewModel> result = db.Query<VATOutCSVViewModel>("VATOut/usp_GetVATOutCSVById", args);
            return result.ToList();
        }

        public int GetCountCSVById(List<Guid> VATOutIdList)
        {
            dynamic args = new
            {
                VATOutIdList = string.Join(";", VATOutIdList)
            };
            return db.SingleOrDefault<int>("VATOut/usp_GetCountVATOutCSVById", args);
        }

        public List<VATOutOriginalViewModel> GetOriginalById(List<Guid> VATOutIdList)
        {
            dynamic args = new
            {
                VATOutIdList = string.Join(";", VATOutIdList)
            };
            IEnumerable<VATOutOriginalViewModel> result = db.Query<VATOutOriginalViewModel>("VATOut/usp_GetVATOutOriginalFileById", args);
            return result.ToList();
        }

        public Result UpdateFlagCSV(List<Guid> VATOutIdList, string FileUrl, string EventActor)
        {
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    VATOutIdList = string.Join(";", VATOutIdList),
                    BatchFileName = Path.GetFileName(@FileUrl)
                };
                db.Execute("VATOut/usp_UpdateCustom_TB_R_VATOut_FlagCSV", args);

                args = new
                {
                    Id = Guid.NewGuid(),
                    CsvType = "FM",
                    CsvTime = DateTime.Now.FormatSQLDateTime(),
                    BatchFileName = Path.GetFileName(@FileUrl),
                    FileUrl = FileUrl,
                    EventDate = DateTime.Now.FormatSQLDateTime(),
                    EventActor = EventActor
                };
                db.Execute("CSVData/usp_InsertCustom_TB_R_CsvData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                throw e;
            }

            return MsgResultSuccessInsert();
        }

        public List<string> GetPDFUrlById(List<Guid> VATOutIdList)
        {
            dynamic args = new
            {
                VATOutIdList = string.Join(";", VATOutIdList)
            };
            IEnumerable<string> result = db.Query<string>("VATOut/usp_GetVATOutPDFUrlById", args);
            return result.ToList();
        }

        public List<VATOutDashboardViewModel> GetList(VATOutDashboardSearchParamViewModel model, string DivisionName, string SortBy, string SortDirection, int FromNumber, int ToNumber, string Id)
        {
            IEnumerable<VATOutDashboardViewModel> result = db.Query<VATOutDashboardViewModel>("VATOut/usp_GetVATOutListDashboard", model.MapFromModel(DivisionName, SortBy, SortDirection, FromNumber, ToNumber, Id));
            return result.ToList();
        }

        public VATOut GetById(Guid Id)
        {
            return db.SingleOrDefault<VATOut>("VATOut/usp_GetVATOutByID", new { Id = Id });
        }

        public Result Save(VATOut objVATOut, DateTime EventDate, string EventActor)
        {
            try
            {
                db.Execute("VATOut/usp_Insert_TB_R_VATOut", objVATOut.MapFromModel(EventDate, EventActor));
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.LogToApp("Save VATOut", MessageType.ERR, EventActor).Message);
            }
        }

        public Result Delete(Guid Id, string EventActor)
        {
            try
            {
                db.Execute("VATOut/usp_DeleteVATOutByID", new { Id = Id });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.LogToApp("Delete VATOut", MessageType.ERR, EventActor).Message);
            }
        }
        //add by DA 25112020
        public void DeleteVatOutSales(Guid Id)
        {
            dynamic args = new
            {
                Id = Id
            };

            db.Execute("VATOut/usp_DeleteVATOutByID", args);


        }

        public Result ExcelTaxInvoiceInsert(string path, string EventActor)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File has been uploaded successfully"
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                VATOut vatOut = new VATOut();
                var cellNum = 32; // 31 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();

                string businessUnit = sheet.GetRow(4).GetCell(1).StringCellValue.ToString().Trim(); //first row

                #region Master - SyncData
                db.BeginTransaction();
                Guid SyncId = Guid.NewGuid(); //generate new Guid for Id - SyncData, to be referenced di VAT dan SyncDataDetail

                dynamic argsSyncData = new SyncDataViewModel() //insert SyncData paling pertama, karena dibutuhkan sebagai reference untuk data VAT dan SyncDataDetail
                    .CreateModel("FK", businessUnit, path, Path.GetFileName(@path))
                    .MapFromModelCreate(SyncId, DateTime.Now, EventActor);

                db.Execute("SyncData/usp_Insert_TB_R_SyncData", argsSyncData);

                List<dynamic> argsSyncDataDetailList = new List<dynamic>(); //Declare list di awal untuk syncDataDetail agar menghemat performance
                db.CommitTransaction();
                #endregion

                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();
                List<string> failNomorFaktur = new List<string>();

                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index number 4 on excel template
                {


                    #region Set Cell Policy, Cell Type and Save Sync Data
                    pipedString = string.Empty;
                    for (int i = 0; i < cellNum; i++)
                    {
                        try
                        {
                            sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                            pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                        }
                        catch (NullReferenceException)
                        {

                            throw new Exception("Row " + (row + 1) + " cannot blank, please delete empty row");
                        }
                    }
                    pipedString = pipedString.Remove(pipedString.Length - 1);
                    #endregion

                    try
                    {
                        #region Detail - SyncDataDetail Populate List
                        argsSyncDataDetailList.Add(new SyncDataDetailViewModel().CreateModel(row - 3, pipedString).MapFromModelCreate(SyncId, DateTime.Now, EventActor));
                        #endregion

                        if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        {
                            #region Validasi jika nomor faktur ini sudah terjadi fail di row sebelumnya
                            if (failNomorFaktur.Contains(sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim()))
                            {
                                throw new Exception(ExceptionHandler.PREVIOUS_ROW_FAIL_MESSAGE);
                            }
                            #endregion

                            #region Cell Mapping & Insert to DB
                            if (vatOut.NomorFaktur != sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim())
                            {

                                if (!string.IsNullOrEmpty(vatOut.NomorFaktur))
                                {
                                    if (failNomorFaktur.Contains(vatOut.NomorFaktur))
                                    {
                                        db.AbortTransaction();
                                    }
                                    else
                                    {
                                        db.CommitTransaction();
                                    }
                                }

                                db.BeginTransaction();
                                vatOut = new VATOut();
                                vatOut.Id = Guid.NewGuid();
                                vatOut.SyncId = SyncId;
                                vatOut.DANumber = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                                vatOut.BusinessUnit = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                                vatOut.KDJenisTransaksi = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim();
                                vatOut.FGPengganti = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();
                                vatOut.NomorFaktur = sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim();
                                vatOut.NomorFakturGabungan = vatOut.NomorFaktur.FormatNomorFakturGabungan(vatOut.KDJenisTransaksi, vatOut.FGPengganti);
                                vatOut.MasaPajak = Convert.ToInt32(sheet.GetRow(row).GetCell(6).StringCellValue.ToString().Trim());
                                vatOut.TahunPajak = Convert.ToInt32(sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim());

                                var tgl_faktur_split = sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim().Split('/');
                                if (tgl_faktur_split.Count() > 1) // when the excel column is a text type
                                {
                                    vatOut.TanggalFaktur = DateTime.ParseExact(
                                        sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                else // when the excel column is a DateTime type
                                {
                                    vatOut.TanggalFaktur = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim()));
                                }

                                vatOut.NPWPCustomer = sheet.GetRow(row).GetCell(9).StringCellValue.ToString().Trim().FormatNPWP();
                                vatOut.NamaCustomer = sheet.GetRow(row).GetCell(10).StringCellValue.ToString().Trim();
                                vatOut.AlamatLawanTransaksi = sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim();
                                vatOut.JumlahDPP = Decimal.Parse(sheet.GetRow(row).GetCell(12).StringCellValue.ToString().Trim());
                                string jmlDPP = vatOut.JumlahDPP.ToString();
                                if ((jmlDPP.Contains(".") || (jmlDPP.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                vatOut.JumlahPPN = Decimal.Parse(sheet.GetRow(row).GetCell(13).StringCellValue.ToString().Trim());
                                string jmlPPN = vatOut.JumlahPPN.ToString();
                                if ((jmlPPN.Contains(".") || (jmlPPN.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                vatOut.JumlahPPNBM = Decimal.Parse(sheet.GetRow(row).GetCell(14).StringCellValue.ToString().Trim());
                                string jmlPPNBM = vatOut.JumlahPPNBM.ToString();
                                if ((jmlPPNBM.Contains(".") || (jmlPPNBM.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                vatOut.IDKeteranganTambahan = sheet.GetRow(row).GetCell(15).StringCellValue.ToString().Trim();
                                vatOut.FGUangMuka = Decimal.Parse(sheet.GetRow(row).GetCell(16).StringCellValue.ToString().Trim());
                                vatOut.UangMukaDPP = Decimal.Parse(sheet.GetRow(row).GetCell(17).StringCellValue.ToString().Trim());
                                string MukaDPP = vatOut.UangMukaDPP.ToString();
                                if ((MukaDPP.Contains(".") || (MukaDPP.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                vatOut.UangMukaPPN = Decimal.Parse(sheet.GetRow(row).GetCell(18).StringCellValue.ToString().Trim());
                                string MukaPPN = vatOut.UangMukaPPN.ToString();
                                if ((MukaPPN.Contains(".") || (MukaPPN.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                vatOut.UangMukaPPNBM = Decimal.Parse(sheet.GetRow(row).GetCell(19).StringCellValue.ToString().Trim());
                                string mukaPPNBM = vatOut.UangMukaPPNBM.ToString();
                                if ((mukaPPNBM.Contains(".") || (mukaPPNBM.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                vatOut.Referensi = sheet.GetRow(row).GetCell(20).StringCellValue.ToString().Trim();
                                vatOut.CreatedBy = EventActor;
                                vatOut.CreatedOn = DateTime.Now;
                                vatOut.RowStatus = false;

                                // Save VAT Out and return result of execution
                                result = db.SingleOrDefault<Result>("VATOut/usp_Insert_TB_R_VATOut", vatOut.MapFromModel(DateTime.Now, EventActor));
                                if (!result.ResultCode)
                                {
                                    throw new Exception(result.ResultDesc);
                                }
                            }

                            VATOutDetail vatOutDetail = new VATOutDetail();
                            vatOutDetail.Id = Guid.NewGuid();
                            vatOutDetail.VATOutId = vatOut.Id;
                            vatOutDetail.KodeObjek = sheet.GetRow(row).GetCell(22).StringCellValue.ToString().Trim();
                            vatOutDetail.NamaObjek = sheet.GetRow(row).GetCell(23).StringCellValue.ToString().Trim();
                            vatOutDetail.HargaSatuan = Decimal.Parse(sheet.GetRow(row).GetCell(24).StringCellValue.ToString().Trim());
                            vatOutDetail.JumlahBarang = Decimal.Parse(sheet.GetRow(row).GetCell(25).StringCellValue.ToString().Trim());
                            vatOutDetail.HargaTotal = Decimal.Parse(sheet.GetRow(row).GetCell(26).StringCellValue.ToString().Trim());
                            vatOutDetail.Diskon = Decimal.Parse(sheet.GetRow(row).GetCell(27).StringCellValue.ToString().Trim());
                            vatOutDetail.DPP = Decimal.Parse(sheet.GetRow(row).GetCell(28).StringCellValue.ToString().Trim());
                            vatOutDetail.PPN = Decimal.Parse(sheet.GetRow(row).GetCell(29).StringCellValue.ToString().Trim());
                            vatOutDetail.TarifPPNBM = Decimal.Parse(sheet.GetRow(row).GetCell(30).StringCellValue.ToString().Trim());
                            vatOutDetail.PPNBM = Decimal.Parse(sheet.GetRow(row).GetCell(31).StringCellValue.ToString().Trim());
                            vatOutDetail.Status = "00"; // Default Value
                            vatOutDetail.CreatedBy = EventActor;
                            vatOutDetail.CreatedOn = DateTime.Now;
                            vatOutDetail.RowStatus = false;

                            // Save VAT Out Detail
                            db.Execute("VATOut/usp_Insert_TB_R_VATOutDetail", vatOutDetail.MapFromModel());

                            logSyncList.Add(new LogSyncCreateUpdate()
                                .CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.INF, ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path));
                            rowSuccess++;
                            #endregion
                        }
                        else
                        {
                            throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE); //jika mandatory field ada yg blank
                        }
                    }
                    catch (Exception e)
                    {
                        if (!failNomorFaktur.Contains(sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim())) //kolom 5 = Nomor Faktur
                        {
                            failNomorFaktur.Add(sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim()); //untuk mencatat no faktur yang error
                        }
                        logSyncList.Add(new LogSyncCreateUpdate().CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.ERR, e.Message, path));
                        rowFail++;
                    }
                }


                #region execute batch terakhir dari VAT
                if (failNomorFaktur.Contains(vatOut.NomorFaktur))
                {
                    db.AbortTransaction();
                }
                else
                {
                    db.CommitTransaction();
                }
                #endregion

                #region SyncDataDetail Insert
                db.BeginTransaction();
                foreach (dynamic argsSyncDataDetail in argsSyncDataDetailList)
                {
                    db.Execute("SyncData/usp_Insert_TB_R_SyncDataDetail", argsSyncDataDetail);
                }
                db.CommitTransaction();
                #endregion

                #region SyncLog Insert
                db.BeginTransaction();
                ExceptionHandler.LogToSync(logSyncList);
                db.CommitTransaction();
                #endregion

                result.ResultCode = true;
                result.ResultDesc = Formatting
                    .UPLOAD_NOTIFICATION
                    .Replace("@success", (rowSuccess).ToString())
                    .Replace("@error", (rowFail).ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        //add 14102020 DA, upload tax data motor vehicle

        public Result ExcelTaxInvoiceInsertMotorVehicle(string path, string EventActor)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File has been uploaded successfully"
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");


            string pipedString = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;
            int rowFailedinvalidation = 0;


            try
            {
                file.Close();
                VATOut vatOut = new VATOut();
                List<VATOutPDFViewModel> vatOutPDFViewModel = new List<VATOutPDFViewModel>();
                //list untuk nampung nilai total harga agar disimpan dan di cek lgsg per row di bagian master, karena dimaster cuma save data sekali aja ketika no fakturnya sama
                List<VATOutTotalHargaModel> vATOutTotalHargaModel = new List<VATOutTotalHargaModel>();
                //utk save ke list agar bisa disave setelah validasi
                List<VATOutDetail> ListVATOutDetail = new List<VATOutDetail>();
                List<VATOut> ListVATOut = new List<VATOut>();

                var cellNum = 36;
                List<int> exceptionCellNum = new List<int>();

                string businessUnit = sheet.GetRow(4).GetCell(1).StringCellValue.ToString().Trim(); //first row   


                #region Master - SyncData
                db.BeginTransaction();
                Guid SyncId = Guid.NewGuid(); //generate new Guid for Id - SyncData, to be referenced di VAT dan SyncDataDetail

                dynamic argsSyncData = new SyncDataViewModel() //insert SyncData paling pertama, karena dibutuhkan sebagai reference untuk data VAT dan SyncDataDetail
                    .CreateModel("FK", businessUnit, path, Path.GetFileName(@path))
                    .MapFromModelCreate(SyncId, DateTime.Now, EventActor);

                db.Execute("SyncData/usp_Insert_TB_R_SyncData", argsSyncData);

                List<dynamic> argsSyncDataDetailList = new List<dynamic>(); //Declare list di awal untuk syncDataDetail agar menghemat performance
                db.CommitTransaction();
                #endregion

                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();
                List<string> failNomorFaktur = new List<string>();


                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index number 4 on excel template
                {

                    #region Set Cell Policy, Cell Type and Save Sync Data
                    pipedString = string.Empty;
                    for (int i = 0; i < cellNum; i++)
                    {
                        try
                        {


                            sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                            pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                        }
                        catch (NullReferenceException)
                        {

                            throw new Exception("Row " + (row + 1) + " cannot blank, please delete empty row");
                        }

                    }
                    pipedString = pipedString.Remove(pipedString.Length - 1);
                    #endregion



                    try
                    {

                        #region Detail - SyncDataDetail Populate List
                        argsSyncDataDetailList.Add(new SyncDataDetailViewModel().CreateModel(row - 3, pipedString).MapFromModelCreate(SyncId, DateTime.Now, EventActor));
                        #endregion

                        if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        {
                            #region Validasi jika nomor faktur ini sudah terjadi fail di row sebelumnya
                            if (failNomorFaktur.Contains(sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim()))
                            {
                                throw new Exception(ExceptionHandler.PREVIOUS_ROW_FAIL_MESSAGE);
                            }
                            #endregion

                            //utk pdf
                            VATOutPDFViewModel DataCreatePdf = new VATOutPDFViewModel();
                            //utk validasi
                            VATOutTotalHargaModel vAtOutTotalHargaValidasi = new VATOutTotalHargaModel();


                            #region Cell Mapping & Insert to DB
                            if (vatOut.NomorFaktur != sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim())
                            {

                                if (!string.IsNullOrEmpty(vatOut.NomorFaktur))
                                {
                                    if (failNomorFaktur.Contains(vatOut.NomorFaktur))
                                    {
                                        db.AbortTransaction();
                                    }
                                    else
                                    {
                                        db.CommitTransaction();
                                    }
                                }

                                #region insert
                                db.BeginTransaction();
                                vatOut = new VATOut();
                                vatOut.Id = Guid.NewGuid();
                                vatOut.SyncId = SyncId;
                                vatOut.DANumber = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                                vatOut.BusinessUnit = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                                vatOut.KDJenisTransaksi = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim();
                                vatOut.FGPengganti = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();
                                vatOut.NomorFaktur = sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim();
                                vatOut.NomorFakturGabungan = vatOut.NomorFaktur.FormatNomorFakturGabungan(vatOut.KDJenisTransaksi, vatOut.FGPengganti);
                                vatOut.MasaPajak = Convert.ToInt32(sheet.GetRow(row).GetCell(6).StringCellValue.ToString().Trim());
                                vatOut.TahunPajak = Convert.ToInt32(sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim());

                                var tgl_faktur_split = sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim().Split('/');
                                if (tgl_faktur_split.Count() > 1) // when the excel column is a text type
                                {
                                    vatOut.TanggalFaktur = DateTime.ParseExact(
                                        sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                else // when the excel column is a DateTime type
                                {
                                    vatOut.TanggalFaktur = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim()));
                                }

                                vatOut.NPWPCustomer = sheet.GetRow(row).GetCell(9).StringCellValue.ToString().Trim().FormatNPWP();
                                vatOut.NamaCustomer = sheet.GetRow(row).GetCell(10).StringCellValue.ToString().Trim();
                                vatOut.AlamatLawanTransaksi = sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim();
                                vatOut.JumlahDPP = Decimal.Parse(sheet.GetRow(row).GetCell(12).StringCellValue.ToString().Trim());
                                string jmlDPP = vatOut.JumlahDPP.ToString();
                                if ((jmlDPP.Contains(".") || (jmlDPP.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }


                                vatOut.JumlahPPN = Decimal.Parse(sheet.GetRow(row).GetCell(13).StringCellValue.ToString().Trim());
                                string jmlPPN = vatOut.JumlahPPN.ToString();
                                if ((jmlPPN.Contains(".") || (jmlPPN.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }

                                vatOut.JumlahPPNBM = Decimal.Parse(sheet.GetRow(row).GetCell(14).StringCellValue.ToString().Trim());
                                string jmlPPNBM = vatOut.JumlahPPNBM.ToString();
                                if ((jmlPPNBM.Contains(".") || (jmlPPNBM.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                vatOut.IDKeteranganTambahan = sheet.GetRow(row).GetCell(15).StringCellValue.ToString().Trim();
                                vatOut.FGUangMuka = Decimal.Parse(sheet.GetRow(row).GetCell(16).StringCellValue.ToString().Trim());
                                vatOut.UangMukaDPP = Decimal.Parse(sheet.GetRow(row).GetCell(17).StringCellValue.ToString().Trim());
                                string MukaDPP = vatOut.UangMukaDPP.ToString();
                                if ((MukaDPP.Contains(".") || (MukaDPP.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                vatOut.UangMukaPPN = Decimal.Parse(sheet.GetRow(row).GetCell(18).StringCellValue.ToString().Trim());
                                string MukaPPN = vatOut.UangMukaPPN.ToString();
                                if ((MukaPPN.Contains(".") || (MukaPPN.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                vatOut.UangMukaPPNBM = Decimal.Parse(sheet.GetRow(row).GetCell(19).StringCellValue.ToString().Trim());
                                string mukaPPNBM = vatOut.UangMukaPPNBM.ToString();
                                if ((mukaPPNBM.Contains(".") || (mukaPPNBM.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                vatOut.Referensi = sheet.GetRow(row).GetCell(20).StringCellValue.ToString().Trim();
                                vatOut.CreatedBy = EventActor;
                                vatOut.CreatedOn = DateTime.Now;
                                vatOut.RowStatus = false;
                                //copy ke list 
                                ListVATOut.Add(vatOut);


                                // Save VAT Out and return result of execution
                                //result = db.SingleOrDefault<Result>("VATOut/usp_Insert_TB_R_VATOut", vatOut.MapFromModel(DateTime.Now, EventActor));
                                //if (!result.ResultCode)
                                //{
                                //    throw new Exception(result.ResultDesc);
                                //}

                            }


                            VATOutDetail vatOutDetail = new VATOutDetail();
                            //utk validasi
                            VATOut vatOut2 = new VATOut();
                            //
                            vatOutDetail.Id = Guid.NewGuid();
                            vatOutDetail.VATOutId = vatOut.Id;
                            vatOutDetail.KodeObjek = sheet.GetRow(row).GetCell(22).StringCellValue.ToString().Trim();
                            vatOutDetail.NamaObjek = sheet.GetRow(row).GetCell(23).StringCellValue.ToString().Trim();
                            //add field tambahan
                            vatOutDetail.FrameNo = sheet.GetRow(row).GetCell(24).StringCellValue.ToString().Trim();
                            vatOutDetail.EngineNo = sheet.GetRow(row).GetCell(25).StringCellValue.ToString().Trim();
                            vatOutDetail.ModelType = sheet.GetRow(row).GetCell(26).StringCellValue.ToString().Trim();
                            //

                            vatOutDetail.HargaSatuan = Decimal.Parse(sheet.GetRow(row).GetCell(27).StringCellValue.ToString().Trim());
                            string _HargaSatuan = Convert.ToString(vatOutDetail.HargaSatuan);
                            if (_HargaSatuan.Contains(","))//String contain " . or , "
                            {
                                throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                            }
                            vatOutDetail.JumlahBarang = Decimal.Parse(sheet.GetRow(row).GetCell(28).StringCellValue.ToString().Trim());

                            string _JumlahBarang = Convert.ToString(vatOutDetail.JumlahBarang);
                            if (_JumlahBarang.Contains(","))//String contain " . or , "
                            {
                                throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                            }

                            vatOutDetail.HargaTotal = Decimal.Parse(sheet.GetRow(row).GetCell(29).StringCellValue.ToString().Trim());
                            string _HargaTotal = Convert.ToString(vatOutDetail.HargaTotal);
                            if (_HargaTotal.Contains(","))//String contain " . or , "
                            {
                                throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                            }

                            vatOutDetail.Diskon = Decimal.Parse(sheet.GetRow(row).GetCell(30).StringCellValue.ToString().Trim());
                            string _Diskon = Convert.ToString(vatOutDetail.Diskon);
                            if ((_Diskon.Contains(",")))//String contain " . or , "
                            {
                                throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                            }

                            vatOutDetail.DPP = Decimal.Parse(sheet.GetRow(row).GetCell(31).StringCellValue.ToString().Trim());
                            string _DPP = Convert.ToString(vatOutDetail.DPP);
                            if (_DPP.Contains(","))//String contain " . or , "
                            {
                                throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                            }

                            vatOutDetail.PPN = Decimal.Parse(sheet.GetRow(row).GetCell(32).StringCellValue.ToString().Trim());
                            string _PPN = Convert.ToString(vatOutDetail.PPN);
                            if (_PPN.Contains(","))//String contain " . or , "
                            {
                                throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                            }

                            vatOutDetail.TarifPPNBM = Decimal.Parse(sheet.GetRow(row).GetCell(33).StringCellValue.ToString().Trim());
                            string _TarifPPNBM = Convert.ToString(vatOutDetail.TarifPPNBM);
                            if (_TarifPPNBM.Contains(","))//String contain " . or , "
                            {
                                throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                            }

                            vatOutDetail.PPNBM = Decimal.Parse(sheet.GetRow(row).GetCell(34).StringCellValue.ToString().Trim());
                            string _PPNBM = Convert.ToString(vatOutDetail.PPNBM);
                            if (_PPNBM.Contains(","))//String contain " . or , "
                            {
                                throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                            }
                            //field tambahan
                            vatOutDetail.PPNBMLINI = Decimal.Parse(sheet.GetRow(row).GetCell(35).StringCellValue.ToString().Trim());
                            string _PPNBMLINI = Convert.ToString(vatOutDetail.PPNBMLINI);
                            if ((_PPNBMLINI.Contains(".") || (_PPNBMLINI.Contains(","))))//String contain " . or , "
                            {
                                throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                            }


                            vatOutDetail.Status = "00"; // Default Value
                            vatOutDetail.CreatedBy = EventActor;
                            vatOutDetail.CreatedOn = DateTime.Now;
                            vatOutDetail.RowStatus = false;

                            //save data ke new model biar bisa disave setelah validasi
                            ListVATOutDetail.Add(vatOutDetail);
                            //

                            #region Tampung ke list utk create PDF
                            //tambahin field nya yang baru ke list utk create pdf
                            DataCreatePdf.NomorFaktur = vatOut.NomorFaktur;
                            DataCreatePdf.BusinessUnit = vatOut.BusinessUnit;
                            DataCreatePdf.DANumber = vatOut.DANumber;
                            DataCreatePdf.NomorFakturGabungan = vatOut.NomorFakturGabungan;
                            DataCreatePdf.TanggalFaktur = vatOut.TanggalFaktur;
                            DataCreatePdf.NamaCustomer = vatOut.NamaCustomer;
                            DataCreatePdf.AlamatLawanTransaksi = vatOut.AlamatLawanTransaksi;
                            DataCreatePdf.NPWPCustomer = vatOut.NPWPCustomer;
                            DataCreatePdf.JumlahDPP = vatOut.JumlahDPP;
                            DataCreatePdf.JumlahPPN = vatOut.JumlahPPN;
                            DataCreatePdf.KodeObjek = vatOutDetail.KodeObjek;
                            DataCreatePdf.NamaObjek = vatOutDetail.NamaObjek;
                            DataCreatePdf.JumlahBarang = vatOutDetail.JumlahBarang;
                            DataCreatePdf.HargaSatuan = vatOutDetail.HargaSatuan;
                            DataCreatePdf.HargaTotal = vatOutDetail.HargaTotal;
                            vatOutPDFViewModel.Add(DataCreatePdf);
                            #endregion


                            //tambahin field ke list untuk cek validasi
                            vatOut2.NomorFaktur = sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim();
                            vatOut2.DANumber = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                            vatOut2.JumlahDPP = Decimal.Parse(sheet.GetRow(row).GetCell(12).StringCellValue.ToString().Trim());
                            vatOut2.JumlahPPN = Decimal.Parse(sheet.GetRow(row).GetCell(13).StringCellValue.ToString().Trim());

                            //
                            vAtOutTotalHargaValidasi.NoFaktur = vatOut2.NomorFaktur;
                            vAtOutTotalHargaValidasi.DANumber = vatOut2.DANumber;
                            vAtOutTotalHargaValidasi.JumlahDPP = vatOut2.JumlahDPP;
                            vAtOutTotalHargaValidasi.JumlahPPN = vatOut2.JumlahPPN;
                            vAtOutTotalHargaValidasi.HargaTotal = vatOutDetail.HargaTotal;
                            vATOutTotalHargaModel.Add(vAtOutTotalHargaValidasi);

                            // Save VAT Out Detail

                            //db.Execute("VATOut/usp_Insert_TB_R_VATOutDetail", vatOutDetail.MapFromModel());

                            //ADD By AD 20210315

                            #endregion
                            result = db.SingleOrDefault<Result>("VATOut/usp_Validation_TB_R_VATOut", vatOut.MapFromModel(DateTime.Now, EventActor));
                            if (!result.ResultCode)
                            {
                                throw new Exception(result.ResultDesc);
                            }
                            else
                            {
                                //ExceptionHandler.LogToSync(row - 3, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE, MessageType.INF);
                                //rowSuccess++;
                                logSyncList.Add(new LogSyncCreateUpdate()
                                .CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.INF, ExceptionHandler.GENERAL_SUCCESS_MESSAGE_Validation, path));
                                rowSuccess++;
                            }
                            //logSyncList.Add(new LogSyncCreateUpdate()
                            //.CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.INF, ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path));
                            //rowSuccess++;

                            #endregion
                            //END

                        }
                        else
                        {
                            throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE); //jika mandatory field ada yg blank
                        }

                    }
                    catch (Exception e)
                    {

                        if (!failNomorFaktur.Contains(sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim())) //kolom 5 = Nomor Faktur
                        {
                            failNomorFaktur.Add(sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim()); //untuk mencatat no faktur yang error
                        }
                        logSyncList.Add(new LogSyncCreateUpdate().CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.ERR, e.Message, path));
                        rowFail++;
                    }


                }
                #region validasi  harga total, JumlahDPP, JumlahPPN

                if (rowFail == 0)
                {

                    int ii = 0;
                    List<VATOutTotalHargaModel> GetDataValidasi = vATOutTotalHargaModel;

                    //di group by dlu berdasarkan  No Faktur, karena 1 DA number bisa 2 No Faktur
                    var GetDataGroupbyDANumber = GetDataValidasi
                   .GroupBy(x => x.NoFaktur)
                   .Select(y => y.ToList())
                   .ToList();

                    pipedString = string.Empty;

                    foreach (var SumTotalHarga in GetDataGroupbyDANumber)
                    {


                        //select Harga total per DA Number
                        var SumTotalperDaNumber = (from xx in SumTotalHarga.AsQueryable()
                                                   group xx by xx.DANumber into g
                                                   from xx in g
                                                   select xx.HargaTotal).Sum();

                        //select Jumlah DPP
                        var GetJumlahDPP = (from xx in SumTotalHarga.AsQueryable()
                                            group xx by xx.DANumber into g
                                            from xx in g
                                            select xx.JumlahDPP).ToList();

                        //select Jumlah PPN
                        var GetJumlahPPN = (from xx in SumTotalHarga.AsQueryable()
                                            group xx by xx.DANumber into g
                                            from xx in g
                                            select xx.JumlahPPN).ToList();



                        //di cek per row di group by nya
                        for (int i = 0; i < SumTotalHarga.Count; i++)
                        {
                            try
                            {
                                ii++;
                                var RowDPP = GetJumlahDPP[i];

                                if (RowDPP != SumTotalperDaNumber)
                                {

                                    throw new Exception(ExceptionHandler.DPP_PPN_FAIL_MESSAGE);

                                }
                                var RowPPN = GetJumlahPPN[i];
                                var RowDPPConvert = Convert.ToInt64(RowDPP);
                                var RowPPNConvert = Convert.ToInt64(RowPPN);
                                var TotalPPN = Math.Round(0.1 * RowDPPConvert);
                                if (RowPPNConvert != TotalPPN)
                                {
                                    throw new Exception(ExceptionHandler.DPP_PPN_FAIL_MESSAGE);

                                }

                            }

                            catch (Exception e)
                            {
                                pipedString = argsSyncDataDetailList[ii - 1].RowData.ToString();
                                logSyncList.Add(new LogSyncCreateUpdate().CreateModel(Path.GetFileName(path), ii, pipedString, MessageType.ERR, e.Message, path));
                                rowFailedinvalidation++;
                            }


                        }

                    }

                    if (rowFailedinvalidation == 0)
                    {
                        //eksekusi data ke master vatout dan tabel detail barang
                        for (int i = 0; i < ListVATOut.Count(); i++)
                        {
                            var datamaster = ListVATOut[i];
                            result = db.SingleOrDefault<Result>("VATOut/usp_Insert_TB_R_VATOut", datamaster.MapFromModel(DateTime.Now, EventActor));
                            if (!result.ResultCode)
                            {
                                throw new Exception(result.ResultDesc);
                            }
                            else
                            {
                                //ExceptionHandler.LogToSync(row - 3, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE, MessageType.INF);
                                //rowSuccess++;
                                logSyncList.Add(new LogSyncCreateUpdate()
                                .CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.INF, ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path));
                            }
                        }
                        for (int i = 0; i < ListVATOutDetail.Count(); i++)
                        {
                            var datadetail = ListVATOutDetail[i];
                            db.Execute("VATOut/usp_Insert_TB_R_VATOutDetail", datadetail.MapFromModel());
                        }
                    }

                }


                #endregion




                #region execute batch terakhir dari VAT
                if (failNomorFaktur.Contains(vatOut.NomorFaktur))
                {
                    db.AbortTransaction();
                }
                else
                {
                    db.CommitTransaction();
                }
                #endregion

                #region SyncDataDetail Insert
                db.BeginTransaction();
                foreach (dynamic argsSyncDataDetail in argsSyncDataDetailList)
                {
                    db.Execute("SyncData/usp_Insert_TB_R_SyncDataDetail", argsSyncDataDetail);
                }
                db.CommitTransaction();
                #endregion

                #region SyncLog Insert
                db.BeginTransaction();
                ExceptionHandler.LogToSync(logSyncList);
                db.CommitTransaction();
                #endregion

                #region Create PDF VAT Out Motor Vehicle / Lexus Motor Vehicle

                if (rowFail == 0 && rowFailedinvalidation == 0)
                {
                    List<VATOutPDFViewModel> DataPDFModel = vatOutPDFViewModel;
                    string fullPath = string.Empty;
                    //buat group per faktur utk create pdf
                    var groupFakturList = DataPDFModel
                    .GroupBy(x => x.NomorFaktur)
                    .Select(y => y.ToList())
                    .ToList();

                    foreach (var datapdfgroup in groupFakturList)
                    {
                        if (datapdfgroup != null)
                        {
                            var getnofakturlist = (from d in datapdfgroup.AsQueryable()
                                                   group d by d.NomorFaktur into g
                                                   from d in g
                                                   select d.NomorFaktur)
                                          .ToList();

                            var NoFaktur = getnofakturlist[0];
                            var charsToRemove = new string[] { "/", "-" };
                            foreach (var e in charsToRemove)
                            {
                                NoFaktur = NoFaktur.Replace(e, string.Empty);
                            }
                            var fileName = Path.GetFileName(NoFaktur);
                            var Pdffilename = fileName;

                            //create list for pdf------------------------------//--------------------------------------------------

                            List<string[]> ListArr = new List<string[]>();

                            var obj = datapdfgroup.First();
                            //1
                            String[] headerDANumber = new string[]
                           {
                        "Nomor: " + obj.DANumber
                           }; //for header
                            ListArr.Add(headerDANumber);

                            //2
                            String[] headernofaktur = new string[]
                            {
                        "(Atas Faktur Pajak Nomor: " + obj.NomorFakturGabungan + "  "  + " Tanggal: " + obj.TanggalFaktur.FormatViewDate() +")"
                            }; //for header
                            ListArr.Add(headernofaktur);


                            //3
                            String[] customer = new string[]
                            {
                        ": " + obj.NamaCustomer
                            }; //for header
                            ListArr.Add(customer);

                            //4
                            String[] alamatcustomer = new string[]
                            {
                        ": " + obj.AlamatLawanTransaksi
                            }; //for header
                            ListArr.Add(alamatcustomer);



                            //5
                            String[] headerNPWP = new string[]
                            {
                        ": " + obj.NPWPCustomer
                            }; //for header
                            ListArr.Add(headerNPWP);


                            //Jumlah BKP, 6
                            String[] Rowbawah = new string[]
                             {
                            obj.JumlahDPP.FormatViewCurrency()
                             };
                            ListArr.Add(Rowbawah);

                            //PPN, 7
                            String[] Rowbawah2 = new string[]
                        {
                            obj.JumlahPPN.FormatViewCurrency()
                        };
                            ListArr.Add(Rowbawah2);

                            //pajak barang mewah, 8
                            String[] Rowbawah3 = new string[]
                       {
                            "Pajak Penjualan Atas Barang Mewah diminta kembali"
                       };
                            ListArr.Add(Rowbawah3);

                            //9
                            String[] header = new string[]
                            {
                        "No Urut", "Macam dan Jenis Barang Kena Pajak","Macam dan Jenis Barang Kena Pajak", "Kuantum ","Harga Satuan Menurut Faktur Pajak (Rp.)  ", "Harga BKP yang dikembalikan (Rp.)"
                            }; //for header

                            ListArr.Add(header);

                            //10
                            foreach (var objDetail in datapdfgroup)
                            {

                                String[] myArr = new string[]
                                {
                            "",
                            objDetail.KodeObjek,
                            objDetail.NamaObjek,
                            objDetail.JumlahBarang.FormatViewCurrency(),
                            objDetail.HargaSatuan.FormatViewCurrency(),
                            objDetail.HargaTotal.FormatViewCurrency(),

                                };
                                ListArr.Add(myArr);

                            }

                            fullPath = PdfHelper.CreatePDFNotaRetur(ListArr, Pdffilename);
                        }

                    }
                }


                #endregion             
                string upload_status = "";
                if (rowFail == 0)
                {
                    if (rowFailedinvalidation > 0)
                    {
                        rowSuccess = rowSuccess - rowFailedinvalidation;
                        rowFail = rowFail + rowFailedinvalidation;
                        upload_status = "File has been fail uploaded";
                        result.ResultCode = false;
                        result.ResultDesc = Formatting
                        .UPLOAD_NOTIFICATION_UPLOAD
                        .Replace("@upload_status", (upload_status).ToString())
                        .Replace("@success", (rowSuccess).ToString())
                        .Replace("@error", (rowFail).ToString());
                    }
                    else
                    {
                        upload_status = "File has been uploaded";
                        result.ResultCode = true;
                        result.ResultDesc = Formatting
                       .UPLOAD_NOTIFICATION_UPLOAD_Success
                       .Replace("@upload_status", (upload_status).ToString())
                       .Replace("@success", (rowSuccess).ToString());
                    }

                }
                else
                {
                    upload_status = "File has been fail uploaded";
                    result.ResultCode = false;
                    result.ResultDesc = Formatting
                    .UPLOAD_NOTIFICATION_UPLOAD
                    .Replace("@upload_status", (upload_status).ToString())
                    .Replace("@success", (rowSuccess).ToString())
                    .Replace("@error", (rowFail).ToString());
                }


                //result.ResultDesc = Formatting
                //.UPLOAD_NOTIFICATION_UPLOAD
                //.Replace("@upload_status", (upload_status).ToString())
                //.Replace("@success", (rowSuccess).ToString())
                //.Replace("@error", (rowFail).ToString());
                //result.ResultDesc = Formatting.UPLOAD_NOTIFICATION_VALIDATION.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());

            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }



        public Result ExcelApprovalStatusUpdate(string path, string user)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = Formatting.UPLOAD_NOTIFICATION
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;


            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;


            try
            {
                string approvalTime = string.Empty;
                string _nomorFakturGabungan = string.Empty;
                string _approvalStatus = string.Empty;
                string _recordTime = string.Empty;
                string _taxInvoiceStatus = string.Empty;

                string _headerNomorDokumenRetur = string.Empty;
                string _headerApprovalStatus = string.Empty;
                string _headerApprovalTime = string.Empty;
                string _headerRecordTime = string.Empty;
                string _headerTaxInvoiceStatus = string.Empty;
                string _heder = string.Empty;

                bool isApproveSukses = false;

                file.Close();
                VATOutApprovalStatusViewModel vatOutApprovalStatus = new VATOutApprovalStatusViewModel();
                var cellNum = 4; // 31 is the cell amount in one row
                //List<int> exceptionCellNum = new List<int>();
                //exceptionCellNum.Add(2);
                db.BeginTransaction();

                _headerNomorDokumenRetur = sheet.GetRow(cellNum - 1).GetCell(0).StringCellValue.ToString().Trim();
                _headerApprovalStatus = sheet.GetRow(cellNum - 1).GetCell(1).StringCellValue.ToString().Trim();
                _headerApprovalTime = sheet.GetRow(cellNum - 1).GetCell(2).StringCellValue.ToString().Trim();
                _headerRecordTime = sheet.GetRow(cellNum - 1).GetCell(3).StringCellValue.ToString().Trim();
                _headerTaxInvoiceStatus = sheet.GetRow(cellNum - 1).GetCell(4).StringCellValue.ToString().Trim();
                if (_headerNomorDokumenRetur != "NOMOR_FAKTUR_PAJAK")
                {
                    _heder += _headerNomorDokumenRetur + ", ";
                }
                if (_headerApprovalStatus != "APPROVAL_STATUS")
                {
                    _heder += _headerApprovalStatus + ", ";
                }
                if (_headerApprovalTime != "TANGGAL_APPROVAL")
                {
                    _heder += _headerApprovalTime + ", ";
                }
                if (_headerRecordTime != "TANGGAL_REKAM")
                {
                    _heder += _headerRecordTime + ", ";
                }
                if (_headerTaxInvoiceStatus != "STATUS_FAKTUR")
                {
                    _heder += _headerTaxInvoiceStatus + ", ";
                }
                if (!string.IsNullOrEmpty(_heder.Trim()))
                {
                    _heder = _heder.Remove(_heder.Length - 2);
                    throw new Exception(_heder.Trim() + " is not valid header template");
                }

                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNum; i++)
                        {
                            try
                            {
                                sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                                sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                                pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                            }
                            catch (NullReferenceException)
                            {

                                throw new Exception("Row " + (row + 1) + " cannot blank, please delete empty row");
                            }
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        #endregion

                        //if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        //{
                        string _validationEmpt = string.Empty;
                        #region Cell Mapping & Insert to DB
                        if (vatOutApprovalStatus.NomorFakturGabungan != sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim()) // if not duplicate from previous
                        {
                            vatOutApprovalStatus = new VATOutApprovalStatusViewModel();
                            _nomorFakturGabungan = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                            if (string.IsNullOrEmpty(_nomorFakturGabungan))
                            {
                                _validationEmpt = _validationEmpt + "Nomor Faktur is mandatory, ";
                            }
                            else if (!string.IsNullOrEmpty(_nomorFakturGabungan))
                            {
                                vatOutApprovalStatus.NomorFakturGabungan = _nomorFakturGabungan;
                            }
                            _approvalStatus = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();

                            if (string.IsNullOrEmpty(_approvalStatus))
                            {
                                //throw new Exception("Approval Status Harus di isi.");
                                _validationEmpt = _validationEmpt + "Approval Status is mandatory, ";
                            }
                            else if (!string.IsNullOrEmpty(_approvalStatus))
                            {
                                vatOutApprovalStatus.ApprovalStatus = _approvalStatus;
                                isApproveSukses = (vatOutApprovalStatus.ApprovalStatus.ToLower().Equals("approval sukses"));
                            }
                            approvalTime = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();


                            if (string.IsNullOrEmpty(approvalTime) && isApproveSukses)
                            {
                                //throw new Exception("Tanggal Approval Harus di isi.");
                                _validationEmpt = _validationEmpt + "Tanggal Approval is mandatory, ";
                            }
                            else if (!string.IsNullOrEmpty(approvalTime))
                            {
                                vatOutApprovalStatus.ApprovalTime = DateTime.ParseExact(
                                                            approvalTime.Replace("WIB ", ""),
                                                            "ddd MMM dd HH:mm:ss yyyy",
                                                            System.Globalization.CultureInfo.InvariantCulture);
                            }
                            _recordTime = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim();
                            if (string.IsNullOrEmpty(_recordTime))
                            {
                                //throw new Exception("Tanggal Rekam Harus di isi.");
                                _validationEmpt = _validationEmpt + "Tanggal Rekam is mandatory, ";
                            }
                            else if (!string.IsNullOrEmpty(approvalTime))
                            {
                                vatOutApprovalStatus.RecordTime = DateTime.ParseExact(
                                     _recordTime.Replace("WIB ", ""),
                                     "ddd MMM dd HH:mm:ss yyyy",
                                     System.Globalization.CultureInfo.InvariantCulture);
                            }
                            try
                            {
                                _taxInvoiceStatus = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();
                            }
                            catch (Exception)
                            {
                                _taxInvoiceStatus = "";
                            }

                            if (string.IsNullOrEmpty(_taxInvoiceStatus))
                            {
                                //throw new Exception("Status Faktur Harus di isi.");
                                _validationEmpt = _validationEmpt + "Status Faktur is mandatory, ";
                            }
                            else if (!string.IsNullOrEmpty(_taxInvoiceStatus))
                            {
                                vatOutApprovalStatus.TaxInvoiceStatus = _taxInvoiceStatus;
                            }
                            // vatOutReturnApprovalStatus.TaxInvoiceStatus;

                            if (!string.IsNullOrEmpty(_validationEmpt.Trim()))
                            {
                                throw new Exception(_validationEmpt.Trim());
                            }
                            result = db.SingleOrDefault<Result>("VATOut/usp_UpdateCustom_TB_R_VATOut_ApprovalStatus",
                                    vatOutApprovalStatus.MapFromModel(DateTime.Now, user));
                        }
                        else
                        {
                            throw new Exception("Immediate duplicate rows of " + vatOutApprovalStatus.NomorFakturGabungan);
                        }
                        #endregion

                        if (!result.ResultCode)
                        {
                            throw new Exception(result.ResultDesc);
                        }
                        else
                        {
                            ExceptionHandler.LogToSync(row - 3, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE,path, MessageType.INF);
                            rowSuccess++;
                        }
                        //}
                        //else
                        //{
                        //  throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        //}
                    }
                    catch (Exception e)
                    {
                        e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }

                db.CommitTransaction();
                result.ResultCode = true;
                result.ResultDesc = Formatting.UPLOAD_NOTIFICATION_OLD.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        public Result ExcelTransitoryStatusUpdate(string path, string user)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = Formatting.UPLOAD_NOTIFICATION
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;



            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                string _transitoryDate = string.Empty;
                string _nomorFakturGabungan = string.Empty;
                string _transitoryStatus = string.Empty;
                string _transitoryNumber = string.Empty;

                string _headerNomorFakturGabungan = string.Empty;
                string _headerTransitoryDate = string.Empty;
                string _headerTransitoryStatus = string.Empty;
                string _headerTransitoryNumber = string.Empty;

                string _heder = string.Empty;

                file.Close();
                VATOutTransitoryStatusViewModel vatOutTransitoryStatus = new VATOutTransitoryStatusViewModel();
                var cellNum = 4; // 31 is the cell amount in one row

                db.BeginTransaction();

                _headerNomorFakturGabungan = sheet.GetRow(cellNum - 1).GetCell(0).StringCellValue.ToString().Trim();
                _headerTransitoryDate = sheet.GetRow(cellNum - 1).GetCell(1).StringCellValue.ToString().Trim();
                _headerTransitoryStatus = sheet.GetRow(cellNum - 1).GetCell(2).StringCellValue.ToString().Trim();
                _headerTransitoryNumber = sheet.GetRow(cellNum - 1).GetCell(3).StringCellValue.ToString().Trim();

                if (_headerNomorFakturGabungan != "NOMOR_FAKTUR_PAJAK")
                {
                    _heder += _headerNomorFakturGabungan + ", ";
                }
                if (_headerTransitoryDate != "CLEARING_TRANSITORY_DATE")
                {
                    _heder += _headerTransitoryDate + ", ";
                }
                if (_headerTransitoryStatus != "STATUS")
                {
                    _heder += _headerTransitoryStatus + ", ";
                }
                if (_headerTransitoryNumber != "TRANSITORY_NUMBER")
                {
                    _heder += _headerTransitoryNumber + ", ";
                }

                if (!string.IsNullOrEmpty(_heder.Trim()))
                {
                    _heder = _heder.Remove(_heder.Length - 2);
                    throw new Exception(_heder.Trim() + " is not valid header template");
                }

                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNum; i++)
                        {
                            try
                            {
                                sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                                sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                                pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                            }
                            catch (NullReferenceException)
                            {

                                throw new Exception("Row " + (row + 1) + " cannot blank, please delete empty row");
                            }
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        #endregion

                        string _validationEmptTransitory = string.Empty;
                        #region Cell Mapping & Insert to DB
                        if (vatOutTransitoryStatus.NomorFakturGabungan != sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim()) // if not duplicate from previous
                        {

                            vatOutTransitoryStatus = new VATOutTransitoryStatusViewModel();
                            _nomorFakturGabungan = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                            if (string.IsNullOrEmpty(_nomorFakturGabungan))
                            {
                                _validationEmptTransitory = _validationEmptTransitory + "Credit Note Number is mandatory, ";
                                // throw new Exception("Credit Note Number Harus di isi.");
                            }
                            else if (!string.IsNullOrEmpty(_nomorFakturGabungan))
                            {
                                vatOutTransitoryStatus.NomorFakturGabungan = _nomorFakturGabungan;
                            }

                            _transitoryDate = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                            if (string.IsNullOrEmpty(_transitoryDate))
                            {
                                _validationEmptTransitory = _validationEmptTransitory + "Clearing Transitory Date is mandatory, ";
                                // throw new Exception("Clearing Transitory Date Harus di isi.");
                            }
                            else if (!string.IsNullOrEmpty(_transitoryDate))
                            {

                                var ransitoryDate = _transitoryDate.Split('/');
                                if (ransitoryDate.Count() > 1) // when the excel column is a text type
                                {
                                    if (ransitoryDate[0].Length == 2 && ransitoryDate[1].Length == 2 && ransitoryDate[2].Length == 4)
                                    {
                                        vatOutTransitoryStatus.TransitoryDate = DateTime.ParseExact(_transitoryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        _validationEmptTransitory = _validationEmptTransitory + "Date Format must be: dd/mm/yyyy, ";
                                        // throw new Exception("Date Format must be: dd/mm/yyyy");
                                    }
                                }
                                else // when the excel column is a DateTime type
                                {
                                    vatOutTransitoryStatus.TransitoryDate = DateTime.FromOADate(Convert.ToDouble(_transitoryDate));
                                }
                            }

                            _transitoryStatus = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();
                            if (string.IsNullOrEmpty(_transitoryStatus))
                            {
                                _validationEmptTransitory = _validationEmptTransitory + "Status is mandatory, ";
                                //throw new Exception("Status Harus di isi.");
                            }
                            else if (!string.IsNullOrEmpty(_transitoryStatus))
                            {
                                if (_transitoryStatus.ToUpper() == "YES" || _transitoryStatus.ToUpper() == "NO")
                                {
                                    vatOutTransitoryStatus.TransitoryStatus = _transitoryStatus;
                                }
                                else
                                {
                                    _validationEmptTransitory = _validationEmptTransitory + "Status Only Yes/No, ";
                                }

                            }

                            _transitoryNumber = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim();

                            if (_transitoryStatus.ToUpper() == "YES")
                            {
                                if (string.IsNullOrEmpty(_transitoryNumber))
                                {
                                    _validationEmptTransitory = _validationEmptTransitory + "Transitory Number is mandatory, ";
                                }
                                else
                                {
                                    vatOutTransitoryStatus.TransitoryNumber = _transitoryNumber;
                                }
                                // throw new Exception("Transitory Number Harus di isi.");
                            }
                            else if (_transitoryStatus.ToUpper() == "NO")
                            {
                                if (!string.IsNullOrEmpty(_transitoryNumber))
                                {
                                    _validationEmptTransitory = _validationEmptTransitory + "If Status No, Transitory Number should be blank, ";
                                }
                                else
                                {
                                    vatOutTransitoryStatus.TransitoryNumber = _transitoryNumber;
                                }

                            }


                            if (!string.IsNullOrEmpty(_validationEmptTransitory.Trim()))
                            {
                                throw new Exception(_validationEmptTransitory.Trim());
                            }

                            result = db.SingleOrDefault<Result>("VATOut/usp_UpdateCustom_TB_R_VATOut_TransitoryStatus",
                                    vatOutTransitoryStatus.MapFromModel(DateTime.Now, user));
                        }
                        else
                        {
                            throw new Exception("Immediate duplicate rows of " + vatOutTransitoryStatus.NomorFakturGabungan);
                        }
                        #endregion

                        if (!result.ResultCode)
                        {
                            throw new Exception(result.ResultDesc);
                        }
                        else
                        {
                            ExceptionHandler.LogToSync(row - 3, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE,path, MessageType.INF);
                            rowSuccess++;
                        }

                    }
                    catch (Exception e)
                    {
                        e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }

                db.CommitTransaction();
                result.ResultCode = true;
                result.ResultDesc = Formatting.UPLOAD_NOTIFICATION_OLD.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }
        #endregion
    }
}