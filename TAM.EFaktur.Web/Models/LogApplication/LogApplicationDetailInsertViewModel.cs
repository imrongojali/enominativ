﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.LogApplication
{
    public class LogApplicationDetailInsertViewModel
    {
        public Guid Id { get; set; }
        public Guid ApplicationLogId { get; set; }
        public String MessageType { get; set; }
        public String MessageLocation { get; set; }
        public String MessageDetail { get; set; }

        public dynamic MapFromModel(Guid ApplicationLogId, DateTime? EventDate = null, string EventActor = "")
        {
            return new
            {
                Id = Guid.NewGuid(),
                ApplicationLogId = ApplicationLogId,
                MessageType = this.MessageType,
                MessageLocation = this.MessageLocation,
                MessageDetail = this.MessageDetail,
                EventDate = EventDate.HasValue ? EventDate.Value.FormatSQLDateTime() : DateTime.Now.FormatSQLDateTime(),
                EventActor = EventActor
            };
        }
    }
}