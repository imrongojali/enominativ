﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models
{
    public class UploadResultViewModel
    {
        public string name { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public string extension { get; set; }
        public string thumbnail_url { get; set; }
        public string delete_type { get; set; }

        
    }
}