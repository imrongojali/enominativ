﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.IO;
using System.Net;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using ICSharpCode.SharpZipLib.Zip;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using TAM.EFaktur.Web.Models.Master_Config;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using NPOI.SS.Formula.Functions;
using Toyota.Common.Web.Platform;
using TAM.EFaktur.Web.Models.VATOut;
using TAM.EFaktur.Web.Models.WHT;
using System.Reflection;
using TAM.EFaktur.Web.Models.Master_Company_Details;
using TAM.EFaktur.Web.Controllers;

namespace TAM.EFaktur.Web.Models
{
    public class GlobalFunction
    {
        #region singleton
        private GlobalFunction() { }
        private static GlobalFunction instance = null;
        public static GlobalFunction Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new GlobalFunction();
                }
                return instance;
            }
        }
        #endregion singleton

        public bool IsValidEmail(string email)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }

        private bool regex(string newPassword)
        {
            string rx = @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,20}$";
            return Regex.IsMatch(newPassword, rx);
        }
         
        public Result CommonPassword(string username,string oldPassword, string newPassword,string confirmPassword)
        {
            Result result = new Result();
            
            result.ResultCode = false;
            if ((string.IsNullOrEmpty(newPassword))||(newPassword == ""))
            { 
                result.ResultDesc = "Please input your password";
            }
            else if (newPassword!=confirmPassword)
            {
                result.ResultDesc = "New password and confirm password does not match";
            }
            else if (newPassword.Length < 6)
            {
                result.ResultDesc = "Password must be at least 6 characters";
            }
            else if(newPassword==username)
            {
                result.ResultDesc="Your password must be different from user ID";
            }
            else if (!regex(newPassword))
            {
                result.ResultDesc = "Password must be combine Uppercase, Lowercase, Numeric and Symbol";
            }
            else
            {
                result.ResultCode = true;
                result.ResultDesc = "Change password succeed";
            }
            return result;
        }

        public string GetPipedLineValue(object obj, string separator)
        {
            string result = "";
            var itemDictionary = obj.GetType().GetProperties()
                        .Where(x => x.CanRead)
                        .Where(x => x.GetValue(obj, null) != null)
                        .ToDictionary(x => x.Name, x => x.GetValue(obj, null).ToString());

            foreach (var item in itemDictionary) result += item.Value + separator;
            return result.Remove(result.Length - separator.Length);
        }

        public string DefaultToDate()
        {
            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)).FormatSQLDate();
        }

        public string DefaultFromDate()
        {
           return new DateTime(DateTime.Now.Year, DateTime.Now.Month,1).FormatSQLDate();
        }

        public List<string> getDefaultDate()
        {
            List<string> date = new List<string>();
            date.Add(GlobalFunction.Instance.DefaultFromDate().FormatViewDate());
            date.Add(GlobalFunction.Instance.DefaultToDate().FormatViewDate());
            return date;
        }

        public string GetValueCell(ICell cell)
        { 
            if (cell != null)
            {
                if (cell.CellType.ToString() == "NUMERIC")
                {
                    return Convert.ToString(cell.NumericCellValue);
                }
                else if (cell.CellType.ToString() == "DateTime")
                {
                    return Convert.ToString(cell.DateCellValue);
                }
                else if (cell.CellType.ToString() == "FORMULA")
                {
                    return Convert.ToString(cell.CellFormula);
                }
                else if (cell.CellType.ToString() == "Boolean")
                {
                    return Convert.ToString(cell.BooleanCellValue);
                }
                else
                {
                    return cell.StringCellValue;
                }
            }
            else
            {
                return "";
            }
        }

        public HSSFWorkbook CreateObjectToSheet(List<string[]> Lstobj, object classname)
        {
            var workbook = new HSSFWorkbook();//reference NPOI HSSF UserModel
            var sheet = workbook.CreateSheet(classname.GetType().Name);
            var font = workbook.CreateFont();
            var style = workbook.CreateCellStyle();
            style.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.BLUE_GREY.index;
            style.FillPattern = FillPatternType.NO_FILL; //SOLID_FOREGROUND
            style.Alignment = HorizontalAlignment.CENTER;
            font.FontHeightInPoints = 10;
            font.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index; //WHITE
            font.FontName = "Arial";
            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
            var header = sheet.CreateRow(0);//create header ======================================================
            string[] columnheader = Lstobj[0];
            for (int i = 0; i < columnheader.Count(); i++)
            {
                var cell = header.CreateCell(i);
                cell.SetCellValue(columnheader[i]);
                cell.CellStyle = style;
                cell.CellStyle.SetFont(font);
            }
            for (int r = 1; r < Lstobj.Count(); r++)// fill data to sheet=========================================
            {
                var row = sheet.CreateRow(r);
                string[] column = Lstobj[r];
                for (int i = 0; i < column.Count(); i++)
                {
                    row.CreateCell(i).SetCellValue(column[i]);
                }
            }
            for (int i = 0; i < columnheader.Count(); i++)// set autozise column==================================
            {
                try
                {
                    sheet.AutoSizeColumn(i);
                }
                catch { }
            }
            return workbook;
        }

        public XSSFWorkbook CreateObjectToSheetDRKBOriginal(List<string[]> Lstobj, object classname)
        {
            var workbook = new XSSFWorkbook();//reference NPOI HSSF UserModel
            var sheet = workbook.CreateSheet(classname.GetType().Name);

            var fontHeader = workbook.CreateFont();
            var styleHeader = workbook.CreateCellStyle();
            styleHeader.FillPattern = FillPatternType.NO_FILL; //SOLID_FOREGROUND
            styleHeader.Alignment = HorizontalAlignment.CENTER;

            fontHeader.FontHeightInPoints = 14;
            fontHeader.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index; //WHITE
            fontHeader.FontName = "Arial";
            fontHeader.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

            var fontSubHeader = workbook.CreateFont();
            var styleSubHeader = workbook.CreateCellStyle();
            fontSubHeader.FontHeightInPoints = 12;
            fontSubHeader.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index; //WHITE
            fontSubHeader.FontName = "Arial";
            fontSubHeader.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

            var font = workbook.CreateFont();
            var style = workbook.CreateCellStyle();
            style.FillPattern = FillPatternType.NO_FILL; //SOLID_FOREGROUND
            style.Alignment = HorizontalAlignment.CENTER;
            font.FontHeightInPoints = 10;
            font.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index; //WHITE
            font.FontName = "Arial";
            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
             
            //create header
            var headerTitle = sheet.CreateRow(0);//create header ======================================================
            var cellFooterTitle = headerTitle.CreateCell(0);
            cellFooterTitle.SetCellValue("DETAILED LIST OF MOTOR VEHICLES");
            cellFooterTitle.CellStyle = styleHeader;
            cellFooterTitle.CellStyle.SetFont(fontHeader);
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$A$1:$P$1"),true,true);
            
            var header2 = sheet.CreateRow(1);//create header ======================================================
            var cellFooter2 = header2.CreateCell(0);
            cellFooter2.SetCellValue(Lstobj[0][0].ToString());
            cellFooter2.CellStyle = style;
            cellFooter2.CellStyle.SetFont(font);
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$A$2:$P$2"), true);

            var header3 = sheet.CreateRow(3);//create header ======================================================
            string[] columnheader3 = Lstobj[1];
            for (int j = 0; j < columnheader3.Count(); j++)
            {
                var cell = header3.CreateCell(j);
                cell.SetCellValue(columnheader3[j]);
                cell.CellStyle = styleSubHeader;
                cell.CellStyle.SetFont(fontSubHeader);
            }

            int headerRow = 5;
            for (int i = 2; i <= 5; i++)
            {
                var headerAddtional = sheet.CreateRow(headerRow);//create company ======================================================
                string[] columnheader1Addtional = Lstobj[i];
                for (int j = 0; j < columnheader1Addtional.Count(); j++)
                {
                    if (columnheader1Addtional[j] != "")
                    {
                        var cell = headerAddtional.CreateCell(j);
                        cell.SetCellValue(columnheader1Addtional[j]);
                        cell.CellStyle = styleSubHeader;
                        cell.CellStyle.SetFont(fontSubHeader);
                    }
                    
                }
                headerRow++;
            }

            headerRow = 10;
            for (int i = 9; i <= 10; i++)
            {
                var header1 = sheet.CreateRow(headerRow );//create table header ======================================================
                string[] columnheader1 = Lstobj[i];
                for (int j = 0; j < columnheader1.Count(); j++)
                {
                    var cell = header1.CreateCell(j);
                    cell.SetCellValue(columnheader1[j]);
                    cell.CellStyle = style;
                    cell.CellStyle.SetFont(font);
                }
                headerRow++;
            }

            //headerRow = 12;
            string[] columnheader = Lstobj[10];

            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$A$11:$A$12"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$B$11:$B$12"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$C$11:$D$11"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$E$11:$F$11"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$G$11:$G$12"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$H$11:$H$12"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$I$11:$I$12"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$J$11:$J$12"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$K$11:$K$12"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$L$11:$N$11"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$O$11:$O$12"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$P$11:$P$12"));

            SetBackgoundHeaderColor(sheet, 10, 16);
            SetBackgoundHeaderColor(sheet, 11, 16);
            
            //var row = sheet.CreateRow(headerRow);
            //for (int i = 0; i < 15 + 1; i++)
            //{
            //    row.CreateCell(i).SetCellValue("(" + (i+1) + ")");
            //}
            
            //SetBackgoundHeaderColorFilter(sheet, headerRow, 16);
            
            //sheet.SetAutoFilter(NPOI.SS.Util.CellRangeAddress.ValueOf("$A$13:$P$13"));

            //headerRow++;
            int rr = 1;



            ICellStyle borderedCellStyle = sheet.Workbook.CreateCellStyle();
            borderedCellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyle.Alignment = HorizontalAlignment.LEFT;


            ICellStyle borderedCellStyleCenter = sheet.Workbook.CreateCellStyle();
            borderedCellStyleCenter.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyleCenter.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyleCenter.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyleCenter.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyleCenter.Alignment = HorizontalAlignment.CENTER;
             
            ICellStyle borderedCellStyleNumeric = sheet.Workbook.CreateCellStyle();
            borderedCellStyleNumeric.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyleNumeric.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyleNumeric.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyleNumeric.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyleNumeric.Alignment = HorizontalAlignment.RIGHT;

            IDataFormat dataFormatCustom = sheet.Workbook.CreateDataFormat();
            borderedCellStyleNumeric.DataFormat = dataFormatCustom.GetFormat("#,#0");



            //11 - mulai data
            for (int r = 11; r < Lstobj.Count(); r++)// fill data to sheet=========================================
            {
                var rowData = sheet.CreateRow(headerRow);

                string[] column = Lstobj[r];
                for (int i = 0; i < column.Count(); i++)
                {
                    var rowCell = rowData.CreateCell(i);

                    if (i == 0)
                    {
                        rowCell.SetCellValue(rr);
                        rowCell.CellStyle = borderedCellStyleCenter;

                    }
                    else
                    {
                        if ( i == 11 || i == 12 || i== 13 || i==14)
                        {
                            rowCell.SetCellType(CellType.NUMERIC);

                            double d = 0;
                            if (column[i] == null)
                            {
                                d = 0;

                            }
                            else
                            {
                                d = double.Parse(column[i]);
                            }

                            rowCell.SetCellValue(d);
                            rowCell.CellStyle = borderedCellStyleNumeric;


                        }else  if (i == 3|| i == 4 || i == 9)
                        {
                            rowCell.SetCellValue(column[i]);
                            rowCell.CellStyle = borderedCellStyleCenter;
                        }

                        else
                        {
                            rowCell.SetCellValue(column[i]);
                            rowCell.CellStyle = borderedCellStyle;

                        }

                    }

                }
                  
                headerRow++;
                rr++;
            }

            for (int i = 1; i < columnheader.Count(); i++)// set autozise column==================================
            {
                try
                {
                    sheet.AutoSizeColumn(i, true);
                }
                catch { }
            }

            sheet.SetColumnWidth(0, 2000);
            sheet.SetColumnWidth(1, 5800);
            sheet.SetColumnWidth(2, 5800);

            string[] footerLokasi = Lstobj[6];
            string[] footerNamaTTD = Lstobj[7];
            string[] footerJabatanTTD = Lstobj[8];


            headerRow = headerRow + 2;
            var footerData = sheet.CreateRow(headerRow);
            footerData.CreateCell(11).SetCellValue(footerLokasi[0] + ", _______________________");

            headerRow = headerRow + 4;
            var footerDataNama = sheet.CreateRow(headerRow);
            footerDataNama.CreateCell(11).SetCellValue(footerNamaTTD[0]);
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$L$" + (headerRow + 1) + ":$M$" + (headerRow + 1) + ""));

            headerRow = headerRow + 1;
            var footerDataTTD = sheet.CreateRow(headerRow);
            footerDataTTD.CreateCell(11).SetCellValue(footerJabatanTTD[0]);
            mergeAndCenterAndAddBorderTop(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$L$" + (headerRow + 1) + ":$M$" + (headerRow + 1) + ""));

           


            return workbook;
        }

        public XSSFWorkbook CreateObjectToSheetDRKBReport(List<string[]> Lstobj, object classname)
        {
            var workbook = new XSSFWorkbook();//reference NPOI HSSF UserModel
            var sheet = workbook.CreateSheet(classname.GetType().Name);
            sheet.PrintSetup.Landscape=true;
            sheet.FitToPage = true;

            var fontHeader = workbook.CreateFont();
            var styleHeader = workbook.CreateCellStyle();
            styleHeader.FillPattern = FillPatternType.NO_FILL; //SOLID_FOREGROUND
            styleHeader.Alignment = HorizontalAlignment.CENTER;
            fontHeader.FontHeight = 14;
            fontHeader.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index; //WHITE
            fontHeader.FontName = "Arial";
            fontHeader.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

            var fontSubHeader = workbook.CreateFont();
            var styleSubHeader = workbook.CreateCellStyle();
            fontSubHeader.FontHeight = 12;
            fontSubHeader.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index; //WHITE
            fontSubHeader.FontName = "Arial";
            fontSubHeader.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

            var font = workbook.CreateFont();
            var style = workbook.CreateCellStyle();
            style.FillPattern = FillPatternType.NO_FILL; //SOLID_FOREGROUND
            style.Alignment = HorizontalAlignment.CENTER;
            font.FontHeight = 10;
            font.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index; //WHITE
            font.FontName = "Arial";
            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
             
             
            var headerTitle = sheet.CreateRow(0);//create header ======================================================
            var cellFooterTitle = headerTitle.CreateCell(0);
            cellFooterTitle.SetCellValue("DETAILED LIST OF MOTOR VEHICLES");
            cellFooterTitle.CellStyle = styleHeader;
            cellFooterTitle.CellStyle.SetFont(fontHeader);
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$A$1:$M$1"),true,true);

            var header2 = sheet.CreateRow(1);//create header ======================================================
            var cellFooter2 = header2.CreateCell(0);
            cellFooter2.SetCellValue(Lstobj[0][0].ToString());
            cellFooter2.CellStyle = styleSubHeader;
            cellFooter2.CellStyle.SetFont(fontSubHeader);
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$A$2:$M$2"),true);

            var header3 = sheet.CreateRow(3);//create header ======================================================
            string[] columnheader3 = Lstobj[1];
            for (int j = 0; j < columnheader3.Count(); j++)
            {
                var cell = header3.CreateCell(j);
                cell.SetCellValue(columnheader3[j]);
                cell.CellStyle = styleSubHeader;
                cell.CellStyle.SetFont(fontSubHeader);
            }

            int headerRow = 5;
            for (int i = 2; i <= 5; i++)
            {
                var headerAddtional = sheet.CreateRow(headerRow);//create header ======================================================
                string[] columnheader1Addtional = Lstobj[i];
                for (int j = 0; j < columnheader1Addtional.Count(); j++)
                {
                    if (columnheader1Addtional[j] != "")
                    {
                        var cell = headerAddtional.CreateCell(j);
                        cell.SetCellValue(columnheader1Addtional[j]);
                        cell.CellStyle = styleSubHeader;
                        cell.CellStyle.SetFont(fontSubHeader);
                    }

                }
                headerRow++;
            }

            headerRow = 10;
            for (int i = 9; i <= 10; i++)
            {
                var header1 = sheet.CreateRow(headerRow);//create header ======================================================
                string[] columnheader1 = Lstobj[i];
                for (int j = 0; j < columnheader1.Count(); j++)
                {
                    var cell = header1.CreateCell(j);
                    cell.SetCellValue(columnheader1[j]);
                    cell.CellStyle = style;
                    cell.CellStyle.SetFont(font);
                }
                headerRow++;
            }

            //headerRow = 12;
            string[] columnheader = Lstobj[10];

            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$A$11:$A$12"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$B$11:$C$11"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$D$11:$E$11"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$F$11:$F$12"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$G$11:$G$12"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$H$11:$H$12"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$I$11:$I$12"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$J$11:$K$11"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$L$11:$L$12"));
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$M$11:$M$12"));
             
            SetBackgoundHeaderColor(sheet, 10, 13);
            SetBackgoundHeaderColor(sheet, 11, 13);


            //var row = sheet.CreateRow(headerRow);
            //for (int i = 0; i < 12 + 1; i++)
            //{
            //    row.CreateCell(i).SetCellValue("(" + (i + 1) + ")");
            //}

            //SetBackgoundHeaderColorFilter(sheet, headerRow, 13);

            //sheet.SetAutoFilter(NPOI.SS.Util.CellRangeAddress.ValueOf("$A$13:$M$13"));

            //headerRow++;
            int rr = 1;

            ICellStyle borderedCellStyle = sheet.Workbook.CreateCellStyle();
            borderedCellStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyle.Alignment = HorizontalAlignment.LEFT;

            ICellStyle borderedCellStyleCenter = sheet.Workbook.CreateCellStyle();
            borderedCellStyleCenter.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyleCenter.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyleCenter.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyleCenter.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyleCenter.Alignment = HorizontalAlignment.CENTER;

            ICellStyle borderedCellStyleNumeric = sheet.Workbook.CreateCellStyle();
            borderedCellStyleNumeric.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyleNumeric.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyleNumeric.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            borderedCellStyleNumeric.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            IDataFormat dataFormatCustom = sheet.Workbook.CreateDataFormat();
            borderedCellStyleNumeric.DataFormat = dataFormatCustom.GetFormat("#,#0");
            borderedCellStyleNumeric.Alignment = HorizontalAlignment.RIGHT;


            //11 - mulai data
            for (int r = 11; r < Lstobj.Count(); r++)// fill data to sheet=========================================
            {
                var rowData = sheet.CreateRow(headerRow);


                string[] column = Lstobj[r];
                for (int i = 0; i < column.Count() ; i++)
                {
                    var rowCell = rowData.CreateCell(i);

                    if ( i == 9 || i == 10 || i==11)
                    {
                        rowCell.SetCellType(CellType.NUMERIC);
                        double d = 0;
                        if (column[i] == null)
                        {
                            d = 0;

                        }
                        else
                        {
                            d = double.Parse(column[i]);
                        }

                        rowCell.SetCellValue(d);
                        rowCell.CellStyle = borderedCellStyleNumeric;

                    }
                    else if (i == 2|| i == 3 || i == 7)
                    {
                        rowCell.SetCellValue(column[i]);
                        rowCell.CellStyle = borderedCellStyleCenter;
                    }
                    else
                    {
                        rowCell.SetCellValue(column[i]);
                        rowCell.CellStyle = borderedCellStyle;

                    }

                }
                 
                headerRow++;
                rr++;
            }

            for (int i = 0; i < columnheader.Count(); i++)// set autozise column==================================
            {
                try
                {
                    sheet.AutoSizeColumn(i, true);

                }
                catch { }
            }

            sheet.SetColumnWidth(0, 5000);
            sheet.SetColumnWidth(1, 6000);
            sheet.SetColumnWidth(2, 4000);

            string[] footerLokasi = Lstobj[6];
            string[] footerNamaTTD = Lstobj[7];
            string[] footerJabatanTTD = Lstobj[8];

            headerRow = headerRow + 2;
            var footerData = sheet.CreateRow(headerRow);
            footerData.CreateCell(8).SetCellValue(footerLokasi[0]+", _______________________");

            headerRow = headerRow + 4;
            var footerDataNama = sheet.CreateRow(headerRow);
            footerDataNama.CreateCell(8).SetCellValue(footerNamaTTD[0]);
            mergeAndCenter(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$I$" + (headerRow + 1) + ":$J$" + (headerRow + 1) + ""));

            headerRow = headerRow + 1;
            var footerDataTTD = sheet.CreateRow(headerRow);
            footerDataTTD.CreateCell(8).SetCellValue(footerJabatanTTD[0]);
            mergeAndCenterAndAddBorderTop(sheet, NPOI.SS.Util.CellRangeAddress.ValueOf("$I$" + (headerRow + 1) + ":$J$" + (headerRow + 1) + ""));




            return workbook;
        }

        public HSSFWorkbook CreateObjectToSheetEbupot(List<string[]> Lstobj, List<string[]> LstobjRef, object classname, string jenisPajak)
        {
            string configebupot = MasterConfigRepository.Instance.GetByConfigKey("TempEbupotFolder").ConfigValue;
            //https://stackoverflow.com/questions/21273949/npoi-date-format-cell
            FileStream fs = new FileStream(configebupot, FileMode.Open, FileAccess.Read);
            var workbook = new HSSFWorkbook(fs);

            var sheet = workbook.GetSheet(jenisPajak);
            var sheetRef = workbook.GetSheet("Dasar Pemotongan");

            int countdata23 = 0;
            int countdata26 = 0;
            if (jenisPajak=="23")
            {
                countdata23 = Lstobj.Count();
                countdata26 = 0;
            }
            else
            {
                countdata23 = 0;
                countdata26 = Lstobj.Count();
            }

            ISheet sheetRekap = workbook.GetSheet("Rekap");
            sheetRekap.GetRow(2).GetCell(2).SetCellValue(countdata23);
            sheetRekap.GetRow(3).GetCell(2).SetCellValue(countdata26);

            for (int r = 0; r < Lstobj.Count(); r++)
            {
                var row = sheet.CreateRow(r + 2);
                string[] column = Lstobj[r];
                for (int i = 0; i < column.Count(); i++)
                {
                    row.CreateCell(i).SetCellValue(column[i]);
                }
            }

            for (int r = 0; r < LstobjRef.Count(); r++)
            {
                var rowRef = sheetRef.CreateRow(r + 2);
                string[] columnRef = LstobjRef[r];
                for (int i = 0; i < columnRef.Count(); i++)
                {
                    rowRef.CreateCell(i).SetCellValue(columnRef[i]);
                }
            }
           
            return workbook;
        }


        public HSSFWorkbook CreateObjectToSheetWithTitle(List<string[]> Lstobj, object classname, string title)
        {
            var workbook = new HSSFWorkbook();//reference NPOI HSSF UserModel
            var sheet = workbook.CreateSheet(classname.GetType().Name);
            var font = workbook.CreateFont();
            var style = workbook.CreateCellStyle();
            style.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.BLUE_GREY.index;
            style.FillPattern = FillPatternType.NO_FILL;
            style.Alignment = HorizontalAlignment.CENTER;
            font.FontHeightInPoints = 10;
            font.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            font.FontName = "Arial";
            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

            var fontitle = workbook.CreateFont();
            var styletitle = workbook.CreateCellStyle();
            styletitle.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.BLUE_GREY.index;
            styletitle.FillPattern = FillPatternType.NO_FILL;
            styletitle.Alignment = HorizontalAlignment.LEFT;
            fontitle.FontHeightInPoints = 12;
            fontitle.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            fontitle.FontName = "Arial";
            fontitle.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
            string titlevalue = title;
            var rowtitle = sheet.CreateRow(0);
            var celltitle = rowtitle.CreateCell(0);
            celltitle.SetCellValue(titlevalue);
            celltitle.CellStyle = styletitle;
            celltitle.CellStyle.SetFont(fontitle);
            mergeAndCenter(sheet,NPOI.SS.Util.CellRangeAddress.ValueOf("$A$1:$IV$1"));

            var header = sheet.CreateRow(2);//create header ======================================================
            string[] columnheader = Lstobj[0];
            for (int i = 0; i < columnheader.Count(); i++)
            {
                var cell = header.CreateCell(i);
                cell.SetCellValue(columnheader[i]);
                cell.CellStyle = style;
                cell.CellStyle.SetFont(font);
            }
            for (int r = 1; r < Lstobj.Count(); r++)// fill data to sheet=========================================
            {
                var row = sheet.CreateRow(r+2);
                string[] column = Lstobj[r];
                for (int i = 0; i < column.Count(); i++)
                {
                    row.CreateCell(i).SetCellValue(column[i]);
                }
            }
            for (int i = 0; i < columnheader.Count(); i++)// set autozise column==================================
            {
                try
                {
                    sheet.AutoSizeColumn(i, true);
                }
                catch { }
            }
            return workbook;
        }

        public string FormatViewDateOrNull(string datetime)
        {
            try
            {
                if ((datetime != null) && (datetime != ""))
                {
                    DateTime d = System.Convert.ToDateTime(datetime);
                    return d.FormatViewDate();
                }
                else
                {
                    return "";
                }
            }
            catch(Exception e)
            {
                return "";
            }
        }

        public static string GetClientIP()
        {
            string ip = HttpContext.Current.Request.UserHostAddress;
            return ip;
        }

        public static string GetClientHostName()
        {
            try
            {

                return System.Net.Dns.GetHostName();
            }
            catch (Exception e)
            {
                return string.Empty;
            }
        }

        public static bool BlankCellCheck(IRow row, int cellNum, List<int> exceptCellNum)
        {
            bool r = true;
            for (var i = 0; i < cellNum; i++)
            {
                if (!exceptCellNum.Exists(x => x == i))
                {
                    row.GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                    row.GetCell(i).SetCellType(CellType.STRING);
                    if (row.GetCell(i).StringCellValue.ToString().Trim() == "")
                    {
                        r = false; break;
                    }
                }
            }
            return r;
        }

        public static bool BlankCellCheckApprovalEbupot(IRow row, int cellNum, List<int> exceptCellNum)
        {
            bool r = true;
            for (var i = 3; i < cellNum; i++)
            {
                if (!exceptCellNum.Exists(x => x == i))
                {
                    row.GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                    row.GetCell(i).SetCellType(CellType.STRING);
                    if (row.GetCell(i).StringCellValue.ToString().Trim() == "")
                    {
                        r = false; break;
                    }
                }
            }
            return r;
        }

        /// <summary>
        /// Check if cells are blank by list of cellNum
        /// </summary>
        /// <param name="row"></param>
        /// <param name="cellNum">0-based Index to check blank cell</param>
        /// <returns></returns>
        public static bool IsCellBlank(IRow row, int cellNum, List<int> checkCellNum)
        {
            bool r = true;
            for (var i = 0; i < cellNum; i++)
            {
                if (checkCellNum.Exists(x => x == i))
                {
                    row.GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                    row.GetCell(i).SetCellType(CellType.STRING);
                    if (row.GetCell(i).StringCellValue.ToString().Trim() != "")
                    {
                        r = false; break;
                    }
                }
            }
            return r;
        }
        private static void mergeAndCenterAndAddBorderTop(ISheet sheet, NPOI.SS.Util.CellRangeAddress range, bool bold = true, bool header = false)
        {
            sheet.AddMergedRegion(range);
            ICellStyle style = sheet.Workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.CENTER;
            style.VerticalAlignment = VerticalAlignment.CENTER;
          

            ICell cell = sheet.GetRow(range.FirstRow).GetCell(range.FirstColumn);
            ICellStyle cellStyle = sheet.Workbook.CreateCellStyle();
            cellStyle.VerticalAlignment = VerticalAlignment.CENTER;
            cellStyle.Alignment = HorizontalAlignment.CENTER;
            cellStyle.TopBorderColor = IndexedColors.BLACK.Index;
            cellStyle.BorderTop = BorderStyle.THIN;

            RegionUtil.SetBorderTop(1, range, sheet, sheet.Workbook); 

            var font = sheet.Workbook.CreateFont();
            if (bold)
            {
                font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
            }
            if (header)
            {
                font.FontHeightInPoints = 14;
            }
            else
            {
                font.FontHeightInPoints = 10;
            }
            font.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index; //WHITE
            font.FontName = "Arial";

            cellStyle.SetFont(font);
            style.SetFont(font);
            cell.CellStyle = cellStyle; 

        }
        private static void mergeAndCenter(ISheet sheet, NPOI.SS.Util.CellRangeAddress range, bool bold = true, bool header = false)
        {
            sheet.AddMergedRegion(range);
            ICellStyle style = sheet.Workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.CENTER;
            style.VerticalAlignment = VerticalAlignment.CENTER;

            ICell cell = sheet.GetRow(range.FirstRow).GetCell(range.FirstColumn);
            ICellStyle cellStyle = sheet.Workbook.CreateCellStyle();
            cellStyle.VerticalAlignment = VerticalAlignment.CENTER;
            cellStyle.Alignment = HorizontalAlignment.CENTER;

            var font = sheet.Workbook.CreateFont();
            if (bold)
            {
                font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
            }
            if (header)
            {
                font.FontHeightInPoints = 14;
            }
            else
            {
                font.FontHeightInPoints = 10;
            }
            font.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index; //WHITE
            font.FontName = "Arial";

            cellStyle.SetFont(font);
            cell.CellStyle = cellStyle;

        }  
      
        private static void MakeItCenter(ISheet sheet, NPOI.SS.Util.CellRangeAddress range, bool bold = false, bool header = false)
        { 
            ICell cell = sheet.GetRow(range.FirstRow).GetCell(range.FirstColumn);
            ICellStyle cellStyle = sheet.Workbook.CreateCellStyle();
            cellStyle.VerticalAlignment = VerticalAlignment.TOP;
            cellStyle.Alignment = HorizontalAlignment.CENTER;

            var font = sheet.Workbook.CreateFont();
            if (bold)
            {
                font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
            }
            if (header)
            {
                font.FontHeightInPoints = 14;
            }
            else
            {
                font.FontHeightInPoints = 10;
            }
            font.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index; //WHITE
            font.FontName = "Arial";

            cellStyle.SetFont(font);
            cell.CellStyle = cellStyle;

        }
         
        private static void MakeItCurrency(ISheet sheet, NPOI.SS.Util.CellRangeAddress range, bool bold = false, bool header =false)
        {
            ICell cell = sheet.GetRow(range.FirstRow).GetCell(range.FirstColumn);
            ICellStyle cellStyle = sheet.Workbook.CreateCellStyle();
            cellStyle.VerticalAlignment = VerticalAlignment.TOP;
            cellStyle.Alignment = HorizontalAlignment.RIGHT;
             
            var font = sheet.Workbook.CreateFont();
            if (bold)
            {
                font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
            }
            if (header)
            {
                font.FontHeightInPoints = 14;
            }
            else
            {
                font.FontHeightInPoints = 10;
            }
            font.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index; //WHITE
            font.FontName = "Arial";

            cellStyle.SetFont(font); 
            cell.CellStyle = cellStyle;

          
            

        }
          
        private static void SetBackgoundHeaderColor(ISheet sheet, int row, int cellnum, bool header=false )
        {
            for (int i = 0; i < cellnum; i++)
            {
                ICell cell = sheet.GetRow(row).GetCell(i);
                XSSFCellStyle cellStyle = (XSSFCellStyle) sheet.Workbook.CreateCellStyle();

                var color = new NPOI.XSSF.UserModel.XSSFColor(new byte[] { 31, 73, 125 });

                cellStyle.SetFillForegroundColor(color);
                cellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
                cellStyle.VerticalAlignment = VerticalAlignment.CENTER;
                cellStyle.Alignment = HorizontalAlignment.CENTER;

                cellStyle.LeftBorderColor = IndexedColors.BLACK.Index;
                cellStyle.TopBorderColor = IndexedColors.BLACK.Index;
                cellStyle.RightBorderColor = IndexedColors.BLACK.Index;
                cellStyle.BottomBorderColor = IndexedColors.BLACK.Index;


                cellStyle.BorderLeft = BorderStyle.THIN;
                cellStyle.BorderTop = BorderStyle.THIN;
                cellStyle.BorderRight = BorderStyle.THIN;
                cellStyle.BorderBottom = BorderStyle.THIN;


                var font = sheet.Workbook.CreateFont();
                if (header)
                {
                    font.FontHeightInPoints = 14;
                }
                else
                {
                    font.FontHeightInPoints = 10;
                }
                font.Color = NPOI.HSSF.Util.HSSFColor.WHITE.index; //WHITE
                font.FontName = "Arial";
                font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                cellStyle.SetFont(font);
                cell.CellStyle = cellStyle;
            }
        }

       
        private static void SetBackgoundHeaderColorFilter(ISheet sheet, int row, int cellnum, bool header = false)
        {
            for (int i = 0; i < cellnum; i++)
            {
                ICell cell = sheet.GetRow(row).GetCell(i);
                XSSFCellStyle cellStyle = (XSSFCellStyle)sheet.Workbook.CreateCellStyle();

                var color = new NPOI.XSSF.UserModel.XSSFColor(new byte[] { 253, 233, 217 });

                cellStyle.SetFillForegroundColor(color);
                cellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
                cellStyle.VerticalAlignment = VerticalAlignment.CENTER;
                cellStyle.Alignment = HorizontalAlignment.CENTER;

                cellStyle.LeftBorderColor = IndexedColors.BLACK.Index;
                cellStyle.TopBorderColor = IndexedColors.BLACK.Index;
                cellStyle.RightBorderColor = IndexedColors.BLACK.Index;
                cellStyle.BottomBorderColor = IndexedColors.BLACK.Index;


                cellStyle.BorderLeft = BorderStyle.THIN;
                cellStyle.BorderTop = BorderStyle.THIN;
                cellStyle.BorderRight = BorderStyle.THIN;
                cellStyle.BorderBottom = BorderStyle.THIN;


                var font = sheet.Workbook.CreateFont();
                if (header)
                {
                    font.FontHeightInPoints = 14;
                }
                else
                {
                    font.FontHeightInPoints = 10;
                }
                font.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index; //WHITE
                font.FontName = "Arial";
                font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

                cellStyle.SetFont(font);
                cell.CellStyle = cellStyle;
            }


        }

        #region SharpZipLib
        /// <summary>
        /// Generate Zip File based on existing file path(s)
        /// </summary>
        /// <param name="FilePaths">List of File Path wanted to be included in zip</param>
        /// <param name="TempZipFolder">Path where zip will be stored</param>
        /// <returns>Zip Full Path</returns>
        public static string GenerateZipFile(List<string> FilePaths, string TempZipFolder)
        {
            string zipFileName = "eFaktur_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss") + ".zip";
            string zipFullPath = Path.Combine(@TempZipFolder, zipFileName);
            int i = 0;
            try
            {
                Directory.CreateDirectory(@TempZipFolder);
                using (ZipOutputStream zip = new ZipOutputStream(File.Create(zipFullPath)))
                {
                    zip.SetLevel(3);
                    byte[] buffer = new byte[4096];
                    
                    foreach (string filePath in FilePaths)
                    {
                        if (File.Exists(filePath)) //only when file exists
                        {
                            i++;
                            ZipEntry entry = new ZipEntry(Path.GetFileName(@filePath));

                            entry.DateTime = DateTime.Now;
                            zip.PutNextEntry(entry);

                            using (FileStream fs = File.OpenRead(@filePath))
                            {
                                int sourceBytes;
                                do
                                {
                                    sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                    zip.Write(buffer, 0, sourceBytes);

                                } while (sourceBytes > 0);
                            }
                        }
                    }
                    zip.Finish();
                    zip.Close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return i > 0 ? zipFullPath : string.Empty;
        }

        #region GenerateZipPdfTTD
        public static string GenerateZipFilePdf(List<string> FilePaths, string TempZipFolder)
        {
            var zipFileName = "eFaktur_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss") + ".zip";
            var zipFullPath = Path.Combine(@TempZipFolder, zipFileName);
            int i = 0;
            try
            {
                Directory.CreateDirectory(@TempZipFolder);
                using (ZipOutputStream zip = new ZipOutputStream(File.Create(zipFullPath)))
                {
                    zip.SetLevel(3);
                    byte[] buffer = new byte[4096];
                    //ZipEntry entry = new ZipEntry(Path.GetFileName(@filePath));
                    //FilePaths.ForEach(f => System.IO.File.Copy(f, Path.Combine(Path.GetFileName(@f))));

                    //foreach (string filePath in FilePaths)
                    //{
                    //    i++;
                    //        byte[] fileBytes = System.IO.File.ReadAllBytes(@filePath);
                    //        ZipEntry entry = new ZipEntry(Path.GetFileName(@filePath));
                    //        //zip.PutNextEntry(entry);
                    //        //zip.Write(fileBytes, 0, fileBytes.Length);
                    //    var fileEntry = new ZipEntry(Path.GetFileName(@filePath))
                    //    {
                    //        Size = fileBytes.Length
                    //    };

                    //zip.PutNextEntry(fileEntry);
                    //zip.Write(fileBytes, 0, fileBytes.Length);

                    //}

                    foreach (string filePath in FilePaths)
                    {
                        if (File.Exists(filePath)) //only when file exists
                        {
                            i++;
                            ZipEntry entry = new ZipEntry(Path.GetFileName(@filePath));

                            entry.DateTime = DateTime.Now;
                            zip.PutNextEntry(entry);

                            using (FileStream fs = File.OpenRead(@filePath))
                            {
                                int sourceBytes;
                                do
                                {
                                    sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                    zip.Write(buffer, 0, sourceBytes);

                                } while (sourceBytes > 0);
                            }
                        }
                    }
                    zip.Finish();
                    zip.Close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            //return i > 0 ? zipFullPath : string.Empty;
            return zipFullPath;
        }
        #endregion

        public static string GenerateZipFileWHT(List<string> FilePaths, string TempZipFolder)
        {
            string zipFileName = "WHT_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss") + ".zip";
            string zipFullPath = Path.Combine(@TempZipFolder, zipFileName);
            int i = 0;
            try
            {
                Directory.CreateDirectory(@TempZipFolder);
                using (ZipOutputStream zip = new ZipOutputStream(File.Create(zipFullPath)))
                {
                    zip.SetLevel(3);
                    byte[] buffer = new byte[4096];

                    foreach (string filePath in FilePaths)
                    {
                        if (File.Exists(filePath)) //only when file exists
                        {
                            i++;
                            ZipEntry entry = new ZipEntry(Path.GetFileName(@filePath));

                            entry.DateTime = DateTime.Now;
                            zip.PutNextEntry(entry);

                            using (FileStream fs = File.OpenRead(@filePath))
                            {
                                int sourceBytes;
                                do
                                {
                                    sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                    zip.Write(buffer, 0, sourceBytes);

                                } while (sourceBytes > 0);
                            }
                        }
                    }
                    zip.Finish();
                    zip.Close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return i > 0 ? zipFullPath : string.Empty;
        }
        #endregion

        #region PDFSharp
        public static string MergePDFFiles(List<string> FilePaths, string TempPrintFolder, string EventActor) 
        {
            string mergedPDFFileName = "eFaktur_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss") + ".pdf";
            string mergedPDFFullPath = Path.Combine(@TempPrintFolder, mergedPDFFileName);
            int i = 0;
            
            try
            {
                using (PdfDocument mergedPDF = new PdfDocument())
                {
                    foreach (string filePath in FilePaths)
                    {
                        if (File.Exists(filePath)) //only when file exists
                        {
                            i++;
                            using (PdfDocument pdfSource = PdfReader.Open(@filePath, PdfDocumentOpenMode.Import))
                            {
                                CopyPDFPages(pdfSource, mergedPDF);
                            }
                        }
                    }
                    if (i > 0)
                    {
                        Directory.CreateDirectory(@TempPrintFolder);
                        mergedPDF.Save(mergedPDFFullPath);
                        ExceptionHandler.LogToApp("VATOut Print PDF", MessageType.INF, "PDF file successfully merged into: " + mergedPDFFullPath, EventActor);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            
            return i > 0 ? mergedPDFFileName : string.Empty;
        }

        public static string MergePDFFilesNameable(List<string> FilePaths, string TempPrintFolder, string EventActor, string Name)
        {
            string mergedPDFFileName = Name + "_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss") + ".pdf";
            string mergedPDFFullPath = Path.Combine(@TempPrintFolder, mergedPDFFileName);
            int i = 0;

            try
            {
                using (PdfDocument mergedPDF = new PdfDocument())
                {
                    foreach (string filePath in FilePaths)
                    {
                        if (File.Exists(filePath)) //only when file exists
                        {
                            i++;
                            using (PdfDocument pdfSource = PdfReader.Open(@filePath, PdfDocumentOpenMode.Import))
                            {
                                CopyPDFPages(pdfSource, mergedPDF);
                            }
                        }
                    }
                    if (i > 0)
                    {
                        Directory.CreateDirectory(@TempPrintFolder);
                        mergedPDF.Save(mergedPDFFullPath);
                        ExceptionHandler.LogToApp(Name + " Print PDF", MessageType.INF, "PDF file successfully merged into: " + mergedPDFFullPath, EventActor);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return i > 0 ? mergedPDFFileName : string.Empty;
        }

        public static void CopyPDFPages(PdfDocument from, PdfDocument to)
        {
            for (int i = 0; i < from.PageCount; i++)
            {
                to.AddPage(from.Pages[i]);
            }
        }
        #endregion


        //by rg 01102020
        #region PDFSharp Create 
        public static void CreatePDF22(WHTDashboardViewModel data, CompanyViewModel dataCompany, string urlPath)
        {
            //var path = HttpServerUtility.MapPath(@"~/");
            try
            {
                
                Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateGeneratePph22");
                if (config == null)
                {
                    throw new Exception("Please define the UrlTemplateGeneratePph22 Config");
                }

                PdfDocument inputDocument = PdfReader.Open(HttpContext.Current.Server.MapPath(config.ConfigValue), PdfDocumentOpenMode.Import);

                PdfDocument outputDocument = new PdfDocument();

                XFont font = new XFont("Arial", 8, XFontStyle.Regular);
                XFont font9 = new XFont("Arial", 9, XFontStyle.Regular);
                XFont fontBold = new XFont("Arial", 8, XFontStyle.Bold);
                XFont fontBold9 = new XFont("Arial", 9, XFontStyle.Bold);
                //TES AD

                XGraphics gfx;
                XRect box;
                int count = inputDocument.PageCount;
                for (int idx = 0; idx < count; idx++)
                {
                    // Get page from 1st document
                    PdfPage page1 = inputDocument.PageCount > idx ? inputDocument.Pages[idx] : new PdfPage();

                    page1 = outputDocument.AddPage(page1);

                    // Write document file name and page number on each page
                    gfx = XGraphics.FromPdfPage(page1);
                    box = page1.MediaBox.ToXRect();
                    box.Inflate(0, -10);

                    //Nomor
                    XRect rect = new XRect(225, 180, 140, 7);
                    gfx.DrawRectangle(new XSolidBrush(XColor.FromArgb(255, 150, 150, 150)), rect);

                    gfx.DrawString("Nomor : " + data.NomorBuktiPotong, fontBold, XBrushes.Black, 245, 186.9);

                    #region NPWP, NAMA, ALAMAT 
                     
                    WriteLineData(gfx, data.NPWPPenjual.Replace(".", " ").Replace("-", " "), font, 143.3, 214, 15.1, "small");

                    WriteLineData(gfx, data.NamaPenjual.ToUpper(), font, 143.3, 233, 15.1, "large");

                    WriteLineData(gfx, data.AlamatNPWP.ToUpper(), font, 143.3, 252, 15.1, "large");

                    #endregion

                    #region TABLE

                    XTextFormatter tf = new XTextFormatter(gfx);
                    tf.Alignment = XParagraphAlignment.Right;

                    string npwpZero = "";

                    if (data.NPWPPenjual == "00.000.000.0-000.000")
                    {
                        npwpZero = "X";
                    }

                    string terbilang = TerbilangHelper.Terbilang(data.JumlahPPH) + " Rupiah";

                    //25012021 validasi tarif 0.45 & 5.00 % dilepas
                    if (data.JenisPajak == "2221" || data.JenisPajak == "2222")
                    { 
                        tf.DrawString(data.JumlahDPP.FormatViewCurrencyiNA().Replace(",", "."), font, XBrushes.Black,
                            new XRect(230, 419, 130, 100));

                        tf.DrawString(npwpZero, font, XBrushes.Black,
                            new XRect(327, 419, 61, 100));

                        tf.DrawString(data.Tarif.ToString().Replace(".", ","), font, XBrushes.Black,
                            new XRect(380, 419, 61, 100));

                        tf.DrawString(data.JumlahPPH.FormatViewCurrencyiNA().Replace(",", "."), font, XBrushes.Black,
                           new XRect(449, 419, 123, 100)); 
                    }
                    else if (data.JenisPajak == "2223" || data.JenisPajak == "2224")
                    {  
                        tf.DrawString(data.JumlahDPP.FormatViewCurrencyiNA().Replace(",", "."), font, XBrushes.Black,
                            new XRect(230, 511, 130, 100));

                        tf.DrawString(npwpZero, font, XBrushes.Black,
                            new XRect(327, 511, 61, 100));

                        tf.DrawString(data.Tarif.ToString().Replace(".", ","), font, XBrushes.Black,
                            new XRect(380, 511, 61, 100));

                        tf.DrawString(data.JumlahPPH.FormatViewCurrencyiNA().Replace(",", "."), font, XBrushes.Black,
                           new XRect(449, 511, 123, 100)); 
                    }

                    //Jumlah
                    tf.DrawString(data.JumlahDPP.FormatViewCurrencyiNA().Replace(",", "."), font9, XBrushes.Black,
                        new XRect(230, 631, 130, 100));

                    tf.DrawString(data.JumlahPPH.FormatViewCurrencyiNA().Replace(",", "."), font9, XBrushes.Black,
                        new XRect(449, 631, 123, 100));
                     
                    gfx.DrawString(terbilang, font9, XBrushes.Black, 94, 652);

                    #endregion 

                    //TANGGAL
                    gfx.DrawString(dataCompany.Lokasi + ", " + data.InvoiceDate.FormatFullDay(), font9, XBrushes.Black, 352, 688);

                    //Pemungut Pajak
                    gfx.DrawRectangle(new XSolidBrush(XColor.FromArgb(255, 255, 255, 255))
                    , new XRect(365, 715, 140, 9));

                    gfx.DrawString("Pemungut Pajak", fontBold9, XBrushes.Black, 365, 722);

                    WriteLineData(gfx, dataCompany.Npwp_pt.Replace(".", " ").Replace("-", " "), font, 249.2, 745, 15.1, "NPWP");

                    WriteLineData(gfx, dataCompany.Nama_pt, font, 249.3, 771.3, 15.1, "footerName");

                    #region Tanda Tangan & Cap

                    gfx.DrawImage(XImage.FromFile(HttpContext.Current.Server.MapPath(@"~/" + dataCompany.Image_ttd)), //("D:/ttd.png"
                        new XRect(357, 823, 100, 50));

                    gfx.DrawImage(XImage.FromFile(HttpContext.Current.Server.MapPath(@"~/" + dataCompany.Image_cap) ), //"D:/tam-cap.png" 
                         new XRect(300, 840, XUnit.FromCentimeter(7), XUnit.FromCentimeter(0.85)));

                    gfx.DrawString(dataCompany.Nama_ttd, fontBold9, XBrushes.Black, 390, 880, XStringFormats.TopLeft);
                    //gfx.DrawString(dataCompany.Nama_ttd, fontBold9, XBrushes.Black, 350, 889);

                    gfx.DrawString(dataCompany.Jabatan_ttd, fontBold9, XBrushes.Black, 375, 900);

                    #endregion
                }

                // Save the document... 
                outputDocument.Save(urlPath);
            }
            catch (Exception e)
            { 
                throw e;
            } 
        } 

        public static void WriteLineData(XGraphics gfx, string data, XFont font, double x, double y, double space, string type)
        {
            int flag = 29;
            
            if (type == "small") 
            { 
                flag = 20; 
            }
            else if (type == "footerName")
            {
                flag = 22;
            }

            char[] charArr = data.ToCharArray();
            int i = 0;
            foreach (char ch in charArr)
            {
                gfx.DrawString(ch.ToString(), font, XBrushes.Black, x, y, XStringFormats.Center);
                x += space;
                i++;

                if (i == flag)
                {
                    break;
                }
            }
        }

        #endregion

        #region Download PDF 
        public static string GenerateZipFileDownloadCreditNote(List<string> FilePaths, string TempZipFolder)
        {
                string zipFileName = "eFaktur_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss") + ".zip";
                string zipFullPath = Path.Combine(@TempZipFolder, zipFileName);
                //string zipFullPath = Path.Combine(@"D:\DGT_Template_Original_Print.pdf", zipFileName);
                int i = 0;
                try
                {
                    Directory.CreateDirectory(@TempZipFolder);
                    using (ZipOutputStream zip = new ZipOutputStream(File.Create(zipFullPath)))
                    {
                        zip.SetLevel(3);
                        byte[] buffer = new byte[4096];

                        foreach (string filePath in FilePaths)
                        {
                            if (File.Exists(filePath)) //only when file exists
                            {
                                i++;
                                ZipEntry entry = new ZipEntry(Path.GetFileName(@filePath));

                                entry.DateTime = DateTime.Now;
                                zip.PutNextEntry(entry);

                                using (FileStream fs = File.OpenRead(@filePath))
                                {
                                    int sourceBytes;
                                    do
                                    {
                                        sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                        zip.Write(buffer, 0, sourceBytes);

                                    } while (sourceBytes > 0);
                                }
                            }
                        }
                        zip.Finish();
                        zip.Close();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }

                return i > 0 ? zipFullPath : string.Empty;
            }
        #endregion
         


      
    }
}