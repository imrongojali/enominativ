﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_Config
{
    public class MasterConfigDashboardSearchParamViewModel
    {
        public string ConfigKey { get; set; } 
        public string ConfigValue { get; set; }
       
    }
}