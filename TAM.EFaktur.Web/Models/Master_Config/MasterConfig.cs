﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Master_Config
{
    public class MasterConfig : BaseModel
    {
       
        public string ConfigKey { get; set; }
        public string ConfigValue { get; set; }

    }
}
