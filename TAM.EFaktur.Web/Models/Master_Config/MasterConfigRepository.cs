﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Master_Config
{
    public class MasterConfigRepository : BaseRepository
    {
        #region Singletonh
        private MasterConfigRepository() { }
        private static MasterConfigRepository instance = null;
        public static MasterConfigRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MasterConfigRepository();
                }
                return instance;
            }
        }
        #endregion

        //#region Counter Data
        ////public int Count(string ConfigName, string configValue)
        //public int Count(VATInDashboardSearchParamViewModel model)
        //{
        //    dynamic args = new
        //    {
        //        ConfigName = ConfigName,
        //        configValue = configValue,

        //    };

        //    return db.SingleOrDefault<int>("Config/countApplications", args); 
        //}
        //#endregion

        #region Counter Data
        public int Count(MasterConfigDashboardSearchParamViewModel model)
        {
            dynamic args = new
            {

                ConfigKey = model.ConfigKey,
                ConfigValue = model.ConfigValue
            };

            return db.SingleOrDefault<int>("Master_Config/usp_CountMasterConfigListDashboard", args);
        }
        #endregion
        #region Processing Data

        public List<MasterConfigDashboardViewModel> GetList(MasterConfigDashboardSearchParamViewModel model, int SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            dynamic args = new
            {
                ConfigKey = model.ConfigKey,
                ConfigValue = model.ConfigValue,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
            IEnumerable<MasterConfigDashboardViewModel> result = db.Query<MasterConfigDashboardViewModel>("Master_Config/usp_GetConfigListDashboard", args);
            return result.ToList();
        }
        #endregion

        public Config GetById(Guid Id)
        {
            return db.SingleOrDefault<Config>("Master_Config/usp_GetConfigurationDataById", new { Id = Id });
        }

        public Config GetByConfigKey(String ConfigKey)
        {
            return db.SingleOrDefault<Config>("Master_Config/usp_GetConfigurationDataByConfigKey", new { ConfigKey = ConfigKey });
        }

        

        //insert Data
        public Result Insert(MasterConfigCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    Id = Guid.NewGuid(),
                    ConfigKey = model.ConfigKey,
                    ConfigValue = model.ConfigValue,
                    EventDate = EventDate.FormatSQLDateTime(),
                    EventActor = EventActor
                };
                db.Execute("Master_Config/usp_Insert_TB_M_ConfigurationData", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        //Update Data
        public Result Update(MasterConfigCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    Id = model.Id,
                    ConfigKey = model.ConfigKey,
                    ConfigValue = model.ConfigValue,
                    EventDate = EventDate.FormatSQLDateTime(),
                    EventActor = EventActor,

                };
                db.Execute("Master_Config/usp_Update_TB_M_ConfigurationData", args);
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        //Delete Data
        public Result Delete(MasterConfigCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    Id = model.Id,
                    EventDate = EventDate.FormatSQLDate(),
                    EventActor = EventActor

                };
                db.Execute("Master_Config/usp_Delete_TB_M_ConfigurationData", args);
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }
        //
        public Result Save(Config objConfig, DateTime EventDate, string EventActor)
        {
            try
            {
                dynamic args = new
                {
                    Id = objConfig.Id,
                    ConfigKey = objConfig.ConfigKey,
                    ConfigValue = objConfig.ConfigValue,
                    RowStatus = objConfig.RowStatus,
                    EventDate = EventDate,
                    EventActor = EventActor
                };
                db.Execute("Config/usp_SaveConfig", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        public Result Delete(Guid Id)
        {
            try
            {
                db.Execute("Config/usp_DeleteConfigByID", new { Id = Id });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }
        //#endregion

    }
}
