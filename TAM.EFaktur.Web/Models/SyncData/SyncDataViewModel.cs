﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.SyncData
{
    public class SyncDataViewModel
    {
        public string SyncType { get; set; }
        public string BussinessUnit { get; set; }
        public DateTime SyncTime { get; set; }
        public string FileUrl { get; set; }
        public string FileName { get; set; }
 

        public SyncDataViewModel CreateModel(string SyncType, string BusinessUnit, string FileUrl = "", string FileName = "")
        {
            SyncDataViewModel model = new SyncDataViewModel
            {
                SyncType = SyncType,
                BussinessUnit = BusinessUnit,
                SyncTime = DateTime.Now,
                FileUrl = FileUrl,
                FileName = FileName

            };
            return model;


        }

        public dynamic MapFromModel(Guid SyncId, DateTime EventDate, string EventActor)
        {
            dynamic argsSyncData = new
            {
                Id = SyncId,
                SyncType = this.SyncType,
                BussinessUnit = this.BussinessUnit,
                SyncTime = this.SyncTime.FormatSQLDateTime(),
                FileUrl = this.FileUrl,
                FileName = this.FileName,
                CreatedOn = EventDate.FormatSQLDateTime(),
                CreatedBy = EventActor
            };

            return argsSyncData;
        }

        public dynamic MapFromModelCreate(Guid SyncId, DateTime EventDate, string EventActor)
        {
            dynamic argsSyncData = new
            {
                Id = SyncId,
                SyncType = this.SyncType,
                BussinessUnit = this.BussinessUnit,
                SyncTime = this.SyncTime.FormatSQLDateTime(),
                FileUrl = this.FileUrl,
                FileName = this.FileName,
                CreatedOn = EventDate.FormatSQLDateTime(),
                CreatedBy = EventActor
            };

            return argsSyncData;
        }
    }
}