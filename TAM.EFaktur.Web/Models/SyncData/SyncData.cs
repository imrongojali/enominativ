﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.SyncData
{
    public class SyncData : BaseModel
    {
        public string SyncType { get; set; }
        public string BussinessUnit { get; set; }
        public DateTime SyncTime { get; set; }
        public string FileUrl { get; set; }
        public string FileName { get; set; }
    }
}