﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_User
{
    public class UserUpdateViewModel
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public Guid DivisionId { get; set; }
        public Guid RoleId { get; set; }
        public DateTime EventDate { get; set; }
        public string EventActor { get; set; }
        public Guid UserId { get; set; }
    }
}