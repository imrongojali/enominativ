﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Master_User
{
    public class User : BaseModel
    {
       
        public string Username { get; set; }
        public string Email { get; set; }
        public Guid DivisionId { get; set; }
        
        

    }
}
