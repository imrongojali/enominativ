﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.VATOutSalesReturn
{
    public class VATOutSalesReturn : BaseModel
    {
        public System.Guid Id { get; set; }
        public System.Guid SyncId { get; set; }
        public string DANumber { get; set; }
        public string BusinessUnit { get; set; }
        public string CADACNumber { get; set; }
        public string NPWPCustomer { get; set; }
        public string NamaCustomer { get; set; }
        public System.Guid KDJenisTransaksiID { get; set; }
        public string KDJenisTransaksi { get; set; }
        public string FGPengganti { get; set; }
        public string NomorFaktur { get; set; }
        public string NomorFakturGabungan { get; set; }
        public System.DateTime TanggalFaktur { get; set; }
        public string NomorDokumenRetur { get; set; }
        public System.DateTime TanggalRetur { get; set; }
        public int MasaPajakRetur { get; set; }
        public int TahunPajakRetur { get; set; }
        public decimal JumlahReturDPP { get; set; }
        public decimal JumlahReturPPN { get; set; }
        public decimal JumlahReturPPNBM { get; set; }
        public string DownloadStatus { get; set; }
        public string ApprovalStatus { get; set; }
        public Nullable<System.DateTime> ApprovalTime { get; set; }
        public string PdfUrl { get; set; }
        public string BatchFileName { get; set; }
        public bool DeleteStatus { get; set; }
        public bool IsApproveOrReject { get; set; }

        //add field 
        public string NamaBarang { get; set; }
        public string FrameNo { get; set; }
        public string EngineNo { get; set; }
        public string ModelType { get; set; }
        public string PpnbmLiniSebelumnya { get; set; }
        public string HargaSatuan { get; set; }
        public string JumlahBarang { get; set; }
        public string RK { get; set; }
        public decimal HargaTotal { get; set; }
        public decimal Diskon { get; set; }

        // CR 
        public string KodeObjek { get; set; }
        public string Alamat { get; set; }

        public dynamic MapFromModel(DateTime? EventDate = null, string EventActor = "")
        {
            return new
            {
                Id = this.Id,
                SyncId = this.SyncId,
                DANumber = this.DANumber,
                BusinessUnit = this.BusinessUnit,
                CADACNumber = this.CADACNumber,
                NPWPCustomer = this.NPWPCustomer,
                NamaCustomer = this.NamaCustomer,
                KDJenisTransaksiID = this.KDJenisTransaksiID,
                KDJenisTransaksi = this.KDJenisTransaksi,
                FGPengganti = this.FGPengganti,
                NomorFaktur = this.NomorFaktur,
                NomorFakturGabungan = this.NomorFakturGabungan,
                TanggalFaktur = this.TanggalFaktur,
                NomorDokumenRetur = this.NomorDokumenRetur,
                TanggalRetur = this.TanggalRetur,
                MasaPajakRetur = this.MasaPajakRetur,
                TahunPajakRetur = this.TahunPajakRetur,
                JumlahReturDPP = this.JumlahReturDPP,
                JumlahReturPPN = this.JumlahReturPPN,
                JumlahReturPPNBM = this.JumlahReturPPNBM,
                DownloadStatus = string.IsNullOrEmpty(this.DownloadStatus) ? Formatting.NOT_AVAILABLE : this.DownloadStatus,
                ApprovalStatus = string.IsNullOrEmpty(this.ApprovalStatus) ? Formatting.NOT_AVAILABLE : this.ApprovalStatus,
                ApprovalTime = this.ApprovalTime,
                PdfUrl = this.PdfUrl,
                BatchFileName = this.BatchFileName,
                DeleteStatus = this.DeleteStatus,
                IsApproveOrReject = this.IsApproveOrReject,
                RowStatus = this.RowStatus,
                //CreatedBy = this.CreatedBy,
                CreatedBy = "System",
                CreatedOn = this.CreatedOn.HasValue ? this.CreatedOn.Value.FormatSQLDateTime() : DateTime.Now.FormatSQLDateTime(),
                //add field
                NamaBarang = this.NamaBarang,
                FrameNo = this.FrameNo,
                EngineNo = this.EngineNo,
                ModelType = this.ModelType,
                PpnbmLiniSebelumnya = this.PpnbmLiniSebelumnya,
                HargaSatuan = this.HargaSatuan,
                JumlahBarang = this.JumlahBarang,
                HargaTotal = this.HargaTotal,
                Diskon = this.Diskon,
                //CR
                KodeObjek=this.KodeObjek,
                Alamat=this.Alamat,
            };
        }
    }
}
