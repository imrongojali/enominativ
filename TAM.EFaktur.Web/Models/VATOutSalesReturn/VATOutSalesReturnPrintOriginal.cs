﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.VATOutSalesReturn
{
    public class VATOutSalesReturnPrintOriginal : BaseModel
    {
        //bawaansebelumdirubah
        //public System.Guid Id { get; set; }
        public Guid VATOutSalesReturnIdList { get; set; }
        public string Nomor { get; set; }
        public string NamaCustomer { get; set; }
        public string Tanggal_Retur { get; set; }
        public string NPWPCustomer { get; set; }
        public string Alamat_Pembeli { get; set; }
        public string Jumlah_Harga_Jual { get; set; }
        public string PPn { get; set; }
        public string PPNBM { get; set; }
        public string harga_satuan { get; set; }
        public string Harga_Total { get; set; }
        public string jumlah_Barang { get; set; }
        public string Nama_Barang { get; set; }
        public string Faktur_pajak { get; set; }
        public string no_pengukuhan_pt { get; set; }
        public string Tanggal { get; set; }
        public string indexalamat2 { get; set; }
        public string indexalamat3 { get; set; }

        public string CreditNoteTTD { get; set; }
        public string CreditNoteOriginal { get; set; }
        public string  CreditNoteDGT { get; set; }


    } 
}
