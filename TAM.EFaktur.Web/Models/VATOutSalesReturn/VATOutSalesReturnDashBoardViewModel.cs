﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.VATOutSalesReturn
{
    public class VATOutSalesReturnDashBoardViewModel : Controller
    {
        public Guid Id { get; set; }
        public string TransactionDataFile { get; set; }
        public string CreditNoteNumber { get; set; }
        public DateTime ReturnDate { get; set; }
        public string ReturnTaxInvoiceNumber { get; set; }
        public string CANumber { get; set; }
        public string ReturnDA { get; set; }
        public DateTime DADate { get; set; }
        public string CustomerNPWP { get; set; }
        public string CustomerName { get; set; }
        public decimal ReturnVATBasedAmount { get; set; }
        public decimal ReturnVATAmount { get; set; }
        public string BusinessUnit { get; set; }
        public Nullable<DateTime> ReceiveFileDate { get; set; }
        public Nullable<DateTime> ReceiveFileTime { get; set; }
        public string RecordStatus { get; set; }
        public DateTime RecordDate { get; set; }
        public string DownloadStatus { get; set; }
        public Nullable<DateTime> DownloadDate { get; set; }
        public Nullable<DateTime> DownloadTime { get; set; }
        public string ApprovalStatus { get; set; }
        public DateTime ApprovalDate { get; set; }
        public string TaxInvoiceStatus { get; set; }
        public string TransitoryStatus { get; set; }
        public string TransitoryNumber { get; set; }
        public string TransitoryDate { get; set; }
        public string BatchFileName { get; set; }


    }
}
