﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models.Log_Sync;
using TAM.EFaktur.Web.Models.SyncData;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using TAM.EFaktur.Web.Models.Master_Config;
using System.Data;
using Toyota.Common.Web.Platform;
using Toyota.Common.Utilities;
using System.Text.RegularExpressions;

namespace TAM.EFaktur.Web.Models.VATOutSalesReturn
{
    public class VATOutSalesReturnRepository : BaseRepository
    {
        #region Singleton
        private VATOutSalesReturnRepository() { }

        private static VATOutSalesReturnRepository instance = null;

        public static VATOutSalesReturnRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new VATOutSalesReturnRepository();
                }
                return instance;
            }
        }

        #endregion
        #region Counter Data
        public int Count(VATOutSalesReturnSearchParamViewModel model, string DivisionName, int page, int size, string Id)
        {
            try
            {
                return db.SingleOrDefault<int>("VATOutSalesReturn/usp_CountVATOutSalesReturnListDashboard", model.MapFromModel(DivisionName, 0, null, page, size, Id));
            }
            catch
            {
                throw;
            }
        }

        #endregion
        #region Processing Data
        public List<Guid> GetIdBySearchParam(VATOutSalesReturnSearchParamViewModel model, string DivisionName)
        {
            IEnumerable<Guid> result = db.Query<Guid>("VATOutSalesReturn/usp_GetVATOutSalesReturnIdBySearchParam", model.MapFromModel(DivisionName));
            return result.ToList();
        }

        public List<VATOutSalesReturnOriginalViewModel> GetOriginalById(List<Guid> VATOutSalesReturnIdList)
        {
            dynamic args = new
            {
                VATOutSalesReturnIdList = string.Join(";", VATOutSalesReturnIdList)
            };
            IEnumerable<VATOutSalesReturnOriginalViewModel> result = db.Query<VATOutSalesReturnOriginalViewModel>("VATOutSalesReturn/usp_GetVATOutSalesReturnOriginalFileById", args);
            return result.ToList();
        }

        public List<VATOutSalesReturnReportExcelViewModel> GetReportExcelById(List<Guid> VATOutSalesReturnIdList)
        {
            dynamic args = new
            {
                VATOutSalesReturnIdList = string.Join(";", VATOutSalesReturnIdList)
            };
            IEnumerable<VATOutSalesReturnReportExcelViewModel> result = db.Query<VATOutSalesReturnReportExcelViewModel>("VATOutSalesReturn/usp_GetVATOutSalesReturnReportFileById", args);
            return result.ToList();
        }

        public List<VATOutSalesReturnCSVViewModel> GetCSVById(List<Guid> VATOutSalesReturnIdList)
        {
            dynamic args = new
            {
                VATOutSalesReturnIdList = string.Join(";", VATOutSalesReturnIdList)
            };
            IEnumerable<VATOutSalesReturnCSVViewModel> result = db.Query<VATOutSalesReturnCSVViewModel>("VATOutSalesReturn/usp_GetVATOutSalesReturnCSVById", args);
            return result.ToList();
        }

        public List<VATOutSalesReturnPrintOriginal> getlistVATreturn(List<Guid> VATOutSalesReturnIdList)
        {
            dynamic args = new
            {
                Id = string.Join(";", VATOutSalesReturnIdList)

            };
            IEnumerable<VATOutSalesReturnPrintOriginal> result = db.Query<VATOutSalesReturnPrintOriginal>("VATOutSalesReturn/usp_get_printVATOut_Ori", args);
            return result.ToList();
        }

        public Result UpdateFlagCSV(List<Guid> VATOutSalesReturnIdList, string FileUrl, string EventActor)
        {
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    VATOutSalesReturnIdList = string.Join(";", VATOutSalesReturnIdList),
                    BatchFileName = Path.GetFileName(@FileUrl)
                };
                db.Execute("VATOutSalesReturn/usp_UpdateCustom_TB_R_VATOutSalesReturn_FlagCSV", args);

                args = new
                {
                    Id = Guid.NewGuid(),
                    CsvType = "RK",
                    CsvTime = DateTime.Now.FormatSQLDateTime(),
                    BatchFileName = Path.GetFileName(@FileUrl),
                    FileUrl = FileUrl,
                    EventDate = DateTime.Now.FormatSQLDateTime(),
                    EventActor = EventActor
                };
                db.Execute("CSVData/usp_InsertCustom_TB_R_CsvData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                throw e;
            }

            return MsgResultSuccessInsert();
        }

        public List<string> GetPDFUrlById(List<Guid> VATOutSalesReturnIdList)
        {
            dynamic args = new
            {
                VATOutSalesReturnIdList = string.Join(";", VATOutSalesReturnIdList)
            };
            IEnumerable<string> result = db.Query<string>("VATOutSalesReturn/usp_GetVATOutSalesReturnPDFUrlById", args);
            return result.ToList();
        }

        public List<VATOutSalesReturnViewModel> GetList(VATOutSalesReturnSearchParamViewModel model, string DivisionName, int SortBy, string SortDirection, int FromNumber, int ToNumber, string Id)
        {
            IEnumerable<VATOutSalesReturnViewModel> result = db.Query<VATOutSalesReturnViewModel>("VATOutSalesReturn/usp_GetVATOutSalesReturnListDashboard", model.MapFromModel(DivisionName, SortBy, SortDirection, FromNumber, ToNumber, Id));
            return result.ToList();
        }

        //public List<VATOutSalesReturnViewModel> GetListAPI(VATOutSalesReturnGetViewAPI model)
        public List<VATOutSalesReturnPrintOriginal> GetListAPI(List<Guid> VATOutSalesReturnIdList)
        {
            dynamic args = new
            {
                Id = string.Join(";", VATOutSalesReturnIdList)

            };
            IEnumerable<VATOutSalesReturnPrintOriginal> result = db.Query<VATOutSalesReturnPrintOriginal>("VATOutSalesReturn/usp_get_printVATOut_Ori", args);
            return result.ToList();
        }

        public VATOutSalesReturn GetById(Guid Id)
        {
            return db.SingleOrDefault<VATOutSalesReturn>("VATOutSalesReturn/usp_GetVATOutSalesReturnByID", new { ID = Id });
        }

        public Result Save(VATOutSalesReturn objVATOutSalesReturn, DateTime EventDate, string EventActor)
        {
            try
            {
                db.Execute("VATOutSalesReturn/usp_Insert_TB_R_ReturnVATOut", objVATOutSalesReturn.MapFromModel());
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.LogToApp("Save VATOut", MessageType.ERR, EventActor).Message);
            }
        }

        public Result Delete(Guid Id, string EventActor)
        {
            try
            {
                db.Execute("VATOutSalesReturn/usp_DeleteReturnVATOutByID", new { ID = Id });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.LogToApp("Delete VATOut Sales Return", MessageType.ERR, EventActor).Message);
            }
        }

        public Result ExcelTaxInvoiceInsert(string path, string EventActor)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File has been uploaded successfully"
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                VATOutSalesReturn vatOutSalesReturn = new VATOutSalesReturn();
                var cellNum = 17; // 17 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();
                exceptionCellNum.Add(2); // index 2 means 3rd column

                string businessUnit = sheet.GetRow(4).GetCell(1).StringCellValue.ToString().Trim(); //first row
                //string businessUnit = "Motor Vehicle";

                #region Master - SyncData
                db.BeginTransaction();
                Guid SyncId = Guid.NewGuid(); //generate new Guid for Id - SyncData, to be referenced di VAT dan SyncDataDetail

                dynamic argsSyncData = new SyncDataViewModel() //insert SyncData paling pertama, karena dibutuhkan sebagai reference untuk data VAT dan SyncDataDetail
                    .CreateModel("RK", businessUnit, path, Path.GetFileName(@path))
                    .MapFromModelCreate(SyncId, DateTime.Now, EventActor);

                db.Execute("SyncData/usp_Insert_TB_R_SyncData", argsSyncData);

                List<dynamic> argsSyncDataDetailList = new List<dynamic>(); //Declare list di awal untuk syncDataDetail agar menghemat performance
                db.CommitTransaction();
                #endregion

                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();
                List<string> failNomorFaktur = new List<string>();

                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    #region Set Cell Policy, Cell Type and Save Sync Data
                    pipedString = string.Empty;
                    for (int i = 0; i < cellNum; i++)
                    {
                        sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                        pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                    }
                    pipedString = pipedString.Remove(pipedString.Length - 1);

                    #endregion

                    db.BeginTransaction();

                    try
                    {
                        //comenttesedologeror
                        #region Detail - SyncDataDetail Populate List
                        argsSyncDataDetailList.Add(new SyncDataDetailViewModel().CreateModel(row - 3, pipedString).MapFromModelCreate(SyncId, DateTime.Now, EventActor));
                        #endregion

                        if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        {
                            #region Cell Mapping & Insert to DB
                            vatOutSalesReturn = new VATOutSalesReturn();
                            vatOutSalesReturn.Id = Guid.NewGuid();
                            vatOutSalesReturn.SyncId = SyncId;
                            vatOutSalesReturn.DANumber = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim(); //kolom 1
                            vatOutSalesReturn.BusinessUnit = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim(); //kolom 2
                            vatOutSalesReturn.CADACNumber = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim(); //kolom 3
                            vatOutSalesReturn.NPWPCustomer = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim().FormatNPWP(); //kolom 5
                            vatOutSalesReturn.NamaCustomer = sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim(); //kolom 6
                            vatOutSalesReturn.KDJenisTransaksi = sheet.GetRow(row).GetCell(6).StringCellValue.ToString().Trim(); //kolom 7
                            vatOutSalesReturn.FGPengganti = sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim(); //kolom 8
                            vatOutSalesReturn.NomorFaktur = sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim(); //kolom 9
                            vatOutSalesReturn.NomorFakturGabungan = vatOutSalesReturn
                                .NomorFaktur
                                .FormatNomorFakturGabungan(vatOutSalesReturn.KDJenisTransaksi, vatOutSalesReturn.FGPengganti);

                            var tgl_faktur_split = sheet.GetRow(row).GetCell(9).StringCellValue.ToString().Trim().Split('/'); //kolom 10
                            if (tgl_faktur_split.Count() > 1) // when the excel column is a text type
                            {
                                vatOutSalesReturn.TanggalFaktur = DateTime
                                    .ParseExact(sheet.GetRow(row).GetCell(9).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            else // when the excel column is a DateTime type
                            {
                                vatOutSalesReturn.TanggalFaktur = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(9).StringCellValue.ToString().Trim()));
                            }

                            vatOutSalesReturn.NomorDokumenRetur = sheet.GetRow(row).GetCell(10).StringCellValue.ToString().Trim(); //kolom 11

                            var tgl_retur_split = sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim().Split('/'); //kolom 12
                            if (tgl_retur_split.Count() > 1) // when the excel column is a text type
                            {
                                vatOutSalesReturn.TanggalRetur = DateTime
                                    .ParseExact(sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            else // when the excel column is a DateTime type
                            {
                                vatOutSalesReturn.TanggalRetur = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim()));
                            }

                            vatOutSalesReturn.MasaPajakRetur = Convert.ToInt32(sheet.GetRow(row).GetCell(12).StringCellValue.ToString().Trim()); //kolom 13
                            vatOutSalesReturn.TahunPajakRetur = Convert.ToInt32(sheet.GetRow(row).GetCell(13).StringCellValue.ToString().Trim()); //kolom 14
                            vatOutSalesReturn.JumlahReturDPP = Convert.ToDecimal(sheet.GetRow(row).GetCell(14).StringCellValue.ToString().Trim()); //kolom 15
                            string jmlDPP = vatOutSalesReturn.JumlahReturDPP.ToString();
                            if ((jmlDPP.Contains(".") || (jmlDPP.Contains(","))))//String contain " . or , "
                            {
                                throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                            }
                            vatOutSalesReturn.JumlahReturPPN = Convert.ToDecimal(sheet.GetRow(row).GetCell(15).StringCellValue.ToString().Trim()); //kolom 16
                            string jmlPPN = vatOutSalesReturn.JumlahReturPPN.ToString();
                            if ((jmlPPN.Contains(".") || (jmlPPN.Contains(","))))//String contain " . or , "
                            {
                                throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                            }
                            vatOutSalesReturn.JumlahReturPPNBM = Convert.ToDecimal(sheet.GetRow(row).GetCell(16).StringCellValue.ToString().Trim()); //kolom 17
                            string jmlPPNBM = vatOutSalesReturn.JumlahReturPPNBM.ToString();
                            if ((jmlPPNBM.Contains(".") || (jmlPPNBM.Contains(","))))//String contain " . or , "
                            {
                                throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                            }


                            vatOutSalesReturn.CreatedBy = EventActor;
                            vatOutSalesReturn.CreatedOn = DateTime.Now;
                            vatOutSalesReturn.RowStatus = false;

                            // Save VAT Out Sales Return
                            result = db.SingleOrDefault<Result>("VATOutSalesReturn/usp_Insert_TB_R_ReturnVATOut", vatOutSalesReturn.MapFromModel(DateTime.Now, EventActor));
                            if (!result.ResultCode)
                            {
                                throw new Exception(result.ResultDesc);
                            }
                            db.CommitTransaction();

                            logSyncList.Add(new LogSyncCreateUpdate()
                                .CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.INF, ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path));
                            rowSuccess++;
                            #endregion
                        }
                        else
                        {
                            throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        }
                    }
                    catch (Exception e)
                    {
                        db.AbortTransaction();
                        var logeror = e.Message;
                        logSyncList.Add(new LogSyncCreateUpdate().CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.ERR, e.Message, path));
                        rowFail++;
                    }
                }

                #region SyncDataDetail Insert
                db.BeginTransaction();
                foreach (dynamic argsSyncDataDetail in argsSyncDataDetailList)
                {
                    db.Execute("SyncData/usp_Insert_TB_R_SyncDataDetail", argsSyncDataDetail);
                }
                db.CommitTransaction();
                #endregion

                #region SyncLog Insert
                db.BeginTransaction();
                ExceptionHandler.LogToSync(logSyncList);
                db.CommitTransaction();
                #endregion

                result.ResultCode = true;
                result.ResultDesc = Formatting
                    .UPLOAD_NOTIFICATION
                    .Replace("@success", (rowSuccess).ToString())
                    .Replace("@error", (rowFail).ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        public Result ExcelTaxInvoiceInsertLexus(string path, string EventActor)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File has been uploaded successfully"
            };
            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");
            string pipedString = string.Empty;

            string _DANumber = string.Empty;
            string _BusinessUnit = string.Empty;
            //string _CADACNumber = string.Empty;
            string _RK = string.Empty;
            string _NPWPCustomer = string.Empty;

            string _NamaCustomer = string.Empty;
            string _KDJenisTransaksi = string.Empty;
            string _FGPengganti = string.Empty;
            string _NomorFaktur = string.Empty;

            string _NomorFakturGabungan = string.Empty;
            string _tgl_faktur_split = string.Empty;
            string _NomorDokumenRetur = string.Empty;
            string _TanggalRetur = string.Empty;
            string _MasaPajakRetur = string.Empty;
            string _TahunPajakRetur = string.Empty;

            string _NamaBarang = string.Empty;
            string _FrameNo = string.Empty;
            string _EngineNo = string.Empty;
            string _ModelType = string.Empty;

            string _HargaSatuan = string.Empty;
            string _JumlahBarang = string.Empty;
            string _HargaTotal = string.Empty;
            string _Diskon = string.Empty;

            string _JumlahReturDPP = string.Empty;
            string _JumlahReturPPN = string.Empty;
            string _JumlahReturPPNBM = string.Empty;
            string _PpnbmLiniSebelumnya = string.Empty;

            //CR//
            string _Alamat = string.Empty;
            string _KodeObjek= string.Empty;


            string _validation = string.Empty;
            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                VATOutSalesReturn vatOutSalesReturn = new VATOutSalesReturn();
                VATOutSalesReturn Bu = new VATOutSalesReturn();
                var cellNum = 28; // 17 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();
                exceptionCellNum.Add(2); // index 2 means 3rd column
                                         // exceptionCellNum.Add(2); // index 2 means 3rd column
                string businessUnit = sheet.GetRow(4).GetCell(1).StringCellValue.ToString().Trim();

                #region Master - SyncData
                db.BeginTransaction();

                Guid SyncId = Guid.NewGuid(); //generate new Guid for Id - SyncData, to be referenced di VAT dan SyncDataDetail
                dynamic argsSyncData = new SyncDataViewModel() //insert SyncData paling pertama, karena dibutuhkan sebagai reference untuk data VAT dan SyncDataDetail
                    .CreateModel("RK", businessUnit, path, Path.GetFileName(@path))
                    .MapFromModelCreate(SyncId, DateTime.Now, EventActor);
                db.Execute("SyncData/usp_Insert_TB_R_SyncData", argsSyncData);


                List<dynamic> argsSyncDataDetailList = new List<dynamic>(); //Declare list di awal untuk syncDataDetail agar menghemat performance
                //db.CommitTransaction();
                #endregion
                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();
                List<string> failNomorFaktur = new List<string>();
                List<VATOutSalesReturn> listVATOutSalesReturn = new List<VATOutSalesReturn>();
                bool isTransactionValid = true;
                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNum; i++)
                        {
                            sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                            pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        #endregion

                        _validation = "";

                        #region Detail - SyncDataDetail Populate List
                        //argsSyncDataDetailList.Add(new SyncDataDetailViewModel().CreateModel(row - 3, pipedString).MapFromModel(SyncId, DateTime.Now, EventActor));
                        #endregion

                        //if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        //{
                        #region Cell Mapping & Insert to DB
                        vatOutSalesReturn = new VATOutSalesReturn();
                        vatOutSalesReturn.Id = Guid.NewGuid();
                        vatOutSalesReturn.SyncId = SyncId;
                        _DANumber = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim(); //kolom 0

                        if (string.IsNullOrEmpty(_DANumber))
                        {
                            _validation = _validation + "DA_NUMBER can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_DANumber))
                        {
                            vatOutSalesReturn.DANumber = _DANumber;
                        }

                        _BusinessUnit = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim(); //kolom 1
                        if (string.IsNullOrEmpty(_BusinessUnit))
                        {
                            _validation = _validation + "BUSINESS_UNIT can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_BusinessUnit))
                        {
                            vatOutSalesReturn.BusinessUnit = _BusinessUnit;
                        }
                        vatOutSalesReturn.CADACNumber = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim(); //kolom 2
                        _RK = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim(); //kolom 3
                        if (string.IsNullOrEmpty(_RK))
                        {
                            _validation = _validation + "RK can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_RK))
                        {
                            vatOutSalesReturn.RK = _RK;
                        }

                        _NPWPCustomer = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim().FormatNPWP(); //kolom 4
                        if (string.IsNullOrEmpty(_NPWPCustomer))
                        {
                            _validation = _validation + "NPWP can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_NPWPCustomer))
                        {
                            vatOutSalesReturn.NPWPCustomer = _NPWPCustomer;
                        }
                        _NamaCustomer = sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim(); //kolom 5
                        if (string.IsNullOrEmpty(_NamaCustomer))
                        {
                            _validation = _validation + "NAMA can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_NamaCustomer))
                        {
                            vatOutSalesReturn.NamaCustomer = _NamaCustomer;
                        }

                        //CR//
                        _Alamat = sheet.GetRow(row).GetCell(6).StringCellValue.ToString().Trim(); //kolom 6
                        if (string.IsNullOrEmpty(_Alamat))
                        {
                            _validation = _validation + "ALAMAT can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_Alamat))
                        {
                            vatOutSalesReturn.Alamat = _Alamat;
                        }

                        _KDJenisTransaksi = sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim(); //kolom 7
                        if (string.IsNullOrEmpty(_KDJenisTransaksi))
                        {
                            _validation = _validation + "KD_JENIS_TRANSAKSI can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_KDJenisTransaksi))
                        {
                            vatOutSalesReturn.KDJenisTransaksi = _KDJenisTransaksi;
                        }

                        _FGPengganti = sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim(); //kolom 8
                        if (string.IsNullOrEmpty(_FGPengganti))
                        {
                            _validation = _validation + "FG_PENGGANTI can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_FGPengganti))
                        {
                            vatOutSalesReturn.FGPengganti = _FGPengganti;
                        }

                        _NomorFaktur = sheet.GetRow(row).GetCell(9).StringCellValue.ToString().Trim(); //kolom 9
                        if (string.IsNullOrEmpty(_NomorFaktur))
                        {
                            _validation = _validation + "NOMOR_FAKTUR can't be empty, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_NomorFaktur))
                        {
                            vatOutSalesReturn.NomorFaktur = _NomorFaktur;
                        }
                        vatOutSalesReturn.NomorFakturGabungan = vatOutSalesReturn
                                .NomorFaktur
                                .FormatNomorFakturGabungan(vatOutSalesReturn.KDJenisTransaksi, vatOutSalesReturn.FGPengganti);

                        var tgl_faktur_split = sheet.GetRow(row).GetCell(10).StringCellValue.ToString().Trim().Split('/'); //kolom 10
                        _tgl_faktur_split = sheet.GetRow(row).GetCell(10).StringCellValue.ToString().Trim();
                        if (string.IsNullOrEmpty(_tgl_faktur_split))
                        {
                            _validation = _validation + "TANGGAL_FAKTUR can't be empty, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_tgl_faktur_split))
                        {
                            var tglfaktur = _tgl_faktur_split.Split('/');
                            if (tglfaktur.Count() > 1) // when the excel column is a text type
                            {
                                if (tglfaktur[0].Length == 2 && tglfaktur[1].Length == 2 && tglfaktur[2].Length == 4)
                                {
                                    vatOutSalesReturn.TanggalFaktur = DateTime
                                   .ParseExact(sheet.GetRow(row).GetCell(10).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    //vatOutSalesReturn.TanggalFaktur = DateTime.ParseExact(tglfaktur, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    _validation = _validation + "TANGGAL_FAKTUR|TANGGAL_RETUR format date must be dd/mm/yyy";
                                    // throw new Exception("Date Format must be: dd/mm/yyyy");
                                }
                            }
                            else // when the excel column is a DateTime type
                            {
                                //vatOutSalesReturn.TanggalFaktur = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(9).StringCellValue.ToString().Trim()));
                                _validation = _validation + "TANGGAL_FAKTUR|TANGGAL_RETUR format date must be dd/mm/yyy";
                            }
                        }
                        _NomorDokumenRetur = sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim(); //kolom 11
                        if (string.IsNullOrEmpty(_NomorDokumenRetur))
                        {
                            _validation = _validation + "NOMOR_FAKTUR can't be empty, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_NomorDokumenRetur))
                        {
                            vatOutSalesReturn.NomorDokumenRetur = _NomorDokumenRetur;
                        }

                        _TanggalRetur = sheet.GetRow(row).GetCell(12).StringCellValue.ToString().Trim();
                        if (string.IsNullOrEmpty(_TanggalRetur))
                        {
                            _validation = _validation + "TANGGAL_RETUR can't be empty, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_TanggalRetur))
                        {
                            var tgl_retur_split = _TanggalRetur.Split('/');
                            if (tgl_retur_split.Count() > 1) // when the excel column is a text type
                            {

                                if (tgl_retur_split[0].Length == 2 && tgl_retur_split[1].Length == 2 && tgl_retur_split[2].Length == 4)
                                {
                                    vatOutSalesReturn.TanggalRetur = DateTime
                                   .ParseExact(sheet.GetRow(row).GetCell(12).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    _validation = _validation + "TANGGAL_FAKTUR|TANGGAL_RETUR format date must be dd/mm/yyy";
                                    // throw new Exception("Date Format must be: dd/mm/yyyy");
                                }
                            }
                            else // when the excel column is a DateTime type
                            {
                                //vatOutSalesReturn.TanggalRetur = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim()));
                                _validation = _validation + "TANGGAL_FAKTUR|TANGGAL_RETUR format date must be dd/mm/yyy";
                            }
                        }

                        _MasaPajakRetur = sheet.GetRow(row).GetCell(13).StringCellValue.ToString().Trim();
                        if (string.IsNullOrEmpty(_MasaPajakRetur))
                        {
                            _validation = _validation + "MASA_PAJAK_RETUR can't be empy ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_MasaPajakRetur))
                        {
                            vatOutSalesReturn.MasaPajakRetur = Convert.ToInt32(_MasaPajakRetur);
                        }

                        _TahunPajakRetur = sheet.GetRow(row).GetCell(14).StringCellValue.ToString().Trim();//kolom 14
                        if (string.IsNullOrEmpty(_TahunPajakRetur))
                        {
                            _validation = _validation + "TAHUN_PAJAK_RETUR can't be empy, ";

                        }
                        else if (!string.IsNullOrEmpty(_TahunPajakRetur))
                        {
                            vatOutSalesReturn.TahunPajakRetur = Convert.ToInt32(_TahunPajakRetur);
                        }

                        //CR//
                        _KodeObjek = sheet.GetRow(row).GetCell(15).StringCellValue.ToString().Trim(); //kolom 15
                        if (string.IsNullOrEmpty(_KodeObjek))
                        {
                            _validation = _validation + "KODE_OBJEK can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_KodeObjek))
                        {
                            vatOutSalesReturn.KodeObjek = _KodeObjek;
                        }
                        //

                        _NamaBarang = sheet.GetRow(row).GetCell(16).StringCellValue.ToString().Trim(); // kolom 16
                        if (string.IsNullOrEmpty(_NamaBarang))
                        {
                            _validation = _validation + "NAMA_BARANG can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_NamaBarang))
                        {
                            vatOutSalesReturn.NamaBarang = _NamaBarang;
                        }

                        _FrameNo = sheet.GetRow(row).GetCell(17).StringCellValue.ToString().Trim(); //kolom 17
                        if (string.IsNullOrEmpty(_FrameNo))
                        {
                            _validation = _validation + "FRAME_NO can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_FrameNo))
                        {
                            vatOutSalesReturn.FrameNo = _FrameNo;
                        }
                        _EngineNo = sheet.GetRow(row).GetCell(18).StringCellValue.ToString().Trim(); //kolom 18
                        if (string.IsNullOrEmpty(_EngineNo))
                        {
                            _validation = _validation + "ENGINE_NO can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_EngineNo))
                        {
                            vatOutSalesReturn.EngineNo = _EngineNo;
                        }
                        _ModelType = sheet.GetRow(row).GetCell(19).StringCellValue.ToString().Trim(); // kolom 19
                        if (string.IsNullOrEmpty(_ModelType))
                        {
                            _validation = _validation + "MODEL_TYPE can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_ModelType))
                        {
                            vatOutSalesReturn.ModelType = _ModelType;
                        }


                        _HargaSatuan = sheet.GetRow(row).GetCell(20).StringCellValue.ToString().Trim(); //kolom 20
                        if (string.IsNullOrEmpty(_HargaSatuan))
                        {


                            _validation = _validation + "HARGA_SATUAN can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_HargaSatuan))
                        {
                            if (_HargaSatuan.Contains(","))//String contain " . or , "
                            {
                                _validation = _validation + "HARGA_SATUAN cannot contains (','), ";
                            }
                            else
                            {
                                vatOutSalesReturn.HargaSatuan = Convert.ToString(Convert.ToDecimal(_HargaSatuan));
                            }
                            
                        }
                        _JumlahBarang = sheet.GetRow(row).GetCell(21).StringCellValue.ToString().Trim(); //kolom 21
                        if (string.IsNullOrEmpty(_JumlahBarang))
                        {
                            _validation = _validation + "JUMLAH_BARANG can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_JumlahBarang))
                        {
                            if (_JumlahBarang.Contains(","))//String contain " . or , "
                            {
                                _validation = _validation + "JUMLAH_BARANG cannot contains (','), ";
                            }
                            else
                            {
                                vatOutSalesReturn.JumlahBarang = Convert.ToString(Convert.ToDecimal(_JumlahBarang));
                            }
                          
                        }

                        _HargaTotal = sheet.GetRow(row).GetCell(22).StringCellValue.ToString().Trim(); //kolom 22
                        if (string.IsNullOrEmpty(_HargaTotal))
                        {
                            _validation = _validation + "HARGA_TOTAL can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_HargaTotal))
                        {
                            if (_HargaTotal.Contains(","))//String contain " . or , "
                            {
                                _validation = _validation + "HARGA_TOTAL cannot contains (','), ";
                            }
                            else
                            {
                                vatOutSalesReturn.HargaTotal = Convert.ToDecimal(_HargaTotal);
                            }
                           
                        }
                        _Diskon = sheet.GetRow(row).GetCell(23).StringCellValue.ToString().Trim();  //kolom 23
                        if (string.IsNullOrEmpty(_Diskon))
                        {
                            _validation = _validation + "Diskon can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_Diskon))
                        {
                            if ((_Diskon.Contains(",")))//String contain " . or , "
                            {
                                _validation = _validation + "Diskon cannot contains (','), ";
                            }
                            else
                            {
                                vatOutSalesReturn.Diskon = Convert.ToDecimal(_Diskon);
                            }
                            
                        }
                        //  vatOutSalesReturn.JumlahReturDPP = Convert.ToDecimal(sheet.GetRow(row).GetCell(22).StringCellValue.ToString().Trim()); //kolom 23
                        _JumlahReturDPP = sheet.GetRow(row).GetCell(24).StringCellValue.ToString().Trim(); //kolom 24
                        string jmlDPP = string.Empty;
                        if (string.IsNullOrEmpty(_JumlahReturDPP))
                        {
                            _validation = _validation + "NILAI_RETUR_DPP can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_JumlahReturDPP))
                        {
                            jmlDPP = _JumlahReturDPP;
                            if (jmlDPP.Contains(","))//String contain " . or , "
                            {
                                _validation = _validation + "NILAI_RETUR_DPP cannot contains (','), ";
                            }
                            else
                            {
                                vatOutSalesReturn.JumlahReturDPP = Convert.ToDecimal(_JumlahReturDPP);
                            }
                        }

                        _JumlahReturPPN = sheet.GetRow(row).GetCell(25).StringCellValue.ToString().Trim(); //kolom 25
                        string jmlPPN = string.Empty;
                        if (string.IsNullOrEmpty(_JumlahReturPPN))
                        {
                            _validation = _validation + "NILAI_RETUR_PPN can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_JumlahReturPPN))
                        {
                            jmlPPN = _JumlahReturPPN;
                            if (jmlPPN.Contains(","))//String contain " . or , "
                            {
                                _validation = _validation + "NILAI_RETUR_PPN cannot contains (','), ";
                            }
                            else
                            {
                                vatOutSalesReturn.JumlahReturPPN = Convert.ToDecimal(_JumlahReturPPN);
                            }
                        }

                        _JumlahReturPPNBM = sheet.GetRow(row).GetCell(26).StringCellValue.ToString().Trim(); //kolom 26
                        if (string.IsNullOrEmpty(_JumlahReturPPNBM))
                        {
                            _validation = _validation + "NILAI_RETUR_PPNBM can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_JumlahReturPPNBM))
                        {
                            string jmlPPNBM = _JumlahReturPPNBM;
                            if (jmlPPNBM.Contains(","))//String contain " . or , "
                            {
                                _validation = _validation + "NILAI_RETUR_PPNBM cannot contains (','), ";
                            }
                            else
                            {
                                vatOutSalesReturn.JumlahReturPPNBM = Convert.ToDecimal(_JumlahReturPPNBM);
                            }
                        }

                        _PpnbmLiniSebelumnya = getnumber(sheet.GetRow(row).GetCell(27).StringCellValue.ToString().Trim()); //kolom 27
                        if (string.IsNullOrEmpty(_PpnbmLiniSebelumnya))
                        {
                            _validation = _validation + "PPNBM_LINI_SEBELUMNYA can't be empy, ";
                            //throw new Exception("Status Harus di isi.");
                        }
                        else if (!string.IsNullOrEmpty(_PpnbmLiniSebelumnya))
                        {
                            if ((_PpnbmLiniSebelumnya.Contains(".") || (_PpnbmLiniSebelumnya.Contains(","))))//String contain " . or , "
                            {
                                _validation = _validation + "JUMLAH_BARANG cannot contains ( '.' or ','), ";
                            }
                            else
                            {
                                vatOutSalesReturn.PpnbmLiniSebelumnya = Convert.ToString(Convert.ToDecimal(_PpnbmLiniSebelumnya));
                            }
                           
                        }
                        vatOutSalesReturn.CreatedBy = EventActor;
                        vatOutSalesReturn.CreatedOn = DateTime.Now;
                        vatOutSalesReturn.RowStatus = false;

                        listVATOutSalesReturn.Add(vatOutSalesReturn);
                        if (!string.IsNullOrEmpty(_validation.Trim()))
                        {
                            throw new Exception(_validation.Trim());
                        }
                        result = db.SingleOrDefault<Result>("VATOutSalesReturn/usp_Validation_TB_R_ReturnVATOut", vatOutSalesReturn.MapFromModel(DateTime.Now, EventActor));
                        if (!result.ResultCode)
                        {
                            throw new Exception(result.ResultDesc);
                        }
                        else
                        {
                            ExceptionHandler.LogToSync(row - 3, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE_Validation, path, MessageType.INF);
                            rowSuccess++;
                        }
                        #endregion
                    }
                    catch (Exception e)
                    {
                        isTransactionValid = false;
                        e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }

                    db.CommitTransaction();

                    if (rowFail > 0)
                    {
                        result.ResultCode = false;
                        result.ResultDesc = Formatting.UPLOAD_NOTIFICATION_VALIDATION.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());
                    }
                    else
                    {
                        result.ResultCode = true;
                        result.ResultDesc = Formatting.UPLOAD_NOTIFICATION.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());
                    }
                }
                bool isSuccessRows = true;

                if (isTransactionValid)
                {
                    var queryGroup = (from p in listVATOutSalesReturn
                                      group p by new
                                      {
                                          p.NomorDokumenRetur,
                                          p.NomorFakturGabungan,
                                          p.TanggalRetur,

                                          p.TanggalFaktur,

                                          p.NamaCustomer,
                                          p.NPWPCustomer,
                                          p.JumlahReturDPP,
                                          p.JumlahReturPPN
                                      } into g
                                      select new
                                      {
                                          __NomorDokumenRetur = g.Key.NomorDokumenRetur,
                                          __NomorFakturGabungan = g.Key.NomorFakturGabungan,
                                          __TanggalRetur = g.Key.TanggalRetur,

                                          __TanggalFaktur = g.Key.TanggalFaktur,

                                          __NamaCustomer = g.Key.NamaCustomer,
                                          __NPWPCustomer = g.Key.NPWPCustomer,
                                          __JumlahReturDPP = g.Key.JumlahReturDPP,
                                          __JumlahReturPPN = g.Key.JumlahReturPPN,
                                          __SUMHargaTotal = (decimal)g.Sum(x => x.HargaTotal),
                                          __DPPValidation = ((decimal)g.Key.JumlahReturDPP == (decimal)g.Sum(x => x.HargaTotal)) ? 0 : 1,
                                          _SUMHargaPPnbm = (decimal)g.Sum(x => x.JumlahReturPPNBM),
                                          Children = g.ToList()
                                          //AvgGoldPrice = (int)g.Average(x => x.GoldPrice),
                                          //AvgSilverPrice = (int)g.Average(x => x.SilverPrice)
                                      }).ToList();

                    var queryDPPValidation = from dpp in queryGroup
                                             where dpp.__DPPValidation == 1
                                             select dpp;
                    if (queryDPPValidation.Count() == 0)
                    {
                        isSuccessRows = true;
                        db.BeginTransaction();
                        bool success = true;
                        try
                        {
                            for (int i = 0; i < queryGroup.Count(); i++)
                            {
                                var data = queryGroup[i];
                                //  result = db.SingleOrDefault<Result>("VATOutSalesReturn/usp_Insert_TB_R_ReturnVATOut", data.MapFromModel(DateTime.Now, EventActor));
                                Config PdfPath = MasterConfigRepository.Instance.GetByConfigKey("UrlTempVATOutSalesReturPdf");
                                var fullPathpdf = PdfPath.ConfigValue;
                                //  var fullPathpdf = @"D:\Program\TAM\TAM V2\Template_Original.pdf";
                                PdfDocument pdfDocument = PdfReader.Open(fullPathpdf, PdfDocumentOpenMode.Modify);
                                PdfPage page = pdfDocument.Pages[0];
                                XGraphics gfx = XGraphics.FromPdfPage(page);
                                XFont font = new XFont("Arial", 7);
                                XFont fontNo = new XFont("Arial", 8);
                                XFont fontHeader = new XFont("Arial", 9);
                                XFont fontbody = new XFont("Arial", 10);
                                decimal hargatotal = Convert.ToDecimal(data.__JumlahReturDPP);
                                decimal PPn = Convert.ToDecimal(data.__JumlahReturPPN);
                                decimal PPnbm = Convert.ToDecimal(data._SUMHargaPPnbm);

                                //string AlamatPem = _Alamat.Substring(1, 75);
                                //string AlamatPem2 = _Alamat.Substring(75, 150);


                                string ReturnNO = _DANumber.Substring(_DANumber.Length - 9);
                                string code = ReturnNO.Contains("B") ? "B" : "J";

                                var vATOutResult = db.SingleOrDefault<VATOut>("VATOutSalesReturn/usp_GetMasterComapny",
                                     new { Code = code });

                                var vATOutResultalamat = db.SingleOrDefault<VATOutAlamat>("VATOutSalesReturn/usp_GetVATOutByNpwp",
                                    new { NPWP = data.__NPWPCustomer });

                                string Tanggalfaktur = Convert.ToDateTime(data.__TanggalFaktur).ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
                                string TanggalRetur = Convert.ToDateTime(data.__TanggalRetur).ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));

                                string Tglreturgab = "," + " " + TanggalRetur;

                                gfx.DrawString(data.__NomorDokumenRetur, fontHeader, XBrushes.Black, new XRect(260, 32, page.Width, page.Height), XStringFormats.TopLeft);
                                gfx.DrawString(data.__NomorFakturGabungan, fontHeader, XBrushes.Black, new XRect(259, -342.5, page.Width, page.Height), XStringFormats.CenterLeft);
                                gfx.DrawString(Tanggalfaktur, fontHeader, XBrushes.Black, new XRect(392, 48.5, page.Width, page.Height), XStringFormats.TopLeft);

                                //Pembeli                                
                                gfx.DrawString(data.__NamaCustomer.ToUpper(), fontHeader, XBrushes.Black, new XRect(180, -308, page.Width, page.Height), XStringFormats.CenterLeft);
                                gfx.DrawString(data.__NPWPCustomer, fontHeader, XBrushes.Black, new XRect(180, -265, page.Width, page.Height), XStringFormats.CenterLeft);
                                // Penjual//
                                gfx.DrawString(vATOutResult.Nama_pt.ToUpper(), fontHeader, XBrushes.Black, new XRect(180, -228, page.Width, page.Height), XStringFormats.CenterLeft);
                                gfx.DrawString(vATOutResult == null ||
                                    string.IsNullOrEmpty(vATOutResult.alamat) ? "" : vATOutResult.alamat.ToUpper()
                                    , fontHeader, XBrushes.Black, new XRect(179, -213, page.Width, page.Height), XStringFormats.CenterLeft);
                                gfx.DrawString(vATOutResult == null ||
                                    string.IsNullOrEmpty(vATOutResult.alamat_2) ? "" : vATOutResult.alamat_2.ToUpper()
                                    , fontHeader, XBrushes.Black, new XRect(179, -203, page.Width, page.Height), XStringFormats.CenterLeft);
                                gfx.DrawString(vATOutResult.no_pengukuhan_pt, fontHeader, XBrushes.Black, new XRect(180, -179, page.Width, page.Height), XStringFormats.CenterLeft);

                                //----detail-
                                int BisUnit = 0;
                                for (int _i = 0; _i < data.Children.Count(); _i++)
                                {
                                    var dataDetail = data.Children[_i];
                                    if (dataDetail.BusinessUnit.StartsWith("Lexus"))
                                    {
                                        BisUnit += 1;
                                    }
                                    decimal hargasatuan = Convert.ToDecimal(dataDetail.HargaSatuan);
                                    decimal jumlahBarang = Convert.ToDecimal(dataDetail.JumlahBarang);
                                    decimal jmlbarangsatuan = hargasatuan * jumlahBarang;
                                    string rowData = Convert.ToString(_i + 1);

                                    gfx.DrawString(rowData, fontbody, XBrushes.Black, new XRect(18, -124 + (_i * 40), page.Width, page.Height), XStringFormats.CenterLeft);
                                    gfx.DrawString(dataDetail.FrameNo, fontbody, XBrushes.Black, new XRect(100, -124 + (_i * 40), page.Width, page.Height), XStringFormats.CenterLeft);
                                    //gfx.DrawString(_KodeObjek, fontbody, XBrushes.Black, new XRect(44, -124 + (_i * 40), page.Width, page.Height), XStringFormats.CenterLeft);
                                    gfx.DrawString(dataDetail.JumlahBarang, fontbody, XBrushes.Black, new XRect(250, -124 + (_i * 40), page.Width, page.Height), XStringFormats.CenterLeft);
                                    gfx.DrawString(hargasatuan.FormatViewCurrencyiNA(), fontbody, XBrushes.Black, new XRect(-190, 266 + (_i * 40), page.Width, page.Height), XStringFormats.TopRight);
                                    gfx.DrawString(jmlbarangsatuan.FormatViewCurrencyiNA(), fontbody, XBrushes.Black, new XRect(-20, 266 + (_i * 40), page.Width, page.Height), XStringFormats.TopRight);

                                    result = db.SingleOrDefault<Result>("VATOutSalesReturn/usp_Insert_TB_R_ReturnVATOut", dataDetail.MapFromModel(DateTime.Now, EventActor));
                                    if (result.ResultCode)
                                    {
                                        ExceptionHandler.LogToSync(row - 3, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path, MessageType.INF);
                                    }

                                    var vATOutResultName = db.SingleOrDefault<VATOutreturn>("VATOutSalesReturn/usp_GetNameBarang"
                                       , new { NomorDokumenRetur = data.__NomorDokumenRetur });

                                    gfx.DrawString(vATOutResultName.KodeObjek1, fontbody, XBrushes.Black, new XRect(44, -124 + (_i * 40), page.Width, page.Height), XStringFormats.CenterLeft);
                                    gfx.DrawString(vATOutResultName.KodeObjek2, fontbody, XBrushes.Black, new XRect(44, -114 + (_i * 40), page.Width, page.Height), XStringFormats.CenterLeft);

                                    gfx.DrawString(vATOutResultName.nama_barang1, fontbody, XBrushes.Black, new XRect(100, -113 + (_i * 40), page.Width, page.Height), XStringFormats.CenterLeft);
                                    gfx.DrawString(vATOutResultName.nama_barang2, fontbody, XBrushes.Black, new XRect(100, -103 + (_i * 40), page.Width, page.Height), XStringFormats.CenterLeft);

                                }
                                //---------
                                var vATOutResultgetalamat = db.SingleOrDefault<VATOutreturn>("VATOutSalesReturn/usp_GetNameBarang"
                                       , new { NomorDokumenRetur = data.__NomorDokumenRetur });

                                gfx.DrawString(vATOutResultgetalamat.AlamatPembeli.ToUpper(), fontHeader, XBrushes.Black, new XRect(179, -292, page.Width, page.Height), XStringFormats.CenterLeft);
                                gfx.DrawString(vATOutResultgetalamat.AlamatPembeli2.ToUpper(), fontHeader, XBrushes.Black, new XRect(179, -282, page.Width, page.Height), XStringFormats.CenterLeft);
                                gfx.DrawString(hargatotal.FormatViewCurrencyiNA(), fontbody, XBrushes.Black, new XRect(-20, 554.5, page.Width, page.Height), XStringFormats.TopRight);
                                gfx.DrawString(PPn.FormatViewCurrencyiNA(), fontbody, XBrushes.Black, new XRect(-20, 571.5, page.Width, page.Height), XStringFormats.TopRight);
                                gfx.DrawString(PPnbm.FormatViewCurrencyiNA(), fontbody, XBrushes.Black, new XRect(-20, 588.5, page.Width, page.Height), XStringFormats.TopRight);
                                gfx.DrawString(Tglreturgab, fontbody, XBrushes.Black, new XRect(-20, 617, page.Width, page.Height), XStringFormats.TopRight);

                                var nomordocretur = data.__NomorDokumenRetur;
                                Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateGenerateNotaVATOutSalesRetur");
                                var urlPdf = config.ConfigValue;
                                // var urlPdf = @"D:\Program\TAM\TAM V2\FilePDFUpload\";

                                if (data.Children.Count() == BisUnit)
                                {
                                    pdfDocument.Save(urlPdf + nomordocretur + ".pdf");
                                }
                            };
                        }
                        catch (Exception ex)
                        {
                            success = false;
                            db.AbortTransaction();
                            throw new Exception(ex.Message);
                        }

                        if (success)
                        {
                            foreach (dynamic argsSyncDataDetail in argsSyncDataDetailList)
                            //foreach (dynamic argsSyncDataDetail in logSyncList)
                            {
                                dynamic argNew = new
                                {
                                    Id = argsSyncDataDetail.Id,
                                    SyncId = argsSyncDataDetail.SyncId,
                                    RowId = argsSyncDataDetail.RowId,
                                    RowData = argsSyncDataDetail.RowData,
                                    SyncStatus = argsSyncDataDetail.SyncStatus,
                                    CreatedOn = argsSyncDataDetail.EventDate,
                                    CreatedBy = "SYSTEM"
                                };
                                result = db.Execute("SyncData/usp_Insert_TB_R_SyncDataDetail", argNew);
                                if (result.ResultCode)
                                {
                                    ExceptionHandler.LogToSync(row - 3, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE_DETAIL, path, MessageType.INF);
                                }
                            }

                            db.CommitTransaction();

                            result.ResultCode = true;
                            result.ResultDesc = Formatting.UPLOAD_NOTIFICATION.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());
                        }

                    }
                    else
                    {
                        isSuccessRows = false;
                        //argsSyncDataDetailList.Add(new SyncDataDetailViewModel().CreateModel(row + 1, "File Already Exist Data Excel").MapFromModel(SyncId, DateTime.Now, EventActor));
                        throw new Exception(ExceptionHandler.NILAI_RETUR_DPP_INVALID);
                    }
                }


            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }
        public string getnumber(string _nilai)
        {

            //string _nilai2 = string.Empty;
            //int val = 0;

            //for (int i = 0; i < _nilai.Length; i++)
            //{
            //    if (Char.IsDigit(_nilai[i]))
            //        _nilai2 += _nilai[i];
            //}
            //if (_nilai2.Length > 0)
            //    val = int.Parse(_nilai2);
            
            string _nilai2 = string.Empty;

            int valreturn = 0;
            var matches = Regex.Matches(_nilai, @"\d+");
            foreach (var match in matches)
            {
                _nilai2 += match;
            }
            valreturn = int.Parse(_nilai2);

            return Convert.ToString(valreturn);
        }
    public Result ExcelApprovalStatusUpdate(string path, string user)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = Formatting.UPLOAD_NOTIFICATION
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;


            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                string approvalTime = string.Empty;
                string _nomorDokumenRetur = string.Empty;
                string _approvalStatus = string.Empty;
                string _recordTime = string.Empty;
                string _taxInvoiceStatus = string.Empty;

                string _headerNomorDokumenRetur = string.Empty;
                string _headerApprovalStatus = string.Empty;
                string _headerApprovalTime = string.Empty;
                string _headerRecordTime = string.Empty;
                string _headerTaxInvoiceStatus = string.Empty;
                string _heder = string.Empty;

                bool isApproveSukses = false;

                file.Close();
                VATOutSalesReturnApprovalStatusViewModel vatOutReturnApprovalStatus = new VATOutSalesReturnApprovalStatusViewModel();
                var cellNum = 4; // 31 is the cell amount in one row
                //List<int> exceptionCellNum = new List<int>();
                //exceptionCellNum.Add(2);
                db.BeginTransaction();

                _headerNomorDokumenRetur = sheet.GetRow(cellNum - 1).GetCell(0).StringCellValue.ToString().Trim();
                _headerApprovalStatus = sheet.GetRow(cellNum - 1).GetCell(1).StringCellValue.ToString().Trim();
                _headerApprovalTime = sheet.GetRow(cellNum - 1).GetCell(2).StringCellValue.ToString().Trim();
                _headerRecordTime = sheet.GetRow(cellNum - 1).GetCell(3).StringCellValue.ToString().Trim();
                _headerTaxInvoiceStatus = sheet.GetRow(cellNum - 1).GetCell(4).StringCellValue.ToString().Trim();
                if (_headerNomorDokumenRetur != "CREDIT_NOTE_NUMBER")
                {
                    _heder += _headerNomorDokumenRetur + ", ";
                }
                if (_headerApprovalStatus != "APPROVAL_STATUS")
                {
                    _heder += _headerApprovalStatus + ", ";
                }
                if (_headerApprovalTime != "TANGGAL_APPROVAL")
                {
                    _heder += _headerApprovalTime + ", ";
                }
                if (_headerRecordTime != "TANGGAL_REKAM")
                {
                    _heder += _headerRecordTime + ", ";
                }
                if (_headerTaxInvoiceStatus != "STATUS_FAKTUR")
                {
                    _heder += _headerTaxInvoiceStatus + ", ";
                }
                if (!string.IsNullOrEmpty(_heder.Trim()))
                {
                    _heder = _heder.Remove(_heder.Length - 2);
                    throw new Exception(_heder.Trim() + " is not valid header template");
                }


                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template

                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNum; i++)
                        {
                            sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                            pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        #endregion

                        //if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        //{
                        string _validationEmpt = string.Empty;
                        #region Cell Mapping & Insert to DB
                        if (vatOutReturnApprovalStatus.NomorDokumenRetur != sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim()) // if not duplicate from previous
                        {
                            vatOutReturnApprovalStatus = new VATOutSalesReturnApprovalStatusViewModel();
                            _nomorDokumenRetur = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                            if (string.IsNullOrEmpty(_nomorDokumenRetur))
                            {
                                _validationEmpt = _validationEmpt + "Credit Note Number is mandatory, ";
                            }
                            else if (!string.IsNullOrEmpty(_nomorDokumenRetur))
                            {
                                vatOutReturnApprovalStatus.NomorDokumenRetur = _nomorDokumenRetur;
                            }
                            _approvalStatus = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();

                            if (string.IsNullOrEmpty(_approvalStatus))
                            {
                                //throw new Exception("Approval Status Harus di isi.");
                                _validationEmpt = _validationEmpt + "Approval Status is mandatory, ";
                            }
                            else if (!string.IsNullOrEmpty(_approvalStatus))
                            {
                                vatOutReturnApprovalStatus.ApprovalStatus = _approvalStatus;
                                isApproveSukses = (vatOutReturnApprovalStatus.ApprovalStatus.ToLower().Equals("approval sukses"));
                            }
                            approvalTime = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();


                            if (string.IsNullOrEmpty(approvalTime) && isApproveSukses)
                            {
                                //throw new Exception("Tanggal Approval Harus di isi.");
                                _validationEmpt = _validationEmpt + "Tanggal Approval is mandatory, ";
                            }
                            else if (!string.IsNullOrEmpty(approvalTime))
                            {
                                vatOutReturnApprovalStatus.ApprovalTime = DateTime.ParseExact(
                                                            approvalTime.Replace("WIB ", ""),
                                                            "ddd MMM dd HH:mm:ss yyyy",
                                                            System.Globalization.CultureInfo.InvariantCulture);
                            }
                            _recordTime = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim();
                            if (string.IsNullOrEmpty(_recordTime))
                            {
                                //throw new Exception("Tanggal Rekam Harus di isi.");
                                _validationEmpt = _validationEmpt + "Tanggal Rekam is mandatory, ";
                            }
                            else if (!string.IsNullOrEmpty(approvalTime))
                            {
                                vatOutReturnApprovalStatus.RecordTime = DateTime.ParseExact(
                                     _recordTime.Replace("WIB ", ""),
                                     "ddd MMM dd HH:mm:ss yyyy",
                                     System.Globalization.CultureInfo.InvariantCulture);
                            }
                            try
                            {


                                _taxInvoiceStatus = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();
                            }
                            catch (Exception)
                            {

                                _taxInvoiceStatus = "";
                            }
                            if (string.IsNullOrEmpty(_taxInvoiceStatus))
                            {
                                //throw new Exception("Status Faktur Harus di isi.");
                                _validationEmpt = _validationEmpt + "Status Faktur is mandatory, ";
                            }
                            else if (!string.IsNullOrEmpty(_taxInvoiceStatus))
                            {
                                vatOutReturnApprovalStatus.TaxInvoiceStatus = _taxInvoiceStatus;
                            }
                            // vatOutReturnApprovalStatus.TaxInvoiceStatus;

                            if (!string.IsNullOrEmpty(_validationEmpt.Trim()))
                            {
                                throw new Exception(_validationEmpt.Trim());
                            }
                            result = db.SingleOrDefault<Result>("VATOutSalesReturn/usp_UpdateCustom_TB_R_VATOutSalesReturn_ApprovalStatus",
                                    vatOutReturnApprovalStatus.MapFromModel(DateTime.Now, user));
                        }
                        else
                        {
                            throw new Exception("Immediate duplicate rows of " + vatOutReturnApprovalStatus.NomorDokumenRetur);
                        }
                        #endregion

                        if (!result.ResultCode)
                        {
                            throw new Exception(result.ResultDesc);
                        }
                        else
                        {
                            ExceptionHandler.LogToSync(row - 3, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path, MessageType.INF);
                            rowSuccess++;
                        }
                        //}
                        //else
                        //{
                        //    throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        //}
                    }
                    catch (Exception e)
                    {
                        e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }


                db.CommitTransaction();
                result.ResultCode = true;
                //result.ResultDesc = Formatting.UPLOAD_NOTIFICATION.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());
                result.ResultDesc = Formatting.UPLOAD_NOTIFICATION_OLD.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        public Result ExcelTransitoryStatusUpdate(string path, string user)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = Formatting.UPLOAD_NOTIFICATION
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;



            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                string _transitoryDate = string.Empty;
                string _nomorFakturGabungan = string.Empty;
                string _transitoryStatus = string.Empty;
                string _transitoryNumber = string.Empty;

                string _headerNomorFakturGabungan = string.Empty;
                string _headerTransitoryDate = string.Empty;
                string _headerTransitoryStatus = string.Empty;
                string _headerTransitoryNumber = string.Empty;

                string _heder = string.Empty;

                file.Close();
                VATOutSalesReturnTransitoryStatusViewModel vatOutReturnTransitoryStatus = new VATOutSalesReturnTransitoryStatusViewModel();
                var cellNum = 4; // 31 is the cell amount in one row

                db.BeginTransaction();
                _headerNomorFakturGabungan = sheet.GetRow(cellNum - 1).GetCell(0).StringCellValue.ToString().Trim();
                _headerTransitoryDate = sheet.GetRow(cellNum - 1).GetCell(1).StringCellValue.ToString().Trim();
                _headerTransitoryStatus = sheet.GetRow(cellNum - 1).GetCell(2).StringCellValue.ToString().Trim();
                _headerTransitoryNumber = sheet.GetRow(cellNum - 1).GetCell(3).StringCellValue.ToString().Trim();

                if (_headerNomorFakturGabungan != "NOMOR_DOKUMEN_RETUR")
                {
                    _heder += _headerNomorFakturGabungan + ", ";
                }
                if (_headerTransitoryDate != "CLEARING_TRANSITORY_DATE")
                {
                    _heder += _headerTransitoryDate + ", ";
                }
                if (_headerTransitoryStatus != "STATUS")
                {
                    _heder += _headerTransitoryStatus + ", ";
                }
                if (_headerTransitoryNumber != "TRANSITORY_NUMBER")
                {
                    _heder += _headerTransitoryNumber + ", ";
                }

                if (!string.IsNullOrEmpty(_heder.Trim()))
                {
                    _heder = _heder.Remove(_heder.Length - 2);
                    throw new Exception(_heder.Trim() + " is not valid header template");
                }



                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNum; i++)
                        {
                            sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                            pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        #endregion

                        string _validationEmptTransitory = string.Empty;
                        #region Cell Mapping & Insert to DB
                        if (vatOutReturnTransitoryStatus.NomorFakturGabungan != sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim()) // if not duplicate from previous
                        {

                            vatOutReturnTransitoryStatus = new VATOutSalesReturnTransitoryStatusViewModel();
                            _nomorFakturGabungan = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                            if (string.IsNullOrEmpty(_nomorFakturGabungan))
                            {
                                _validationEmptTransitory = _validationEmptTransitory + "Nomor Dokumen Retur is mandatory, ";
                                // throw new Exception("Credit Note Number Harus di isi.");NOMOR_DOKUMEN_RETUR
                            }
                            else if (!string.IsNullOrEmpty(_nomorFakturGabungan))
                            {
                                vatOutReturnTransitoryStatus.NomorFakturGabungan = _nomorFakturGabungan;
                            }

                            _transitoryDate = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                            if (string.IsNullOrEmpty(_transitoryDate))
                            {
                                _validationEmptTransitory = _validationEmptTransitory + "Clearing Transitory Date is mandatory, ";
                                // throw new Exception("Clearing Transitory Date Harus di isi.");
                            }
                            else if (!string.IsNullOrEmpty(_transitoryDate))
                            {

                                var ransitoryDate = _transitoryDate.Split('/');
                                if (ransitoryDate.Count() > 1) // when the excel column is a text type
                                {
                                    if (ransitoryDate[0].Length == 2 && ransitoryDate[1].Length == 2 && ransitoryDate[2].Length == 4)
                                    {
                                        vatOutReturnTransitoryStatus.TransitoryDate = DateTime.ParseExact(_transitoryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        _validationEmptTransitory = _validationEmptTransitory + "Date Format must be: dd/mm/yyyy, ";
                                        // throw new Exception("Date Format must be: dd/mm/yyyy");
                                    }
                                }
                                else // when the excel column is a DateTime type
                                {
                                    vatOutReturnTransitoryStatus.TransitoryDate = DateTime.FromOADate(Convert.ToDouble(_transitoryDate));
                                }
                            }

                            _transitoryStatus = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();
                            if (string.IsNullOrEmpty(_transitoryStatus))
                            {
                                _validationEmptTransitory = _validationEmptTransitory + "Status is mandatory, ";
                                //throw new Exception("Status Harus di isi.");
                            }
                            else if (!string.IsNullOrEmpty(_transitoryStatus))
                            {
                                if (_transitoryStatus.ToUpper() == "YES" || _transitoryStatus.ToUpper() == "NO")
                                {
                                    vatOutReturnTransitoryStatus.TransitoryStatus = _transitoryStatus;
                                }
                                else
                                {
                                    _validationEmptTransitory = _validationEmptTransitory + "Status Only Yes/No, ";
                                }

                            }

                            _transitoryNumber = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim();

                            if (_transitoryStatus.ToUpper() == "YES")
                            {
                                if (string.IsNullOrEmpty(_transitoryNumber))
                                {
                                    _validationEmptTransitory = _validationEmptTransitory + "Transitory Number is mandatory, ";
                                }
                                else
                                {
                                    vatOutReturnTransitoryStatus.TransitoryNumber = _transitoryNumber;
                                }
                                // throw new Exception("Transitory Number Harus di isi.");
                            }
                            else if (_transitoryStatus.ToUpper() == "NO")
                            {
                                if (!string.IsNullOrEmpty(_transitoryNumber))
                                {
                                    _validationEmptTransitory = _validationEmptTransitory + "If Status No, Transitory Number should be blank, ";
                                }
                                else
                                {
                                    vatOutReturnTransitoryStatus.TransitoryNumber = _transitoryNumber;
                                }

                            }


                            if (!string.IsNullOrEmpty(_validationEmptTransitory.Trim()))
                            {
                                throw new Exception(_validationEmptTransitory.Trim());
                            }

                            result = db.SingleOrDefault<Result>("VATOutSalesReturn/usp_UpdateCustom_TB_R_VATOutSalesReturn_TransitoryStatus",
                                    vatOutReturnTransitoryStatus.MapFromModel(DateTime.Now, user));
                        }
                        else
                        {
                            throw new Exception("Immediate duplicate rows of " + vatOutReturnTransitoryStatus.NomorFakturGabungan);
                        }
                        #endregion

                        if (!result.ResultCode)
                        {
                            throw new Exception(result.ResultDesc);
                        }
                        else
                        {
                            ExceptionHandler.LogToSync(row - 3, pipedString, Path.GetFileName(path),ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path, MessageType.INF);
                            rowSuccess++;
                        }

                    }
                    catch (Exception e)
                    {
                        e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }

                db.CommitTransaction();
                result.ResultCode = true;
                result.ResultDesc = Formatting.UPLOAD_NOTIFICATION_OLD.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        //public Result uploadfilepdfcreditnote(List<CreditNoteModel> oData, HttpFileCollectionBase files, string _Browser)
        public Result uploadfilepdfcreditnote(List<CreditNoteModel> oData, HttpFileCollectionBase files, string _Browser)
        {
            string CreditNoteSignSuccess = Path.Combine(MasterConfigRepository.Instance.GetByConfigKey("CreditNoteSign_Success").ConfigValue);
            // string CreditNoteSignSuccess = Path.Combine(@"D:\TestPDF\CreditNoteSign\");
            string CreditNoteSignFailed = Path.Combine(MasterConfigRepository.Instance.GetByConfigKey("CreditNoteSign_Failed").ConfigValue);

            string CreditNoteDGTSuccess = Path.Combine(MasterConfigRepository.Instance.GetByConfigKey("CreditNoteDGT_Success").ConfigValue);
            //  string CreditNoteDGTSuccess = Path.Combine(@"D:\TestPDF\CreditNoteDGT\");
            string CreditNoteDGTFailed = Path.Combine(MasterConfigRepository.Instance.GetByConfigKey("CreditNoteDGT_Failed").ConfigValue);
            string _CreditNoteType = string.Empty;

            string fullPathFailed = string.Empty;
            string fname = string.Empty;
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = Formatting.UPLOAD_NOTIFICATION
            };

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                db.BeginTransaction();
                //HttpFileCollectionBase _files = Request.Files;
                for (int i = 0; i < oData.Count; i++)
                {
                    try
                    {

                        // HttpPostedFileBase file = files[oData[i].FileName];// or files[i];
                        HttpPostedFileBase file = files[i];
                        // Checking for Internet Explorer      
                        if (_Browser == "IE" || _Browser == "INTERNETEXPLORER")
                        {
                            string[] _files = file.FileName.Split(new char[] { '\\' });
                            fname = _files[_files.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        string extension = Path.GetExtension(fname);
                        _CreditNoteType = oData[i].CreditNoteType;
                        var _CreditNoteTypePath = _CreditNoteType == "Credit Note DGT" ? CreditNoteDGTSuccess : CreditNoteSignSuccess;

                        var fullPath = _CreditNoteType == "Credit Note DGT" ? Path.Combine(CreditNoteDGTSuccess, Path.GetFileName(fname)) : Path.Combine(CreditNoteSignSuccess, Path.GetFileName(fname));

                        fullPathFailed = _CreditNoteType == "Credit Note DGT" ? Path.Combine(CreditNoteDGTFailed, Path.GetFileName(fname)) : Path.Combine(CreditNoteSignFailed, Path.GetFileName(fname));

                        // Get the complete folder path and store the file inside it.      
                        //fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        //file.SaveAs(fname);
                        result = db.SingleOrDefault<Result>("VATOutSalesReturn/usp_UpdateCustom_TB_R_VATOutSalesReturn_CreditNote"
                                      , new
                                      {
                                          CreditNoteNumber = fname.Replace(extension, ""),
                                          CreditNoteType = _CreditNoteType,
                                          FileName = fname,
                                          DestinationPath = _CreditNoteTypePath,
                                          FullPath = fullPath

                                      });
                        if (!result.ResultCode)
                        {
                            file.SaveAs(fullPathFailed);
                            throw new Exception(result.ResultDesc);

                        }
                        else
                        {
                            file.SaveAs(fullPath);
                            ExceptionHandler.LogToSync(i + 1, "", fname, ExceptionHandler.GENERAL_SUCCESS_MESSAGE, fname, MessageType.INF);
                            rowSuccess++;
                        }
                    }
                    catch (Exception e)
                    {

                        e.LogToSync(i + 1, "", fname, fullPathFailed, MessageType.ERR);
                        rowFail++;
                    }
                }

                db.CommitTransaction();
                result.ResultCode = true;
                result.ResultDesc = Formatting.UPLOAD_NOTIFICATION_DGT_TTD.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());


            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row + 1, "", fname, fullPathFailed, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }


        #endregion

    }
}
