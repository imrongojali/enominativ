﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.DRKB
{
    public class DRKBOriginalViewModel
    {
        public String IdNya { get; set; }
        public String Field1 { get; set; }
        public String Field2 { get; set; }
        public String Field3 { get; set; }
        public String Field4 { get; set; }
        public String Field5 { get; set; }
        public String Field6 { get; set; }
        public String Field7 { get; set; }
        public String Field8 { get; set; }
        public String Field9 { get; set; }
        public String Field10 { get; set; }
        public String Field11 { get; set; }
        public String Field12 { get; set; }
        public String Field13 { get; set; }
        public String Field14 { get; set; }
        public String Field15 { get; set; }
    }
}