﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.DRKB
{
    public class DRKBDashboardSearchParamViewModel
    {
        public string TaxNumber { get; set; }
        public string TanggalFakturFrom { get; set; }
        public string TanggalFakturTo { get; set; }
        public string NPWPCustomer { get; set; }
        public string NamaCustomer { get; set; }
        public string FrameNumber { get; set; }
        public string EngineNumber { get; set; }
        public string MerkOrType { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string BusinessUnit { get; set; }

        public Nullable<Decimal> VATBaseAmount { get; set; }
        public string VATBaseAmountOperator { get; set; }
        public Nullable<Decimal> VATAmount { get; set; }
        public string VATAmountOperator { get; set; }
        public Nullable<Decimal> LuxTaxAmount { get; set; }
        public string LuxTaxAmountOperator { get; set; }
        public Nullable<Decimal> PrevLuxTaxAmount { get; set; }
        public string PrevLuxTaxAmountOperator { get; set; }
        public string Remarks { get; set; }

        public int page { get; set; }
        public int size { get; set; }

        public dynamic MapFromModel(string DivisionName, string SortBy = "BusinessUnit Desc,TaxNumber Desc,TanggalFaktur Desc,NPWPCustomer", string SortDirection = "Desc", int FromNumber = 1, int ToNumber = 10)
        {
            return new
            {
                SortBy = SortBy,
                SortDirection = SortDirection,
                TaxNumber = this.TaxNumber,
                TanggalFakturFrom = this.TanggalFakturFrom.FormatDefaultDateWhenNullOrEmpty(),
                TanggalFakturTo = this.TanggalFakturTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                NPWPCustomer = this.NPWPCustomer,
                NamaCustomer = this.NamaCustomer,
                FrameNumber = this.FrameNumber,
                EngineNumber = this.EngineNumber,
                MerkOrType = this.MerkOrType,
                Model = this.Model,
                Year = this.Year,
                BusinessUnit = this.BusinessUnit,
                VATBaseAmount = this.VATBaseAmount,
                VATBaseAmountOperator = this.VATBaseAmountOperator,
                VATAmount = this.VATAmount,
                VATAmountOperator = this.VATAmountOperator,
                LuxTaxAmount = this.LuxTaxAmount,
                LuxTaxAmountOperator = this.LuxTaxAmountOperator,
                PrevLuxTaxAmount = this.PrevLuxTaxAmount,
                PrevLuxTaxAmountOperator = this.PrevLuxTaxAmountOperator,
                Remarks = this.Remarks,
                DivisionName = DivisionName,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
        }

    }
}