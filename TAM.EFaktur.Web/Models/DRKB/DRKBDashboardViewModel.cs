﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.DRKB
{
    public class DRKBDashboardViewModel : BaseModel
    {
        public int RowNum { get; set; }
        //public Guid Id { get; set; }
        public string IdNya { get; set; }

        public string TaxNumber { get; set; }
        public DateTime TanggalFaktur { get; set; }
        public string NPWPCustomer { get; set; }
        public string NamaCustomer { get; set; }
        public decimal JumlahDPP { get; set; }
        public decimal JumlahLuxTax { get; set; }
        public decimal JumlahPPN { get; set; }
        public decimal PrevLuxTaxAmount { get; set; }
        public string FrameNumber { get; set; }
        public string EngineNumber { get; set; }
        public string MerkorType { get; set; }
        public string Model { get; set; }
        public string Year{ get; set; }
        public string BusinessUnit { get; set; }

        public string Remarks{ get; set; }

    }
}
