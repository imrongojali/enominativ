﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.WHT
{
    public class WHTUploadEbupotViewModel
    {
        public string eBupotReferenceNo { get; set; }
        public string ApprovalStatus { get; set; }
        public string ApprovalDesc { get; set; }
        public string MapCode { get; set; }
        public string eBupotNumber { get; set; }

        public Nullable<DateTime> ApprovalTime { get; set; }
        public DateTime RecordTime { get; set; }
        public dynamic MapFromModel()
        {
            return new
            {
                eBupotReferenceNo = eBupotReferenceNo,
                ApprovalStatus = ApprovalStatus,
                ApprovalDesc = ApprovalDesc,
                MapCode = MapCode,
                eBupotNumber = eBupotNumber,
                ApprovalTime = ApprovalTime,
                RecordTime = RecordTime
            };
        }

    }

    
}