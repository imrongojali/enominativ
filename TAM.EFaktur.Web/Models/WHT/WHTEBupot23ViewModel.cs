﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.WHT
{
    public class WHTEBupot23ViewModel
    {
        public int RowNum { get; set; }
        public string MasaPajak { get; set; }
        public string TahunPajak { get; set; }
        public DateTime TglPemotongan { get; set; }
        public string BerNPWP { get; set; }
        public string NPWP { get; set; }
        public string NIK { get; set; }
        public string NomorTelp { get; set; }
        public string KodeObjekPajak { get; set; }
        public string PenandaTanganBPPengurus { get; set; }
        public decimal PenghasilanBruto { get; set; }
        public string MendapatkanFasilitas { get; set; }
        public string NomorSKB { get; set; }
        public string NomorAturanDTP { get; set; }
        public string NTPNDTP { get; set; }
        public string JenisPajak { get; set; }
        public string JenisDokumen { get; set; }
        public string eBupotReferenceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
    }
}