﻿using Dapper;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models.Log_Sync;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.SyncData;
using TAM.EFaktur.Web.Models.Master_Company_Details;
using System.Text.RegularExpressions;

namespace TAM.EFaktur.Web.Models.WHT
{
    public class WHTRepository : BaseRepository
    {
        #region Singleton
        private WHTRepository() { }
        private static WHTRepository instance = null;
        public static WHTRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new WHTRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(WHTDashboardSearchParamViewModel model, string DivisionName, string Username, bool isFullAccess,int page, int size, string Id)
        {
            try
            {
                return db.SingleOrDefault<int>("WHT/usp_CountWHTListDashboard", model.MapFromModel(isFullAccess, DivisionName, Username,0,null,page,size,Id));
            }
            catch
            {
                throw;
            }

        }
        #endregion

        #region Processing Data
        public List<WHTDashboardViewModel> GetList(WHTDashboardSearchParamViewModel model, string DivisionName, string Username, int SortBy, string SortDirection, int FromNumber, int ToNumber, bool isFullAccess, string Id)
        {
            IEnumerable<WHTDashboardViewModel> result = db.Query<WHTDashboardViewModel>("WHT/usp_GetWHTListDashboard", model.MapFromModel(isFullAccess, DivisionName, Username, SortBy, SortDirection, FromNumber, ToNumber, Id));
            return result.ToList();
        }

        public WHT GetById(Guid Id)
        {
            return db.SingleOrDefault<WHT>("VATIn/usp_GetVATInByID", new { Id = Id });
        }

        public List<Guid> GetIdBySearchParam(WHTDashboardSearchParamViewModel model, string DivisionName, string Username, bool isFullAccess)
        {
           
            //IEnumerable<Guid> result = db.Query<Guid>("WHT/usp_GetWHTIdBySearchParam", model.MapFromModel(isFullAccess, DivisionName, Username));
            IEnumerable<Guid> result = db.Query<Guid>("WHT/usp_GetWHTIdBySearchParam", model.MapFromModel(true, DivisionName, Username));
            return result.ToList();
        }

        //Get CSV
        public List<WHTCSVViewModel> GetCSVById_WHT22(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<WHTCSVViewModel> result = db.Query<WHTCSVViewModel>("WHT/usp_GetWHT22CSVById", args);
            return result.ToList();
        }


        public List<Result> GetCSVvalidation(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<Result> result = db.Query<Result>("WHT/usp_GetWHTCSVValidationById", args);
            return result.ToList();
        }

        public List<WHTCSVViewModel> GetCSVById_WHT23(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<WHTCSVViewModel> result = db.Query<WHTCSVViewModel>("WHT/usp_GetWHT23CSVById", args);
            return result.ToList();
        }
        public List<WHTCSVViewModel> GetCSVById_WHT15(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<WHTCSVViewModel> result = db.Query<WHTCSVViewModel>("WHT/usp_GetWHT15CSVById", args);
            return result.ToList();
        }
        public List<WHTCSVViewModel> GetCSVById_WHT2126(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<WHTCSVViewModel> result = db.Query<WHTCSVViewModel>("WHT/usp_GetWHT2126CSVById", args);
            return result.ToList();
        }
        public List<WHTCSVViewModel> GetCSVById_WHT2326(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<WHTCSVViewModel> result = db.Query<WHTCSVViewModel>("WHT/usp_GetWHT2326CSVById", args);
            return result.ToList();
        }
        public List<WHTCSVViewModel> GetCSVById_WHT42(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<WHTCSVViewModel> result = db.Query<WHTCSVViewModel>("WHT/usp_GetWHT42CSVById", args);
            return result.ToList();
        }
        public List<WHTCSVViewModel> GetCSVById_WHTBadan(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<WHTCSVViewModel> result = db.Query<WHTCSVViewModel>("WHT/usp_GetWHTBadanCSVById", args);
            return result.ToList();
        }
        #region old
        /*
        //Get Original File
        public List<WHTOriginalViewModel> GetXlsById(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<WHTOriginalViewModel> result = db.Query<WHTOriginalViewModel>("VATIn/usp_GetVATInOriginalFileById", args);
            return result.ToList();
        }

        //Get Report Excel
        public List<WHTReportExcelViewModel> GetReportExcelById(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<WHTReportExcelViewModel> result = db.Query<WHTReportExcelViewModel>("VATIn/usp_GetVATInReportFileById", args);
            return result.ToList();
        }
        */
        #endregion

        //Get Excel
        public List<WHTOriginalViewModel> GetXlsById(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<WHTOriginalViewModel> result = db.Query<WHTOriginalViewModel>("WHT/usp_GetWHTOriginalFileById", args);
            return result.ToList();
        }

        //Get Excel Ebupot
        public List<WHTEBupot23ViewModel> GetXlsEBupot23ById(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<WHTEBupot23ViewModel> result = db.Query<WHTEBupot23ViewModel>("WHT/usp_GetWHTEBupot23FileById", args);
            return result.ToList();
        }

        public List<WHTEBupot26ViewModel> GetXlsEBupot26ById(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<WHTEBupot26ViewModel> result = db.Query<WHTEBupot26ViewModel>("WHT/usp_GetWHTEBupot26FileById", args);
            return result.ToList();
        }

        public List<WHTDashboardViewModel> GetErrorDataList(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<WHTDashboardViewModel> result = db.Query<WHTDashboardViewModel>("WHT/usp_GetWHTEBupotOutherFileById", args);
            return result.ToList();
        }

        public List<Result> GetDownloadEbupotDataList(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<Result> result = db.Query<Result>("WHT/usp_GetDownloadEBupotFileById", args);
            return result.ToList();
        }

        public void GetDownloadEbupotDataLog(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
           db.Execute("WHT/usp_GetWHTEBupotOutherFileByIdLog", args);
          
        }

        public Result InsertLogCustom(LogSyncCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    Id = model.Id,
                    FileName = model.FileName,
                    RowId = model.RowId,
                    RowData = model.RowData,
                    MessageType = model.MessageType,
                    Message = model.Message,
                    EventDate = EventDate.FormatSQLDateTime(),
                    EventActor = EventActor
                };
                db.Execute("SyncLog/usp_Insert_TB_R_SyncLog", args);
                
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        public List<WHTOriginalViewModel> GetXlsReportById(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<WHTOriginalViewModel> result = db.Query<WHTOriginalViewModel>("WHT/usp_GetWHTReportById", args);
            return result.ToList();
        }
        //Update flags
        public Result UpdateFlagCSV(List<Guid> WHTIdList, string FileUrl, string EventActor)
        {
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    WHTIdList = string.Join(";", WHTIdList),
                    BatchFileName = Path.GetFileName(@FileUrl)
                };
                db.Execute("WHT/usp_UpdateCustom_TB_R_WHT_FlagCSV", args);

                args = new
                {
                    Id = Guid.NewGuid(),
                    CsvType = "FM",
                    CsvTime = DateTime.Now.FormatSQLDateTime(),
                    BatchFileName = Path.GetFileName(@FileUrl),
                    FileUrl = FileUrl,
                    EventDate = DateTime.Now.FormatSQLDateTime(),
                    EventActor = EventActor
                };
                db.Execute("CSVData/usp_InsertCustom_TB_R_CsvData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                throw e;
            }

            return MsgResultSuccessInsert();
        }

        //Get PDF Download
        public List<string> GetPDFUrlById(List<Guid> WHTIdList)
        {
            dynamic args = new
            {
                WHTIdList = string.Join(";", WHTIdList)
            };
            IEnumerable<string> result = db.Query<string>("WHT/usp_GetWHTPDFUrlById", args);
            return result.ToList();
        }

        public List<WHTDetailManualInputViewModel> GetDetailByVATInId(Guid VATInId)
        {
            IEnumerable<WHTDetailManualInputViewModel> result = db.Query<WHTDetailManualInputViewModel>("VATIn/usp_GetCustomVATInDetailByVATInID", new { VATInId = VATInId });
            return result.ToList();

        }

        public WHTManualInputViewModel GetByFileUrl(string FileUrl)
        {
            WHTManualInputViewModel model = new WHTManualInputViewModel();
            try
            {
                model = db.SingleOrDefault<WHTManualInputViewModel>("VATIn/usp_GetCustomVATInByFileUrl", new { FileUrl = FileUrl });
                if (model != null)
                {
                    model.WHTDetails = GetDetailByVATInId(model.Id);
                    model.InvoiceDate = model.InvoiceDate.FormatViewDate();
                }
                else
                {
                    throw new Exception("Tax Invoice does not exist");
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return model;
        }

        public Result ValidateWHTTransitoryStatusNotAvailable(List<Guid> WHTIdList)
        {
            try
            {
                dynamic args = new
                {
                    WHTIdList = string.Join(";", WHTIdList)
                };
                return db.SingleOrDefault<Result>("VATIn/usp_ValidateVATInTransitoryStatusNotAvailable", args);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Result ValidateVATInTransitoryStatusUpdated(List<Guid> VATInIdList)
        {
            try
            {
                dynamic args = new
                {
                    VATInIdList = string.Join(";", VATInIdList)
                };
                return db.SingleOrDefault<Result>("VATIn/usp_ValidateVATInTransitoryStatusUpdated", args);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Result DeleteWHTById(List<Guid> WHTIdList, string EventActor)
        {
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    VATInIdList = string.Join(";", WHTIdList)
                    //EventDate = DateTime.Now.FormatSQLDateTime(),
                    //EventActor = EventActor
                };
                db.Execute("WHT/usp_DeleteWHTByID", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                throw e;

            }
            return MsgResultSuccessDelete();
        }

        public Result Insert(WHTManualInputViewModel model, string XMLUrl, string EventActor, DateTime EventDate)
        {
            try
            {
                #region Validasi VAT In
                Result validateResult = db.SingleOrDefault<Result>("VATIn/usp_ValidateVATInInvoiceNumber", new { NomorFakturGabungan = model.InvoiceNumberFull });

                if (!validateResult.ResultCode)
                {
                    throw new Exception(validateResult.ResultDesc);
                }

                var config = MasterConfigRepository.Instance.GetByConfigKey("CompanyNPWP");
                if (config == null)
                {
                    throw new Exception("Please define the CompanyNPWP Config");
                }

                if (!model.NPWPLawanTransaksi.Equals(config.ConfigValue))
                {
                    throw new Exception("Buyer NPWP '" + model.NPWPLawanTransaksi + "' does not match with CompanyNPWP '" + config.ConfigValue + "'");
                }
                #endregion

                db.BeginTransaction();
                Nullable<Guid> SyncId = null;
                string FakturType = Formatting.TYPE_NON_EFAKTUR;
                if (!string.IsNullOrEmpty(XMLUrl))
                {
                    FakturType = Formatting.TYPE_EFAKTUR;
                    #region Master - SyncData
                    //generate new Guid for Id - SyncData
                    SyncId = Guid.NewGuid();
                    dynamic argsSyncData = new SyncDataViewModel().CreateModel("FM", null, XMLUrl).MapFromModel(SyncId.Value, EventDate, EventActor);
                    db.Execute("SyncData/usp_Insert_TB_R_SyncData", argsSyncData);
                    #endregion

                    #region Detail - SyncDataDetail
                    List<SyncDataDetailViewModel> syncDataDetailsModel = MapSyncDataDetailsModel(model);

                    foreach (SyncDataDetailViewModel syncDataDetailModel in syncDataDetailsModel)
                    {
                        dynamic argsSyncDataDetail = syncDataDetailModel.MapFromModel(SyncId.Value, EventDate, EventActor);
                        db.Execute("SyncData/usp_Insert_TB_R_SyncDataDetail", argsSyncDataDetail);
                    }
                    #endregion
                }
                #region Master - VATIn
                //generate new Guid for Id - VATIn
                Guid VATInID = Guid.NewGuid();

                dynamic args = new
                {
                    Id = VATInID,
                    SyncId = SyncId,
                    InvoiceNumber = model.InvoiceNumber,
                    InvoiceNumberFull = model.InvoiceNumberFull,
                    KDJenisTransaksi = model.KDJenisTransaksi,
                    FGPengganti = model.FGPengganti,
                    InvoiceDate = model.InvoiceDate,
                    SupplierNPWP = model.SupplierNPWP,
                    SupplierName = model.SupplierName,
                    SupplierAddress = model.SupplierAddress,
                    //NPWPLawanTransaksi  = model.NPWPLawanTransaksi,
                    StatusApprovalXML = model.StatusApprovalXML,
                    StatusFakturXML = model.StatusFakturXML,
                    FakturType = FakturType,
                    VATBaseAmount = model.VATBaseAmount,
                    VATAmount = model.VATAmount,
                    JumlahPPNBM = model.JumlahPPnBM,
                    EventDate = DateTime.Now.FormatSQLDateTime(),
                    EventActor = EventActor
                };
                db.Execute("VATIn/usp_InsertCustom_TB_R_VATIn", args);
                #endregion

                #region Detail - VATInDetail
                foreach (WHTDetailManualInputViewModel detailModel in model.WHTDetails)
                {
                    dynamic argsDetail = new
                    {
                        Id = Guid.NewGuid(),
                        VATInId = VATInID,
                        UnitName = detailModel.UnitName,
                        UnitPrice = detailModel.UnitPrice,
                        Quantity = detailModel.Quantity,
                        TotalPrice = detailModel.TotalPrice,
                        Discount = detailModel.Discount,
                        DPP = detailModel.DPP,
                        PPN = detailModel.PPN,
                        TarifPPNBM = detailModel.TarifPPNBM,
                        PPNBM = detailModel.PPNBM,
                        EventDate = DateTime.Now.FormatSQLDateTime(),
                        EventActor = EventActor
                    };
                    db.Execute("VATIn/usp_InsertCustom_TB_R_VATInDetail", argsDetail);
                }
                #endregion

                db.CommitTransaction();
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                throw e;
            }
        }

        public Result Delete(Guid Id)
        {
            try
            {
                db.Execute("VATIn/usp_DeleteVATInByID", new { Id = Id });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        public Result WHTFileInsert(string path, string EventActor)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File has been uploaded successfully"
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                WHT wht = new WHT();
                var cellNum = 27; // 22 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();
                exceptionCellNum.Add(17);
                exceptionCellNum.Add(19);
              
                exceptionCellNum.Add(23);
                exceptionCellNum.Add(24);
                exceptionCellNum.Add(25);
                exceptionCellNum.Add(26);
                #region Master - SyncData
                //db.BeginTransaction();
                //Guid SyncId = Guid.NewGuid(); //generate new Guid for Id - SyncData, to be referenced di VAT dan SyncDataDetail

                //dynamic argsSyncData = new SyncDataViewModel() //insert SyncData paling pertama, karena dibutuhkan sebagai reference untuk data VAT dan SyncDataDetail
                //    .CreateModel("FM", null, path, Path.GetFileName(@path))
                //    .MapFromModel(SyncId, DateTime.Now, EventActor);

                //db.Execute("SyncData/usp_Insert_TB_R_SyncData", argsSyncData);

                //List<dynamic> argsSyncDataDetailList = new List<dynamic>(); //Declare list di awal untuk syncDataDetail agar menghemat performance
                //db.CommitTransaction();
                #endregion

                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();
                List<string> failTaxNumber = new List<string>();

                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    #region Set Cell Policy, Cell Type and Save Sync Data
                    pipedString = string.Empty;
                    for (int i = 0; i < cellNum; i++)
                    {
                        sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                        pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                    }
                    pipedString = pipedString.Remove(pipedString.Length - 1);
                    #endregion

                    try
                    {
                        if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        {
                            #region Validasi jika nomor faktur ini sudah terjadi fail di row sebelumnya
                            if (failTaxNumber.Contains(sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim() + sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim() +
                                sheet.GetRow(row).GetCell(18).StringCellValue.ToString().Trim()))
                            {
                                throw new Exception(ExceptionHandler.PREVIOUS_ROW_FAIL_MESSAGE);
                            }
                            #endregion

                            #region Cell Mapping & Insert to DB
                            //wht.NomorBuktiPotong = null;
                            if (wht.NomorBuktiPotong + wht.NomorInvoice + wht.PVNumber != sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim() + sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim() +
                                sheet.GetRow(row).GetCell(18).StringCellValue.ToString().Trim())
                            {
                                if (!string.IsNullOrEmpty(wht.NomorBuktiPotong))
                                {
                                    if (failTaxNumber.Contains(wht.NomorBuktiPotong))
                                    {
                                        db.AbortTransaction();
                                    }
                                    else
                                    {
                                        db.CommitTransaction();
                                    }
                                }

                                db.BeginTransaction();
                                wht = new WHT();
                                wht.Id = Guid.NewGuid();
                                wht.TaxArticle = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                                wht.NomorBuktiPotong = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                                wht.NomorInvoice = sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim();
                                wht.StatusInvoice = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();
                                wht.SupplierName = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim();
                                wht.SupplierAddress = sheet.GetRow(row).GetCell(22).StringCellValue.ToString().Trim();
                                wht.OriginalSupplierName = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();
                                wht.SupplierNPWP = sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim().FormatNPWP();
                                wht.Description = sheet.GetRow(row).GetCell(9).StringCellValue.ToString().Trim();
                                wht.TaxType = sheet.GetRow(row).GetCell(12).StringCellValue.ToString().Trim();
                                wht.SPTType = sheet.GetRow(row).GetCell(13).StringCellValue.ToString().Trim();

                                var tgl_wht = sheet.GetRow(row).GetCell(14).StringCellValue.ToString().Trim().Split('/');
                                if (tgl_wht.Count() > 1) // when the excel column is a text type
                                {
                                    wht.WithholdingTaxDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(14).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                else // when the excel column is a DateTime type
                                {
                                    wht.WithholdingTaxDate = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(14).StringCellValue.ToString().Trim()));
                                }

                                wht.TaxPeriodMonth = sheet.GetRow(row).GetCell(15).StringCellValue.ToString().Trim();
                                wht.TaxPeriodYear = sheet.GetRow(row).GetCell(16).StringCellValue.ToString().Trim();

                                if (sheet.GetRow(row).GetCell(23).StringCellValue.ToString().Trim() != "")
                                {
                                    var tgl_receive = sheet.GetRow(row).GetCell(23).StringCellValue.ToString().Trim().Split('/');
                                    if (tgl_receive.Count() > 1) // when the excel column is a text type
                                    {
                                        wht.ReceiveFileDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(23).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else // when the excel column is a DateTime type
                                    {
                                        wht.ReceiveFileDate = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(23).StringCellValue.ToString().Trim()));
                                    }
                                }

                                wht.PVNumber = sheet.GetRow(row).GetCell(18).StringCellValue.ToString().Trim();
                                wht.PVCreatedBy = sheet.GetRow(row).GetCell(19).StringCellValue.ToString().Trim();
                                wht.SAPDocNumber = sheet.GetRow(row).GetCell(20).StringCellValue.ToString().Trim();

                                if (sheet.GetRow(row).GetCell(21).StringCellValue.ToString().Trim() != "")
                                {
                                    var tgl_posting = sheet.GetRow(row).GetCell(21).StringCellValue.ToString().Trim().Split('/');
                                    if (tgl_posting.Count() > 1) // when the excel column is a text type
                                    {
                                        wht.PostingDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(21).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else // when the excel column is a DateTime type
                                    {
                                        wht.PostingDate = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(21).StringCellValue.ToString().Trim()));
                                    }
                                }

                                if (sheet.GetRow(row).GetCell(26).StringCellValue.ToString().Trim() == null)
                                {
                                    wht.GLAccount = "";
                                }
                                else
                                {
                                    wht.GLAccount = sheet.GetRow(row).GetCell(26).StringCellValue.ToString().Trim();
                                }

                                wht.AdditionalInfo = sheet.GetRow(row).GetCell(17).StringCellValue.ToString().Trim();

                                wht.WHTAmount = Decimal.Parse(sheet.GetRow(row).GetCell(6).StringCellValue.ToString().Trim());
                                string whtamount = wht.WHTAmount.ToString();
                                if ((whtamount.Contains(".") || (whtamount.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                wht.WHTTarif = Decimal.Parse(sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim());
                                string tarif = wht.WHTTarif.ToString();

                                wht.WHTTaxAmount = Decimal.Parse(sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim());
                                string taxamount = wht.WHTTaxAmount.ToString();
                                if ((taxamount.Contains(".") || (taxamount.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }

                                var tgl_invoice = sheet.GetRow(row).GetCell(10).StringCellValue.ToString().Trim().Split('/');
                                if (tgl_invoice.Count() > 1) // when the excel column is a text type
                                {
                                    wht.InvoiceDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(10).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                else // when the excel column is a DateTime type
                                {
                                    wht.InvoiceDate = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(10).StringCellValue.ToString().Trim()));
                                }


                                wht.CreatedBy = EventActor;
                                wht.CreatedOn = DateTime.Now;
                                wht.RowStatus = true;
                                wht.ApprovalStatus = sheet.GetRow(row).GetCell(24).StringCellValue.ToString().Trim();
                                wht.ApprovalDesc = sheet.GetRow(row).GetCell(25).StringCellValue.ToString().Trim();

                                // Save VAT In
                                result = db.SingleOrDefault<Result>("WHT/usp_Insert_TB_R_WHT", wht.MapFromModel(DateTime.Now, EventActor));
                               
                                if (!result.ResultCode)
                                {
                                    throw new Exception(result.ResultDesc);
                                }
                            }
                            else
                            {
                                throw new Exception("Duplicate value, Invoice No and PV No and NomorBuktiPotong must be unique");
                            }

                            logSyncList.Add(new LogSyncCreateUpdate()
                               .CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.INF, ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path));
                            rowSuccess++;
                            #endregion
                        }
                        else
                        {
                            throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE); //jika mandatory field ada yg blank
                        }
                    }
                    catch (Exception e)
                    {
                        if (!failTaxNumber.Contains(sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim())) //kolom 3 = Nomor Faktur
                        {
                            failTaxNumber.Add(sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim()); //untuk mencatat no faktur yang error
                        }
                        logSyncList.Add(new LogSyncCreateUpdate().CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.ERR, e.Message, path));
                        rowFail++;
                    }
                }

                #region execute batch terakhir dari VAT
                if (failTaxNumber.Contains(wht.NomorBuktiPotong + wht.NomorInvoice + wht.PVNumber))
                {
                    db.AbortTransaction();
                }
                else
                {
                    db.CommitTransaction();
                }
                #endregion

                #region SyncLog Insert
                db.BeginTransaction();
                ExceptionHandler.LogToSync(logSyncList);
                db.CommitTransaction();
                #endregion

                result.ResultCode = true;
                result.ResultDesc = Formatting
                    .UPLOAD_NOTIFICATION
                    .Replace("@success", (rowSuccess).ToString())
                    .Replace("@error", (rowFail).ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }
            
            db.Close();
            return result;
        }

        public Result EBupotFileInsert(string path, string EventActor)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File has been uploaded successfully"
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                WHTEBupot whtEBupot = new WHTEBupot();
                var cellNum = 29; // 22 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();
                exceptionCellNum.Add(19);

                #region Master - SyncData
                //db.BeginTransaction();
                //Guid SyncId = Guid.NewGuid(); //generate new Guid for Id - SyncData, to be referenced di VAT dan SyncDataDetail

                //dynamic argsSyncData = new SyncDataViewModel() //insert SyncData paling pertama, karena dibutuhkan sebagai reference untuk data VAT dan SyncDataDetail
                //    .CreateModel("FM", null, path, Path.GetFileName(@path))
                //    .MapFromModel(SyncId, DateTime.Now, EventActor);

                //db.Execute("SyncData/usp_Insert_TB_R_SyncData", argsSyncData);

                //List<dynamic> argsSyncDataDetailList = new List<dynamic>(); //Declare list di awal untuk syncDataDetail agar menghemat performance
                //db.CommitTransaction();
                #endregion

                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();
                List<string> failTaxNumber = new List<string>();

                if (sheet.LastRowNum < 4)
                {
                    throw new Exception("mandatory field cannot be empty");
                }

                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    #region Set Cell Policy, Cell Type and Save Sync Data
                    pipedString = string.Empty;
                    for (int i = 0; i < cellNum; i++)
                    {
                        try
                        {

                        sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);


                        pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";

                        }
                        catch 
                        {
                            break;
                           // System.Environment.Exit(0);
                        }
                    }
                    try
                    {

                   
                    pipedString = pipedString.Remove(pipedString.Length - 1);
                    if (pipedString == "")
                    {
                        throw new Exception("mandatory field cannot be empty");
                    }
                    }
                    catch (Exception)
                    {

                        break;
                    }

                    #endregion

                    try
                    {
                        //if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        //{

                        int Emptycount = 0;
                        StringBuilder builder = new StringBuilder();

                        builder.Append("Excel Row : " + Convert.ToString(row + 1) + "  mandatory field(");
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim()))

                        {
                            Emptycount += 1;
                            builder.Append("Tax Article,");
                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" Invoice Status,");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" Supplier Name, ");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" Original Supplier Name, ");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" Supplier NPWP, ");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(6).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" Supplier Address, ");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" Withholding Tax Based Amount, ");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" Withholding Tax Tarif, ");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(9).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" Withholding Tax Amount, ");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(10).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" Description, ");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" Invoice Date, ");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(12).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" Invoice Number, ");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(13).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" eBupot Reference Number, ");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(14).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" Tax Type, ");

                        }
                        //if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(15).StringCellValue.ToString().Trim()))
                        //{
                        //    Emptycount += 1;
                        //    builder.Append(" Map Code, ");

                        //}
                        //if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(16).StringCellValue.ToString().Trim()))
                        //{
                        //    Emptycount += 1;
                        //    builder.Append(" SPT Type, ");

                        //}
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(15).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" Withholding Tax Date, ");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(16).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" Tax Period Month, ");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(17).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" Tax Period Year, ");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(19).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" PV Number, ");

                        }

                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(21).StringCellValue.ToString()))
                        {
                            Emptycount += 1;
                            builder.Append(" SAP Document Number, ");

                        }

                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(22).StringCellValue.ToString()))
                        {
                            Emptycount += 1;
                            builder.Append(" Posting Date, ");

                        }

                       

                        if (Emptycount > 0)
                        {
                            //builder.Append(" data cannot be empty");
                            //throw new Exception(builder.ToString());
                            // builder = builder.Remove(builder.Length, builder.Length - 1);

                            builder.Append(")mandatory field cannot be empty");
                            throw new Exception(builder.ToString());
                        }


                        //#region Validasi jika nomor faktur ini sudah terjadi fail di row sebelumnya
                        //if (failTaxNumber.Contains(sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim() + sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim() +
                        //        sheet.GetRow(row).GetCell(18).StringCellValue.ToString().Trim()))
                        //{
                        //    throw new Exception(ExceptionHandler.PREVIOUS_ROW_FAIL_MESSAGE);
                        //}
                        //#endregion

                        #region Cell Mapping & Insert to DB
                        //wht.NomorBuktiPotong = null;
                        //if (whtEBupot.eBupotNumber + whtEBupot.NomorInvoice + whtEBupot.PVNumber != sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim() + sheet.GetRow(row).GetCell(12).StringCellValue.ToString().Trim() +
                        //    sheet.GetRow(row).GetCell(21).StringCellValue.ToString().Trim())
                        //{
                        //if (!string.IsNullOrEmpty(whtEBupot.eBupotNumber))
                        //{
                        //    if (failTaxNumber.Contains(whtEBupot.eBupotNumber))
                        //    {
                        //        db.AbortTransaction();
                        //    }
                        //    else
                        //    {
                        //        db.CommitTransaction();
                        //    }
                        //}

                        //db.BeginTransaction();
                        whtEBupot = new WHTEBupot();

                        whtEBupot.TaxArticle = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();

                        List<string> firstlist = new List<string>();
                        firstlist.Add("23");
                        firstlist.Add("26");
                        List<string> results = firstlist.Where(x => firstlist.Contains(whtEBupot.TaxArticle.Trim())).ToList();
                        if (results.Count() <= 0)
                        {
                            throw new Exception("The Tax Article must be 23 and 26");
                        }




                        whtEBupot.eBupotNumber = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                        whtEBupot.StatusInvoice = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();
                        whtEBupot.SupplierName = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim();
                        whtEBupot.OriginalSupplierName = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();

                        if (sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim()=="23")
                        {
                            whtEBupot.SupplierNPWP = sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim().FormatNPWP();
                        }
                        else
                        {
                            whtEBupot.SupplierNPWP = sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim();
                        }
                        //if (sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim() == "26")
                        //{
                        //    whtEBupot.SupplierNPWP = sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim();
                        //}
                        //else
                        //{
                        //    whtEBupot.SupplierNPWP = sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim().FormatNPWP();
                        //}

                        //if (sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim().Length != 15)
                        //{
                        //    throw new Exception("NPWP must be 15 characters");

                        //}

                        whtEBupot.SupplierAddress = sheet.GetRow(row).GetCell(6).StringCellValue.ToString().Trim();

                        whtEBupot.WHTAmount = Decimal.Parse(sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim());
                        string whtamount = whtEBupot.WHTAmount.ToString();
                        if ((whtamount.Contains(".") || (whtamount.Contains(","))))//String contain " . or , "
                        {
                            throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                        }

                        whtEBupot.WHTTarif = sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim().Replace(",",".");
                        string tarif = whtEBupot.WHTTarif.ToString();

                        whtEBupot.WHTTaxAmount = Decimal.Parse(sheet.GetRow(row).GetCell(9).StringCellValue.ToString().Trim());
                        string taxamount = whtEBupot.WHTTaxAmount.ToString();
                        if ((taxamount.Contains(".") || (taxamount.Contains(","))))//String contain " . or , "
                        {
                            throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                        }

                        whtEBupot.Description = sheet.GetRow(row).GetCell(10).StringCellValue.ToString().Trim();
                        var tgl_invoice = sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim().Split('/');
                        if (tgl_invoice.Count() > 1) // when the excel column is a text type
                        {
                            whtEBupot.InvoiceDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        }
                        else // when the excel column is a DateTime type
                        {
                            whtEBupot.InvoiceDate = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim()));
                        }

                        whtEBupot.NomorInvoice = sheet.GetRow(row).GetCell(12).StringCellValue.ToString().Trim();
                        whtEBupot.eBupotReferenceNo = sheet.GetRow(row).GetCell(13).StringCellValue.ToString().Trim();
                        whtEBupot.TaxType = sheet.GetRow(row).GetCell(14).StringCellValue.ToString().Trim();
                       

                        var tgl_wht = sheet.GetRow(row).GetCell(15).StringCellValue.ToString().Trim().Split('/');
                        if (tgl_wht.Count() > 1) // when the excel column is a text type
                        {
                            whtEBupot.WithholdingTaxDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(15).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        }
                        else // when the excel column is a DateTime type
                        {
                            whtEBupot.WithholdingTaxDate = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(15).StringCellValue.ToString().Trim()));
                        }

                        whtEBupot.TaxPeriodMonth = sheet.GetRow(row).GetCell(16).StringCellValue.ToString().Trim();
                        whtEBupot.TaxPeriodYear = sheet.GetRow(row).GetCell(17).StringCellValue.ToString().Trim();
                        whtEBupot.AdditionalInfo = sheet.GetRow(row).GetCell(18).StringCellValue.ToString().Trim();
                        if (!string.IsNullOrEmpty(whtEBupot.AdditionalInfo))
                        {
                            if (whtEBupot.AdditionalInfo.Trim().Length != 18)
                            {
                                throw new Exception("Additional Info must be 16 characters");

                            }
                        }
                        whtEBupot.PVNumber = sheet.GetRow(row).GetCell(19).StringCellValue.ToString().Trim();
                        whtEBupot.PVCreatedBy = sheet.GetRow(row).GetCell(20).StringCellValue.ToString().Trim();
                        whtEBupot.SAPDocNumber = sheet.GetRow(row).GetCell(21).StringCellValue.ToString().Trim();




                        if (sheet.GetRow(row).GetCell(22).StringCellValue.ToString().Trim() != "")
                        {
                            var tgl_posting = sheet.GetRow(row).GetCell(22).StringCellValue.ToString().Trim().Split('/');
                            if (tgl_posting.Count() > 1) // when the excel column is a text type
                            {
                                whtEBupot.PostingDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(22).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            else // when the excel column is a DateTime type
                            {
                                whtEBupot.PostingDate = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(22).StringCellValue.ToString().Trim()));
                            }
                        }

                        if (sheet.GetRow(row).GetCell(23).StringCellValue.ToString().Trim() != "")
                        {
                            var tgl_receive = sheet.GetRow(row).GetCell(23).StringCellValue.ToString().Trim().Split('/');
                            if (tgl_receive.Count() > 1) // when the excel column is a text type
                            {
                                whtEBupot.ReceiveFileDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(23).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            else // when the excel column is a DateTime type
                            {
                                whtEBupot.ReceiveFileDate = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(23).StringCellValue.ToString().Trim()));
                            }
                        }

                        whtEBupot.ApprovalStatus = sheet.GetRow(row).GetCell(24).StringCellValue.ToString().Trim();

                        whtEBupot.ApprovalDesc = sheet.GetRow(row).GetCell(25).StringCellValue.ToString().Trim();
                        DataFormatter formatter = new DataFormatter();
                        if (formatter.FormatCellValue(sheet.GetRow(row).GetCell(26)) == null)
                        {
                            whtEBupot.GLAccount = "";
                        }
                        else
                        {
                            whtEBupot.GLAccount = formatter.FormatCellValue(sheet.GetRow(row).GetCell(26));
                        }


                        whtEBupot.CreatedBy = EventActor;
                        whtEBupot.CreatedOn = DateTime.Now;
                        whtEBupot.RowStatus = true;


                        // Save VAT In
                        result = db.SingleOrDefault<Result>("WHT/usp_Insert_TB_R_WHTEbupot", whtEBupot.MapFromModel(DateTime.Now, EventActor));
                        if (!result.ResultCode)
                        {
                            throw new Exception(result.ResultDesc);
                        }
                        //}
                        //else
                        //{
                        //    throw new Exception("Duplicate value, Invoice No and PV No and Ebupot No must be unique");
                        //}

                        logSyncList.Add(new LogSyncCreateUpdate()
                           .CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.INF, ExceptionHandler.GENERAL_SUCCESS_MESSAGE, ""));
                        rowSuccess++;

                        #endregion
                        //}
                        //else
                        //{
                        //    throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE); //jika mandatory field ada yg blank
                        //}
                    }
                    catch (Exception e)
                    {
                        if (!failTaxNumber.Contains(sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim())) //kolom 3 = Nomor Faktur
                        {
                            failTaxNumber.Add(sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim()); //untuk mencatat no faktur yang error
                        }
                        logSyncList.Add(new LogSyncCreateUpdate().CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.ERR, e.Message, path));
                        rowFail++;
                    }
                }

                //#region execute batch terakhir dari VAT
                //if (failTaxNumber.Contains(whtEBupot.eBupotNumber + whtEBupot.NomorInvoice + whtEBupot.PVNumber))
                //{
                //    db.AbortTransaction();
                //}
                //else
                //{
                //    db.CommitTransaction();
                //}
                //#endregion

                #region SyncLog Insert
                db.BeginTransaction();
                ExceptionHandler.LogToSync(logSyncList);
                db.CommitTransaction();
                #endregion

                result.ResultCode = true;
                result.ResultDesc = Formatting
                    .UPLOAD_NOTIFICATION
                    .Replace("@success", (rowSuccess).ToString())
                    .Replace("@error", (rowFail).ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }
           
            db.Close();
            return result;
        }

        public Result ExcelApprovalStatusUpdate(string path, string user)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = Formatting.UPLOAD_NOTIFICATION
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;
            string approvalTime = string.Empty;
            bool isApproveSukses = false;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                WHTApprovalStatusViewModel whtApprovalStatus = new WHTApprovalStatusViewModel();
                var cellNum = 5; // 5 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();
                exceptionCellNum.Add(2);
                db.BeginTransaction();

                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNum; i++)
                        {
                            sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                            pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        #endregion

                        if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        {
                            #region Cell Mapping & Insert to DB
                            if (whtApprovalStatus.NomorBuktiPotong != sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim()) //ignore if duplicate
                            {
                                whtApprovalStatus = new WHTApprovalStatusViewModel();
                                whtApprovalStatus.NomorBuktiPotong = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                                whtApprovalStatus.ApprovalStatus = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                                whtApprovalStatus.ApprovalDescription = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();
                                approvalTime = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();
                                isApproveSukses = (whtApprovalStatus.ApprovalStatus.ToLower().Equals("approval sukses"));


                                if (string.IsNullOrEmpty(approvalTime) && isApproveSukses)
                                {
                                    throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                                }
                                else if (!string.IsNullOrEmpty(approvalTime))
                                {
                                    //whtApprovalStatus.ApprovalTime = DateTime.ParseExact(
                                    //                            approvalTime.Replace("WIB ", ""),
                                    //                            "ddd MMM dd HH:mm:ss yyyy",
                                    //                            System.Globalization.CultureInfo.InvariantCulture);
                                    var approvalDate = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim().Split('/');
                                    if (approvalDate.Count() > 1) // when the excel column is a text type
                                    {
                                        if (approvalDate[0].Length == 2 && approvalDate[1].Length == 2 && approvalDate[2].Length == 4)
                                        {
                                            whtApprovalStatus.ApprovalTime = DateTime.ParseExact(sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        }
                                        else
                                        {
                                            throw new Exception("Date Format must be: dd/mm/yyyy");
                                        }
                                    }
                                    else // when the excel column is a DateTime type
                                    {
                                        whtApprovalStatus.ApprovalTime = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim()));
                                    }
                                }

                                //whtApprovalStatus.RecordTime = DateTime.ParseExact(
                                //    sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim().Replace("WIB ", ""),
                                //    "ddd MMM dd HH:mm:ss yyyy",
                                //    System.Globalization.CultureInfo.InvariantCulture);
                                var recordlDate = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim().Split('/');
                                if (recordlDate.Count() > 1) // when the excel column is a text type
                                {
                                    if (recordlDate[0].Length == 2 && recordlDate[1].Length == 2 && recordlDate[2].Length == 4)
                                    {
                                        whtApprovalStatus.RecordTime = DateTime.ParseExact(sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        throw new Exception("Date Format must be: dd/mm/yyyy");
                                    }
                                }
                                else // when the excel column is a DateTime type
                                {
                                    whtApprovalStatus.RecordTime = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim()));
                                }

                                result = db.SingleOrDefault<Result>("WHT/usp_UpdateCustom_TB_R_WHT_ApprovalStatus", whtApprovalStatus.MapFromModel(DateTime.Now, user));
                            }
                            else
                            {
                                throw new Exception("Immediate duplicate rows of " + whtApprovalStatus.NomorBuktiPotong);
                            }
                            #endregion

                            if (!result.ResultCode)
                            {
                                throw new Exception(result.ResultDesc);
                            }
                            else
                            {
                                ExceptionHandler.LogToSync(row - 3, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path,MessageType.INF);
                                rowSuccess++;
                            }
                        }
                        else
                        {
                            throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        }
                    }
                    catch (Exception e)
                    {
                        e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }

                db.CommitTransaction();
                result.ResultCode = true;
                result.ResultDesc = Formatting.UPLOAD_NOTIFICATION_OLD.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        public Result ExcelEBupotApprovalUpdate(string path, string user)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = Formatting.UPLOAD_NOTIFICATION
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;
            string approvalTime = string.Empty;
            bool isApproveSukses = false;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0; 

            try
            {
                file.Close();
                WHTEBupotApprovalViewModel whtEbupotApproval = new WHTEBupotApprovalViewModel();
                var cellNum = 5; // 5 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();
                exceptionCellNum.Add(3);
                db.BeginTransaction();
                if (sheet.LastRowNum < 3)
                {
                    throw new Exception("mandatory field cannot be empty");
                }
                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNum; i++)
                        {
                            sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                            pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);

                        if (pipedString == "")
                        {
                            throw new Exception("mandatory field cannot be empty");
                        }

                        if (pipedString == "|||")
                        { break; }



                        #endregion

                        //if (GlobalFunction.BlankCellCheckApprovalEbupot(sheet.GetRow(row), cellNum, exceptionCellNum))
                        //{

                        //if (sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim() == "" && sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim() == "")
                        //{
                        //    //goto SkipToEnd;
                        //     new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        //     break;



                        //}

                        #region Cell Mapping & Insert to DB 

                        whtEbupotApproval = new WHTEBupotApprovalViewModel();

                        whtEbupotApproval.NomorBuktiPotong = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                        whtEbupotApproval.ApprovalStatus = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                        whtEbupotApproval.ApprovalDescription = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();
                        approvalTime = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();
                        isApproveSukses = (whtEbupotApproval.ApprovalStatus.ToLower().Equals("approval sukses"));

                        int Emptycount = 0;
                        StringBuilder builder = new StringBuilder();

                        builder.Append("Excel Row : " + Convert.ToString(row + 1) + "  mandatory field(");
                        if (string.IsNullOrEmpty(whtEbupotApproval.NomorBuktiPotong))

                        {
                            Emptycount += 1;
                            builder.Append("NOMOR_BUKTI_POTONG,");
                        }
                        if (string.IsNullOrEmpty(whtEbupotApproval.ApprovalStatus))

                        {
                            Emptycount += 1;
                            builder.Append(" APPROVAL_STATUS,");

                        }
                        if (string.IsNullOrEmpty(approvalTime))

                        {
                            Emptycount += 1;
                            builder.Append(" TANGGAL_APPROVAL,");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim()))

                        {
                            Emptycount += 1;
                            builder.Append(" TANGGAL_REKAM,");

                        }

                        if (Emptycount > 0)
                        {
                            //builder.Append(" data cannot be empty");
                            //throw new Exception(builder.ToString());
                            // builder = builder.Remove(builder.Length, builder.Length - 1);

                            builder.Append(")mandatory field cannot be empty");
                            throw new Exception(builder.ToString());
                        }
                        //if (whtEbupotApproval.NomorBuktiPotong != sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim()) //ignore if duplicate
                        //{
                        int ExistsData = db.SingleOrDefault<int>("WHT/usp_GetExistsByNoBuktiPotongPajak", new { refno = whtEbupotApproval.NomorBuktiPotong });
                        if (ExistsData > 0)
                        {
                            if (!string.IsNullOrEmpty(approvalTime))
                            {
                                //whtApprovalStatus.ApprovalTime = DateTime.ParseExact(
                                //                            approvalTime.Replace("WIB ", ""),
                                //                            "ddd MMM dd HH:mm:ss yyyy",
                                //                            System.Globalization.CultureInfo.InvariantCulture);
                                var approvalDate = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim().Split('/');
                                if (approvalDate.Count() > 1) // when the excel column is a text type
                                {
                                    if (approvalDate[0].Length == 2 && approvalDate[1].Length == 2 && approvalDate[2].Length == 4)
                                    {
                                        whtEbupotApproval.ApprovalTime = DateTime.ParseExact(sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        throw new Exception("Date Format must be: dd/mm/yyyy");
                                    }
                                }
                                else // when the excel column is a DateTime type
                                {  
                                    whtEbupotApproval.ApprovalTime = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim()));
                                }
                            }

                            //whtApprovalStatus.RecordTime = DateTime.ParseExact(
                            //    sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim().Replace("WIB ", ""),
                            //    "ddd MMM dd HH:mm:ss yyyy",
                            //    System.Globalization.CultureInfo.InvariantCulture);
                            var recordlDate = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim().Split('/');
                            if (recordlDate.Count() > 1) // when the excel column is a text type
                            {
                                if (recordlDate[0].Length == 2 && recordlDate[1].Length == 2 && recordlDate[2].Length == 4)
                                {
                                    whtEbupotApproval.RecordTime = DateTime.ParseExact(sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    throw new Exception("Date Format must be: dd/mm/yyyy");
                                }
                            }
                            else // when the excel column is a DateTime type
                            {
                                whtEbupotApproval.RecordTime = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim()));
                            } 

                            string urlPdfWHT = "";

                            IEnumerable<WHTDashboardViewModel> WHTDashboardViewModel = db.Query<WHTDashboardViewModel>("WHT/usp_GetByNoBuktiPotongPajak", new { refno = whtEbupotApproval.NomorBuktiPotong });

                            foreach (WHTDashboardViewModel item in WHTDashboardViewModel)
                            { 
                                urlPdfWHT = item.PDFWithholding;

                                if (whtEbupotApproval.ApprovalStatus == "Approval Sukses" && (item.JenisPajak == "2221" || item.JenisPajak == "2222" || item.JenisPajak == "2223" || item.JenisPajak == "2224"))
                                {
                                    Config configURLResult = MasterConfigRepository.Instance.GetByConfigKey("UrlGenerateResultPph22");
                                    if (configURLResult == null)
                                    {
                                        throw new Exception("Please define the UrlTemplateGenerateResultPph22 Config");
                                    }

                                    string nomorBuktiPotong = whtEbupotApproval.NomorBuktiPotong.Replace("/", "-").Replace(" ", "");  

                                    string[] tmp = item.NomorInvoice.Split('/');

                                    string noInvoice = tmp[tmp.Length - 1];

                                    string codeInvoice = Regex.Replace(noInvoice.ToUpper(), @"[^A-Z]+", String.Empty);         

                                    var company = db.SingleOrDefault<CompanyViewModel>("WHT/usp_GetCompanyByNPWP", new { code = codeInvoice });
                                    if (company == null)
                                    {
                                        company = db.SingleOrDefault<CompanyViewModel>("WHT/usp_GetCompanyByNPWP", new { code = "J" });
                                    }

                                    urlPdfWHT = configURLResult.ConfigValue + "\\" + nomorBuktiPotong + ".pdf";

                                    GlobalFunction.CreatePDF22(item, company, urlPdfWHT); 
                                }

                                whtEbupotApproval.PDFWithholding = urlPdfWHT;

                                result = db.SingleOrDefault<Result>("WHT/usp_UpdateCustom_TB_R_WHT_ApprovalStatusEbupot", whtEbupotApproval.MapFromModel(DateTime.Now, user));
                            }  
                        }
                        else
                        {
                            throw new Exception("Ebupot number not found " + whtEbupotApproval.NomorBuktiPotong); 
                        }

                        //}
                        //else
                        //{string duplicateerr = string.IsNullOrEmpty(whtEbupotApproval.NomorBuktiPotong).ToString() == "" ? whtEbupotApproval.eBupotNumber: whtEbupotApproval.NomorBuktiPotong;
                        //    throw new Exception("Immediate duplicate rows of " + whtEbupotApproval.NomorBuktiPotong);
                        //}
                        #endregion

                        if (!result.ResultCode)
                        {
                            throw new Exception(result.ResultDesc);
                        }
                        else
                        {
                            // logSyncList.Add(new LogSyncCreateUpdate().CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.INF, ExceptionHandler.GENERAL_SUCCESS_MESSAGE, ""));
                            ExceptionHandler.LogToSync(row - 2, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path,MessageType.INF);
                            rowSuccess++;
                        }
                        //}
                        //else
                        //{
                        //    throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        //}
                    }
                    catch (Exception e)
                    {
                        e.LogToSync(row - 2, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }

                db.CommitTransaction();
                result.ResultCode = true; 
                result.ResultDesc = Formatting.UPLOAD_NOTIFICATION.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());

            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }


        public Result ExcelEbupotFile(string path, string user)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = Formatting.UPLOAD_NOTIFICATION
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;
            string approvalTime = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                WHTUploadEbupotViewModel whtUploadEbupot = new WHTUploadEbupotViewModel();

                var cellNum = 7; // 5 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();
                exceptionCellNum.Add(2);
                db.BeginTransaction();
                if (sheet.LastRowNum < 3)
                {
                    throw new Exception("mandatory field cannot be empty");
                }
                for (row = 3; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNum; i++)
                        {
                            sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                            pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        if (pipedString == "")
                        {
                            throw new Exception("mandatory field cannot be empty");
                        }

                        #endregion

                        //if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        //{
                        #region Cell Mapping & Insert to DB



                        whtUploadEbupot = new WHTUploadEbupotViewModel();
                        whtUploadEbupot.eBupotReferenceNo = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                        whtUploadEbupot.eBupotNumber = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                        whtUploadEbupot.MapCode = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();
                        whtUploadEbupot.ApprovalStatus = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim();

                        approvalTime = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();
                        whtUploadEbupot.ApprovalDesc = sheet.GetRow(row).GetCell(6).StringCellValue.ToString().Trim();
                        int Emptycount = 0;
                        StringBuilder builder = new StringBuilder();

                        builder.Append("Excel Row : " + Convert.ToString(row + 1) + "  mandatory field(");
                        if (string.IsNullOrEmpty(whtUploadEbupot.eBupotReferenceNo))
                        {
                            Emptycount += 1;
                            builder.Append("EBUPOT_REFERENCE_NO.,");
                        }
                        if (string.IsNullOrEmpty(whtUploadEbupot.eBupotNumber))
                        {
                            Emptycount += 1;
                            builder.Append(" EBUPOT_NUMBER,");

                        }
                        if (string.IsNullOrEmpty(whtUploadEbupot.MapCode))
                        {
                            Emptycount += 1;
                            builder.Append(" MAP_CODE,");

                        }
                        if (string.IsNullOrEmpty(approvalTime))

                        {
                            Emptycount += 1;
                            builder.Append(" TANGGAL_APPROVAL,");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim()))

                        {
                            Emptycount += 1;
                            builder.Append(" TANGGAL_REKAM,");

                        }

                        if (string.IsNullOrEmpty(whtUploadEbupot.ApprovalStatus))
                        {
                            Emptycount += 1;
                            builder.Append(" APPROVAL_STATUS,");

                        }

                        if (Emptycount > 0)
                        {

                            builder.Append(")mandatory field cannot be empty");
                            throw new Exception(builder.ToString());
                        }

                        //if (whtUploadEbupot.eBupotReferenceNo != sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim()) //ignore if duplicate
                        //{
                        int ExistsData = db.SingleOrDefault<int>("WHT/usp_GetExistsByeBupotNumberApproval", new { refno = whtUploadEbupot.eBupotReferenceNo });
                        if (ExistsData > 0)
                        {
                            if (!string.IsNullOrEmpty(approvalTime))
                            {
                                //whtApprovalStatus.ApprovalTime = DateTime.ParseExact(
                                //                            approvalTime.Replace("WIB ", ""),
                                //                            "ddd MMM dd HH:mm:ss yyyy",
                                //                            System.Globalization.CultureInfo.InvariantCulture);
                                var approvalDate = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim().Split('/');
                                if (approvalDate.Count() > 1) // when the excel column is a text type
                                {
                                    if (approvalDate[0].Length == 2 && approvalDate[1].Length == 2 && approvalDate[2].Length == 4)
                                    {
                                        whtUploadEbupot.ApprovalTime = DateTime.ParseExact(sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        throw new Exception("Date Format must be: dd/mm/yyyy");
                                    }
                                }
                                else // when the excel column is a DateTime type
                                {
                                    whtUploadEbupot.ApprovalTime = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim()));
                                }
                            }
                            var recordlDate = sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim().Split('/');
                            if (recordlDate.Count() > 1) // when the excel column is a text type
                            {
                                if (recordlDate[0].Length == 2 && recordlDate[1].Length == 2 && recordlDate[2].Length == 4)
                                {
                                    whtUploadEbupot.RecordTime = DateTime.ParseExact(sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    throw new Exception("Date Format must be: dd/mm/yyyy");
                                }
                            }
                            else // when the excel column is a DateTime type
                            {
                                whtUploadEbupot.RecordTime = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim()));
                            }
                            result = db.SingleOrDefault<Result>("WHT/usp_UpdateCustom_TB_R_WHT_eBupot", whtUploadEbupot.MapFromModel());

                        }

                        else
                        {
                            throw new Exception("Referensi number not found " + whtUploadEbupot.eBupotReferenceNo);

                        }


                        //result = db.SingleOrDefault<Result>("WHT/usp_UpdateCustom_TB_R_WHT_eBupot", whtUploadEbupot.MapFromModel());

                        //}
                        //else
                        //{
                        //    throw new Exception("Immediate duplicate " + whtUploadEbupot.eBupotReferenceNo);
                        //}
                        #endregion

                        if (!result.ResultCode)
                        {
                            throw new Exception(result.ResultDesc);
                        }
                        else
                        {
                            ExceptionHandler.LogToSync(row - 2, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE,path, MessageType.INF);
                            rowSuccess++;
                        }
                        //}
                        //else
                        //{
                        //     //throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        //    throw new Exception("Empty Data");
                        //}
                    }
                    catch (Exception e)
                    {
                        e.LogToSync(row - 2, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }



                db.CommitTransaction();
                //result.ResultCode = rowFail > 0 ? false : true;
                result.ResultCode = true;
                result.ResultDesc = Formatting.UPLOAD_NOTIFICATION.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());

            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }




        public Result ExcelNIK(string path, string user)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = Formatting.UPLOAD_NOTIFICATION
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("eBupot");
            ISheet sheetnoebupot = hssfwb.GetSheet("Non eBupot");
            string pipedString = string.Empty;


            int row = 0;
            int rowNoEbupot = 0;
            int rowFail = 0;
            int rowSuccess = 0;



            try
            {
                file.Close();
                WHTUploadNIKModel whtUploadNIK = new WHTUploadNIKModel();


                var cellNum = 4; // 5 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();
                exceptionCellNum.Add(2);
                db.BeginTransaction();
                //if (sheet.LastRowNum < 3)
                //{
                //    throw new Exception("mandatory field cannot be empty");
                //}

                for (row = 3; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNum; i++)
                        {
                            sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                            pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        //if (pipedString == "")
                        //{
                        //    throw new Exception("mandatory field cannot be empty");
                        //}

                        #endregion

                        //if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        //{
                        #region Cell Mapping & Insert to DB


                        whtUploadNIK = new WHTUploadNIKModel();
                        whtUploadNIK.eBupotReferenceNo = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                        whtUploadNIK.NomorPV = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                        whtUploadNIK.TaxAdditionalInfo = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();
                        whtUploadNIK.status = "EB";

                        //if (string.IsNullOrEmpty(whtUploadNIK.eBupotReferenceNo)
                        //       && string.IsNullOrEmpty(whtUploadNIK.TaxAdditionalInfo))
                        //{
                        //    // throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        //    throw new Exception("Empty Data");
                        //}
                        int Emptycount = 0;
                        StringBuilder builder = new StringBuilder();

                        builder.Append("eBupot Row : " + Convert.ToString(row + 1) + "  mandatory field(");
                        if (string.IsNullOrEmpty(whtUploadNIK.eBupotReferenceNo))

                        {
                            Emptycount += 1;
                            builder.Append("EBUPOT_REFERENCE_NO.,");
                        }
                        if (string.IsNullOrEmpty(whtUploadNIK.NomorPV))

                        {
                            Emptycount += 1;
                            builder.Append("PV_NUMBER.,");
                        }
                        if (string.IsNullOrEmpty(whtUploadNIK.TaxAdditionalInfo))

                        {
                            Emptycount += 1;
                            builder.Append(" ADDITIONAL_INFORMATION,");

                        }
                        if (Emptycount > 0)
                        {
                            //builder.Append(" data cannot be empty");
                            //throw new Exception(builder.ToString());
                            // builder = builder.Remove(builder.Length, builder.Length - 1);

                            builder.Append(")mandatory field cannot be empty");
                            throw new Exception(builder.ToString());
                        }

                        if (whtUploadNIK.TaxAdditionalInfo.Length != 16)
                        {
                            throw new Exception("eBupot Row: " + Convert.ToString(row + 1) + " NIK must be 16 characters");

                        }
                        //if (whtUploadNIK.eBupotReferenceNo != sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim()) //ignore if duplicate
                        //{

                        int ExistsData = db.SingleOrDefault<int>("WHT/usp_GetExistsByeBupotNumber",
                            new { status = "EB", refno = whtUploadNIK.eBupotReferenceNo, NomorPV = whtUploadNIK.NomorPV, NamaPenjual = "" });
                        if (ExistsData > 0)
                        {
                            result = db.SingleOrDefault<Result>("WHT/usp_UpdateCustom_TB_R_WHT_NIK", whtUploadNIK.MapFromModel());

                        }

                        else
                        {
                            throw new Exception("Referensi number and PV Number and not found " + whtUploadNIK.eBupotReferenceNo + " : " + whtUploadNIK.NomorPV);

                        }


                        //result = db.SingleOrDefault<Result>("WHT/usp_UpdateCustom_TB_R_WHT_eBupot", whtUploadEbupot.MapFromModel());

                        //}
                        //else
                        //{
                        //    throw new Exception("Immediate duplicate " + whtUploadNIK.eBupotReferenceNo);
                        //}
                        #endregion

                        if (!result.ResultCode)
                        {
                            throw new Exception(result.ResultDesc);
                        }
                        else
                        {
                            ExceptionHandler.LogToSync(row - 2, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE,path, MessageType.INF);
                            rowSuccess++;
                        }
                        //}
                        //else
                        //{
                        //    //throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        //    throw new Exception("Empty Data");
                        //}
                    }
                    catch (Exception e)
                    {
                        e.LogToSync(row - 2, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }


                for (rowNoEbupot = 3; rowNoEbupot <= sheetnoebupot.LastRowNum; rowNoEbupot++) // Data row start at row 5, index 4 on excel template
                {
                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNum; i++)
                        {
                            sheetnoebupot.GetRow(rowNoEbupot).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            sheetnoebupot.GetRow(rowNoEbupot).GetCell(i).SetCellType(CellType.STRING);

                            pipedString += sheetnoebupot.GetRow(rowNoEbupot).GetCell(i).StringCellValue.ToString().Trim() + "|";
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        #endregion

                        //if (GlobalFunction.BlankCellCheck(sheetnoebupot.GetRow(rowNoEbupot), cellNum, exceptionCellNum))
                        //{
                        #region Cell Mapping & Insert to DB
                        //if (whtUploadNIK.NomorInvoice != sheetnoebupot.GetRow(rowNoEbupot).GetCell(0).StringCellValue.ToString().Trim()) //ignore if duplicate
                        //{


                        whtUploadNIK = new WHTUploadNIKModel();
                        whtUploadNIK.NamaPenjual = sheetnoebupot.GetRow(rowNoEbupot).GetCell(0).StringCellValue.ToString().Trim();
                        whtUploadNIK.NomorPV = sheetnoebupot.GetRow(rowNoEbupot).GetCell(1).StringCellValue.ToString().Trim();
                        whtUploadNIK.TaxAdditionalInfo = sheetnoebupot.GetRow(rowNoEbupot).GetCell(2).StringCellValue.ToString().Trim();
                        whtUploadNIK.status = "IV";



                        //if (string.IsNullOrEmpty(whtUploadNIK.NomorInvoice)
                        //    && string.IsNullOrEmpty(whtUploadNIK.TaxAdditionalInfo))
                        //{
                        //    // throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        //    throw new Exception("Empty Data");
                        //}

                        int EmptycountNon = 0;
                        StringBuilder builderNon = new StringBuilder();
                        builderNon.Append("Non eBupot Row : " + Convert.ToString(rowNoEbupot + 1) + "  mandatory field(");


                        if (string.IsNullOrEmpty(whtUploadNIK.NamaPenjual))

                        {
                            EmptycountNon += 1;
                            builderNon.Append("SUPPLIER_NAME,");
                        }
                        if (string.IsNullOrEmpty(whtUploadNIK.NomorPV))

                        {
                            EmptycountNon += 1;
                            builderNon.Append("PV_NUMBER.,");
                        }
                        if (string.IsNullOrEmpty(whtUploadNIK.TaxAdditionalInfo))

                        {
                            EmptycountNon += 1;

                            builderNon.Append(" ADDITIONAL_INFORMATION,");

                        }
                        if (EmptycountNon > 0)
                        {
                            // builderNon=builderNon.Remove(builderNon.Length, builderNon.Length-1);

                            builderNon.Append(")mandatory field cannot be empty");
                            throw new Exception(builderNon.ToString());
                        }

                        if (whtUploadNIK.TaxAdditionalInfo.Length != 16)
                        {
                            throw new Exception("Non eBupot Row: " + Convert.ToString(rowNoEbupot + 1) + " NIK must be 16 characters");

                        }

                        int ExistsData = db.SingleOrDefault<int>("WHT/usp_GetExistsByeBupotNumber",

                new { status = "IV", refno = "", NomorPV = whtUploadNIK.NomorPV, NamaPenjual = whtUploadNIK.NamaPenjual });
                        if (ExistsData > 0)
                        {
                            result = db.SingleOrDefault<Result>("WHT/usp_UpdateCustom_TB_R_WHT_NIK", whtUploadNIK.MapFromModel());

                        }

                        else
                        {
                            throw new Exception("PV Number and Suppler Name not found " + whtUploadNIK.NamaPenjual +" : "+ whtUploadNIK.NomorPV);

                        }


                        //result = db.SingleOrDefault<Result>("WHT/usp_UpdateCustom_TB_R_WHT_eBupot", whtUploadEbupot.MapFromModel());

                        //}
                        //else
                        //{
                        //    throw new Exception("Immediate duplicate " + whtUploadNIK.eBupotReferenceNo);
                        //}
                        #endregion

                        if (!result.ResultCode)
                        {
                            throw new Exception(result.ResultDesc);
                        }
                        else
                        {
                            ExceptionHandler.LogToSync(rowNoEbupot - 2, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path,MessageType.INF);
                            rowSuccess++;
                        }
                        //}
                        //else
                        //{
                        //    //throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        //    throw new Exception("Empty Data");
                        //}
                    }
                    catch (Exception e)
                    {
                        e.LogToSync(rowNoEbupot - 2, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }



                db.CommitTransaction();
                result.ResultCode = true;
                //result.ResultCode = rowFail > 0 ? false : true;
                result.ResultDesc = Formatting.UPLOAD_NOTIFICATION.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());

            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }



        public Result ExcelTransitoryStatusUpdate(string path, string EventActor)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = Formatting.UPLOAD_NOTIFICATION
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                WHTTransitoryStatusViewModel whtApprovalStatus = new WHTTransitoryStatusViewModel();
                var cellNum = 3; // 3 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();
                db.BeginTransaction();

                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNum; i++)
                        {
                            sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                            pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        #endregion
                        if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        {


                            #region Cell Mapping & Insert to DB
                            if (whtApprovalStatus.NomorFakturGabungan != sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim()) //ignore if duplicate
                            {
                                whtApprovalStatus = new WHTTransitoryStatusViewModel();
                                whtApprovalStatus.NomorFakturGabungan = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                                var tgl_faktur_split = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim().Split('/');
                                if (tgl_faktur_split.Count() > 1) // when the excel column is a text type
                                {
                                    whtApprovalStatus.TransitoryDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                else // when the excel column is a DateTime type
                                {
                                    whtApprovalStatus.TransitoryDate = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim()));
                                }
                                whtApprovalStatus.TransitoryStatus = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();
                                result = db.SingleOrDefault<Result>("VATIn/usp_UpdateCustom_TB_R_VATIn_TransitoryStatus", whtApprovalStatus.MapFromModel(DateTime.Now, EventActor));
                            }
                            else
                            {
                                throw new Exception("Immediate duplicate rows of " + whtApprovalStatus.NomorFakturGabungan);
                            }
                            #endregion

                            if (!result.ResultCode)
                            {
                                throw new Exception(result.ResultDesc);
                            }
                            else
                            {
                                ExceptionHandler.LogToSync(row - 3, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path,MessageType.INF);
                                rowSuccess++;
                            }
                        }
                        else
                        {
                            throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        }
                    }
                    catch (Exception e)
                    {
                        e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }

                db.CommitTransaction();
                result.ResultCode = true;
                result.ResultDesc = Formatting.UPLOAD_NOTIFICATION.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        public Result SyncWHT()
        {
            #region DB -> Timeout
            //try
            //{
            //    db.BeginTransaction();
            //    db.Execute("efb_sp_ELVIS_WHT_DataSync_Batch");
            //    db.CommitTransaction();
            //}
            //catch (Exception e)
            //{
            //    db.AbortTransaction();
            //    throw e;

            //}
            //return MsgResultSuccessSync();
            #endregion

            var conn = System.Configuration.ConfigurationManager.ConnectionStrings["TAM_EFAKTURConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(conn))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                transaction = connection.BeginTransaction();

                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    command.CommandText = "efb_sp_ExecuteSync";
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandTimeout = 0;
                    command.ExecuteNonQuery();

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Commit Exception Type: {0}", ex.GetType());
                    Console.WriteLine("  Message: {0}", ex.Message);
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType());
                        Console.WriteLine("  Message: {0}", ex2.Message);
                    }
                }
                connection.Close();
                connection.Dispose();
            }
            return MsgResultSuccessSync();
        }
        #endregion

        #region Private Method
        private List<SyncDataDetailViewModel> MapSyncDataDetailsModel(WHTManualInputViewModel WHTModel)
        {
            string headerDataPiped =
                WHTModel.KDJenisTransaksi + "|" + WHTModel.FGPengganti + "|" +
                WHTModel.InvoiceNumber + "|" + WHTModel.InvoiceDate + "|" +
                WHTModel.SupplierNPWP + "|" + WHTModel.SupplierName + "|" +
                WHTModel.SupplierAddress + "|" + WHTModel.NPWPLawanTransaksi + "|" +
                WHTModel.NamaLawanTransaksi + "|" + WHTModel.AlamatLawanTransaksi + "|" +
                WHTModel.VATBaseAmount + "|" + WHTModel.VATAmount + "|" +
                WHTModel.JumlahPPnBM + "|" + WHTModel.StatusApprovalXML + "|" + WHTModel.StatusFakturXML + "|";
            string detailDataPiped = string.Empty;

            List<SyncDataDetailViewModel> model = new List<SyncDataDetailViewModel>();

            int i = 1;
            foreach (WHTDetailManualInputViewModel whtDetail in WHTModel.WHTDetails)
            {
                detailDataPiped =
                    whtDetail.UnitName + "|" + whtDetail.UnitPrice + "|" +
                    whtDetail.Quantity + "|" + whtDetail.TotalPrice + "|" +
                    whtDetail.Discount + "|" + whtDetail.DPP + "|" +
                    whtDetail.PPN + "|" + whtDetail.TarifPPNBM + "|" + whtDetail.PPNBM;

                SyncDataDetailViewModel detailModel = new SyncDataDetailViewModel();
                detailModel.RowId = i + 1;
                detailModel.RowData = headerDataPiped + detailDataPiped;
                detailModel.SyncStatus = true;

                model.Add(detailModel);
                i++;
            }

            return model;
        }
        #endregion
    }
}