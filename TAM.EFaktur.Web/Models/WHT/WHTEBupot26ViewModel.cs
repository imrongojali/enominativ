﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.WHT
{
    public class WHTEBupot26ViewModel
    {
        public int RowNum { get; set; }
        public string MasaPajak { get; set; }
        public string TahunPajak { get; set; }
        public DateTime TglPemotongan { get; set; }
        public string TIN { get; set; }
        public string NamaWPTerpotong { get; set; }
        public DateTime TglLahirWPTerpotong { get; set; }
        public string AlamatWPTerpotong { get; set; }
        public string NoPasporWPTerpotong { get; set; }
        public string NoKitasWPTerpotong { get; set; }
        public string KodeNegara { get; set; }
        public string KodeObjekPajak { get; set; }
        public string PenandatanganBPPengurus { get; set; }
        public decimal PenghasilanBruto { get; set; }
        public string PerkiraanPenghasilan { get; set; }
        public string MendapatkanFasilitas { get; set; }
        public string TarifSKD { get; set; }
        public string NomorTandaTerimaSKD { get; set; }
        public string NomorAturanDTP { get; set; }
        public string NTPNDTP { get; set; }
        public string JenisPajak { get; set; }
        public string JenisDokumen { get; set; }
        public string eBupotReferenceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
    }
}