﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.WHT
{
    public class WHTDetail : BaseModel
    {
        public Guid VATInId { get; set; }
        public String NamaBarang { get; set; }
        public Decimal HargaSatuan { get; set; }
        public Decimal JumlahBarang { get; set; }
        public Decimal HargaTotal { get; set; }
        public Decimal Diskon { get; set; }
        public Decimal DPP { get; set; }
        public Decimal PPN { get; set; }
        public Decimal TarifPPNBM { get; set; }
        public Decimal PPNBM { get; set; }
        public String Status { get; set; }

        public dynamic MapFromModel()
        {
            dynamic args = new
            {
                Id = this.Id,
                VATInId = this.VATInId,
                NamaBarang = this.NamaBarang,
                HargaSatuan = this.HargaSatuan,
                JumlahBarang = this.JumlahBarang,
                HargaTotal = this.HargaTotal,
                Diskon = this.Diskon,
                DPP = this.DPP,
                PPN = this.PPN,
                TarifPPNBM = this.TarifPPNBM,
                PPNBM = this.PPNBM,
                Status = this.Status,
                EventDate = this.CreatedOn.Value.FormatSQLDateTime(),
                EventActor = this.CreatedBy,
                RowStatus = false
            };

            return args;
        }
    }
}