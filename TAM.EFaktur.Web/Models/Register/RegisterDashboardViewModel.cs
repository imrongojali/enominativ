﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Register
{
    public class RegisterDashboardViewModel : Controller
    {
        public Guid Id { get; set; }
        public int RowNum { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string CODSignerName { get; set; }
        public string CODSignerTitle { get; set; }

        public string TaxFacCategory { get; set; }
        public string TaxFacNo { get; set; }
        public string SKBType { get; set; }
        public string SKBTypeOthers { get; set; }
        public DateTime TaxFacDate { get; set; }
        public string SupplierName { get; set; }
        public string SupplierNPWP { get; set; }
        public string SupplierAddress { get; set; }
        public string SupplierPhoneNo { get; set; }
        public string SupplierCountry { get; set; }
        public string SupplierCountryName { get; set; }
        public DateTime SupplierEstDate { get; set; }
        public DateTime FacValidPeriodFrom { get; set; }
        public DateTime FacValidPeriodTo { get; set; }
        public string FacDocSignerName { get; set; }
        public string FacDocSignerTitle { get; set; }
        public DateTime RecordedDate { get; set; }
        public string RecordedBy { get; set; }
        public string ApprovalStatus { get; set; }
        public DateTime ApprovalDate { get; set; }
        public string UploadFile { get; set; }
        public string RegistrationNumber { get; set; }
        public string PDFFacStatus { get; set; }
        public int Seq { get; set; }

        public string MAPCode { get; set; }
    }
}
