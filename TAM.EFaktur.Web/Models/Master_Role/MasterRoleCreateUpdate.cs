﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_Role
{
    public class MasterRoleCreateUpdate
    {
        public Guid Id { get; set; }
        public string RoleCode { get; set; }
        public string RoleName { get; set; }
        public string ApplicationCode { get; set; }
        public DateTime EvenDate { get; set; }
        public string EvenActor { get; set; }
    }
}