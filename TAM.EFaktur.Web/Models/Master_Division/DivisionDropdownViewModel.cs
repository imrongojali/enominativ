﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_Division
{
    public class DivisionDropdownViewModel
    {
        public Guid Id { get; set; }       
        public string DivisionName { get; set; }
    }
}